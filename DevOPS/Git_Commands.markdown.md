## Git Commands

- ```git log``` Lists version history for the current branch
- ```git log --stat``` lists version history with stat
- ```git diff commit_id_1 commit_id_2``` compare two commits
- ```git diff``` compare staging files (which already added into .git) with working files which has been updated since you added the files into stage
- ```git diff --staged``` compare staging file with already committed file into the repo
- ```git --version``` git version check
- ```git clone https://github.com/udacity/asteroids.git``` copy entire repository along with history
- ```git init``` creates a new local repository
- ```git add <file_name's>``` add file from working dir to staging area for final commit
- ```git reset <file_name's>``` remove file from stagin area to working area
- ```git commit -m "_message_"``` commit file into the repo
- ```git branch``` list all branches
- ```git branch <branch_name>``` to create a branch
- ```git checkout <branch_name>``` switch to the branch
- ```git log --graph --oneline <branch_name's>``` list the commits of the branch names
- ```git merge master <branch_name>``` merge <branch_name> into master
- ```git branch -d <branch_name>``` delete branch label

Learn more: https://classroom.udacity.com/courses/ud775

## Generating a new SSH key and adding it to the ssh-agent

[https://help.github.com/en/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent](https://help.github.com/en/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent)

## Git and Github

### …or create a new repository on the command line

echo "# node-weather-app" >> README.md
git init
git add README.md
git commit -m "first commit"
git remote add origin https://github.com/dbkamal/node-weather-app.git
git push -u origin master

### …or push an existing repository from the command line

#### Create new repo on the command line
```
echo "# node-weather-app" >> README.md
git init
git add README.md
git commit -m "first commit"
git remote add origin https://github.com/dbkamal/node-weather-app.git
git push -u origin master
```

#### Push existing repo from the command line
```
git remote add origin https://github.com/dbkamal/node-weather-app.git
git push -u origin master
```
<!--stackedit_data:
eyJoaXN0b3J5IjpbMzYwMzEzNzUsLTI1MDcwNDM1NiwtNDkwNz
AwODE4XX0=
-->