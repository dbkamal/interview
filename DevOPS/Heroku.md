# Heroku

## Heroku CLI

Downloa and install heroku CLI [https://devcenter.heroku.com/articles/heroku-cli](https://devcenter.heroku.com/articles/heroku-cli)

- `$ heroku create <app-name>` create a new app

### Deploy your changes into heroku

If you haven't already, log in to your Heroku account and follow the prompts to create a new SSH public key.

`$ heroku login`

#### Clone the repository

Use Git to clone  dbkamal-weather-application's source code to your local machine.
`
$ heroku git:clone -a dbkamal-weather-application
$ cd dbkamal-weather-application
`
#### Deploy your changes

Make some changes to the code you just cloned and deploy them to Heroku using Git.
`
$ git add .
$ git commit -am "make it better"
$ git push heroku master
`
<!--stackedit_data:
eyJoaXN0b3J5IjpbNzYwODgwNzY5LC01NzM5NjM3MjRdfQ==
-->