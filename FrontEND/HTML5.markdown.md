# HTML5 Interview QA

## Key goals and motivations for HTML5 Specification

HTML5 was designed to replace both HTML4, XHTML, and the HTML DOM Level 2.

Major goals of the HTML specification were to:

- Deliver rich content (graphics, movies, etc.) without the need for additional plugins (e.g., Flash).
- Provide better semantic support for web page structure through the introduction of new structural element tags.
- Provide a stricter parsing standard to simplify error handling, ensure more consistent cross-browser behavior, and simplify backward compatibility with documents written to older standards.
- Provide better cross-platform support (i.e., to work well whether running on a PC, Tablet, or Smartphone).

**Ref**: https://www.toptal.com/html5/interview-questions

## Key new features of HTML5 include:

- Improved support for embedding graphics, audio, and video content via the new ```<canvas>, <audio>, and <video>``` tags
- Extensions to the JavaScript API such as geolocation and drag-and-drop as well for storage and caching
- Introduction of ```web workers```
- Several new semantic tags were also added to complement the structural logic of modern web applications. These include the ```<main>, <nav>, <article>, <section>, <header>, <footer>, <aside>``` tags
- New form controls, such as ```<calendar>, <date>, <time>, <email>, <url>, <search>```
- Connectivity: allowing you to communicate with the server in new and innovative ways.
- Offline and storage: allowing webpages to store data on the client-side locally and operate offline more efficiently.
- 2D/3D graphics and effects: allowing a much more diverse range of presentation options.
- Performance and integration: providing greater speed optimization and better usage of computer hardware.
- Device access: allowing for the usage of various input and output devices.
- Styling: letting authors write more sophisticated themes.

## Web Workers

Web workers at long last bring multi-threading to JavaScript.

A web worker is a script that runs in the background (i.e., in another thread) without the page needing to wait for it to complete. The user can continue to interact with the page while the web worker runs in the background. Workers utilize thread-like message passing to achieve parallelism.

- A worker is an object created using a constructor (e.g. ```Worker()```) that runs a named JavaScript file — this file contains the code that will run in the worker thread
- You can run whatever code you like inside the worker thread, with some exceptions. For example, you can't directly manipulate the DOM from inside a worker, or use some default methods and properties of the window object. But you can use a large number of items available under window, including WebSockets, and data storage mechanisms like IndexedDB and the Firefox OS-only Data Store API
- Data is sent between workers and the main thread via a system of messages — both sides send their messages using the ```postMessage()``` method, and respond to messages via the ```onmessage``` event handler (the message is contained within the Message event's ```data``` attribute.) The data is copied rather than shared
- Workers may, in turn, spawn new workers, as long as those workers are hosted within the same origin as the parent page. In addition, workers may use ```XMLHttpRequest``` for network I/O, with the exception that the responseXML and channel attributes on XMLHttpRequest always return null.

**Code** https://github.com/dbkamal/FrontEndCodingDemo/tree/master/WebWorkerDemo

**Ref**
- https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API/Using_web_workers
- https://www.youtube.com/watch?v=EiPytIxrZtU

## Cross-Origin Resource Sharing (CORS)

Cross-Origin Resource Sharing (CORS) is a mechanism that uses additional HTTP headers to tell a browser to let a web application running at one origin (domain) have permission to access selected resources from a server at a different origin. A web application executes a cross-origin HTTP request when it requests a resource that has a different origin (domain, protocol, and port) than its own origin.

**[MUST UPDATE THE CONTENT]**

**Must Read:** https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS

## Briefly describe the correct usage of the following HTML5 semantic elements

- The `<header>` element is used to contain introductory and navigational information about a section of the page. This can include the section heading, the author’s name, time and date of publication, table of contents, or other navigational information.

- The `<article>` element is meant to house a self-contained composition that can logically be independently recreated outside of the page without losing it’s meaining. Individual blog posts or news stories are good examples.

- The `<section>` element is a flexible container for holding content that shares a common informational theme or purpose.

- The `<footer>` element is used to hold information that should appear at the end of a section of content and contain additional information about the section. Author’s name, copyright information, and related links are typical examples of such content.

A `<section>` can contain `<article>` elements, and an `<article>` can contain `<section>` elements.

For example, a personal dashboard page might contain a `<section>` for social network interactions as well as a `<section>` for the latest news articles, the latter of which could contain several `<article>` elements.

Conversely, an `<article>` might contain a `<section>` at the end for reader comments.

The difference between span and div is that span gives the output with `display: inline` and div gives the output with `display: block`.

span is used when we need our elements to be shown in a line, one after the other.

## HTML5 Web Storage, Local Storage and Session Storage

With HTML5, web pages can store data locally within the user’s browser.

Earlier, this was done with cookies. However, Web Storage is more secure and faster. The data is not included with every server request, but used ONLY when asked for.

The data is stored in name/value pairs, and a web page can only access data stored by itself. Unlike cookies, the storage limit is far larger (at least 5MB) and information is never transferred to the server.

The difference between localStorage and sessionStorage involves the lifetime and scope of the storage.

Data stored through localStorage is permanent: it does not expire and remains stored on the user’s computer until a web app deletes it or the user asks the browser to delete it. SessionStorage has the same lifetime as the top-level window or browser tab in which the script that stored it is running. When the window or tab is permanently closed, any data stored through sessionStorage is deleted.

Both forms of storage are scoped to the document origin so that documents with different origins will never share the stored objects. But sessionStorage is also scoped on a per-window basis. If a user has two browser tabs displaying documents from the same origin, those two tabs have separate sessionStorage data: the scripts running in one tab cannot read or overwrite the data written by scripts in the other tab, even if both tabs are visiting exactly the same page and are running exactly the same scripts.


## Geolocation API

HTML5’s Geolocation API lets users share their physical location with chosen web sites. JavaScript can capture a user’s latitude and longitude and can send it to the back-end web server to enable location-aware features like finding local businesses or showing their location on a map.

Today, most browsers and mobile devices support the Geolocation API. The Geolocation API works with a new property of the global `navigator` object.

A Geolocation object can be created as follows:

`var geolocation = navigator.geolocation;`

The `geolocation` object is a service object that allows widgets to retrieve information about the geographic location of the user’s device.

## Send data to HTTPS from insecure page

The action `<form action="">` attribute send data to the back end server and it could be server path or a URL altogether. It's possible to specify a URL that uses the HTTPS (secure HTTP) protocol. When you do this, the data is encrypted along with the rest of the request, even if the form itself is hosted on an insecure page accessed using HTTP. On the other hand, if the form is hosted on a secure page but you specify an insecure HTTP URL with the action attribute, all browsers display a security warning to the user each time they try to send data because the data will not be encrypted.

## Send file to the server from HTML form

Sending files with HTML forms is a special case. Files are binary data — or considered as such — whereas all other data is text data. Because HTTP is a text protocol, there are special requirements for handling binary data.

The `enctype` attribute lets you specify the value of the `Content-Type` HTTP header included in the request generated when the form is submitted. This header is very important because it tells the server what kind of data is being sent. By default, its value is `application/x-www-form-urlencoded`. In human terms, this means: "This is form data that has been encoded into URL parameters."

If you want to send files, you need to take three extra steps:

- Set the `method` attribute to POST because file content can't be put inside URL parameters.
- Set the value of `enctype` to `multipart/form-data` because the data will be split into multiple parts, one for each file plus one for the text data included in the form body (if text is also entered into the form).
- Include one or more `File picker` widgets to allow your users to select the file(s) that will be uploaded.

```js
<form method="post" enctype="multipart/form-data">
  <div>
    <label for="file">Choose a file</label>
    <input type="file" id="file" name="myFile">
  </div>
  <div>
    <button>Send the file</button>
  </div>
</form>
```

## Common security concerns

Each time you send data to a server, you need to consider security. HTML forms are by far the most common attack vectors (places where attacks can occur) against servers. The problems never come from the HTML forms themselves — they come from how the server handles data.

Depending on what you're doing, there are some very well-known security issues that you'll come up against: `XSS` and `CSRF`.

Cross-Site Scripting (XSS) and Cross-Site Request Forgery (CSRF) are common types of attacks that occur when you display data sent by a user back to the user or to another user.

XSS lets attackers inject client-side script into Web pages viewed by other users. A cross-site scripting vulnerability may be used by attackers to bypass access controls such as the same origin policy. The effect of these attacks may range from a petty nuisance to a significant security risk.

CSRF attacks are similar to XSS attacks in that they start the same way — by injecting client-side script into Web pages — but their target is different. CSRF attackers try to escalate privileges to those of a higher-privileged user (such as a site administrator) to perform an action they shouldn't be able to do (for example, sending data to an untrusted user).

XSS attacks exploit the trust a user has for a web site, while CSRF attacks exploit the trust a web site has for its users.

To prevent these attacks, you should always check the data a user sends to your server and (if you need to display it) try not to display HTML content as provided by the user. Instead, you should process the user-provided data so you don't display it verbatim. Almost all frameworks on the market today implement a minimal filter that removes the HTML `<script>, <iframe> and <object>` elements from data sent by any user. This helps to mitigate the risk, but doesn't necessarily eradicate it.

### SQL Injection

SQL injection is a type of attack that tries to perform actions on a database used by the target web site. This typically involves sending a SQL request in the hope that the server will execute it (usually when the application server tries to store data sent by a user). This is actually one of the main vector attacks against web sites.

The consequences can be terrible, ranging from data loss to attacks taking control of a whole website infrastructure by using privilege escalation. This is a very serious threat and you should never store data sent by a user without performing some sanitization (for example, by using `mysqli_real_escape_string()`

## HTML Form validation using regular expression

Another very common validation feature is the `pattern` attribute, which expects a Regular Expression as its value. A regular expression (regex) is a pattern that can be used to match character combinations in text strings, so regexs are ideal for form validation and serve a variety of other uses in JavaScript.

```js
<form>
  <label for="choose">Would you prefer a banana or a cherry?</label>
  <input id="choose" name="i_like" required pattern="banana|cherry">
  <button>Submit</button>
</form>
```

### Remote validation

In some cases, it can be useful to perform some remote validation. This kind of validation is necessary when the data entered by the user is tied to additional data stored on the server side of your application. One use case for this is registration forms, where you ask for a username. To avoid duplication, it's smarter to perform an AJAX request to check the availability of the username rather than asking the user to send the data and then sending back the form with an error.

Performing such a validation requires taking a few precautions:

- It requires exposing an API and some data publicly; be sure it is not sensitive data.
- Network lag requires performing asynchronous validation. This requires some UI work in order to be sure that the user will not be blocked if the validation is not performed properly.

## ARIA

Accessible Rich Internet Applications (ARIA) is a set of attributes that define ways to make web content and web applications (especially those developed with JavaScript) more accessible to people with disabilities.

It supplements HTML so that interactions and widgets commonly used in applications can be passed to assistive technologies when there is not otherwise a mechanism. For example, ARIA enables accessible navigation landmarks in HTML5, JavaScript widgets, form hints and error messages, live content updates, and more.

The key attribute used by ARIA is the `role` attribute. The role attribute accepts a value that defines what an element is used for. Each role defines its own requirements and behaviors.

```js
  <ul class="optList" role="presentation">
    <!-- And we add the role="option" attribute to all the li elements -->
    <li role="option" class="option">Cherry</li>
    <li role="option" class="option">Lemon</li>
    <li role="option" class="option">Banana</li>
    <li role="option" class="option">Strawberry</li>
    <li role="option" class="option">Apple</li>
  </ul>
```

Using the `role` attribute is not enough. ARIA also provides many states and property attributes. The more and better you use them, the better your widget will be understood by assistive technologies.

The `aria-selected` attribute is used to mark which option is currently selected; this lets assistive technologies inform the user what the current selection is. We will use it dynamically with JavaScript to mark the selected option each time the user chooses one.

Ref: 
- https://developers.google.com/web/fundamentals/accessibility/semantics-aria/
- https://a11yproject.com/posts/getting-started-aria/
- https://www.udacity.com/course/web-accessibility--ud891

## Polyfill for Legacy Browser

A polyfill, or polyfiller, is a piece of code (or plugin) that provides the technology that you, the developer, expect the browser to provide natively. Flattening the API landscape if you will.

There are many cases where a good "polyfill" can help a lot by providing a missing API. A polyfill is a bit of JavaScript that "fills in the holes" in the functionality of legacy browsers. While they can be used to improve support for any functionality, using them for JavaScript is less risky than for CSS or HTML; there many cases where JavaScript can break (network issues, script conflicts, etc.).

https://remysharp.com/2010/10/08/what-is-a-polyfill/

The best way to polyfill missing API is by using the Modernizr library (https://modernizr.com/)

### Modernizr

Modernizr is a small piece of JavaScript code that automatically detects the availability of next-generation web technologies in your user's browsers. Rather than blacklisting entire ranges of browsers based on "UA sniffing" Modernizr uses feature detection to allow you to easily tailor your user's experiences based on the actual capabilities of their browser.

With this knowledge that Modernizr gives you, you can take advantage of these new features in the browsers that can render or utilize them, and still have easy and reliable means of controlling the situation for the browsers that cannot.

### Performance hit by Polyfill

Even though scripts like Modernizr are very aware of performance, loading a 200 kilobyte polyfill can affect the performance of your application. This is especially critical with legacy browsers; many of them have a very slow JavaScript engine that can make the execution of all your polyfills painful for the user. Performance is a subject on its own, but legacy browsers are very sensitive to it: basically, they are slow and the more polyfills they need, the more JavaScript they have to process. So they are doubly burdened compared to modern browsers. Test your code with legacy browsers to see how they actually perform. Sometimes, dropping some functionality leads to a better user experience than having exactly the same functionality in all browsers.

## HTML5 Parser

Gecko 2 introduces a new parser, based on HTML5. The HTML parser is one of the most complicated and sensitive pieces of a browser. It controls how your HTML source code is turned into web pages and, as such, changes to it are rare. The new parser is faster, complies with the HTML5 standard, and enables a lot of new functionality as well.

The new parser introduces these major improvements:

- You can now use SVG and MathML inline in HTML5 pages, without XML namespace syntax.
- Parsing is now done in a separate thread from Firefox’s main UI thread, improving overall browser responsiveness.
- Calls to innerHTML are a lot faster.
- Dozens of long-standing parser related bugs are now fixed.

The HTML5 specification provides a more detailed description than previous HTML standards of how to turn a stream of bytes into a DOM tree. This will result in more consistent behavior across browser implementations. In other words, in supporting HTML5, Gecko, WebKit, and Internet Explorer (IE) will behave more consistently with each other.

## Web Socket

The WebSocket API is an advanced technology that makes it possible to open a two-way interactive communication session between the user's browser and a server. With this API, you can send messages to a server and receive event-driven responses without having to poll the server for a reply.

**Tool:** Socket.IO: A long polling/WebSocket based third party transfer protocol for Node.js.

### Server Polling

Polling is the process where the computer or controlling device waits for an external device to check for its readiness or state, often with low-level hardware. For example, when a printer is connected via a parallel port, the computer waits until the printer has received the next character.

https://socket.io/get-started/chat/
https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API

## Application Cache

It's becoming increasingly important for web-based applications to be accessible offline. Yes, all browsers can cache pages and resources for long periods if told to do so, but the browser can kick individual items out of the cache at any point to make room for other things. HTML5 addresses some of the annoyances of being offline with the ApplicationCache interface.

Using the cache interface gives your application three advantages:

1. Offline browsing - users can navigate your full site when they're offline
2. Speed - resources come straight from disk, no trip to the network.
3. Resilience - if your site goes down for "maintenance" (as in, someone accidentally breaks everything), your users will get the offline experience

The Application Cache (or AppCache) allows a developer to specify which files the browser should cache and make available to offline users. Your app will load and work correctly, even if the user presses the refresh button while they're offline.

### Cache Manifest file

The cache manifest file is a simple text file that lists the resources the browser should cache for offline access.

To enable the application cache for an app, include the manifest attribute on the document's html tag

```js
<html manifest="example.appcache">
```

The manifest attribute should be included on every page of your web application that you want cached. The browser does not cache a page if it does not contain the manifest attribute (unless it is explicitly listed in the manifest file itself. This means that any page the user navigates to that includes a manifest will be implicitly added to the application cache. Thus, there's no need to list every page in your manifest. If a page points to a manifest, there's no way to prevent this page being cached.

https://www.html5rocks.com/en/tutorials/appcache/beginner/

## Web Storage API

[https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API]

## IndexDB

[https://developer.mozilla.org/en-US/docs/Web/API/IndexedDB_API]

## Drag & Drop

[https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API]

## Google Web Developer Training
[https://developers.google.com/training/web/]

## Google Web Fundamental (MUST READ)

[https://developers.google.com/web/fundamentals/]

## Browser Rendering Optimization

[https://classroom.udacity.com/courses/ud860]

## Reduce Page Weight

[https://www.sitepoint.com/complete-guide-reducing-page-weight/]
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTk2NjY4NTU0M119
-->