# Should You Know

## CSS Preprocessor
- [https://raygun.com/blog/10-reasons-css-preprocessor/](https://raygun.com/blog/10-reasons-css-preprocessor/)
- [https://htmlmag.com/article/an-introduction-to-css-preprocessors-sass-less-stylus](https://htmlmag.com/article/an-introduction-to-css-preprocessors-sass-less-stylus)


## Java
### Constructor
Constructors are used to initialize the object’s state. Like a constructor also contains  **collection of statements(i.e. instructions)**  that are executed at time of Object creation. So constructors are used to assign values to the class variables at the time of object creation, either explicitly done by the programmer or by Java itself (default constructor).

**When is a Constructor called ?**  
Each time an object is created using  **new()**  keyword at least one constructor (it could be default constructor) is invoked to assign initial values to the  **data members** of the same class.

-   Constructor(s) of a class must has same name as the class name in which it resides.
-   A constructor in Java can not be abstract, final, static and Synchronized.
-   Access modifiers can be used in constructor declaration to control its access i.e which other class can call the constructor.

**No-argument constructor:** A constructor that has no parameter is known as default constructor. If we don’t define a constructor in a class, then compiler creates **default constructor(with no arguments)** for the class. And if we write a constructor with arguments or no-arguments then the compiler does not create a default constructor.  
Default constructor provides the default values to the object like 0, null, etc. depending on the type.

**Parameterized Constructor:** A constructor that has parameters is known as parameterized constructor. If we want to initialize fields of the class with your own values, then use a parameterized constructor.

**There are no “return value” statements in constructor, but constructor returns current class instance. We can write ‘return’ inside a constructor.**

**Constructor Overloading:** Like methods, we can overload constructors for creating objects in different ways. Compiler differentiates constructors on the basis of numbers of parameters, types of the parameters and order of the parameters.

#### Constructor Chaining

Constructor chaining is the process of calling one constructor from another constructor with respect to current object.  
Constructor chaining can be done in two ways:

-   **Within same class**: It can be done using  **this()**  keyword for constructors in same class
-   **From base class:** by using  **super()**  keyword to call constructor from the base class.

Constructor chaining occurs through  **inheritance**. A sub class constructor’s task is to call super class’s constructor first. This ensures that creation of sub class’s object starts with the initialization of the data members of the super class. There could be any numbers of classes in inheritance chain. Every constructor calls up the chain till class at the top is reached.

**Why do we need constructor chaining ?**  
This process is used when we want to perform multiple tasks in a single constructor rather than creating a code for each task in a single constructor we create a separate constructor for each task and make their chain which makes the program more readable.

```java
class Main {
	public  static  void main(String[] args) {
		Test obj = new Test(5, 3);
	}
}

class Test {
	Test(int x, int y) {
		this(x);
		System.out.println(x * y);
	}

	Test(int x) {
		this();
		System.out.println(x * x);
	}

	Test() {
		System.out.println("finally we are done!!");
	}
}
```

**Rules of constructor chaining :**

1.  The  **this()**  expression should always be the first line of the constructor.
2.  There should be at-least be one constructor without the this() keyword (constructor 3 in above example).
3.  Constructor chaining can be achieved in any order.
Note : Similar to constructor chaining in same class, **super()** should be the first line of the constructor as super class’s constructor are invoked before the sub class’s constructor.

#### Copy Constructor
```java
class A {
	private double val;

	A (double val) { this.val = val; }

	// copy constructor
	A(A a) { this.val = a.val; }
}

class Main {
	public static void main(String[] args)  {
		A a1 = new A(10.0);
		A a2 = new A(a1); // creates a deepcopy not a reference
	}
}
```
### ```this``` keyword
Sometimes a method will need to refer to the object that invoked it. To allow this, Java defines the **this** keyword. **this** can be used inside any method to refer to the _current_ object. That is, **this** is always areference to the object on which the method was invoked. You can use **this** anywhere a reference to an object of the current class’ type is permitted.'

#### Instance Variable Hiding

It is illegal in Java to declare two local variables with the same name inside the same or enclosing scopes. Interestingly, you can have local variables, including formal parameters to methods, which overlap with the names of the class’ instance variables. However, when a local variable has the same name as an instance variable, the local variable _hides_ the instance variable. Because **this** lets you refer directly to the object, you can use it to resolve any namespace collisions that might occur between instance variables and local variables.
```java
class A {
	int val;

	A(int val) {
		this.val = val;
	}
}
```

### Garbase Collection

Since objects are dynamically allocated by using the **new** operator, you might be wondering how such objects are destroyed and their memory released for later reallocation. In some languages, such as traditional C++, dynamically allocated objects must be manually released by use of a **delete** operator. Java takes a different approach; it handles deallocation for you automatically. The technique that accomplishes this is called **garbage collection**. It works like this: when no references to an object exist, that object is assumed to be no longer needed, and the memory occupied by the object can be reclaimed. There is no need to explicitly destroy objects. Garbage collection only occurs sporadically (if at all) during the execution of your program. It will not occur simply because one or more objects exist that are no longer used. Furthermore, different Java run-time implementations will take varying approaches to garbage collection, but for the most part, you should not have to think about it while writing your programs.

**Unreachable objects :** An object is said to be unreachable iff it doesn’t contain any reference to it. Also note that objects which are part of [island of isolation](https://www.geeksforgeeks.org/island-of-isolation-in-java/) are also unreachable.

We can also request JVM to run Garbage Collector. There are two ways to do it :

1.  **Using  _System.gc()_  method**  : System class contain static method  _gc()_  for requesting JVM to run Garbage Collector.
2.  **Using  _Runtime.getRuntime().gc()_  method**  :  [Runtime class](https://www.geeksforgeeks.org/java-lang-runtime-class-in-java/)  allows the application to interface with the JVM in which the application is running. Hence by using its gc() method, we can request JVM to run Garbage Collector. There is no guarantee that any one of above two methods will definitely run Garbage Collector.

### Overloading and Overriding Method

_Overloading_ occurs when two or more methods in one class have the same method name but different parameters. _Overriding_ means having two methods with the same method name and parameters (i.e., _method signature_). One of the methods is in the parent class and the other is in the child class. Overriding allows a child class to provide a specific implementation of a method that is already provided its parent class.

#### Dynamic Method Dispatch

Method overriding forms the basis for one of Java’s most powerful concepts: _dynamic method dispatch_. Dynamic method dispatch is the mechanism by which a call to an overridden method is resolved at run time, rather than compile time. Dynamic method dispatch is important because this is how Java implements run-time polymorphism.

Let’s begin by restating an important principle: a superclass reference variable can refer to a subclass object. Java uses this fact to resolve calls to overridden methods at run time. Here is how. When an overridden method is called through a superclass reference, Java determines which version of that method to execute based upon the type of the object being referred to at the time the call occurs. Thus, this determination is made at run time. When different types of objects are referred to, different versions of an overridden method will be called. In other words, _it is the type of the object being referred to_ (not the type of the reference variable) that determines which version of an overridden method will be executed. Therefore, if a superclass contains a method that is overridden by a subclass, then when different types of objects are referred to through a superclass reference variable, different versions of the method are executed.

```java
class A {
	void callme() {
		System.out.println("call class A");
	}
}

class B extends A {
	void callme() {
		System.out.println("call class B");
	}
}

class Main {
	public static void main(String[] args)  {
		A a = new A();
		B b = new B();

		A r; //obtain a reference of type A
		r.callme(); //call A class method

		r = b; // r refers to B object
		r.callme(); //call B class method
	}
}
```

As stated earlier, overridden methods allow Java to support run-time polymorphism. Polymorphism is essential to object-oriented programming for one reason: it allows a general class to specify methods that will be common to all of its derivatives, while allowing subclasses to define the specific implementation of some or all of those methods. Overridden methods are another way that Java implements the “one interface, multiple methods” aspect of polymorphism.

Part of the key to successfully applying polymorphism is understanding that the superclasses and subclasses form a hierarchy which moves from lesser to greater specialization. Used correctly, the superclass provides all elements that a subclass can use directly. It also defines those methods that the derived class must implement on its own. This allows the subclass the flexibility to define its own methods, yet still enforces a consistent interface. Thus, by combining inheritance with overridden methods, a superclass can define the general form of the methods that will be used by all of its subclasses.

Dynamic, run-time polymorphism is one of the most powerful mechanisms that object-oriented design brings to bear on code reuse and robustness. The ability of existing code libraries to call methods on instances of new classes without recompiling while maintaining a clean abstract interface is a profoundly powerful tool.


#### ```static``` keyword

In the Java programming language, **the keyword  _static_  indicates that the particular member belongs to a type itself, rather than to an instance of that type**.

In Java,  **if a field is declared  _static_, then exactly a single copy of that field is created and shared among all instances of that class**. It doesn’t matter how many times we initialize a class; there will always be only one copy of  _static_  field belonging to it. The value of this  _static_  field will be shared across all object of either same of any different class.

From the memory perspective,  **static variables go in a particular pool in JVM memory called Metaspace** (before Java 8, this pool was called Permanent Generation or PermGen, which was completely removed and replaced with Metaspace).

-   Since _static_ variables belong to a class, they can be accessed directly using class name and don’t need any object reference
-   _static_  variables can only be declared at the class level
-   _static_  fields can be accessed without object initialization
-   Although we can access  _static_  fields using an object reference (like  _ford.numberOfCars++_) , we should refrain from using it as in this case it becomes difficult to figure whether it’s an instance variable or a class variable; instead, we should always refer to  _static_  variables using class name (for example, in this case,  _Car.numberOfCars++_)

Similar to _static_ fields, _static_ methods also belong to a class instead of the object, and so they can be called without creating the object of the class in which they reside. They’re meant to be used without creating objects of the class.

-   _static_  methods in Java are resolved at compile time. Since method overriding is part of Runtime Polymorphism,  **so static methods can’t be overridden**
-   abstract methods can’t be static
-   _static_  methods cannot use  _this_  or  _super_  keywords
-   The following combinations of the instance, class methods and variables are valid:
    1.  Instance methods can directly access both instance methods and instance variables
    2.  Instance methods can also access  _static_  variables and  _static_  methods directly
    3.  _static_  methods can access all  _static_  variables and other  _static_  methods
    4.  **_static_  methods cannot access instance variables and instance methods directly**; they need some object reference to do so

A  _static_  block is used for initializing  _static_  variables. Although  _static_  variables can be initialized directly during declaration, there are situations when we’re required to do the multiline processing.

In such cases,  _static_  blocks come in handy.

**If  _static_  variables require additional, multi-statement logic while initialization, then a  _static_  block can be used.**

```java
public class StaticBlockDemo {
    public static List<String> ranks = new LinkedList<>();
 
    static {
        ranks.add("Lieutenant");
        ranks.add("Captain");
        ranks.add("Major");
    }
     
    static {
        ranks.add("Colonel");
        ranks.add("General");
    }
}
```

Java programming language allows us to create a class within a class. It provides a compelling way of grouping elements that are only going to be used in one place, this helps to keep our code more organized and readable.

The nested class architecture is divided into two:

-   nested classes that are declared  _static_  are called  **_static_  nested classes**  whereas,
-   nested classes that are non-_static_  are called  **inner classes**

The main difference between these two is that the inner classes have access to all member of the enclosing class (including private), whereas the  _static_ nested classes only have access to static members of the outer class.

In fact,  **_static_  nested classes behaved exactly like any other top-level class but enclosed in the only class which will access it, to provide better packaging convenience.**

### Interitance

A reference variable of a superclass can be assigned a reference to any subclass derived from that superclass. You will find this aspect of inheritance quite useful in a variety of situations.

#### ```super``` keyword

The **super**  keyword in java is a reference variable that is used to refer parent class objects. The keyword “super” came into the picture with the concept of Inheritance. It is majorly used in the following contexts:

**1. Use of super with variables:** This scenario occurs when a derived class and base class has same data members. In that case there is a possibility of ambiguity for the JVM.
```java
class A {
	int val = 100;
}

class B extends A {
	int val = 120;
	System.out.println("Display the subclass instance variable: " + val);
	System.out.println("Display the parent class instance variable: " + super.val);
}
```
**2. Use of super with methods:** This is used when we want to call parent class method. So whenever a parent and child class have same named methods then to resolve ambiguity we use super keyword.

**3**. **Use of super with constructors:** super keyword can also be used to access the parent class constructor. One more important thing is that, ‘’super’ can call both parametric as well as non parametric constructors depending upon the situation

1.  Call to super() must be first statement in Derived(Student) Class constructor.
2.  If a constructor does not explicitly invoke a superclass constructor, the Java compiler automatically inserts a call to the no-argument constructor of the superclass. If the superclass does not have a no-argument constructor, you will get a compile-time error. Object _does_  have such a constructor, so if Object is the only superclass, there is no problem.
3.  If a subclass constructor invokes a constructor of its superclass, either explicitly or implicitly, you might think that a whole chain of constructors called, all the way back to the constructor of Object. This, in fact, is the case. It is called  _constructor chaining_..

### Abstract class

Abstract class in Java is similar to interface except that it can contain default method implementation. An abstract class can have an abstract method without body and it can have methods with implementation also.

`abstract`  keyword is used to create a abstract class and method. Abstract class in java can’t be instantiated. An abstract class is mostly used to provide a base for subclasses to extend and implement the abstract methods and override or use the implemented methods in abstract class.

#### Abstract class in Java Important Points

1.  `abstract`  keyword is used to create an abstract class in java.
2.  Abstract class in java can’t be instantiated.
3.  We can use  `abstract`  keyword to create an abstract method, an abstract method doesn’t have body.
4.  If a class have abstract methods, then the class should also be abstract using abstract keyword, else it will not compile.
5.  It’s not necessary for an abstract class to have abstract method. We can mark a class as abstract even if it doesn’t declare any abstract methods.
6.  If abstract class doesn’t have any method implementation, its better to use interface because java doesn’t support multiple class inheritance.
7.  The subclass of abstract class in java must implement all the abstract methods unless the subclass is also an abstract class.
8.  All the methods in an interface are implicitly abstract unless the interface methods are static or default. Static methods and default methods in interfaces are added in  [Java 8](https://www.journaldev.com/2389/java-8-features-with-examples), for more details read  [Java 8 interface changes](https://www.journaldev.com/2752/java-8-interface-changes-static-method-default-method "Java 8 Interface Changes – static methods, default methods, functional Interfaces").
9.  Java Abstract class can implement interfaces without even providing the implementation of interface methods.
10.  Java Abstract class is used to provide common method implementation to all the subclasses or to provide default implementation.
11.  We can run abstract class in java like any other class if it has  `main()`  method.

### `final` keyword

The keyword **final** has three uses. 
- it can be used to create the equivalent of a named constant
- to disallow a method from being overridden, specify **final** as a modifier at the start of its declaration. Methods declared as **final** cannot be overridden. Methods declared as **final** can sometimes provide a performance enhancement: The compiler is free to _inline_ calls to them because it “knows” they will not be overridden by a subclass. When a small **final**method is called, often the Java compiler can copy the bytecode for the subroutine directly inline with the compiled code of the calling method, thus eliminating the costly overhead associated with a method call. Inlining is an option only with **final** methods. Normally, Java resolves calls to methods dynamically, at run time. This is called _`late binding`_. However, since **final** methods cannot be overridden, a call to one can be resolved at compile time. This is called _`early binding`_.
- sometimes you will want to prevent a class from being inherited. To do this, precede the class declaration with **final**. Declaring a class as **final** implicitly declares all of its methods as **final**, too

### Java Interfaces

Like a class, an interface can have methods and variables, but the methods declared in interface are by default abstract (only method signature, no body).

-   Interfaces specify what a class must do and not how. It is the blueprint of the class.
-   An Interface is about capabilities like a Player may be an interface and any class implementing Player must be able to (or must implement) move(). So it specifies a set of methods that the class has to implement.
-   If a class implements an interface and does not provide method bodies for all functions specified in the interface, then class must be declared abstract.
-   A Java library example is,  [Comparator Interface](https://www.geeksforgeeks.org/comparator-interface-java/). If a class implements this interface, then it can be used to sort a collection.

To declare an interface, use **interface** keyword. It is used to provide total abstraction. That means all the methods in interface are declared with empty body and are public and all fields are public, static and final by default. A class that implement interface must implement all the methods declared in the interface. To implement interface use **implements** keyword.

**Why do we use interface ?**

-   It is used to achieve total abstraction.
-   Since java does not support multiple inheritance in case of class, but by using interface it can achieve multiple inheritance .
-   It is also used to achieve loose coupling.
-   Interfaces are used to implement abstraction. So the question arises why use interfaces when we have abstract classes? The reason is, abstract classes may contain non-final variables, whereas variables in interface are final, public and static.

```java
interface callme {
	void display();
}

class A implements callme {
	public void display() {
		//TO DO
	}
}
```

Let’s consider the example of vehicles like bicycle, car, bike…, they have common functionalities like `changeGear, applyBrakes, speedUp`. So we make an interface and put all these common functionalities. And lets Bicylce, Bike, car ….etc implement all these functionalities in their own class in their own way.

Prior to JDK 8, interface could not define implementation. We can now add default implementation for interface methods. This default implementation has special use and does not affect the intention behind interfaces.

Suppose we need to add a new function in an existing interface. Obviously the old code will not work as the classes have not implemented those new functions. So with the help of default implementation, we will give a default body for the newly added functions. Then the old codes will still work.

Another feature that was added in JDK 8 is that we can now define static methods in interfaces which can be called independently without an object. Note: these methods are not inherited.

-   We can’t create instance(interface can’t be instantiated) of interface but we can make reference of it that refers to the Object of its implementing class.
-   A class can implement more than one interface.
-   An interface can extends another interface or interfaces (more than one interface) .
-   A class that implements interface must implements all the methods in interface.
-   All the methods are public and abstract. And all the fields are public, static, and final.
-   It is used to achieve multiple inheritance.
-   It is used to achieve loose coupling.

### Java Exception Handling

An exception is an unwanted or unexpected event, which occurs during the execution of a program i.e at run time, that disrupts the normal flow of the program’s instructions. All exception and errors types are sub classes of class **Throwable**, which is base class of hierarchy.One branch is headed by **Exception**. This class is used for exceptional conditions that user programs should catch. NullPointerException is an example of such an exception.Another branch,**Error** are used by the Java run-time system([JVM](https://www.geeksforgeeks.org/jvm-works-jvm-architecture/)) to indicate errors having to do with the run-time environment itself(JRE). StackOverflowError is an example of such an error.

#### Checked vs Unchecked exception

**Checked:**  are the exceptions that are checked at compile time. If some code within a method throws a checked exception,  then the method must either handle the exception or it must specify the exception using  _throws_ keyword.

For example, below code uses FileReader() and FileReader() throws a checked exception  _FileNotFoundException_. To fix the above program, we either need to specify list of exceptions using `throws`, or we need to use try-catch block.

```java
import java.io.FileReader;

class Main {
	public static void main(String[] args) throws FileNotFoundException {
		FileReader fr = new FileReader("_path_");
	}
}
```

**Unchecked** are the exceptions that are not checked at compiled time. In Java exceptions under _Error_ and _RuntimeException_ classes are unchecked exceptions, everything else under throwable is checked.

Consider the following Java program. It compiles fine, but it throws _ArithmeticException_ when run. The compiler allows it to compile, because _ArithmeticException_ is an unchecked exception.

```java
class Main {
	public static void main(String[] args) {
		int a = 10 / 0;	
	}
}
```

_If a client can reasonably be expected to recover from an exception, make it a checked exception. If a client cannot do anything to recover from the exception, make it an unchecked exception_

#### How JVM Handles an Exception

**Default Exception Handling :** Whenever inside a method, if an exception has occurred, the method creates an Object known as Exception Object and hands it off to the run-time system(JVM). The exception object contains name and description of the exception, and current state of the program where exception has occurred. Creating the Exception Object and handling it to the run-time system is called throwing an Exception.There might be the list of the methods that had been called to get to the method where exception was occurred. This ordered list of the methods is called  **Call Stack**.Now the following procedure will happen.

-   The run-time system searches the call stack to find the method that contains block of code that can handle the occurred exception. The block of the code is called  **Exception handler**.
-   The run-time system starts searching from the method in which exception occurred, proceeds through call stack in the reverse order in which methods were called.
-   If it finds appropriate handler then it passes the occurred exception to it. Appropriate handler means the type of the exception object thrown matches the type of the exception object it can handle.
-   If run-time system searches all the methods on call stack and couldn’t have found the appropriate handler then run-time system handover the Exception Object to  **default exception handler** , which is part of run-time system. This handler prints the exception information in the following format and terminates program  **abnormally**.

**Customized Exception Handling :** Java exception handling is managed via five keywords: **try**, **catch**, **[throw](https://www.geeksforgeeks.org/throw-throws-java/)**, **[throws](https://www.geeksforgeeks.org/throw-throws-java/)**, and **finally**. Briefly, here is how they work. Program statements that you think can raise exceptions are contained within a `try` block. If an exception occurs within the try block, it is thrown. Your code can `catch` this exception (using catch block) and handle it in some rational manner. System-generated exceptions are automatically thrown by the Java run-time system. To manually throw an exception, use the keyword [`throw`](https://www.geeksforgeeks.org/throw-throws-java/). Any exception that is thrown out of a method must be specified as such by a [`throws`](https://www.geeksforgeeks.org/throw-throws-java/) clause. Any code that absolutely must be executed after a try block completes is put in a `finally` block.

#### Create Custom Exception

Although Java’s built-in exceptions handle most common errors, you will probably want to create your own exception types to handle situations specific to your applications. This is quite easy to do: just define a subclass of **Exception** (which is, of course, a subclass of **Throwable**). Your subclasses don’t need to actually implement anything—it is their existence in the type system that allows you to use them as exceptions.

The **Exception** class does not define any methods of its own. It does, of course, inherit those methods provided by **Throwable**. Thus, all exceptions, including those that you create, have the methods defined by **Throwable** available to them.

Although specifying a description when an exception is created is often useful, sometimes it is better to override **toString( )**. Here’s why: The version of **toString( )** defined by **Throwable** (and inherited by **Exception**) first displays the name of the exception followed by a colon, which is then followed by your description. By overriding **toString( )**, you can prevent the exception name and colon from being displayed. This makes for a cleaner output, which is desirable in some cases.

```java
class MyException extends Exception {
	private int detail;

	MyException(int details) { this.detail = detail; }

	public String toString() {
		return "MyException [" + detail + "]";
	}
}

class ExceptionDemo {
	static void compute(int a) throws MyException {
		if (a > 0)
			throw new MyException(a);
	}

	public static void main(String[] args) {
		try {
			compute(10);
		}
		catch(MyException err) {
			System.out.println(err);		
		}
	}
}
```

#### Chained Exception

A number of years ago, a feature was incorporated into the exception subsystem: _chained exceptions_. The chained exception feature allows you to associate another exception with an exception. This second exception describes the cause of the first exception. For example, imagine a situation in which a method throws an **ArithmeticException** because of an attempt to divide by zero. However, the actual cause of the problem was that an I/O error occurred, which caused the divisor to be set improperly. Although the method must certainly throw an **ArithmeticException**, since that is the error that occurred, you might also want to let the calling code know that the underlying cause was an I/O error. Chained exceptions let you handle this, and any other situation in which layers of exceptions exist.

#### JDK7 Exception New Features

- Automates the process of releasing a resource, such as a file, when it is no longer needed. It is based on an expanded form of the **try** statement called `try-with-resources`
- The multi-catch feature allows two or more exceptions to be caught by the same **catch** clause. It is not uncommon for two or more exception handlers to use the same code sequence even though they respond to different exceptions. Instead of having to catch each exception type individually, you can use a single **catch** clause to handle all of the exceptions without code duplication.

### Multithreading

The Java run-time system depends on threads for many things, and all the class libraries are designed with multithreading in mind. In fact, Java uses threads to enable the entire environment to be asynchronous. This helps reduce inefficiency by preventing the waste of CPU cycles.

The value of a multithreaded environment is best understood in contrast to its counterpart. Single-threaded systems use an approach called an  _event loop_  with  _polling_. In this model, a single thread of control runs in an infinite loop, polling a single event queue to decide what to do next. Once this polling mechanism returns with, say, a signal that a network file is ready to be read, then the event loop dispatches control to the appropriate event handler. Until this event handler returns, nothing else can happen in the program. This wastes CPU time. It can also result in one part of a program dominating the system and preventing any other events from being processed. In general, in a single-threaded environment, when a thread  _blocks_  (that is, suspends execution) because it is waiting for some resource, the entire program stops running.

The benefit of Java’s multithreading is that the main loop/polling mechanism is eliminated. One thread can pause without stopping other parts of your program. For example, the idle time created when a thread reads data from a network or waits for user input can be utilized elsewhere. Multithreading allows animation loops to sleep for a second between each frame without causing the whole system to pause. When a thread blocks in a Java program, only the single thread that is blocked pauses. All other threads continue to run.

As most readers know, over the past few years, multicore systems have become commonplace. Of course, single-core systems are still in widespread use. It is important to understand that Java’s multithreading features work in both types of systems. In a single-core system, concurrently executing threads share the CPU, with each thread receiving a slice of CPU time. Therefore, in a single-core system, two or more threads do not actually run at the same time, but idle CPU time is utilized. However, in multicore systems, it is possible for two or more threads to actually execute simultaneously. In many cases, this can further improve program efficiency and increase the speed of certain operations.

#### Thread Lifecycle

Threads exist in several states. Here is a general description. A thread can be _running_. It can be _ready to run_ as soon as it gets CPU time. A running thread can be _suspended_, which temporarily halts its activity. A suspended thread can then be _resumed_, allowing it to pick up where it left off. A thread can be _blocked_ when waiting for a resource. At any time, a thread can be terminated, which halts its execution immediately. Once terminated, a thread cannot be resumed.

#### Thread Priorities

Java assigns to each thread a priority that determines how that thread should be treated with respect to the others. Thread priorities are integers that specify the relative priority of one thread to another. As an absolute value, a priority is meaningless; a higher-priority thread doesn’t run any faster than a lower-priority thread if it is the only thread running. Instead, a thread’s priority is used to decide when to switch from one running thread to the next. This is called a  _context switch_. The rules that determine when a context switch takes place are simple:

• _A thread can voluntarily relinquish control_. This occurs when explicitly yielding, sleeping, or when blocked. In this scenario, all other threads are examined, and the highest-priority thread that is ready to run is given the CPU.

• _A thread can be preempted by a higher-priority thread_. In this case, a lower-priority thread that does not yield the processor is simply preempted—no matter what it is doing—by a higher-priority thread. Basically, as soon as a higher-priority thread wants to run, it does. This is called  _preemptive multitasking_.

In cases where two threads with the same priority are competing for CPU cycles, the situation is a bit complicated. For some operating systems, threads of equal priority are time-sliced automatically in round-robin fashion. For other types of operating systems, threads of equal priority must voluntarily yield control to their peers. If they don’t, the other threads will not run.

**CAUTION**  Portability problems can arise from the differences in the way that operating systems context-switch threads of equal priority.

#### Synchronization

Because multithreading introduces an asynchronous behavior to your programs, there must be a way for you to enforce synchronicity when you need it. For example, if you want two threads to communicate and share a complicated data structure, such as a linked list, you need some way to ensure that they don’t conflict with each other. That is, you must prevent one thread  from writing data while another thread is in the middle of reading it. For this purpose, Java implements an elegant twist on an age-old model of interprocess synchronization: the  _monitor_. The monitor is a control mechanism first defined by C.A.R. Hoare. You can think of a monitor as a very small box that can hold only one thread. Once a thread enters a monitor, all other threads must wait until that thread exits the monitor. In this way, a monitor can be used to protect a shared asset from being manipulated by more than one thread at a time.

In Java, there is no class “Monitor”; instead, each object has its own implicit monitor that is automatically entered when one of the object’s synchronized methods is called. Once a thread is inside a synchronized method, no other thread can call any other synchronized method on the same object. This enables you to write very clear and concise multithreaded code, because synchronization support is built into the language.

#### Messaging

After you divide your program into separate threads, you need to define how they will communicate with each other. When programming with some other languages, you must depend on the operating system to establish communication between threads. This, of course, adds overhead. By contrast, Java provides a clean, low-cost way for two or more threads to talk to each other, via calls to predefined methods that all objects have. Java’s messaging system allows a thread to enter a synchronized method on an object, and then wait there until some other thread explicitly notifies it to come out.

#### The Thread Class and the Runnable Interface

Java’s multithreading system is built upon the  **Thread**  class, its methods, and its companion interface,  **Runnable**.  **Thread**  encapsulates a thread of execution. Since you can’t directly refer to the ethereal state of a running thread, you will deal with it through its proxy, the  **Thread**  instance that spawned it. To create a new thread, your program will either extend  **Thread**  or implement the  **Runnable**interface.

#### _Main_ Thread

When a Java program starts up, one thread begins running immediately. This is usually called the  _main thread_  of your program, because it is the one that is executed when your program begins. The main thread is important for two reasons:

• It is the thread from which other “child” threads will be spawned.
• Often, it must be the last thread to finish execution because it performs various shutdown actions.

Although the main thread is created automatically when your program is started, it can be controlled through a  **Thread**  object. To do so, you must obtain a reference to it by calling the method  **currentThread( )**, which is a  **public static**  member of  **Thread**.

```java
class ThreadDemo {
	public static void main(String[] args) {
		Thread t = Thread.currentThread();
		//change the name of the current thread
		t.setName("My thread");

		try {
			for (int i = 5; i > 0; i--) {
				System.out.println(i);
				Thread.sleep(1000);
			}
		}
		catch (InterruptedException e) {
			System.out.println("Main thread interrupted!");
		}
	}
}
```

#### Create a Thread

In the most general sense, you create a thread by instantiating an object of type  **Thread**. Java defines two ways in which this can be accomplished:

• You can implement the  **Runnable**  interface.
• You can extend the  **Thread**  class, itself.

The easiest way to create a thread is to create a class that implements the  **Runnable**  interface.  **Runnable**  abstracts a unit of executable code. You can construct a thread on any object that implements  **Runnable**. To implement  **Runnable**, a class need only implement a single method called  **run( )**, which is declared like this:

public void run( )

Inside  **run( )**, you will define the code that constitutes the new thread. It is important to understand that  **run( )**  can call other methods, use other classes, and declare variables, just like the main thread can. The only difference is that  **run( )**  establishes the entry point for another, concurrent thread of execution within your program. This thread will end when  **run( )**  returns.

After you create a class that implements  **Runnable**, you will instantiate an object of type  **Thread**from within that class.  **Thread**  defines several constructors. The one that we will use is shown here:

Thread(Runnable  _threadOb_, String  _threadName_)

In this constructor,  _threadOb_  is an instance of a class that implements the  **Runnable**  interface. This defines where execution of the thread will begin. The name of the new thread is specified by  _threadName_.

After the new thread is created, it will not start running until you call its  **start( )**  method, which is declared within  **Thread**. In essence,  **start( )**  initiates a call to  **run( )**. The  **start( )**  method is shown here:

void start( )

```java
class MyThread implements Runnable {
	Thread t;

	MyThread() {
		t = new Thread(this, "Thread_Demo");
	}

	public void run() {
		try {
			for (int i = 5; i > 0; i--) {
				System.out.println("Child thread " + i);
				Thread.sleep(1000);
			}
		}
		catch (InterruptedException e) {
			System.out.println("Child interuppted..");
		}

		System.out.println("Child thread done..");
	}
}

class ThreadDemo {
	public static void main (String[] args) {
		MyThread myThread = new MyThread();
		myThread.t.start();

		try {
			for (int i = 5; i > 0; i--) {
				System.out.println("Main thread " + i);
				Thread.sleep(1000);
			}
		}
		catch (InterruptedException e) {
			System.out.println("Main interuppted..");
		}

		System.out.println("Main thread done..");
	}
}
```

#### Multiprocessing vs Multithreading
[https://techdifferences.com/difference-between-multiprocessing-and-multithreading.html](https://techdifferences.com/difference-between-multiprocessing-and-multithreading.html)

#### isAlive() and join()

Two ways exist to determine whether a thread has finished. First, you can call **isAlive( )** on the thread. This method is defined by **Thread**. The **isAlive( )** method returns **true** if the thread upon which it is called is still running. It returns **false** otherwise.

**join()** method waits until the thread on which it is called terminates. Its name comes from the concept of the calling thread waiting until the specified thread _joins_ it. Additional forms of **join( )** allow you to specify a maximum amount of time that you want to wait for the specified thread to terminate.

#### Thread priority

Thread priorities are used by the thread scheduler to decide when each thread should be allowed to run. In theory, over a given period of time, higher-priority threads get more CPU time than lower-priority threads. In practice, the amount of CPU time that a thread gets often depends on several factors besides its priority. (For example, how an operating system implements multitasking can affect the relative availability of CPU time.) A higher-priority thread can also preempt a lower-priority one. For instance, when a lower-priority thread is running and a higher-priority thread resumes (from sleeping or waiting on I/O, for example), it will preempt the lower-priority thread.

In theory, threads of equal priority should get equal access to the CPU. But you need to be careful. Remember, Java is designed to work in a wide range of environments. Some of those environments implement multitasking fundamentally differently than others. For safety, threads that share the same priority should yield control once in a while. This ensures that all threads have a chance to run under a nonpreemptive operating system. In practice, even in nonpreemptive environments, most threads still get a chance to run, because most threads inevitably encounter some blocking situation, such as waiting for I/O. When this happens, the blocked thread is suspended and other threads can run. But, if you want smooth multithreaded execution, you are better off not relying on this. Also, some types of tasks are CPU-intensive. Such threads dominate the CPU. For these types of threads, you want to yield control occasionally so that other threads can run.

### SYNCHRONIZATION

When two or more threads need access to a shared resource, they need some way to ensure that the resource will be used by only one thread at a time. The process by which this is achieved is called  _synchronization_. As you will see, Java provides unique, language-level support for it.

Key to synchronization is the concept of the monitor. A  _monitor_  is an object that is used as a mutually exclusive lock. Only one thread can  _own_  a monitor at a given time. When a thread acquires a lock, it is said to have  _entered_  the monitor. All other threads attempting to enter the locked monitor will be suspended until the first thread  _exits_  the monitor. These other threads are said to be  _waiting_  for the monitor. A thread that owns a monitor can reenter the same monitor if it so desires.

You can synchronize your code in either of two ways. Both involve the use of the  **synchronized**keyword, and both are examined here.

#### Using Synchronized Methods

Synchronization is easy in Java, because all objects have their own implicit monitor associated with them. To  enter an object’s monitor, just call a method that has been modified with the  **synchronized**keyword. While a thread is inside a synchronized method, all other threads that try to call it (or any other synchronized method) on the same instance have to wait. To exit the monitor and relinquish control of the object to the next waiting thread, the owner of the monitor simply returns from the synchronized method.

```java
synchronized void call() {...}
```
This prevents other threads from entering **call( )** while another thread is using it. Any time that you have a method, or group of methods, that manipulates the internal state of an object in a multithreaded situation, you should use the **synchronized** keyword to guard the state from race conditions. Remember, once a thread enters any synchronized method on an instance, no other thread can enter any other synchronized method on the same instance. However, nonsynchronized methods on that instance will continue to be callable.

#### The synchronized Statement

While creating  **synchronized**  methods within classes that you create is an easy and effective means of achieving synchronization, it will not work in all cases. To understand why, consider the following. Imagine that you want to synchronize access to objects of a class that was not designed for multithreaded access. That is, the class does not use  **synchronized**  methods. Further, this class was not created by you, but by a third party, and you do not have access to the source code. Thus, you can’t add  **synchronized**  to the appropriate methods within the class. How can access to an object of this class be synchronized? Fortunately, the solution to this problem is quite easy: You simply put calls to the methods defined by this class inside a  **synchronized**  block.

```java
		synchronized(target) {
			target.call(msg);
		}
```

### INTERTHREAD COMMUNICATION

The preceding examples unconditionally blocked other threads from asynchronous access to certain methods. This use of the implicit monitors in Java objects is powerful, but you can achieve a more subtle level of control through interprocess communication. As you will see, this is especially easy in Java.

As  discussed earlier, multithreading replaces event loop programming by dividing your tasks into discrete, logical units. Threads also provide a secondary benefit: they do away with polling. Polling is usually implemented by a loop that is used to check some condition repeatedly. Once the condition is true, appropriate action is taken. This wastes CPU time. For example, consider the classic queuing problem, where one thread is producing some data and another is consuming it. To make the problem more interesting, suppose that the producer has to wait until the consumer is finished before it generates more data. In a polling system, the consumer would waste many CPU cycles while it waited for the producer to produce. Once the producer was finished, it would start polling, wasting more CPU cycles waiting for the consumer to finish, and so on. Clearly, this situation is undesirable.

To avoid polling, Java includes an elegant interprocess communication mechanism via the  **wait( )**,  **notify( )**, and  **notifyAll( )**  methods. These methods are implemented as  **final**  methods in  **Object**, so all classes have them. All three methods can be called only from within a  **synchronized**  context. Although conceptually advanced from a computer science perspective, the rules for using these methods are actually quite simple:

• **wait( )**  tells the calling thread to give up the monitor and go to sleep until some other thread enters the same monitor and calls  **notify( )**  or  **notifyAll( )**.

• **notify( )**  wakes up a thread that called  **wait( )**  on the same object.

• **notifyAll( )**  wakes up all the threads that called  **wait( )**  on the same object. One of the threads will be granted access.

Although **wait( )** normally waits until **notify( )** or **notifyAll( )** is called, there is a possibility that in very rare cases the waiting thread could be awakened due to a _spurious wakeup_. In this case, a waiting thread resumes without **notify( )** or **notifyAll( )** having been called. (In essence, the thread resumes for no apparent reason.) Because of this remote possibility, the Java API documentation recommends that calls to **wait( )** should take place within a loop that checks the condition on which the thread is waiting.

### Deadlock

A special type of error that you need to avoid that relates specifically to multitasking is  _deadlock_, which occurs when two threads have a circular dependency on a pair of synchronized objects. For example, suppose one thread enters the monitor on object X and another thread enters the monitor on object Y. If the thread in X tries to call any synchronized method on Y, it will block as expected. However, if the thread in Y, in turn, tries to call any synchronized method on X, the thread waits forever, because to access X, it would have to release its own lock on Y so that the first thread could complete. Deadlock is a difficult error to debug for two reasons:

• In general, it occurs only rarely, when the two threads time-slice in just the right way.

• It may involve more than two threads and two synchronized objects. (That is, deadlock can occur through a more convoluted sequence of events than just described.)

**One use case:** The **suspend( )** method of the **Thread** class was deprecated by Java 2 several years ago. This was done because **suspend( )** can sometimes cause serious system failures. Assume that a thread has obtained locks on critical data structures. If that thread is suspended at that point, those locks are not relinquished. Other threads that may be waiting for those resources can be deadlocked.

### Annotations in Java

Annotations are used to provide supplement information about a program.

-   Annotations start with ‘**@**’.
-   Annotations do not change action of a compiled program.
-   Annotations help to associate  _metadata_  (information) to the program elements i.e. instance variables, constructors, methods, classes, etc.
-   Annotations are not pure comments as they can change the way a program is treated by compiler. See below code for example.

```java
class Base 
{ 
     public void display() 
     { 
         System.out.println("Base display()"); 
     } 
} 
class Derived extends Base 
{ 
     @Override
     public void display(int x) 
     { 
         System.out.println("Derived display(int )"); 
     } 
  
     public static void main(String args[]) 
     { 
         Derived obj = new Derived(); 
         obj.display(); 
     } 
}
```

If we remove parameter (int x) or we remove @override, the program compiles fine.

**Categories of Annotations**

There are 3 categories of Annotations:-  
**1. Marker Annotations:**  
The only purpose is to mark a declaration. These annotations contain no members and do not consist any data. Thus, its presence as an annotation is sufficient. Since, marker interface contains no members, simply determining whether it is present or absent is sufficient.  **@Override**  is an example of Marker Annotation.

Example: - @TestAnnotation()

**2. Single value Annotations:**  
These annotations contain only one member and allow a shorthand form of specifying the value of the member. We only need to specify the value for that member when the annotation is applied and don’t need to specify the name of the member. However in order to use this shorthand, the name of the member must be  **value.**

Example: - @TestAnnotation(“testing”);

**3. Full Annotations:**  
These annotations consist of multiple data members/ name, value, pairs.

Example:- @TestAnnotation(owner=”Rahul”, value=”Class Geeks”)

For further details: [[https://www.geeksforgeeks.org/annotations-in-java/](https://www.geeksforgeeks.org/annotations-in-java/)]

### Java Generics

At its core, the term  _generics_  means  _parameterized types_. Parameterized types are important because they enable you to create classes, interfaces, and methods in which the type of data upon which they operate is specified as a parameter. Using generics, it is possible to create a single class, for example, that automatically works with different types of data. A class, interface, or method that operates on a parameterized type is called  _generic_, as in  _generic class_  or  _generic method_.

It is important to understand that Java has always given you the ability to create generalized classes, interfaces, and methods by operating through references of type  **Object**. Because  **Object**  is the superclass of all other classes, an  **Object**  reference can refer to any type object. Thus, in pre-generics code, generalized classes, interfaces, and methods used  **Object**  references to operate on various types of objects. The problem was that they could not do so with type safety.

Generics added the type safety that was lacking. They also streamlined the process, because it is no longer necessary to explicitly employ casts to translate between  **Object**  and the type of data that is actually being operated upon. With generics, all casts are automatic and implicit. Thus, generics expanded your ability to reuse code and let you do so safely and easily.

```java
class Gen<T> {
	T ob;

	Gen(T ob) { this.ob = ob; }

	T getOb() { return ob; }

	void showType() {
		System.out.println("Type of T: " + ob.getClass().getName());
	}
}

class Main {
	public static void main(String[] args) {

		Gen<String> a = new Gen<String> ("hello generics");
		String str = a.getOb();

		System.out.println(str);
	}
}
```

Here, **T** is the name of a _type parameter_. This name is used as a placeholder for the actual type that will be passed to **Gen** when an object is created. Thus, **T** is used within **Gen** whenever the type parameter is needed. Notice that **T** is contained within **< >**. This syntax can be generalized. Whenever a type parameter is being declared, it is specified within angle brackets. Because **Gen** uses a type parameter, **Gen** is a generic class, which is also called a _parameterized type_.

Before moving on, it’s necessary to state that the Java compiler does not actually create different versions of **Gen**, or of any other generic class. Although it’s helpful to think in these terms, it is not what actually happens. Instead, the compiler removes all generic type information, substituting the necessary casts, to make your code _behave as if_ a specific version of **Gen** were created. Thus, there is really only one version of **Gen** that actually exists in your program. The process of removing generic type information is called **Erasure**. The ability to create type-safe code in which type-mismatch errors are caught at compile time is a key advantage of generics. Although using **Object**references to create “generic” code has always been possible, that code was not type safe, and its misuse could result in run-time exceptions. Generics prevent this from occurring. In essence, through generics, run-time errors are converted into compile-time errors. This is a major advantage.

#### Generic Bounded Type

When specifying a type parameter, you can create an upper bound that declares the superclass from which all type arguments must be derived. This is accomplished through the use of an  **extends**  clause when specifying the type parameter, as shown here:

`<T extends superclass>`

This specifies that  _T_  can only be replaced by  _superclass_, or subclasses of  _superclass_. Thus,  _superclass_defines an inclusive, upper limit.

In addition to using a class type as a bound, you can also use an interface type. In fact, you can specify multiple interfaces as bounds. Furthermore, a bound can include both a class type and one or more interfaces. In this case, the class type must be specified first. When a bound includes an interface type, only type arguments that implement that interface are legal. When specifying a bound that has a class and an interface, or multiple interfaces, use the  **&**  operator to connect them. This creates an  _intersection type_. For example,

`class Gen<T extends MyClass & MyInterface> { // ...`

#### Generic Method

As the preceding examples have shown, methods inside a generic class can make use of a class’ type parameter and are, therefore, automatically generic relative to the type parameter. However, it is possible to declare a generic method that uses one or more type parameters of  its own. Furthermore, it is possible to create a generic method that is enclosed within a non-generic class.

`static <T extends Comparable<T>, V extends T> boolean callme(T t, V v) { .. }`

**It is possible for constructors to be generic, even if their class is not**

To handle the legacy java code prior to the generics, Java allows a generic class to be used without any type arguments. This creates a _raw type_ for the class. This raw type is compatible with legacy code, which has no knowledge of generics. The main drawback to using the raw type is that the type safety of generics is lost.

### Erasure

### ERASURE

Usually, it is not necessary to know the details about how the Java compiler transforms your source code into object code. However, in the case of generics, some general understanding of the process is important because it explains why the generic features work as they do—and why their behavior is sometimes a bit surprising. For this reason, a brief discussion of how generics are implemented in Java is in order.

An important constraint that governed the way that generics were added to Java was the need for compatibility with previous versions of Java. Simply put, generic code had to be compatible with preexisting, non-generic code. Thus, any changes to the syntax of the Java language, or to the JVM, had to avoid breaking older code. The way Java implements generics while satisfying this constraint is through the use of  _erasure_.

In general, here is how erasure works. When your Java code is compiled, all generic type information is removed (erased). This means replacing type parameters with their bound type, which is  **Object**  if no explicit bound is specified, and then applying the appropriate casts (as determined by the type arguments) to maintain type compatibility with the types specified by the type arguments. The compiler also enforces this type compatibility. This approach to generics means that no type parameters exist at run time. They are simply a source-code mechanism.

### Java Collection Framework

1. The Collections Framework was designed to meet several goals. First, the framework had to be high-performance. The implementations for the fundamental collections (dynamic arrays, linked lists, trees, and hash tables) are highly efficient. You seldom, if ever, need to code one of these “data engines” manually. Second, the framework had to allow different types of collections to work in a similar manner and with a high degree of interoperability. Third, extending and/or adapting a collection had to be easy. Toward this end, the entire Collections Framework is built upon a set of standard interfaces. Several standard implementations (such as **LinkedList**, **HashSet**, and **TreeSet**) of these interfaces are provided that you may use as-is. You may also implement your own collection, if you choose. Various special-purpose implementations are created for your convenience, and some partial implementations are provided that make creating your own collection class easier. Finally, mechanisms were added that allow the integration of standard arrays into the Collections Framework.
2. _Algorithms_ are another important part of the collection mechanism. Algorithms operate on collections and are defined as static methods within the **Collections** class. Thus, they are available for all collections. Each collection class need not implement its own versions. The algorithms provide a standard means of manipulating collections.
3. Another item closely associated with the Collections Framework is the **Iterator** interface. An _iterator_offers a general-purpose, standardized way of accessing the elements within a collection, one at a time. Thus, an iterator provides a means of _enumerating the contents of a collection_. Because each collection provides an iterator, the elements of any collection class can be accessed through the methods defined by **Iterator**. Thus, with only small changes, the code that cycles through a set can also be used to cycle through a list, for example.

![java-arraylist](https://media.geeksforgeeks.org/wp-content/uploads/java-collection.jpg)

#### ArrayList

**ArrayList** supports dynamic arrays that can grow as needed. In Java, standard arrays are of a fixed length. After arrays are created, they cannot grow or shrink, which means that you must know in advance how many elements an array will hold. But, sometimes, you may not know until run time precisely how large an array you need. To handle this situation, the Collections Framework defines **ArrayList**. In essence, an **ArrayList** is a variable-length array of object references. That is, an **ArrayList** can dynamically increase or decrease in size. Array lists are created with an initial size. When this size is exceeded, the collection is automatically enlarged. When objects are removed, the array can be shrunk.

#### LinkedList

The **LinkedList** class extends **AbstractSequentialList** and implements the **List**, **Deque**, and **Queue** interfaces. It provides a linked-list data structure.

Because **LinkedList** implements the **Deque** interface, you have access to the methods defined by **Deque**. For example, to add elements to the start of a list, you can use **addFirst( )** or **offerFirst( )**. To add elements to the end of the list, use **addLast( )** or **offerLast( )**. To obtain the first element, you can use **getFirst( )** or **peekFirst( )**. To obtain the last element, use **getLast( )** or **peekLast( )**. To remove the first element, use **removeFirst( )** or **pollFirst( )**. To remove the last element, use **removeLast( )** or **pollLast( )**.

#### The HashSet Class

**HashSet**  extends  **AbstractSet**  and implements the  **Set**  interface. It creates a collection that uses a hash table for storage.  **HashSet**  is a generic class that has this declaration:

class HashSet<E>

Here,  **E**  specifies the type of objects that the set will hold.

As most readers likely know, a hash table stores information by using a mechanism called hashing. In  _hashing_, the informational content of a key is used to determine a unique value, called its  _hash code_. The hash code is then used as the index at which the data associated with the key is stored. The transformation of the key into its hash code is performed automatically—you never see the hash code itself. Also, your code can’t directly index the hash table. The advantage of hashing is that it allows the execution time of  **add( )**,  **contains( )**,  **remove( )**, and  **size( )**  to remain constant even for large sets.

It is important to note that **HashSet** does not guarantee the order of its elements, because the process of hashing doesn’t usually lend itself to the creation of sorted sets. If you need sorted storage, then another collection, such as **TreeSet**, is a better choice.

#### The TreeSet Class

**TreeSet**  extends  **AbstractSet**  and implements the  **NavigableSet**  interface. It creates a collection that uses a tree for storage. Objects are stored in sorted, ascending order. Access and retrieval times are quite fast, which makes  **TreeSet**  an excellent choice when storing large amounts of sorted information that must be found quickly.

#### Iterator

Before you can access a collection through an iterator, you must obtain one. Each of the collection classes provides an  **iterator( )**  method that returns an iterator to the start of the collection. By using this iterator object, you can access each element in the collection, one element at a time. In general, to use an iterator to cycle through the contents of a collection, follow these steps:

1. Obtain an iterator to the start of the collection by calling the collection’s  **iterator( )**  method.
2. Set up a loop that makes a call to  **hasNext( )**. Have the loop iterate as long as  **hasNext( )**  returns  **true**.
3. Within the loop, obtain each element by calling  **next( )**.

```java
import java.util.*;

class Main {
	public static void main(String[] args) {

		ArrayList<String> al = new ArrayList<String> ();
		al.add("c");
		al.add("a");
		al.add("b");

		Iterator iterator = al.iterator();

		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}
	}
}
```

### Comparators

Both  **TreeSet**  and  **TreeMap**  store elements in sorted order. However, it is the comparator that defines precisely what “sorted order” means. By default, these classes store their elements by using what Java refers to as “natural ordering,” which is usually the ordering that you would expect (A before B, 1 before 2, and so forth). If you want to order elements a different way,  then specify a  **Comparator**when you construct the set or map. Doing so gives you the ability to govern precisely how elements are stored within sorted collections and maps. Implement below methods to customize the natural ordering

int compare(T  _obj1_, T  _obj2_)

_obj1_  and  _obj2_  are the objects to be compared. Normally, this method returns zero if the objects are equal. It returns a positive value if  _obj1_  is greater than  _obj2_. Otherwise, a negative value is returned. The method can throw a  **ClassCastException**  if the types of the objects are not compatible for comparison. By implementing  **compare( )**, you can alter the way that objects are ordered. For example, to sort in reverse order, you can create a comparator that reverses the outcome of a comparison.

```java
import java.util.*;

class MyComp implements Comparator<String> {
	public int compare(String a, String b) {
		return b.compareTo(a);
	}
}

class Main {
	public static void main(String[] args) {

		TreeSet<String> al = new TreeSet<String> (new MyComp());
		al.add("c");
		al.add("a");
		al.add("b");

		Iterator iterator = al.iterator();

		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}
	}
}
```

### JVM Internals

[https://www.cubrid.org/blog/understanding-jvm-internals/](https://www.cubrid.org/blog/understanding-jvm-internals/)

![](https://www.cubrid.org/files/attach/images/1750/751/001/b94d4a812724c780250e0aa47711e40e.png)

### Garbase Collection

[https://www.baeldung.com/jvm-garbage-collectors](https://www.baeldung.com/jvm-garbage-collectors)

#### Tune GC Processing

[https://www.cubrid.org/blog/how-to-tune-java-garbage-collection/](https://www.cubrid.org/blog/how-to-tune-java-garbage-collection/)

### Java Design Pattern

Design patterns represent the best practices used by experienced object-oriented software developers. Design patterns are solutions to general problems that software developers faced during software development. These solutions were obtained by trial and error by numerous software developers over quite a substantial period of time.

#### What is Gang of Four (GOF)?

In 1994, four authors Erich Gamma, Richard Helm, Ralph Johnson and John Vlissides published a book titled  **Design Patterns - Elements of Reusable Object-Oriented Software**  which initiated the concept of Design Pattern in Software development.

These authors are collectively known as  **Gang of Four (GOF)**. According to these authors design patterns are primarily based on the following principles of object orientated design.

-   Program to an interface not an implementation
-   Favor object composition over inheritance

#### Types of Design Patterns

As per the design pattern reference book  **Design Patterns - Elements of Reusable Object-Oriented Software**  , there are 23 design patterns which can be classified in three categories: Creational, Structural and Behavioral patterns. We'll also discuss another category of design pattern: J2EE design patterns.

**Creational Patterns**  
These design patterns provide a way to create objects while hiding the creation logic, rather than instantiating objects directly using `new` operator. This gives program more flexibility in deciding which objects need to be created for a given use case. This pattern is good for generic instantiation, simplicity and constraint creation. Popular design pattern are:

- Abstruct factory pattern
- Builder pattern
- Factory method pattern
- Prototype pattern
- Singleton pattern

**Structural Patterns**  
These design patterns concern class and object composition. Concept of inheritance is used to compose interfaces and define ways to compose objects to obtain new functionalities. This pattern is good for performance, refactoring, memory utilization. Popular design pattern are:

- Adapter pattern
- Bridge pattern
- Composite pattern
- Decorator pattern
- Facade pattern
- Flyweight pattern
- Proxy pattern

**Behavioral Patterns**  
These design patterns are specifically concerned with communication between objects. These design patterns are used to provide solution for the better interaction between objects and how to provide loose coupling and flexibility to extend easily.

- Command pattern
- Observer pattern

#### Factory pattern

[https://www.tutorialspoint.com/design_pattern/factory_pattern.htm](https://www.tutorialspoint.com/design_pattern/factory_pattern.htm)

**Use case**

- Whenever we have a super class and N number of sub-classes and based on data provided if we have to return the object of one of the sub-classes
- at run-time if we need to get an object of similar type based on the parameter value we pass


<!--stackedit_data:
eyJoaXN0b3J5IjpbLTIxMjI3NTgyMjldfQ==
-->