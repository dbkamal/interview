# JavaScript Design Pattern & Best Practices

Ref:
1. [https://www.toptal.com/javascript/comprehensive-guide-javascript-design-patterns](https://www.toptal.com/javascript/comprehensive-guide-javascript-design-patterns)
2. [http://jstherightway.org/](http://jstherightway.org/)
3. [https://github.com/airbnb/javascript](https://github.com/airbnb/javascript)

The main benefits we get from design patterns are the following:

-   **They are proven solutions:**  Because design patterns are often used by many developers, you can be certain that they work. And not only that, you can be certain that they were revised multiple times and optimizations were probably implemented.
-   **They are easily reusable:**  Design patterns document a reusable solution which can be modified to solve multiple particular problems, as they are not tied to a specific problem.
-   **They are expressive:**  Design patterns can explain a large solution quite elegantly.
-   **They ease communication:**  When developers are familiar with design patterns, they can more easily communicate with one another about potential solutions to a given problem.
-   **They prevent the need for refactoring code:**  If an application is written with design patterns in mind, it is often the case that you won’t need to refactor the code later on because applying the correct design pattern to a given problem is already an optimal solution.
-   **They lower the size of the codebase:**  Because design patterns are usually elegant and optimal solutions, they usually require less code than other solutions.

## JavaScript Features

### JavaScript Supports First-class Functions

JavaScript treats functions as first-class citizens, meaning you can pass functions as parameters to other functions just like you would any other variable.

```javascript
// we send in the function as an argument to be
// executed from inside the calling function
function performOperation(a, b, cb) {
    var c = a + b;
    cb(c);
}

performOperation(2, 3, function(result) {
    // prints out 5
    console.log("The result of the operation is " + result);
})
```

### JavaScript Is Prototype-based

As is the case with many other object-oriented languages, JavaScript supports objects, and one of the first terms that comes to mind when thinking about objects is classes and inheritance. This is where it gets a little tricky, as the language doesn’t support classes in its plain language form but rather uses something called prototype-based or instance-based inheritance.

It is just now, in ES6, that the formal term  **class**  is introduced, however, that even though the term “class” is introduced into JavaScript, it still utilizes prototype-based inheritance under the hood.

Prototype-based programming is a style of object-oriented programming in which behavior reuse (known as inheritance) is performed via a process of reusing existing objects via delegations that serve as prototypes.

### JavaScript Event Loops

If you have experience working with JavaScript, you are surely familiar with the term  **callback function**. For those not familiar with the term, a callback function is a function sent as a parameter (remember, JavaScript treats functions as first-class citizens) to another function and gets executed after an event fires. This is usually used for subscribing to events such as a mouse click or a keyboard button press.

![Graphic depiction of the JavaScript event loop](https://uploads.toptal.io/blog/image/125781/toptal-blog-image-1522333483007-ac9070c74c6ae7747cb2fa551667d8e5.png)


<!--stackedit_data:
eyJoaXN0b3J5IjpbMTg4NjIxMDI0NywyMTIyMjkxMDgxLC0yMD
IxNTk0ODE2LDk0Njc1MTE0NSwxNzkxMTkwNzg3LDE4NjAzODM3
ODBdfQ==
-->