
# ReactJS

## Single-page application vs. multiple-page application

[https://stackoverflow.com/questions/21862054/single-page-application-advantages-and-disadvantages](https://stackoverflow.com/questions/21862054/single-page-application-advantages-and-disadvantages)

**Single  Page Application:** A single-page application is an app that works inside a browser and does not require page reloading during use. You are using this type of applications every day. These are, for instance: Gmail, Google Maps, Facebook or GitHub.  
SPAs are all about serving an outstanding UX by trying to imitate a “natural” environment in the browser — no page reloads, no extra wait time. It is just one web page that you visit which then loads all other content using JavaScript — which they heavily depend on.

SPA requests the markup and data independently and renders pages straight in the browser. We can do this thanks to the advanced JavaScript frameworks like AngularJS, Ember.js, Meteor.js, Knockout.js, ReactJS.
Single-page sites help keep the user in one, comfortable web space where content is presented to the user in a simple, easy and workable fashion.

Single page apps are distinguished by their ability to redraw any part of the UI without requiring a server roundtrip to retrieve HTML. This is achieved by separating the data from the presentation of data by having a model layer that handles data and a view layer that reads from the models.

### Pros of the Single-Page Application:

-   SPA is fast, as most resources (HTML+CSS+Scripts) are only loaded once throughout the lifespan of application. Only data is transmitted back and forth.
-   The development is simplified and streamlined. There is no need to write code to render pages on the server. It is much easier to get started because you can usually kick off development from a file file://URI, without using any server at all.
-   SPAs are easy to debug with Chrome, as you can monitor network operations, investigate page elements and data associated with it.
-   It’s easier to make a mobile application because the developer can reuse the same backend code for web application and native mobile application.
-   SPA can cache any local storage effectively (read about local storage [https://stackoverflow.com/questions/43519890/what-is-the-difference-between-local-storage-and-cache-in-the-browser](https://stackoverflow.com/questions/43519890/what-is-the-difference-between-local-storage-and-cache-in-the-browser)). An application sends only one request, store all data, then it can use this data and works even offline.

### Cons of the Single-Page Application:

-   It is very tricky and not an easy task to make SEO optimization of a Single-Page Application. Its content is loaded by AJAX (Asynchronous JavaScript and XML) — a method of exchanging data and updating in the application without refreshing the page.  
    **UPDATE 27.09.2017**: In  [her comment](https://medium.com/@iris_schaffer/i-have-a-few-problems-with-the-cons-of-spas-you-list-d7055e5a29f5), Iris Shaffer correctly pointed out that it can be done on server side as well. Indeed, it is easier today than it used to be.
-   It is slow to download because heavy client frameworks are required to be loaded to the client.
-   It requires JavaScript to be present and enabled. If any user disables JavaScript in his or her browser, it won’t be possible to present application and its actions in a correct way.  
    **UPDATE 27.09.2017**: In  [her comment](https://medium.com/@iris_schaffer/i-have-a-few-problems-with-the-cons-of-spas-you-list-d7055e5a29f5), Iris Shaffer has noticed that with isomorphic rendering / server side rendering, you can render the page on the server already. When the initial render is on the server and can be cached, disabling JS would not be a concern for getting a rendered page. Theoretically, that’s right. Obviously you can render on server side. But lack of JS can be a concern for other functionalities. Lots of things can be done in HTML & CSS but from my experience it would be hell to do it this way instead of use JavaScript.
-   Compared to the “traditional” application, SPA is less secure. Due to Cross-Site Scripting (XSS), it enables attackers to inject client-side scripts into web application by other users.
-   Memory leak in JavaScript can even cause powerful system to slow down.

### Multi-Page Application

Multiple-page applications work in a “traditional” way. Every change eg. display the data or submit data back to server requests rendering a new page from the server in the browser. These applications are large, bigger than SPAs because they need to be. Due to the amount of content, these applications have many levels of UI. Luckily, it’s not a problem anymore. Thanks to AJAX, we don’t have to worry that big and complex applications have to transfer a lot of data between server and browser. That solution improves and it allows to refresh only particular parts of the application. On the other hand, it adds more complexity and it is more difficult to develop than a single-page application.

### Pros of the Multiple-Page Application:

-   It’s the perfect approach for users who need a visual map of where to go in the application. Solid, few level menu navigation is an essential part of traditional Multi-Page Application.
-   Very good and easy for proper SEO management. It gives better chances to rank for different keywords since an application can be optimized for one keyword per page.
### Cons of the multiple-page application:

-   There is no option to use the same backend with mobile applications.  
    **UPDATE 27.09.2017**: Back then, when I was writing this article, I didn’t have much experience with backend and with mobile apps. It’s obvious for me now that you can use the same backend for both. And I’d like to thank all the readers who pointed that out.
-   Frontend and backend development are tightly coupled.
-   The development becomes quite complex. The developer needs to use frameworks for either client and server side. This results in the longer time of application development.
### SPA or MPA?

Before deploying a web application, you need to consider the goal of it. If you know you need multiple categories (because, for instance, you run an online shop or publish a lot of other content) — use a multi-page site. If you are sure that your site is appropriate for a pure single-page experience — go for it. And if you like SPA but can just barely fit everything into a single page, consider the hybrid site instead. A hybrid application takes what is the best in both approaches and try to minimize the disadvantages. It is, in fact, a single page application which uses URL anchors as synthetic pages enabling more in build browser navigation and preference functionality. But this is the topic for another article.

Perhaps in the future, everyone will use Single Page Application model (including a hybrid app), as it seems to bring a lot of advantages. Many apps on the market are migrating towards this model. However, as some projects simply cannot fit into SPA, the MPA model is still vivid.

Ref: [https://medium.com/@NeotericEU/single-page-application-vs-multiple-page-application-2591588efe58](https://medium.com/@NeotericEU/single-page-application-vs-multiple-page-application-2591588efe58)

## Why choose React?
Originally developed for Facebook, **React** is a JavaScript library that builds user interfaces for single-page applications by dividing UI into composable components. Since it requires only a minimal understanding of HTML and JavaScript, React has risen in popularity as a front-end web development tool.

### 1. Fast Learning Curve

React is very a simple and lightweight library that  **only deals with the view layer**. It is not a beast like other MV* frameworks such as Angular or Ember. Any Javascript developer can understand the basics and start developing an awesome web application after only a couple of days reading tutorial.

### 2. Reusable Components

React provides a component based structure. Components are your lego pieces. You start with tiny components like button, checkbox, dropdown etc. and then you create wrapper components composed of those smaller components. And then you write higher level wrapper components. And, it goes on like that until you have this one root component and that component is your app.

Each component decides how it should be rendered. Each component has its own internal logic. This approach has some amazing results. You can re-use components anywhere you need. As a result, (1) your app has  **consistent look and feel**, (2) code re-use makes it easier to  **maintain and grow your codebase**, and (3) it is  **easier to develop**  your app.

### 3. Fast render with Virtual DOM

When you are about to develop a web application that involves high user interaction and view updates, you have to consider the possible performance issues. Although today’s javascript engines are fast enough to handle such complex applications, DOM manipulations are still not that fast.  **Updating DOM is usually the bottleneck when it comes to the web performance**. React is trying to solve this problem by using something called  **virtual DOM**; a DOM kept in memory. Any view changes are first reflected to virtual DOM, then an efficient diff algorithm compares the previous and current states of the virtual DOM and calculates the best way (minimum amount of updates needed) to apply these changes. Finally those updates are applied to the DOM to ensure minimum read/write time. This is the main reason behind React’s high performance.

### 4. Clean Abstraction

One of the powerful sides of React is that it provides a good abstraction which means that it does not expose any complex internals to the user.

Compare this to Angular: Why on earth should you have to learn an internal process such as digest cycles? Those kind of details better be kept as internals to provide a clean abstraction. You only need to understand a component’s life cycles, states and props to master React to accomplish everything you need. React does not dictate any patterns or architecture like MVC/MVVM, after all its only about the view layer and you are free to design your app’s architecture in any way you see fit.

Yet there is one good architecture that fits React really well called  **Flux**.

### 5. Flux and Redux

Flux architecture is introduced and maintained by Facebook and they use it for their web applications. It complements React components by unidirectional data flow. The overall structure is as follows.
![](https://cdn-images-1.medium.com/max/1600/1*Ek68XwgLgxwlAZl6hhxx1w.png)

The main idea is to create actions which are orchestrated by a central dispatcher to update stores. Then the views are updated with respect to the changes in that stores. All the data to be displayed by components are kept in stores and are not duplicated as models in MVC structure which saves you from trying to keep your model data in sync throughout the application all the time.

Unfortunately, flux is not a ready to use library but there are such implementations. The most popular one is  **Redux**  which is a kind of reinterpretation of flux architecture. It provides a single store which is not required in flux. In my opinion, this is a great decision you should make, for the sake of having a  **single source of truth**. There is only one single object where you keep all the application data. This makes it easier to observe and manipulate. Any change on the store (data) will trigger a render for related components and the view is always kept in sync with data.

Another great feature of Redux is that you can define a middleware to intercept dispatched actions. Generally it used for logging, exception handling and async API calls but you can also write a middleware easily to solve all kinds of other problems.

If you choose to use redux it comes with a great dev tool. That will make things a lot easier for you.

### 6. React Native

Learning React comes with a bonus:  **React Native**. React is not a ‘write once run anywhere library’, as the creators says, it’s a ‘**learn once write anywhere**’ library. Yes, you can write native apps for Android and iOS using React Native. Although you will not be able to use the exact same code you wrote for web, you will be able to use the same methodology and the same architecture.

Ref: [https://stories.jotform.com/7-reasons-why-you-should-use-react-ad420c634247](https://stories.jotform.com/7-reasons-why-you-should-use-react-ad420c634247)

## JSX

[https://reactjs.org/docs/introducing-jsx.html](https://reactjs.org/docs/introducing-jsx.html)

### JSX Prevents Injection Attacks

It is safe to embed user input in JSX:

```
const title = response.potentiallyMaliciousInput;
// This is safe:
const element = <h1>{title}</h1>;
```

By default, React DOM  [escapes](https://stackoverflow.com/questions/7381974/which-characters-need-to-be-escaped-on-html)  any values embedded in JSX before rendering them. Thus it ensures that you can never inject anything that’s not explicitly written in your application. Everything is converted to a string before being rendered. This helps prevent  [XSS (cross-site-scripting)](https://en.wikipedia.org/wiki/Cross-site_scripting)  attacks.

### state vs props
[https://stackoverflow.com/questions/27991366/what-is-the-difference-between-state-and-props-in-react](https://stackoverflow.com/questions/27991366/what-is-the-difference-between-state-and-props-in-react)

## React Basics

- Elements are the smallest building blocks of React apps. 
- Components let you split the UI into independent, reusable pieces, and think about each piece in isolation.
- **All React components must act like pure functions with respect to their props.** A functions is called [“pure”](https://en.wikipedia.org/wiki/Pure_function)if it does not attempt to change it's inputs, and always return the same result for the same inputs. Of course, application UIs are dynamic and change over time. **"State"** allows React components to change their output over time in response to user actions, network responses, and anything else, without violating this rule
- The `componentDidMount()` method runs after the component output has been rendered to the DOM
- Do not modify the `state` directly, rather use `this.setState({ .. })`. The only place where you can assign  `this.state`  is the constructor. React may batch multiple  `setState()`  calls into a single update for performance. Because  `this.props`  and  `this.state`  may be updated asynchronously, you should not rely on their values for calculating the next state.
- Neither parent nor child components can know if a certain component is stateful or stateless, and they shouldn’t care whether it is defined as a function or a class. This is why state is often called local or encapsulated. It is not accessible to any component other than the one that owns and sets it. A component may choose to pass its state down as props to its child components. This is commonly called a **“top-down” or “unidirectional”** data flow. Any state is always owned by some specific component, and any data or UI derived from that state can only affect components “below” them in the tree. If you imagine a component tree as a waterfall of props, each component’s state is like an additional water source that joins it at an arbitrary point but also flows down.
- When you deal with list (like list of components ), make sure to assign key fiels to each of the list item. Keys help React identify which items have changed, are added, or are removed. Keys should be given to the elements inside the array to give the elements a stable identity. We don’t recommend using indexes for keys if the order of items may change. This can negatively impact performance and may cause issues with component state. Check out Robin Pokorny’s article for an [in-depth explanation on the negative impacts of using an index as a key](https://medium.com/@robinpokorny/index-as-a-key-is-an-anti-pattern-e0349aece318). If you choose not to assign an explicit key to list items then React will default to using indexes as keys.

### React Form

HTML form elements work a little bit differently from other DOM elements in React, because form elements naturally keep some internal state. In most cases, it’s convenient to have a JavaScript function that handles the submission of the form and has access to the data that the user entered into the form. The standard way to achieve this is with a technique called **“controlled components”**.

In HTML, form elements such as `<input>`, `<textarea>`, and `<select>` typically maintain their own state and update it based on user input. In React, mutable state is typically kept in the state property of components, and only updated with [`setState()`](https://reactjs.org/docs/react-component.html#setstate).

There should be a single “source of truth” for any data that changes in a React application. Usually, the state is first added to the component that needs it for rendering. Then, if other components also need it, you can lift it up to their closest common ancestor. Instead of trying to sync the state between different components, you should rely on the [top-down data flow](https://reactjs.org/docs/state-and-lifecycle.html#the-data-flows-down).

### Code Splitting

Bundling is great, but as your app grows, your bundle will grow too. Especially if you are including large third-party libraries. You need to keep an eye on the code you are including in your bundle so that you don’t accidentally make it so large that your app takes a long time to load.

To avoid winding up with a large bundle, it’s good to get ahead of the problem and start “splitting” your bundle.  [Code-Splitting](https://webpack.js.org/guides/code-splitting/)  is a feature supported by bundlers like Webpack and Browserify (via  [factor-bundle](https://github.com/browserify/factor-bundle)) which can create multiple bundles that can be dynamically loaded at runtime.

Code-splitting your app can help you “lazy-load” just the things that are currently needed by the user, which can dramatically improve the performance of your app. While you haven’t reduced the overall amount of code in your app, you’ve avoided loading code that the user may never need, and reduced the amount of code needed during the initial load.

## React JS Interview QA

**Q1. How React works? How Virtual-DOM works in React?**

React creates a virtual DOM. When state changes in a component it firstly runs a “diffing” algorithm, which identifies what has changed in the virtual DOM. The second step is reconciliation, where it updates the DOM with the results of diff.

The HTML DOM is always tree-structured — which is allowed by the structure of HTML document. The DOM trees are huge nowadays because of large apps. Since we are more and more pushed towards dynamic web apps (Single Page Applications — SPAs), we need to modify the DOM tree incessantly and a lot. And this is a real performance and development pain.

The Virtual DOM is an abstraction of the HTML DOM. It is lightweight and detached from the browser-specific implementation details. It is not invented by React but it uses it and provides it for free.  `ReactElements`  lives in the virtual DOM. They make the basic nodes here. Once we defined the elements,  `ReactElements`  can be render into the "real" DOM.

Whenever a  `ReactComponent`  is changing the state, diff algorithm in React runs and identifies what has changed. And then it updates the DOM with the results of diff. The point is - it’s done faster than it would be in the regular DOM.

**Q2. What is JSX?**

JSX is a syntax extension to JavaScript and comes with the full power of JavaScript. JSX produces React “elements”. You can embed any JavaScript expression in JSX by wrapping it in curly braces. After compilation, JSX expressions become regular JavaScript objects. This means that you can use JSX inside of if statements and for loops, assign it to variables, accept it as arguments, and return it from functions. Eventhough React does not require JSX, it is the recommended way of describing our UI in React app.

For example, below is the syntax for a basic element in React with JSX and its equivalent without it.

![](https://cdn-images-1.medium.com/max/800/1*jdz_xhFqhqiAQaPZGaRjRw.png)

Equivalent of the above using  `React.createElement`

![](https://cdn-images-1.medium.com/max/800/1*CdjJk4B7TL5LbZFDLBX8sQ.png)

**Q3. What is** `**React.createClass**`**?**

`React.createClass`  allows us to generate component "classes." But with ES6, React allows us to implement component classes that use ES6 JavaScript classes. The end result is the same -- we have a component class. But the style is different. And one is using a "custom" JavaScript class system (createClass) while the other is using a "native" JavaScript class system.

When using React’s  `createClass()`  method, we pass in an object as an argument. So we can write a component using  `createClass`  that looks like this:

![](https://cdn-images-1.medium.com/max/800/1*q58HA3tryv9hpw2dxtpxmg.png)

Using an ES6 class to write the same component is a little different. Instead of using a method from the  `react`  library, we extend an ES6 class that the library defines,  `Component`.

![](https://cdn-images-1.medium.com/max/800/1*zteV_2CbkJcRO1oHxL-lHQ.png)

`constructor()`  is a special function in a JavaScript class. JavaScript invokes  `constructor()`  whenever an object is created via a class.

**Q4. What is** `**ReactDOM**` **and what is the difference between** `**ReactDOM**` **and**`**React**`**?**

Prior to v0.14, all  `ReactDOM`  functionality was part of  `React`. But later,  `React`and  `ReactDOM`  were split into two different libraries.

As the name implies,  `ReactDOM`  is the glue between React and the DOM. Often, we will only use it for one single thing: mounting with  `ReactDOM`. Another useful feature of  `ReactDOM`  is  `ReactDOM.findDOMNode()`  which we can use to gain direct access to a DOM element.

For everything else, there’s  `React`. We use React to define and create our elements, for lifecycle hooks, etc. i.e. the guts of a React application.

**Q5. What are the differences between a class component and functional component?**

Class components allows us to use additional features such as local state and lifecycle hooks. Also, to enable our component to have direct access to our store and thus holds state.

When our component just receives props and renders them to the page, this is a ‘stateless component’, for which a pure function can be used. These are also called dumb components or presentational components.

From the previous question, we can say that our  `Booklist`  component is functional components and are stateless.

![](https://cdn-images-1.medium.com/max/800/1*2lHksigcP_3MFjKqCi7NBg.png)

On the other hand, the  `BookListContainer`  component is a class component.

**Q6. What is the difference between state and props?**

The state is a data structure that starts with a default value when a Component mounts. It may be mutated across time, mostly as a result of user events.

Props (short for properties) are a Component’s configuration. Props are how components talk to each other. They are received from above component and immutable as far as the Component receiving them is concerned. A Component cannot change its props, but it is responsible for putting together the props of its child Components. Props do not have to just be data — callback functions may be passed in as props.

There is also the case that we can have default props so that props are set even if a parent component doesn’t pass props down.

![](https://cdn-images-1.medium.com/max/800/1*a3zDRW805YMtAGVPuFZLMQ.png)

Props and State do similar things but are used in different ways. The majority of our components will probably be stateless. Props are used to pass data from parent to child or by the component itself. They are immutable and thus will not be changed. State is used for mutable data, or data that will change. This is particularly useful for user input.

**Q7. What are controlled components?**

In HTML, form elements such as  `<input>`,  `<textarea>`, and  `<select>`typically maintain their own state and update it based on user input. When a user submits a form the values from the aforementioned elements are sent with the form. With React it works differently. The component containing the form will keep track of the value of the input in it's state and will re-render the component each time the callback function e.g.  `onChange`  is fired as the state will be updated. A form element whose value is controlled by React in this way is called a "controlled component".

With a controlled component, every state mutation will have an associated handler function. This makes it straightforward to modify or validate user input.

**Q8. What is a higher order component?**

A higher-order component (HOC) is an advanced technique in React for reusing component logic. HOCs are not part of the React API. They are a pattern that emerges from React’s compositional nature.

A higher-order component is a function that takes a component and returns a new component.

HOC’s allow you to reuse code, logic and bootstrap abstraction. HOCs are common in third-party React libraries. The most common is probably Redux’s connect function. Beyond simply sharing utility libraries and simple composition, HOCs are the best way to share behavior between React Components. If you find yourself writing a lot of code in different places that does the same thing, you may be able to refactor that code into a reusable HOC.

**Q9. What is** `**create-react-app**`**?**

`create-react-app`  is the official CLI (Command Line Interface) for React to create React apps with no build configuration.

We don’t need to install or configure tools like Webpack or Babel. They are preconfigured and hidden so that we can focus on the code. We can install easily just like any other node modules. Then it is just one command to start the React project.

![](https://cdn-images-1.medium.com/max/800/1*2jC_2JPgxKTEN5bvp3SrUw.png)

It includes everything we need to build a React app:

-   React, JSX, ES6, and Flow syntax support.
-   Language extras beyond ES6 like the object spread operator.
-   Autoprefixed CSS, so you don’t need  `-webkit-`  or other prefixes.
-   A fast interactive unit test runner with built-in support for coverage reporting.
-   A live development server that warns about common mistakes.
-   A build script to bundle JS, CSS, and images for production, with hashes and sourcemaps.

**Q10. What is Redux?**

The basic idea of Redux is that the entire application state is kept in a single store. The store is simply a javascript object. The only way to change the state is by firing actions from your application and then writing reducers for these actions that modify the state. The entire state transition is kept inside reducers and should not have any side-effects.

Redux is based on the idea that there should be only a single source of truth for your application state, be it UI state like which tab is active or Data state like the user profile details.

![](https://cdn-images-1.medium.com/max/800/1*UixQj8JaMzLmXOsHocWYeA.png)

All of these data is retained by redux in a closure that redux calls a store . It also provides us a recipe of creating the said store, namely  `createStore(x)`.

The  `createStore`  function accepts another function,  `x`  as an argument. The passed in function is responsible for returning the state of the application at that point in time, which is then persisted in the store. This passed in function is known as the  `reducer`.

This is a valid example reducer function:

![](https://cdn-images-1.medium.com/max/800/1*ejuTaJ-mXMWnPVn8ItwFPA.png)

This store can only be updated by dispatching an action. Our App dispatches an  `action`, it is passed into  `reducer`; the reducer returns a fresh instance of the  `state`; the store notifies our App and it can begin it's re render as required.

**Q11. What is Redux Thunk used for?**

Redux thunk is middleware that allows us to write action creators that return a function instead of an action. The thunk can then be used to delay the dispatch of an action if a certain condition is met. This allows us to handle the asyncronous dispatching of actions. The inner function receives the store methods  `dispatch`  and  `getState`  as parameters.

To enable Redux Thunk, we need to use  `applyMiddleware()`  as below

![](https://cdn-images-1.medium.com/max/800/1*cCuCnVMum5Y453iFjYQKwQ.png)

**Q12. What is** `PureComponent`**? When to use** `PureComponent` **over**`Component`**?**

`PureComponent`  is exactly the same as Component except that it handles the  `shouldComponentUpdate`  method for us. When  `props`  or  `state`  changes,  `PureComponent`  will do a shallow comparison on both  `props`  and  `state`.  `Component`  on the other hand won't compare current props and state to next out of the box. Thus, the component will re-render by default whenever  `shouldComponentUpdate`  is called.

When comparing previous  `props`  and  `state`  to next, a shallow comparison will check that primitives have the same value (eg, 1 equals 1 or that true equals true) and that the references are the same between more complex javascript values like objects and arrays.

It is good to prefer  `PureComponent`  over  `Component`  whenever we never mutate our objects.

![](https://cdn-images-1.medium.com/max/800/1*iisL6VZlGGpO2W5wAKTb_g.png)

**Q13. How Virtual-DOM is more efficient than Dirty checking?**

In React, each of our components have a state. This state is like an observable. Essentially, React knows when to re-render the scene because it is able to observe when this data changes. Dirty checking is slower than observables because we must poll the data at a regular interval and check all of the values in the data structure recursively. By comparison, setting a value on the state will signal to a listener that some state has changed, so React can simply listen for change events on the state and queue up re-rendering.

The virtual DOM is used for efficient re-rendering of the DOM. This isn’t really related to dirty checking your data. We could re-render using a virtual DOM with or without dirty checking. In fact, the diff algorithm is a dirty checker itself.

We aim to re-render the virtual tree only when the state changes. So using an observable to check if the state has changed is an efficient way to prevent unnecessary re-renders, which would cause lots of unnecessary tree diffs. If nothing has changed, we do nothing.

**Q14. Is** `**setState()**` **is async? Why is** `**setState()**` **in React Async instead of Sync?**

`setState()`  actions are asynchronous and are batched for performance gains. This is explained in documentation as below.

`setState()`  does not immediately mutate this.state but creates a pending state transition. Accessing this.state after calling this method can potentially return the existing value. There is no guarantee of synchronous operation of calls to setState and calls may be batched for performance gains.

This is because setState alters the state and causes rerendering. This can be an expensive operation and making it synchronous might leave the browser unresponsive. Thus the setState calls are asynchronous as well as batched for better UI experience and performance.

**Q15. What is** `**render()**` **in React? And explain its purpose?**

Each React component must have a  `render()`  mandatorily. It returns a single React element which is the representation of the native DOM component. If more than one HTML element needs to be rendered, then they must be grouped together inside one enclosing tag such as  `<form>`,  `<group>`,  `<div>`etc. This function must be kept pure i.e., it must return the same result each time it is invoked.

**Q16. What are controlled and uncontrolled components in React?**

This relates to stateful DOM components (form elements) and the difference:

-   A  **Controlled Component**  is one that takes its current value through props and notifies changes through callbacks like onChange. A parent component “controls” it by handling the callback and managing its own state and passing the new values as props to the controlled component. You could also call this a “dumb component”.
-   A Uncontrolled Component is one that stores its own state internally, and you query the DOM using a ref to find its current value when you need it. This is a bit more like traditional HTML.

In most (or all) cases we should use controlled components.

**Q17. Explain the components of Redux.**

Redux is composed of the following components:

-   **Action** — Actions are payloads of information that send data from our application to our store. They are the only source of information for the store. We send them to the store using  `store.dispatch()`. Primarly, they are just an object describes what happened in our app.
-   **Reducer** — Reducers specify how the application’s state changes in response to actions sent to the store. Remember that actions only describe what happened, but don’t describe how the application’s state changes. So this place determines how state will change to an action.
-   **Store** — The Store is the object that brings Action and Reducer together. The store has the following responsibilities: Holds application state; Allows access to state via  `getState()`; Allows state to be updated via  `dispatch(action)`; Registers listeners via  `subscribe(listener)`; Handles unregistering of listeners via the function returned by  `subscribe(listener)`.

It’s important to note that we’ll only have a single store in a Redux application. When we want to split your data handling logic, we’ll use reducer composition instead of many stores.

**Q18. What is** `**React.cloneElement**`**? And the difference with**`**this.props.children**`**?**

`React.cloneElement`  clone and return a new React element using using the passed  `element`  as the starting point. The resulting element will have the original element's props with the new props merged in shallowly. New children will replace existing children.  `key`  and  `ref`  from the original element will be preserved.

`React.cloneElement`  only works if our child is a single React element. For almost everything  `{this.props.children}`  is the better solution. Cloning is useful in some more advanced scenarios, where a parent send in an element and the child component needs to change some props on that element or add things like ref for accessing the actual DOM element.

**Q19. What is the second argument that can optionally be passed to**`**setState**` **and what is its purpose?**

A callback function which will be invoked when  `setState`  has finished and the component is re-rendered.

Since the  `setState`  is asynchronous, which is why it takes in a second callback function. With this function, we can do what we want immediately after state has been updated.

**Q20. What is the difference between React Native and React?**

React is a JavaScript library, supporting both front end web and being run on the server, for building user interfaces and web applications.

On the other hand, React Native is a mobile framework that compiles to native app components, allowing us to build native mobile applications (iOS, Android, and Windows) in JavaScript that allows us to use ReactJS to build our components, and implements ReactJS under the hood.

With React Native it is possible to mimic the behavior of the native app in JavaScript and at the end, we will get platform specific code as the output. We may even mix the native code with the JavaScript if we need to optimize our application further.

**Very Important, MUST READ [https://github.com/sudheerj/reactjs-interview-questions](https://github.com/sudheerj/reactjs-interview-questions)**

## Few notes

- As of React 16, errors that were not caught by any error boundary will result in unmounting of the whole React component tree. React doesn’t need error boundaries to recover from errors in event handlers. Unlike the render method and lifecycle methods, the event handlers don’t happen during rendering. So if they throw, React still knows what to display on the screen.  
  
- Ref forwarding is a technique for automatically passing a ref through a component to one of its children. This is typically not necessary for most components in the application. However, it can be useful for some kinds of components, especially in reusable component libraries. Ref forwarding is an opt-in feature that lets some components take a ref they receive, and pass it further down (in other words, “forward” it) to a child.  
[https://medium.com/@rossbulat/how-to-use-react-refs-4541a7501663](https://medium.com/@rossbulat/how-to-use-react-refs-4541a7501663)  
  
- A common pattern in React is for a component to return multiple elements. Fragments let you group a list of children without adding extra nodes to the DOM.  
  
- a higher-order component is a function that takes a component and returns a new component. In a simple sentence, they are just JavaScript functions that take a component as an argument and returns a new enhanced component. Main use of HoCs is to achieve reusability. Typically, developers pull out reusable codes into a higher order component (HoC). It involves passing props and hoisting statics. It can leads to collisions, in simple words, we can get props name conflicts, especially when we are composing a component with multiple HoCs that might use the same prop names.  
  
- Hooks are a new addition in React 16.8. They let you use state and other React features without writing a class. The Effect Hook, useEffect, adds the ability to perform side effects from a function component. It serves the same purpose as componentDidMount, componentDidUpdate, and componentWillUnmount in React classes, but unified into a single API.  
  
- SSR: Server-side rendering is when HTML pages are built on the server rather than in the browser. With client-side rendering, a fairly empty HTML file is first served to the browser and then it's the browser's responsibility to render the entire UI for the application. There are two primary reasons why you should consider using server-side rendering: search engine optimization (SEO) and perceived performance. 

- `componentDidMount()`  is invoked immediately after a component is mounted (inserted into the tree). Initialization that requires DOM nodes should go here. If you need to load data from a remote endpoint, this is a good place to instantiate the network request.

This method is a good place to set up any subscriptions. If you do that, don’t forget to unsubscribe in  `componentWillUnmount()`.

You  **may call  `setState()`  immediately**  in  `componentDidMount()`. It will trigger an extra rendering, but it will happen before the browser updates the screen. This guarantees that even though the  `render()`  will be called twice in this case, the user won’t see the intermediate state. Use this pattern with caution because it often causes performance issues. In most cases, you should be able to assign the initial state in the  `constructor()`  instead. It can, however, be necessary for cases like modals and tooltips when you need to measure a DOM node before rendering something that depends on its size or position. 
  
**Server-side Rendering Drawbacks**

React development with SSR is more complex than it is with CSR. Even though NextJS makes the SSR development experience much better, it still has it's own nuances compared to a CSR React app. For example, not all the React related libraries will be compatible with SSR, and even if they are, it might require some non-obvious setup to get them working. Also, if you encounter a bug with a library that is caused by the use of SSR, it may be difficult to get support for it. The majority of people using React libraries are not using server-side rendering so there will be less people experiencing the SSR-related bug and therefore less incentive in fixing it.  
  
Furthermore, although server-side rendering should always have a better perceived performance than a client-side rendered app, it may not actually be more performant. When rendering HTML on the server, the server needs to itself load the React app, and it also needs to run a synchronous method called renderToString() in order to generate the required HTML markup that will be sent to the client. Because of this, the first server response to the browser will likely be slower for SSR than for a CSR app. Don't forget that after sending the first HTML response to the browser, the browser will still need to load React in the browser, just like with client-side rendering.  
  
Finally, it must be accepted that new features made available in React libraries may not be immediately compatible with server-side rendering. A good example at the time of writing is that the React.lazy and Suspense features released in one of the latest versions of React is not yet supported with server-side rendering (there are however alternative ways to lazy load components with SSR).  
  
A wep app with a lot of dynamic content and most pages accessible only by authenticated users  
Build the app with client-side rendering since most of your pages don't need to be indexed by search engines. This will allow for faster development since the nuances of server-side rendering don't need to be considered. Also, this allows for the usage of client-side code splitting with React.lazy and Suspense to keep JavaScript bundle sizes low.  
  
[https://www.robertcooper.me/react-server-side-rendering](https://www.robertcooper.me/react-server-side-rendering)  
  
React Performance Optimization  
------------------------------  
  
Internally, React uses several clever techniques to minimize the number of costly DOM operations required to update the UI. For many applications, using React will lead to a fast user interface without doing much work to specifically optimize for performance. Nevertheless, there are several ways you can speed up your React application.  
  
1. Use the Production Build  
If you’re benchmarking or experiencing performance problems in your React apps, make sure you’re testing with the minified production build.  
  
By default, React includes many helpful warnings. These warnings are very useful in development. However, they make React larger and slower so you should make sure to use the production version when you deploy the app.  
  
2. use production-ready versions of React and React DOM as single files  
3. For the most efficient Brunch production build, install the terser-brunch plugin, Then, to create a production build, add the -p flag to the build command:  
4. To create a production build, make sure that you add these transforms (the order matters)  
  
- The envify transform ensures the right build environment is set. Make it global (-g).  
- The uglifyify transform removes development imports. Make it global too (-g).  
- Finally, the resulting bundle is piped to terser for mangling (read why).  
  
5. The Profiler measures how often a React application renders and what the “cost” of rendering is. Its purpose is to help identify parts of an application that are slow and may benefit from optimizations such as memoization.  
  
[https://calibreapp.com/blog/react-performance-profiling-optimization/](https://calibreapp.com/blog/react-performance-profiling-optimization/)  

Ref: [https://medium.com/@paularmstrong/twitter-lite-and-high-performance-react-progressive-web-apps-at-scale-d28a00e780a3](https://medium.com/@paularmstrong/twitter-lite-and-high-performance-react-progressive-web-apps-at-scale-d28a00e780a3)  

### Virtualize Long Lists

If your application renders long lists of data (hundreds or thousands of rows), we recommended using a technique known as “windowing”. This technique only renders a small subset of your rows at any given time, and can dramatically reduce the time it takes to re-render the components as well as the number of DOM nodes created.

[react-window](https://react-window.now.sh/)  and  [react-virtualized](https://bvaughn.github.io/react-virtualized/)  are popular windowing libraries. They provide several reusable components for displaying lists, grids, and tabular data.

### Profiler

The `Profiler` measures how often a React application renders and what the “cost” of rendering is. Its purpose is to help identify parts of an application that are slow and may benefit from [optimizations such as memoization](https://reactjs.org/docs/hooks-faq.html#how-to-memoize-calculations).
  
### React Issues  

1) Server-side vulnerability fix -> [https://reactjs.org/blog/2018/08/01/react-v-16-4-2.html](https://reactjs.org/blog/2018/08/01/react-v-16-4-2.html)  
2) isMounted is an Antipattern -> [https://reactjs.org/blog/2015/12/16/ismounted-antipattern.html](https://reactjs.org/blog/2015/12/16/ismounted-antipattern.html)  
3) Flux: An Application Architecture for React -> [https://reactjs.org/blog/2014/05/06/flux.html](https://reactjs.org/blog/2014/05/06/flux.html)  
  
[https://www.youtube.com/watch?v=82tZAPMHfT4&t=336s](https://www.youtube.com/watch?v=82tZAPMHfT4&t=336s)  
[https://www.youtube.com/user/Firebase/search?query=react](https://www.youtube.com/user/Firebase/search?query=react)  

## Server side rendering for React

For ReactJS, use `isomorphic-fetch`: This adds `fetch` as a global so that its API is consistent between client and server. Use Babel to use new ES2015 features.

[https://www.youtube.com/watch?v=82tZAPMHfT4&list=PLl-K7zZEsYLkbvTj8AUUCfBO7DoEHJ-ME&index=6&t=0s](https://www.youtube.com/watch?v=82tZAPMHfT4&list=PLl-K7zZEsYLkbvTj8AUUCfBO7DoEHJ-ME&index=6&t=0s)

[https://www.youtube.com/watch?v=GH3kJwQ7mxM&list=PLl-K7zZEsYLkbvTj8AUUCfBO7DoEHJ-ME&index=6](https://www.youtube.com/watch?v=GH3kJwQ7mxM&list=PLl-K7zZEsYLkbvTj8AUUCfBO7DoEHJ-ME&index=6)

### React Design Principles

[https://reactjs.org/docs/design-principles.html](https://reactjs.org/docs/design-principles.html)

[https://engineering.musefind.com/our-best-practices-for-writing-react-components-dec3eb5c3fc8](https://engineering.musefind.com/our-best-practices-for-writing-react-components-dec3eb5c3fc8)

[https://github.com/lyft/universal-async-component](https://github.com/lyft/universal-async-component)

### Render Caching instead of SSR

[https://css-tricks.com/render-caching-for-react/](https://css-tricks.com/render-caching-for-react/)

## Container Component Pattern (MUST READ)

[https://medium.com/@learnreact/container-components-c0e67432e005](https://medium.com/@learnreact/container-components-c0e67432e005)
[https://www.youtube.com/watch?v=NazjKgJp7sQ](https://www.youtube.com/watch?v=NazjKgJp7sQ)

## React Router

[https://tylermcginnis.com/react-router-animated-transitions/](https://tylermcginnis.com/react-router-animated-transitions/)
[https://buttercms.com/blog/best-practices-for-building-a-large-scale-react-application](https://buttercms.com/blog/best-practices-for-building-a-large-scale-react-application)

[https://github.com/airbnb/javascript/tree/master/react](https://github.com/airbnb/javascript/tree/master/react)

Common problems with React Router
[https://medium.com/@shoaibbhimani1392/react-router-frequently-faced-problems-f7d30d02087e](https://medium.com/@shoaibbhimani1392/react-router-frequently-faced-problems-f7d30d02087e)

## Redux

The whole state of your app is stored in an object tree inside a single _store_.  
The only way to change the state tree is to emit an _action_, an object describing what happened.  
To specify how the actions transform the state tree, you write pure _reducers_.

```js
import { createStore } from 'redux'

/**
 * This is a reducer, a pure function with (state, action) => state signature.
 * It describes how an action transforms the state into the next state.
 *
 * The shape of the state is up to you: it can be a primitive, an array, an object,
 * or even an Immutable.js data structure. The only important part is that you should
 * not mutate the state object, but return a new object if the state changes.
 *
 * In this example, we use a `switch` statement and strings, but you can use a helper that
 * follows a different convention (such as function maps) if it makes sense for your
 * project.
 */
function counter(state = 0, action) {
  switch (action.type) {
    case 'INCREMENT':
      return state + 1
    case 'DECREMENT':
      return state - 1
    default:
      return state
  }
}

// Create a Redux store holding the state of your app.
// Its API is { subscribe, dispatch, getState }.
let store = createStore(counter)

// You can use subscribe() to update the UI in response to state changes.
// Normally you'd use a view binding library (e.g. React Redux) rather than subscribe() directly.
// However it can also be handy to persist the current state in the localStorage.

store.subscribe(() => console.log(store.getState()))

// The only way to mutate the internal state is to dispatch an action.
// The actions can be serialized, logged or stored and later replayed.
store.dispatch({ type: 'INCREMENT' })
// 1
store.dispatch({ type: 'INCREMENT' })
// 2
store.dispatch({ type: 'DECREMENT' })
// 1
```

Instead of mutating the state directly, you specify the mutations you want to happen with plain objects called  _actions_. Then you write a special function called a  _reducer_  to decide how every action transforms the entire application's state.

In a typical Redux app, there is just a single store with a single root reducing function. As your app grows, you split the root reducer into smaller reducers independently operating on the different parts of the state tree. This is exactly like how there is just one root component in a React app, but it is composed out of many small components.

This architecture might seem like an overkill for a counter app, but the beauty of this pattern is how well it scales to large and complex apps. It also enables very powerful developer tools, because it is possible to trace every mutation to the action that caused it. You can record user sessions and reproduce them just by replaying every action.

### Motivation

[https://redux.js.org/introduction/motivation](https://redux.js.org/introduction/motivation)

### Core concept

[https://redux.js.org/introduction/core-concepts](https://redux.js.org/introduction/core-concepts)

**Check all the basic and advance tutorial on the above link**

## Optimize Redux

[https://redux.js.org/advanced/middleware](https://redux.js.org/advanced/middleware)
[https://techblog.appnexus.com/five-tips-for-working-with-redux-in-large-applications-89452af4fdcb](https://techblog.appnexus.com/five-tips-for-working-with-redux-in-large-applications-89452af4fdcb)
[https://medium.com/js-imaginea/best-practices-with-react-and-redux-application-1e94a6f214a0](https://medium.com/js-imaginea/best-practices-with-react-and-redux-application-1e94a6f214a0)

### Avoid Storing State Too Often

While  [controlled components](https://facebook.github.io/react/docs/forms.html#controlled-components)  seem to be the recommended approach, making inputs controlled means that they have to update and re-render for every keypress.

While this is not very taxing on a 3GHz desktop computer, a small mobile device with very limited CPU will notice significant lag while typing–especially when deleting many characters from the input.

In order to persist the value of composing Tweets, as well as calculating the number of characters remaining, we were using a controlled component and  _also_ passing the current value of the input to our Redux state at each keypress.

On a typical Android 5 device, every keypress leading to a change could cause nearly 200ms of overhead. Compound this by a fast typist, and we ended up in a really bad state, with users often reporting that their character insertion point was moving all over the place, resulting in jumbled sentences.

By removing the draft Tweet state from updating the main Redux state on every keypress and keeping things local in the React component’s state, we were able to reduce the overhead by over 50%.

### Batch Actions into a Single Dispatch

In Twitter Lite, we’re using  [redux](http://redux.js.org/)  with  [react-redux](http://redux.js.org/docs/basics/UsageWithReact.html)  to subscribe our components to data state changes. We’ve optimized our data into separate areas of a larger store with  [Normalizr](https://github.com/paularmstrong/normalizr)  and  [combineReducers](http://redux.js.org/docs/api/combineReducers.html). This all works wonderfully to prevent duplication of data and keep our stores small. However, each time we get new data, we have to dispatch multiple actions in order to add it to the appropriate stores.

With the way that react-redux works, this means that every action dispatched will cause our connected components (called Containers) to recalculate changes and possibly re-render.

While we use a custom middleware, there are  [other](https://www.npmjs.com/package/redux-batched-actions)  [batch](https://www.npmjs.com/package/redux-batch-middleware)  [middleware](https://www.npmjs.com/package/redux-batch-enhancer)  [available](https://www.npmjs.com/package/redux-batch-actions). Choose the one that’s right for you, or write your own.

The best way to illustrate the benefits of batching actions is by using the  [Chrome React Perf Extension](https://github.com/crysislinux/chrome-react-perf). After the initial load, we pre-cache and calculate unread DMs in the background. When that happens we add a lot of various entities (conversations, users, message entries, etc).  _Without batching_ , you can see that we end up with double the number of times we render each component (~16) versus  _with batching_  (~8).

[https://medium.com/@paularmstrong/twitter-lite-and-high-performance-react-progressive-web-apps-at-scale-d28a00e780a3](https://medium.com/@paularmstrong/twitter-lite-and-high-performance-react-progressive-web-apps-at-scale-d28a00e780a3)

## Redux best practices

Ref: [https://medium.com/@alexmngn/how-to-use-redux-on-highly-scalable-javascript-applications-4e4b8cb5ef38](https://medium.com/@alexmngn/how-to-use-redux-on-highly-scalable-javascript-applications-4e4b8cb5ef38)

[https://www.freecodecamp.org/news/the-best-way-to-architect-your-redux-app-ad9bd16c8e2d/](https://www.freecodecamp.org/news/the-best-way-to-architect-your-redux-app-ad9bd16c8e2d/)

1. Redux is a very great tool and you might want to use it for many reasons. You might need to share some state from a specific component without the need to pass it through props, get data from a server API and share it across your app, keep an history and time-travel over the actions or even store the app state in the local storage of the user’s device to restore it later.

2. You would want to define your Redux actions creators and reducers within the feature that consumes it to keep track of what is used and where it’s used. If you decide you want to remove an entire feature of your app, you can simply unplug it and delete that entire folder which contains the container and presentational components, reducers, actions, resources etc, and you know it will be fine because the state is not coupled to any other feature. Quick, clean and easy.

3. As your application grows, your features become more complex. To avoid having a single feature that manages too many things, you need to separate them into several smaller features. A feature must have everything it needs to work on its own. It must also have a limited scope and ideally no awareness of the entire app, but that sometimes is difficult as  [container components](https://medium.com/@learnreact/container-components-c0e67432e005)  will need to be aware of the shape of the Redux state. Of course, you can nest features into features as long as they stay as much standalone as possible and the nested ones get only used by their direct parent.

Below,  `Books`  is a scene, one feature of an app. It has a sub-feature  `ListItems`  which is a simple presentational component. The scene also has its own  `actions`  and  `reducers`  defined in the feature’s folder where the action creators are used by the container component of the feature.

```js
/scenes  
  /Books  
    /components  
      /ListItems /actions.js  
    /index.js  
    /reducer.js
```

The same way you compose your features into smaller ones, you should also apply this rule to your reducers and divide them, so they are easy to read and maintain. With such a composition, it is preferable to keep the structure of your entire Redux state tree following the folder structure of your features.

4. Most of the time, container components consume data that are not tightly bound to a specific component and you might wonder where the action creators and reducers are supposed to be defined. A way to do it is to create data modules that take care of individual data objects.

Data module core’s responsibility is to manage a chunk of data of your Redux state. As an example, you can have a module that manages a  `users`  object and another one for a  `books`  object. You can nest data modules into each other as long as they stay standalone, the same way how features are assembled.

The anatomy of a data module looks like this:

**Action creators**

They take care of the business logical part. They receive a payload, transforming it as needed and return an action that gets dispatched to update the Redux store. In most projects, you might also want use  [redux-thunk](https://github.com/gaearon/redux-thunk)  in order to have asynchronous actions such as sending requests to the server API.

**Reducers and Selectors**

Reducers handle actions and update the Redux state. There shouldn’t be any logic in your reducers as they need to stay pure. If you tend to have logical code, you should create another action instead.

Selectors are important if you’d like to filter your state before using them in your container components. You can define them in the  `reducer.js`  file as they are bound to the same chunk of state.

**API**
I recommend you to create a file called  `api.js`  where you can define your network requests.

```js
export  const  create  =  payload  =>  fetchApi(endPoints.create, payload, 'post');
```

I’ve seen many projects where the only way to send an API request was via a Redux middleware, suggesting that every call will have an impact on the Redux store. But this is not true, you could have API calls that don’t impact the store, such as a simple  `POST`  request to send an email, it depends how you’ve organized your app. But for this reason, I prefer to define them in a separate file.

Once the data module is defined, you just have to import and combine its reducer with the data reducer which is combined itself along with your feature’s main `reducer`.
<!--stackedit_data:
eyJoaXN0b3J5IjpbNTc1OTI1MDY0LDI4MjQ2MjcyMSwtMTQxNT
I2MDk0Myw2OTI2NDA4NTEsNTMyODI3NjkwLDU3MTgxNTA4NCwt
OTY1NjE4MDE1LC0yMDkzNjE5MTE0LC02MzQ1MTU1NjksMTM4NT
c5NTA2NSwtMTA5MTA2MDAwMiwtMTQ5MjMyMTc3NiwtMjUzNDI4
NTg5LDEyNzg1NDc3NiwtNjI0MjA5NjUwLC04MzcwNTU3MSwxNj
EyMDE1NTQyLDEyMjI0NjU1MTgsOTkxMTQzMTYyLDEyMjY1MTU0
NzldfQ==
-->