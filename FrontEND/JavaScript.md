# JavaScript Basics
### JavaScript bind()
```bind()``` function binds an objects to a function and referencing it using ```this```

Below sample code shows that it has two objects and a function which has no argument. Now, to refer the object, ```this``` is used here: ```this.x```

```javascript
		let c1 = { x: 5, y: 10 };
		let c2 = { x: 15, y: 20 };

		function printCoords() {
			console.log(this.x + ", " + this.y);
		}

		(printCoords.bind(c2))();
```
The `bind()` method creates a new function that, when called, has its `this` keyword set to the provided value, with a given sequence of arguments preceding any provided when the new function is called.

Another use case is when we have a method defined within an object. Referencing that function may not able to solve any `this` that defined within the function

```js
let obj = {
    x: 10,
    get: function() {
        return this.x;
    }
}

let unbound = obj.get;
console.log(obj.get()) // print 10
console.log(unbound()) // undefined as the function invokes in a global scope

//use bind to overcome the undefined issue
const myObj = {x: 20}
console.log((unbound.bind(myObj)) ()) // print 20
```

**Ref** 
1. [https://www.youtube.com/watch?v=g2WcckBB_q0](https://www.youtube.com/watch?v=g2WcckBB_q0)
2. [https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_objects/Function/bind](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_objects/Function/bind)

### ```this``` keyword
1.  The value of  `this`  is usually determined by a functions  **execution context**. Execution context simply means  _how_  a function is called.
2.  It’s important to know that  `this`  may be different (refer to something different) each time the function is called

Ref: [https://codeburst.io/javascript-the-keyword-this-for-beginners-fb5238d99f85](https://codeburst.io/javascript-the-keyword-this-for-beginners-fb5238d99f85)

## Next Gen JavaScript
### "let" and "const"
**_`const`_  means that the identifier can’t be reassigned**. However, for reference type, we can mutate the variable within the same block.

```js
const a = 'hi';
a = 'hello'; // this gives error
const b = {x: 123}
b[y] = 234
console.log(b) // {x: 123, y: 234}
```

```var``` and ```let``` are both used for function declaration in javascript but the difference between them is that var is function scoped and let is block scoped.  It can be said that a variable declared with var is defined throughout the program as compared to let. 

- Function scope is within the ```function```
- Block scope is within curly brackets


Ref: [https://medium.com/@josephcardillo/the-difference-between-function-and-block-scope-in-javascript-4296b2322abe](https://medium.com/@josephcardillo/the-difference-between-function-and-block-scope-in-javascript-4296b2322abe)

### Arrow function

### 1: Shorter Syntax
```javascript
function f1(num) {
	return num * 2;
}
```
Arrow function is comparatively a shorted syntax that the above function defination.

```javascript
var f1 = (num) => num * 2
```
For more than one argument or more complex function can be written as below: [This is a example from Express to render the EJS file with callbacks.]
```javascript
response.render('home', (req, res) => {
	console.log(req.body);
	console.log(res.statusCode);
})
```
There are two different approach for anonymous and named function in Arrow function.

for example for anonymous function
```js
function(e){   
	console.log(e)  
	return 1  
}             
(e) => { // same as above  
	console.log(e)  
	return 1  
}
```

For named function, exclude the arrow sign
```js
const fun1 = function(name){  
	console.log(`Hello ${name}`)  
}  
const fun2 = (name) {  
	console.log(`Hello ${name}`)  
}
```

### 2: No binding of ```this```

Unlike a regular function, an arrow function does not bind  `this`. Instead,`this`  is bound lexically (i.e. `this` keeps its meaning from its original context).

Ref: [https://codeburst.io/javascript-arrow-functions-for-beginners-926947fc0cdc](https://codeburst.io/javascript-arrow-functions-for-beginners-926947fc0cdc)

## setInterval()

The  **`setInterval()`**  method, offered on the  [`Window`](https://developer.mozilla.org/en-US/docs/Web/API/Window "The Window interface represents a window containing a DOM document; the document property points to the DOM document loaded in that window.")  and  [`Worker`](https://developer.mozilla.org/en-US/docs/Web/API/Worker "The Worker interface of the Web Workers API represents a background task that can be easily created and can send messages back to its creator. Creating a worker is as simple as calling the Worker() constructor and specifying a script to be run in the worker thread.")  interfaces, repeatedly calls a function or executes a code snippet, with a fixed time delay between each call. It returns an interval ID which uniquely identifies the interval, so you can remove it later by calling [`clearInterval()`](https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/clearInterval "The clearInterval() method of the WindowOrWorkerGlobalScope mixin cancels a timed, repeating action which was previously established by a call to setInterval()."). This method is defined by the [`WindowOrWorkerGlobalScope`](https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope "The WindowOrWorkerGlobalScope mixin describes several features common to the Window and WorkerGlobalScope interfaces.") mixin.

```javascript
var id = setInterval(function() {
	console.log("hello js");
}, 2000);

// above code snippet returns one number 
// which is passed to clearInterval() to stop the above code run
```
## Export and Import

We can label any declaration as exported by placing `export` before it, be it a variable, function or a class.
```javascript
// 📁 say.js
function sayHi(user) {
  alert(`Hello, ${user}!`);
}

function sayBye(user) {
  alert(`Bye, ${user}!`);
}

export {sayHi, sayBye}; // a list of exported variables
```
Usually, we put a list of what to import into `import {...}`, like this:

```javascript
// 📁 main.js
import {sayHi, sayBye} from './say.js';

sayHi('John'); // Hello, John!
sayBye('John'); // Bye, John!
```

## JavaScript Array Functions
The JavaScript **`Array`** object is a global object that is used in the construction of arrays; which are high-level, list-like objects.

Create an array
```js
var fruits = ['Apple', 'Banana'];

console.log(fruits.length);
// 2
```

### forEach()
The `**forEach()**` method executes a provided function (callback) once for each array element.

```js
var array1 = ['a', 'b', 'c'];
array1.forEach(function(element) {
  console.log(element);
});

// expected output: "a"
// expected output: "b"
// expected output: "c"
```
## JavaScript OOPS

When an object instance is created from a class, the class's **constructor function** is run to create it. This process of creating an object instance from a class is called **instantiation** — the object instance is **instantiated** from the class.

### Constructors and object instances

JavaScript uses special functions called  **constructor functions**  to define and initialize objects and their features. They are useful because you'll often come across situations in which you don't know how many objects you will be creating; constructors provide the means to create as many objects as you need in an effective way, attaching data and functions to them as required.

```js
function Person(name) {
    this.name = name;
    this.greeting = () => {
        console.log('Hi ' + name);
    }
}
const salva = new Person('Salva');
```

The constructor function is JavaScript's version of a class. You'll notice that it has all the features you'd expect in a function, although it doesn't return anything or explicitly create an object — it basically just defines properties and methods. You'll see the `this` keyword being used here as well — it is basically saying that whenever one of these object instances is created, the object's `name` property will be equal to the name value passed to the constructor call, and the `greeting()` method will use the name value passed to the constructor call too.

There are other ways to create object instances:

1. use `Object()` constructor
```js
var person = new Object(); // store empty object

person.name = 'John'
person.greet = () => {
    console.log('Hi ' + this.name);
}

console.log(person);
```

2. use `create()` method

Constructors can help you give your code order—you can create constructors in one place, then create instances as needed, and it is clear where they came from.

However, some people prefer to create object instances without first creating constructors, especially if they are creating only a few instances of an object. JavaScript has a built-in method called  `[create()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/create)`  that allows you to do that. With it, you can create a new object based on any existing object.

```js
var person_2 = Object.create(person)
console.log(person_2);
```

You'll see that `person_2` has been created based on `person`—it has the same properties and method available to it.

### Object Prototype

[Ref: [https://medium.com/better-programming/prototypes-in-javascript-5bba2990e04b](https://medium.com/better-programming/prototypes-in-javascript-5bba2990e04b)]

### JavaScript Prototype Inheritance

[Ref: [https://hackernoon.com/inheritance-in-javascript-21d2b82ffa6f](https://hackernoon.com/inheritance-in-javascript-21d2b82ffa6f)]

### JavaScript Class and Inheritance

[https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Inheritance#ECMAScript_2015_Classes](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Inheritance#ECMAScript_2015_Classes)

Under the hood, your classes are being converted into prototypal Inheritance models — this is just syntactic sugar. But I'm sure you'll agree that it's easier to write.

**PRACTICE** [https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Object_building_practice](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Object_building_practice)

[https://medium.com/javascript-scene/master-the-javascript-interview-what-s-the-difference-between-class-prototypal-inheritance-e4cd0a7562e9](https://medium.com/javascript-scene/master-the-javascript-interview-what-s-the-difference-between-class-prototypal-inheritance-e4cd0a7562e9)

### JavaScript Closure

REF: 
- [https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures)
- [https://medium.com/javascript-scene/master-the-javascript-interview-what-is-a-closure-b2f0d2152b36](https://medium.com/javascript-scene/master-the-javascript-interview-what-is-a-closure-b2f0d2152b36)

Closures are **frequently used in JavaScript** for object data privacy, in event handlers and callback functions, and in [partial applications, currying](https://medium.com/javascript-scene/curry-or-partial-application-8150044c78b8), and other functional programming patterns.

## JavaScript ES6 Features

[https://hackernoon.com/es6-features-in-javascript-6a55bbe46dff](https://hackernoon.com/es6-features-in-javascript-6a55bbe46dff)

[https://www.taniarascia.com/es6-syntax-and-feature-overview/](https://www.taniarascia.com/es6-syntax-and-feature-overview/)

## JavaScript Promise

Ref:
1. [https://www.geeksforgeeks.org/javascript-promises/](https://www.geeksforgeeks.org/javascript-promises/)
2. [https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)

A promise is an object that may produce a single value some time in the future: either a resolved value, or a reason that it’s not resolved (e.g., a network error occurred). A promise may be in one of 3 possible states: fulfilled, rejected, or pending. Promise users can attach callbacks to handle the fulfilled value or the reason for rejection.

Promises are eager, meaning that a promise will start doing whatever task you give it as soon as the promise constructor is invoked. If you need lazy, check out  [observables](https://github.com/Reactive-Extensions/RxJS)  or  [tasks](https://github.com/rpominov/fun-task).

**Promises**  are used to handle asynchronous operations in JavaScript. They are easy to manage when dealing with multiple asynchronous operations where callbacks can create callback hell leading to unmanageable code.

Prior to promises events and callback functions were used but they had limited functionalities and created unmanageable code.  
Multiple callback functions would create callback hell that leads to unmanageable code.  
Events were not good at handling asynchronous operations.

Promises are the ideal choice for handling asynchronous operations in the simplest manner. They can handle multiple asynchronous operations easily and provide better error handling than callbacks and events.

-   **Benefits of Promises**
    1.  Improves Code Readability
    2.  Better handling of asynchronous operations
    3.  Better flow of control definition in asynchronous logic
    4.  Better Error Handling
-   **A Promise has four states:**
    1.  **fulfilled**: Action related to the promise succeeded
    2.  **rejected**: Action related to the promise failed
    3.  **pending**: Promise is still pending i.e not fulfilled or rejected yet
    4.  **settled**: Promise has fulfilled or rejected

### Promise Constructor

The **`Promise`** object represents the eventual completion (or failure) of an asynchronous operation, and its resulting value.

A `Promise` object is created using the `new` keyword and its constructor. This constructor takes as its argument a function, called the "executor function". This function should take two functions as parameters. The first of these functions (`resolve`) is called when the asynchronous task completes successfully and returns the results of the task as a value. The second (`reject`) is called when the task fails, and returns the reason for failure, which is typically an error object.

```js
const myFirstPromise = new Promise((resolve, reject) => {
  resolve("Success!");
})

myFirstPromise.then((result) => {
  console.log(result)
}).catch((error) => {
  console.log(error)
})
```

### Promise Consumer

Promises can be consumed by registering functions using  _.then_  and  _.catch_methods.

- **then()**   is invoked when a promise is either resolved or rejected.
- **catch()** is invoked when a promise is either rejected or some error has occured in execution.

1.  Promises are used for asynchronous handling of events.
2.  Promises are used to handle asynchronous http requests.

## JavaScript Async-Await

Ref: [https://javascript.info/async-await](https://javascript.info/async-await)

There’s a special syntax to work with promises in a more comfortable fashion, called “async/await”. It’s surprisingly easy to understand and use.

### Async functions

Let’s start with the  `async`  keyword. It can be placed before a function, like this:

```javascript
async function f() {
  return 1;
}
```

The word “async” before a function means one simple thing: a function always returns a promise. Even If a function actually returns a non-promise value, prepending the function definition with the “async” keyword directs JavaScript to automatically wrap that value in a resolved promise.

For instance, the code above returns a resolved promise with the result of  `1`, let’s test it:

  
[](https://javascript.info/async-await# "run")

```javascript
async function f() {
  return 1;
}

f().then(alert); // 1
```

…We could explicitly return a promise, that would be the same as:

```javascript
async function f() {
  return Promise.resolve(1);
}

f().then(alert); // 1
```

So,  `async`  ensures that the function returns a promise, and wraps non-promises in it. Simple enough, right? But not only that. There’s another keyword,  `await`, that works only inside  `async`  functions, and it’s pretty cool.

### Await

The syntax:

```javascript
// works only inside async functions
let value = await promise;
```

The keyword  `await`  makes JavaScript wait until that promise settles and returns its result.

Here’s an example with a promise that resolves in 1 second:

```
```javascript
async function f() {

  let promise = new Promise((resolve, reject) => {
    setTimeout(() => resolve("done!"), 1000)
  });

  let result = await promise; // wait till the promise resolves (*)

  alert(result); // "done!"
}

f();
```

The function execution “pauses” at the line  `(*)`  and resumes when the promise settles, with  `result`  becoming its result. So the code above shows “done!” in one second.

Let’s emphasize:  `await`  literally makes JavaScript wait until the promise settles, and then go on with the result. That doesn’t cost any CPU resources, because the engine can do other jobs meanwhile: execute other scripts, handle events etc.

It’s just a more elegant syntax of getting the promise result than  `promise.then`, easier to read and write.

Can’t use  `await`  in regular functions

If we try to use  `await`  in non-async function, there would be a syntax error:

```
```javascript
function f() {
  let promise = Promise.resolve(1);
  let result = await promise; // Syntax error
}
```

We will get this error if we do not put  `async`  before a function. As said,  `await`  only works inside an  `async function`.

### Error Handling

If a promise resolves normally, then `await promise` returns the result. But in case of a rejection, it throws the error. We can catch that error using `try..catch`, the same way as a regular `throw`:

```javascript
async function f() {

  try {
    let response = await fetch('http://no-such-url');
  } catch(err) {
    alert(err); // TypeError: failed to fetch
  }
}

f();
```

## JavaScript Fetch API

Ref:
1. [https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch)
2. [https://developers.google.com/web/updates/2015/03/introduction-to-fetch](https://developers.google.com/web/updates/2015/03/introduction-to-fetch)

The  [Fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API)  provides a JavaScript interface for accessing and manipulating parts of the HTTP pipeline, such as requests and responses. It also provides a global  [`fetch()`](https://developer.mozilla.org/en-US/docs/Web/API/GlobalFetch/fetch "The documentation about this has not yet been written; please consider contributing!")  method that provides an easy, logical way to fetch resources asynchronously across the network.

This kind of functionality was previously achieved using  [`XMLHttpRequest`](https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest "Use XMLHttpRequest (XHR) objects to interact with servers. You can retrieve data from a URL without having to do a full page refresh. This enables a Web page to update just part of a page without disrupting what the user is doing."). Fetch provides a better alternative that can be easily used by other technologies such as  [`Service Workers`](https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorker_API "The documentation about this has not yet been written; please consider contributing!"). Fetch also provides a single logical place to define other HTTP-related concepts such as CORS and extensions to HTTP.

Fetch provides a generic definition of [`Request`](https://developer.mozilla.org/en-US/docs/Web/API/Request "The Request interface of the Fetch API represents a resource request.") and [`Response`](https://developer.mozilla.org/en-US/docs/Web/API/Response "The Response interface of the Fetch API represents the response to a request.") objects (and other things involved with network requests). This will allow them to be used wherever they are needed in the future, whether it’s for service workers, Cache API and other similar things that handle or modify requests and responses, or any kind of use case that might require you to generate your own responses programmatically.

The `fetch()` method takes one mandatory argument, the path to the resource you want to fetch. It returns a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/API/Promise "REDIRECT Promise") that resolves to the [`Response`](https://developer.mozilla.org/en-US/docs/Web/API/Response "The Response interface of the Fetch API represents the response to a request.") to that request, whether it is successful or not. You can also optionally pass in an `init` options object as the second argument.

Once a [`Response`](https://developer.mozilla.org/en-US/docs/Web/API/Response "The Response interface of the Fetch API represents the response to a request.") is retrieved, there are a number of methods available to define what the body content is and how it should be handled, these methods include:

-   **clone()**  - As the method implies this method creates a clone of the response.
-   **redirect()**  - This method creates a new response but with a different URL.
-   **arrayBuffer()**  - In here we return a promise that resolves with an ArrayBuffer.
-   **formData()**  - Also returns a promise but one that resolves with FormData object.
-   **blob()**  - This is one resolves with a Blob.
-   **text()**  - In this case it resolves with a string.
-   **json()**  - Lastly we have the method to that resolves the promise with JSON.

```js
fetch('http://example.com/movies.json')
  .then(function(response) {
    return response.json();
  })
  .then(function(myJson) {
    console.log(JSON.stringify(myJson));
  });
```

### fetch() vs jQuery.ajax()

The  `fetch`  specification differs from  `jQuery.ajax()`  in two main ways:

-   The Promise returned from  `fetch()`  **won’t reject on HTTP error status**  even if the response is an HTTP 404 or 500. Instead, it will resolve normally (with  `ok`  status set to false), and it will only reject on network failure or if anything prevented the request from completing.
-   By default,  `fetch`  **won't send or receive any cookies**  from the server, resulting in unauthenticated requests if the site relies on maintaining a user session (to send cookies, the  _credentials_  [init option](https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/fetch#Parameters)  must be set)

## JSON toJSON() Override

[https://www.telerik.com/blogs/hijacking-tojson-for-fun-and-profit](https://www.telerik.com/blogs/hijacking-tojson-for-fun-and-profit)

## JavaScript Error Handling

Ref:

1. [https://levelup.gitconnected.com/the-definite-guide-to-handling-errors-gracefully-in-javascript-58424d9c60e6](https://levelup.gitconnected.com/the-definite-guide-to-handling-errors-gracefully-in-javascript-58424d9c60e6)

`throw new Error('something went wrong')` — will create an instance of an Error in JavaScript and **stop the execution** of your script, unless you do something with the Error.

### The Error Object

The Error object has two properties built in for us to use. The first one is the message, which is what you pass as argument to the Error constructor, e.g.  `new Error('This is the message')`  . You can access the message through the  `message`  property:

const myError = new Error(‘please improve your code’)console.log(myError.message) // please improve your code

The second, very important one is the Error stack trace. You can access it through the `stack` property. The error stack will give you a history (call stack) of what files were ‘responsible’ of causing that `ReferenceError`. The stack also includes the message at the top and is then followed by the actual stack starting with the most recent / isolated point of the error and going down to the most outward ‘responsible’ file:

```plain-text
Error: please improve your code  
 at Object.<anonymous> (/Users/gisderdube/Documents/_projects/hacking.nosync/error-handling/src/general.js:1:79)  
 at Module._compile (internal/modules/cjs/loader.js:689:30)  
 at Object.Module._extensions..js (internal/modules/cjs/loader.js:700:10)  
 at Module.load (internal/modules/cjs/loader.js:599:32)  
 at tryModuleLoad (internal/modules/cjs/loader.js:538:12)  
 at Function.Module._load (internal/modules/cjs/loader.js:530:3)  
 at Function.Module.runMain (internal/modules/cjs/loader.js:742:12)  
 at startup (internal/bootstrap/node.js:266:19)  
 at bootstrapNodeJSCore (internal/bootstrap/node.js:596:3)
 ```

### Throwing and Handling Errors

Now the Error instance alone does not cause anything. E.g.  `new Error('...')`  does not do anything. When the Error gets  `throw`  n, it gets a bit more interesting. Then, as said before, your script will stop executing, unless you somehow handle it in your process. Remember, it does not matter if you  `throw` an Error manually, it is thrown by a library, or even the runtime itself (node or the browser)

### `try .... catch`

This is the simplest, but often times forgotten way to handle errors — it does get used a lot more nowadays again, thanks to async / await, see below. This can be used to catch any kind of  **synchronous**  error. 
```js
const a = 10
try {
  console.log(b) //will throw an error
}
catch(error) {
  console.log(error)
}

console.log(a) //no effect of previous error
```

### … finally

Sometimes it is necessary to execute code in either case, whether there is an Error or not. You can use the third, optional block  `finally`  for that. Often, it is the same as just having a line after the try … catch statement, but sometimes it can be useful.

### asyncronous callbacks for error

When you have an asynchronous function, and an Error occurs inside of that function, your script will have continued with the execution already, so there will not be any Error immediately. When handling asynchronous functions with callbacks (not recommended by the way), you usually receive two parameters in your callback function, which look something like this:

```js
myFunc(val, (err, result) => {
  if(err) {
    return err
  }
  
  console.log(result)
})
```

If there is an Error, the `err` parameter will be equal to that Error. If not, the parameter will be `undefined` or `null`. It is important to either return something in the `if(err)` -block, or to wrap your other instruction in an `else` -block, otherwise you might get another Error, e.g. `result` could be undefined and you try to access `result.data` or similar.

### Promise in asyncronous error handling

A better way of dealing with asynchronity is using promises. Here, in addition to having more readable code, we also have improved error handling. We do no longer need to care so much about the exact Error catching, as long as we have a  `catch`  block. When chaining promises, a  `catch`  block catches all Errors since the execution of the promise or the last catch block. Note that promises without  `catch`-block will not terminate the script

```js
const doWorkPromise = new Promise((resolve, reject) => {
	setTimeout(() => {
	// resolve([1, 4, 7]);
	reject('My Error')
	}, 2000)
})

doWorkPromise.then((result) => {
	console.log('Sucess:', result)
}).catch((error) => {
	console.log('Error: ', error)
})
```

With the introduction of async / await in JavaScript, we are back to the original way of handling Errors, with try … catch … finally, which makes handling them a breeze:

```js
;(async  function() {
	try {
		await  someFuncThatThrowsAnError()
	} catch (err) {
		console.error(err)
	}
		console.log('Easy!') // will get executed
})()
```

## Notes
### Benefit of seperate JS file

The benefit of a separate JS file is that the browser will download it and store it in its  [cache](https://en.wikipedia.org/wiki/Web_cache).

Other pages that reference the same script will take it from the cache instead of downloading it, so the file is actually downloaded only once. That reduces traffic and makes pages faster. Anothe reason is "separation of concern". JS Developer can work on the JS file without knowing the style content or the web page content which reduce the developing time.

### use strict
The directive looks like a string: `"use strict"` or `'use strict'`. When it is located at the top of a script, the whole script works the “modern” way i.e follow ES5 and after.

### functional programming note

It’s interesting to note that there exist  [functional](https://en.wikipedia.org/wiki/Functional_programming)  programming languages, like  [Scala](http://www.scala-lang.org/)  or  [Erlang](http://www.erlang.org/)  that forbid changing variable values.

In such languages, once the value is stored “in the box”, it’s there forever. If we need to store something else, the language forces us to create a new box (declare a new variable). We can’t reuse the old one.

Though it may seem a little odd at first sight, these languages are quite capable of serious development. More than that, there are areas like parallel computations where this limitation confers certain benefits. Studying such a language (even if you’re not planning to use it soon) is recommended to broaden the mind.

### declare new variable
There are some lazy programmers who, instead of declaring new variables, tend to reuse existing ones.

As a result, their variables are like boxes into which people throw different things without changing their stickers. What’s inside the box now? Who knows? We need to come closer and check.

Such programmers save a little bit on variable declaration but lose ten times more on debugging.

An extra variable is good, not evil.

Modern JavaScript minifiers and browsers optimize code well enough, so it won’t create performance issues. Using different variables for different values can even help the engine optimize your code.

### JavaScript Essential Interview QA

[https://github.com/ganqqwerty/123-Essential-JavaScript-Interview-Questions](https://github.com/ganqqwerty/123-Essential-JavaScript-Interview-Questions)

### JS Scope Rule and Hoisting

[https://www.sitepoint.com/demystifying-javascript-variable-scope-hoisting/](https://www.sitepoint.com/demystifying-javascript-variable-scope-hoisting/)
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTIwNTgxOTYyMTEsMjEwMDY0NzUzOCwtOD
kyOTY1NDEsMTY1NTI3NDQ2OCwyMTAyNjk1Nzc4LDEzMzUzMjQz
MDIsMTkxMzk1NjgsMTg3NzA4ODcwNSwxNTk4MDg1NjczLC0xNj
g3NTcyMDUxLC0xNjgxMDQyMDA4LDgxMTIyNjY0NywxMzA0MDI5
OTc1LDg4MDI2NzEyNCwtNDY3NTg5NTA1LDE1MTAwOTUxNiwtMT
g5MDc1NjQ4OSwtMTAyOTEwMTc2NiwxMzYzODUzNjU0LDEwMjU1
NTc5NjldfQ==
-->