# CSS3

## Optimize CSS

[https://www.sitepoint.com/optimizing-css-performance/]

## Optimize CSS Delivery

[https://developers.google.com/speed/docs/insights/OptimizeCSSDelivery]

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTYxNzY4NzgwN119
-->