# Web Security

## JSON Web Token (JWT)

Ref:
1. [https://medium.com/vandium-software/5-easy-steps-to-understanding-json-web-tokens-jwt-1164c0adfcec](https://medium.com/vandium-software/5-easy-steps-to-understanding-json-web-tokens-jwt-1164c0adfcec)
2. [https://jwt.io/introduction/](https://jwt.io/introduction/)

JWT are an important piece in ensuring trust and security in your application. JWT allow claims, such as user data, to be represented in a secure manner.

To explain how JWT work, let’s begin with an abstract definition:

_"A JSON Web Token (JWT) is a [JSON object](http://www.w3schools.com/json/) that is defined in [RFC 7519](https://tools.ietf.org/html/rfc7519) as a safe way to represent a set of information between two parties. The token is composed of a header, a payload, and a signature."_

Simply put, a JWT is just a string with the following format:

`header.payload.signature`

_It should be noted that a double quoted string is actually considered a valid JSON object._

To show how and why JWT are actually used, we will use a simple 3 entity example (see the below diagram). The entities in this example are the user, the application server, and the authentication server. The authentication server will provide the JWT to the user. With the JWT, the user can then safely communicate with the application.

![](https://miro.medium.com/max/1400/1*SSXUQJ1dWjiUrDoKaaiGLA.png)

In this example, the user first signs into the authentication server using the authentication server’s login system (e.g. username and password, Facebook login, Google login, etc). The authentication server then creates the JWT and sends it to the user. When the user makes API calls to the application, the user passes the JWT along with the API call. In this setup, the application server would be configured to verify that the incoming JWT are created by the authentication server (the verification process will be explained in more detail later). So, when the user makes API calls with the attached JWT, the application can use the JWT to verify that the API call is coming from an authenticated user.

```js
const  myFunction  =  async () => {
	const  token  =  jwt.sign({ _id: 'abc123' }, 'thisismycourse', {expiresIn: '2 days'})
	console.log(token) // token with header.payload.signature

	const  payload  =  jwt.verify(token, 'thisismycourse')
	console.log(payload)
}
``


<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE0NDM4MDg5NDIsMTk4Njg4NjQ3MV19
-->