# Web Fundamentals (Resource from https://developers.google.com/web/fundamentals/)

## What are web components?

Web components are a set of web platform APIs that allow you to create new custom, reusable, encapsulated HTML tags to use in web pages and web apps. Custom components and widgets build on the Web Component standards, will work across modern browsers, and can be used with any JavaScript library or framework that works with HTML.

Web components are based on existing web standards. Features to support web components are currently being added to the HTML and DOM specs, letting web developers easily extend HTML with new elements with encapsulated styling and custom behavior.

Web Components is comprised of four parts:

- Templates
- Shadow DOM
- Custom Elements
- HTML Imports

[https://www.webcomponents.org/introduction]
[https://www.polymer-project.org/]

## Custom Element

The browser gives us an excellent tool for structuring web applications. It's called HTML. You may have heard of it! It's declarative, portable, well supported, and easy to work with. Great as HTML may be, its vocabulary and extensibility are limited. The HTML living standard has always lacked a way to automatically associate JS behavior with your markup... until now.

Custom elements are the answer to modernizing HTML, filling in the missing pieces, and bundling structure with behavior. If HTML doesn't provide the solution to a problem, we can create a custom element that does. Custom elements teach the browser new tricks while preserving the benefits of HTML.

```javascript
class AppDrawer extends HTMLElement {...}

window.customElement.define('app-drawer', AppDrawer);
```

Read more [Ref: https://developers.google.com/web/fundamentals/web-components/customelements]

## Shadow DOM

An important aspect of web components is encapsulation — being able to keep the markup structure, style, and behavior hidden and separate from other code on the page so that different parts do not clash, and the code can be kept nice and clean. The Shadow DOM API is a key part of this, providing a way to attach a hidden separated DOM to an element. This article covers the basics of using the Shadow DOM.

Shadow DOM fixes CSS and DOM. It introduces scoped styles to the web platform. Without tools or naming conventions, you can bundle CSS with markup, hide implementation details, and author self-contained components in vanilla JavaScript.

Shadow DOM is just normal DOM with two differences: 1) how it's created/used and 2) how it behaves in relation to the rest of the page. Normally, you create DOM nodes and append them as children of another element. With shadow DOM, you create a scoped DOM tree that's attached to the element, but separate from its actual children. This scoped subtree is called a shadow tree. The element it's attached to is its shadow host. Anything you add in the shadows becomes local to the hosting element, including `<style>`. This is how shadow DOM achieves CSS style scoping.

[https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_shadow_DOM]
[https://developers.google.com/web/fundamentals/web-components/shadowdom]

## Fetch API

The Fetch API provides an interface for fetching resources (including across the network). It will seem familiar to anyone who has used [`XMLHttpRequest`](https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest "Use XMLHttpRequest (XHR) objects to interact with servers. You can retrieve data from a URL without having to do a full page refresh. This enables a Web page to update just part of a page without disrupting what the user is doing."), but the new API provides a more powerful and flexible feature set.

Fetch provides a generic definition of  [`Request`](https://developer.mozilla.org/en-US/docs/Web/API/Request "The Request interface of the Fetch API represents a resource request.")  and  [`Response`](https://developer.mozilla.org/en-US/docs/Web/API/Response "The Response interface of the Fetch API represents the response to a request.")  objects (and other things involved with network requests). This will allow them to be used wherever they are needed in the future, whether it’s for service workers, Cache API and other similar things that handle or modify requests and responses, or any kind of use case that might require you to generate your own responses programmatically.

It also provides a definition for related concepts such as CORS and the HTTP origin header semantics, supplanting their separate definitions elsewhere.

The `fetch()` method takes one mandatory argument, the path to the resource you want to fetch. It returns a  [`Promise`](https://developer.mozilla.org/en-US/docs/Web/API/Promise "REDIRECT Promise")  that resolves to the  [`Response`](https://developer.mozilla.org/en-US/docs/Web/API/Response "The Response interface of the Fetch API represents the response to a request.")  to that request, whether it is successful or not. You can also optionally pass in an  `init`  options object as the second argument (see  [`Request`](https://developer.mozilla.org/en-US/docs/Web/API/Request "The Request interface of the Fetch API represents a resource request.")).

Once a  [`Response`](https://developer.mozilla.org/en-US/docs/Web/API/Response "The Response interface of the Fetch API represents the response to a request.")  is retrieved, there are a number of methods available to define what the body content is and how it should be handled (see  [`Body`](https://developer.mozilla.org/en-US/docs/Web/API/Body "The Body mixin of the Fetch API represents the body of the response/request, allowing you to declare what its content type is and how it should be handled.")).

**REF** [[https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API)]

## SOAP vs REST API

The words "web services" mean many things to people with different fields. For general users, it is about using online services, like surfing the internet, but for developers and webmasters, it has different meanings. Overall it is a broad term that tells us how the communication between two different set of devices or applications held over the World Wide Web (WWW).

This communication system can be categorized into two types, namely Simple Object Access Protocol or SOAP, and Representational State Transfer or REST.

### **What Is a REST API?**

REST is basically an architectural style of the web services that work as a channel of communication between different computers or systems on the internet. The term REST API is something else.

Those application programming interfaces that are backed by the architectural style of REST architectural system are called REST APIs. REST API compliant web services, database systems, and computer systems permit requesting systems to get robust access and redefine representations of web based resources by deploying a predefined set of stateless protocols and standard operations.

By these protocols and operations and redeploying the manageable and updatable components without causing the effect on the system, REST API systems deliver fast performance, reliability, and more progression.

### **What Is a SOAP API?**

SOAP is a standard communication protocol system that permits processes using different operating systems like Linux and Windows to communicate via  [HTTP](https://dzone.com/articles/the-http-series-part-1-overview-of-the-basic-conce)  and its  [XML](https://dzone.com/articles/writing-and-reading-xml-file). SOAP based APIs are designed to create, recover, update and delete records like accounts, passwords, leads, and custom objects.

These offers over twenty different kinds of calls that make it easy for the API developers to maintain their accounts, perform accurate searches and much more. These can then be used with all those languages that support web services.

SOAP APIs take the advantages of making web based protocols such as HTTP and its XML that are already operating the all operating systems that are why its developers can easily manipulate web services and get responses without caring about language and platforms at all.

#### **Differences:**

-   REST API has no has no official standard at all because it is an architectural style. SOAP API, on the other hand, has an official standard because it is a protocol.
-   REST APIs uses multiple standards like HTTP, JSON, URL, and XML while SOAP APIs is largely based on HTTP and XML.
-   As REST API deploys multiple standards, so it takes fewer resources and bandwidth as compared to SOAP that uses XML for the creation of Payload and results in the large sized file.
-   The ways both APIs exposes the business logics are also different. REST API takes advantage of URL exposure like @path("/WeatherService") while SOAP API use of services interfaces like @WebService.
-   SOAP API defines too many standards, and its implementer implements the things in a standard way only. In the case of miscommunication from service, the result will be the error. REST API, on the other hand, don't make emphasis on too many standards and results in corrupt API in the end.
-   REST API uses Web Application Description Language, and SOAP API used Web Services Description language for describing the functionalities being offered by web services.
-   REST APIs are more convenient with JavaScript and can be implemented easily as well. SOAP APIs are also convenient with JavaScript but don't support for greater implementation.

## Server side rendering

When browser makes a call to server, server responded by sending the HTML file. As soon as browser while parsing HTML file encounter the CSS file is missing, it will make another request to the server to download. Now, once the HTML + CSS parsing is done, the website is starts to render and it request JS and any other static content to download from server. But for SPA, JS and CSS are glued together. So, the rendering wouldn't be completed until browser download and parse both the JS + CSS (as a component) and it will have bad rendering. To overcome this use Server side rendering that is generate the HTML page on the server and then transmit to the browser. Now, HTML file generation is bit time consuming so that we use caching or CDN caching to pre-generate the file and serve instantly.

1. [https://www.youtube.com/watch?v=GQzn7XRdzxY](https://www.youtube.com/watch?v=GQzn7XRdzxY)
2. [https://www.freecodecamp.org/news/demystifying-reacts-server-side-render-de335d408fe4/](https://www.freecodecamp.org/news/demystifying-reacts-server-side-render-de335d408fe4/)

**Watch the series**: [https://www.youtube.com/playlist?list=PLl-K7zZEsYLkbvTj8AUUCfBO7DoEHJ-ME](https://www.youtube.com/playlist?list=PLl-K7zZEsYLkbvTj8AUUCfBO7DoEHJ-ME)

## Front end interviews QA

[https://www.freecodecamp.org/news/cracking-the-front-end-interview-9a34cd46237/](https://www.freecodecamp.org/news/cracking-the-front-end-interview-9a34cd46237/)

[https://medium.com/@fat/mediums-css-is-actually-pretty-fucking-good-b8e2a6c78b06#.7i1ey8j4g](https://medium.com/@fat/mediums-css-is-actually-pretty-fucking-good-b8e2a6c78b06#.7i1ey8j4g)

## Service Workers Benefit

While Service Workers aren’t available in all browsers yet, they’re an invaluable part of Twitter Lite. When available, we use ours for push notifications, to pre-cache application assets, and more. Unfortunately, being a fairly new technology, there’s still a lot to learn around performance.

### Pre-Cache Assets

Like most products, Twitter Lite is by no means done. We’re still actively developing it, adding features, fixing bugs, and making it faster. This means we frequently need to deploy new versions of our JavaScript assets.

Unfortunately, this can be a burden when users come back to the application and need to re-download a bunch of script files just to view a single Tweet.

In ServiceWorker-enabled browsers, we get the benefit of being able to have the worker automatically update, download, and cache any changed files in the background, on its own, before you come back.

So what does this mean for the user? Near instant subsequent application loads, even after we’ve deployed a new version!

### Delay ServiceWorker Registration

In many applications, it’s safe to register a ServiceWorker immediately on page load:

```js
<script>  
window.navigator.serviceWorker.register('/sw.js');  
</script>
```

While we try to send as much data to the browser as possible to render a complete-looking page, in Twitter Lite this isn’t always possible. We may not have sent enough data, or the page you’re landing on may not support data pre-filled from the server. Because of this and various other limitations, we need to make some API requests immediately after the initial page load.

Normally, this isn’t a problem. However, if the browser hasn’t installed the current version of our ServiceWorker yet, we need to tell it to install–and with that comes about 50 requests to pre-cache various JS, CSS, and image assets.

When we were using the simple approach of registering our ServiceWorker immediately, we could see the network contention happening within the browser, maxing out our parallel request limit (below left).

![](https://miro.medium.com/max/60/1*jLT8J20RRfKY_oxcsAiswQ.png?q=20)

![](https://miro.medium.com/max/5568/1*jLT8J20RRfKY_oxcsAiswQ.png)

![](https://miro.medium.com/max/60/1*poli81EjMdim8__g1HMioQ.png?q=20)

![](https://miro.medium.com/max/5568/1*poli81EjMdim8__g1HMioQ.png)

Notice how when registering your service worker immediately, it can block all other network requests (left). Deferring the service worker (right) allows your initial page load to make required network requests without getting blocked by the concurrent connection limit of the browser. Click or tap to zoom.

By delaying the ServiceWorker registration until we’ve finished loading extra API requests, CSS and image assets, we allow the page to finish rendering and be responsive, as illustrated in the after screenshot (above right).
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTEzNzc2Mzg1MTYsLTE4OTUzMTU5MTMsLT
E3MzI4NTEyODQsLTUzNzQ1MzczMSwxNjIwNzkyNTQzLC0yODA1
Njg1NTksNjM2MjE0ODMwLDE5MjY4NzI1MTEsMzI4NjkxNTYxLD
IyMjAyNTM1Nyw0OTI3OTM0NzIsLTIwOTc3MzQyOSwtMTc4ODQ4
MDk4NywtMjEyODkwNTE1NCwtMTkzMzU2MjUzOF19
-->