**Q #1) What is Oracle and what are its different editions?**

**Ans:**  Oracle is one of the popular database provided by Oracle Corporation, which works on relational management concepts and hence it is referred as Oracle RDBMS as well.

It is widely used for online transaction processing, data warehousing, and enterprise grid computing.

**Q #2) How will you identify Oracle Database Software Release?**

**Ans:**  Oracle follows a number of format for every release.

**For Example_,_  release 10.1.0.1.1 can be referred as below mentioned:**

10: Major DB Release Number  
1: DB Maintenance Release Number  
0: Application Server Release Number  
1: Component Specific Release Number  
1: Platform Specific Release Number

**Q #3) How will you differentiate between VARCHAR & VARCHAR2?**

**Ans:**  Both VARCHAR & VARCHAR2 are Oracle data types that are used to store character strings of variable length.

VARCHAR can store characters up to 2000 bytes while VARCHAR2 can store up to 4000 bytes.

VARCHAR will hold the space for characters defined during declaration even if all of them are not used whereas VARCHAR2 will release the unused space.

**Q #4) What is the difference between TRUNCATE & DELETE command?**

**Ans:**  Both the commands are used to remove data from a database.

**The finer differences between the two include:**

-   TRUNCATE is a DDL operation while DELETE is a DML operation.
-   TRUNCATE drops the structure of a database and hence cannot be rolled back while DELETE command can be rolled back.
-   The TRUNCATE command will free the object storage space while the DELETE command does not.

**Q #5) What is meant by RAW datatype?**

**Ans:** RAW datatype is used to store variable-length binary data or byte strings.

The difference between RAW & VARCHAR2 datatype is that PL/SQL does not recognize this data type and hence, cannot do any conversions when RAW data is transferred to different systems. This data type can only be queried or inserted in a table.

**Syntax:**  RAW (precision)

**Q #6) What is meant by Joins? List out the types of joins.**

**Ans:**  Joins are used to extract data from multiple tables using some common column or condition.

**There are various types of Joins as listed below:**

-   INNER JOIN
-   OUTER JOIN
-   CROSS JOINS or CARTESIAN PRODUCT
-   EQUI JOIN
-   ANTI JOIN
-   SEMI JOIN

**Q #7) What is the difference between SUBSTR & INSTR functions?**

**Ans:**  SUBSTR function returns the sub-part identified by numeric values from the provided string.

**Example:**  [Select SUBSTR (‘India is my country’, 1, 4) from dual] will return “Indi”.

INSTR will return the position number of the sub-string within the string.

**Example:**  [SELECT INSTR (‘India is my country’, ‘a’) from dual] will return 5.

**Q #8) How can we find out the duplicate values in an Oracle table?**

**Ans:**  **We can use the below example query to fetch the duplicate records.**

SELECT EMP_NAME, COUNT (EMP_NAME)  
FROM EMP  
GROUP BY EMP_NAME  
HAVING COUNT (EMP_NAME) > 1;

**Q #9) How does the ON-DELETE-CASCADE statement**  **work?**

**Ans:**  Using ON DELETE CASCADE will automatically delete a record in the child table when the same is deleted from the parent table. This statement can be used with Foreign Keys.

We can add ON DELETE CASCADE option on an existing table using the below set of commands.

**Syntax:**

ALTER TABLE CHILD_T1 ADD CONSTRAINT CHILD_PARENT_FK REFERENCES  
PARENT_T1 (COLUMN1) ON DELETE CASCADE;

**Q #10) What is a NVL function? How can it be used?**

**Ans:**  NVL is a function, which helps the user to substitute a value if null is encountered for an expression.

**It can be used as the below syntax.**

[NVL (Value_In, Replace_With)]

**Q #11) What is the difference between a Primary Key & a Unique Key?**

**Ans:** Primary key is used to identify each table row uniquely, while a Unique Key prevents duplicate values in a table column.

**Given below are few differences:**

-   The primary key can be only one on the table while unique keys can be multiple.
-   The primary key cannot hold null value at all while Unique key allows multiple null values.
-   The primary key is a clustered index while a unique key is a non-clustered index.

**Q #12) How TRANSLATE command is different from REPLACE?**

**Ans:**  TRANSLATE command translates characters one by one in the provided string with the substitution character. REPLACE will replace a character or a set of characters with a complete substitution string.

**Example:**

TRANSLATE (‘Missisippi’,’is’,’15) => M155151pp1  
REPLACE (‘Missisippi’,’is’,’15) => M15s15ippi

**Q #13) How can we find out the current date and time in Oracle?**

**Ans:**  We can find the current Date & Time using SYSDATE in Oracle.

**Syntax:**

SELECT SYSDATE into CURRENT_DATE from dual;

**Q #14) Why do we use COALESCE function in Oracle?**

**Ans:**  COALESCE function is used to return the first non-null expression from the list of arguments provided in the expression. Minimum two arguments should be there in an expression.

**Syntax:**

COALESCE (expr 1, expr 2, expr 3…expr n)

**Q #15) How will you write a query to get a 5th RANK student from a table STUDENT_REPORT?**

**Ans: The Query will be as follows:**

SELECT TOP 1 RANK  
FROM (SELECT TOP 5 RANK  
FROM STUDENT_REPORT  
ORDER BY RANK DESC) AS STUDENT  
ORDER BY RANK ASC;

**Q #16) When do we use GROUP BY clause in a SQL Query?**

**Ans:**  GROUP BY clause is used to identify and group the data by one or more columns in the query results. This clause is often used with aggregate functions like COUNT, MAX, MIN, SUM, AVG etc.

**Syntax:**

SELECT COLUMN_1, COLUMN_2  
FROM TABLENAME  
WHERE [condition]  
GROUP BY COLUMN_1, COLUMN_2

**Q #17) What is the quickest way to fetch the data from a table?**

**Ans:**  The quickest way to fetch the data would be to use ROWID in the SQL Query.

**Q #18) Where do we use DECODE and CASE Statements?**

**Ans:**  Both DECODE & CASE statements will function like IF-THEN-ELSE statement and they are the alternatives for each other. These functions are used in Oracle to transform the data values.

**Example:**

**DECODE Function**

Select ORDERNUM,  
DECODE (STATUS,'O', ‘ORDERED’,'P', ‘PACKED,’S’,’SHIPPED’,’A’,’ARRIVED’)  
FROM ORDERS;

**CASE**  **Function**

Select ORDERNUM  
, CASE (WHEN STATUS ='O' then ‘ORDERED’  
WHEN STATUS ='P' then PACKED  
WHEN STATUS ='S' then ’SHIPPED’  
ELSE ’ARRIVED’) END  
FROM ORDERS;

**Both the commands will display Order Numbers with respective Status as,**

If,

Status O= Ordered  
Status P= Packed  
Status S= Shipped  
Status A= Arrived

**Q #19) Why do we need integrity constraints in a database?**

**Ans:**  Integrity constraints are required to enforce business rules so as to maintain the integrity of the database and prevent the entry of invalid data into the tables. With the help of the below-mentioned constraints, relationships can be maintained between the tables.

Various integrity constraints available include Primary Key, Foreign Key, UNIQUE KEY, NOT NULL & CHECK.

**Q #20) What do you mean by MERGE in Oracle and how can we merge two tables?**

**Ans:**  MERGE statement is used to merge the data from two tables. It selects the data from the source table and inserts/updates it in the other table based on the condition provided in the MERGE query.

**Syntax:**

MERGE INTO TARGET_TABLE_1  
USING SOURCE_TABLE_1  
ON SEARCH_CONDITION  
WHEN MATCHED THEN  
INSERT (COL_1, COL_2…)  
VALUES (VAL_1, VAL_2…)  
WHERE <CONDITION>  
WHEN NOT MATCHED THEN  
UPDATE SET COL_1=VAL_1, COL_2=VAL_2…  
WHEN <CONDITION>

**Q #21) What is the use of Aggregate functions in Oracle?**

**Ans:**  Aggregate functions perform summary operations on a set of values to provide a single value. There are several aggregate functions that we use in our code to perform calculations.

**Few of them are listed below:**

-   AVG
-   MIN
-   MAX
-   COUNT
-   SUM
-   STDEV

**Q #22) What are the set operators UNION, UNION ALL, MINUS & INTERSECT meant to do?**

**Ans:**  Set operator facilitates the user to fetch the data from two or more than two tables at once if the columns and relative data types are same in the source tables.

-   UNION operator returns all the rows from both the tables except the duplicate rows.
-   UNION ALL returns all the rows from both the tables along with the duplicate rows.
-   MINUS returns rows from the first table, which does not exist in the second table.
-   INTERSECT returns only the common rows in both the tables.

**Q #23) Can we convert a date to char in Oracle and if so, what would be the syntax?**

**Ans:**  We can use the TO_CHAR function to do the above conversion.

**The syntax will be as follows:**

[SELECT to_char (to_date ('30-01-2018′, ‘DD-MM-YYYY'), ‘YYYY-MM-DD') FROM dual;]

**Q #24) What do you mean by a database transaction & what all TCL statements are available in Oracle?**

**Ans:**  Transaction occurs when a set of SQL statements are executed in one go. To control the execution of these statements, Oracle has introduced TCL i.e. Transaction Control Statements that use a set of statements.

**The set of statements include:**

-   **COMMIT:**  Used to make a transaction permanent.
-   **ROLLBACK:**  Used to roll back the state of DB to last the commit point.
-   **SAVEPOINT:**  Helps to specify a transaction point to which rollback can be done later.

**Q #25) What do you understand by a database object? Can you list a few of them?**

**Ans:**  An object used to store the data or references of the data in a database is known as a Database object.

The database consists of various types of DB objects such as tables, views, indexes, constraints, stored procedures, triggers etc.

**Q #26) What is a Nested table and how is it different from a normal table?**

**Ans:**  A nested table is a database collection object, which can be stored as a column in a table. While creating a normal table, an entire nested table can be referenced in a single column. Nested tables have only one column with no restriction of rows.

**Example:**

CREATE TABLE EMP (  
EMP_ID NUMBER,  
EMP_NAME TYPE_NAME)

Here we are creating a normal table as EMP and referring a nested table TYPE_NAME as a column.

**Q #27) Can we save images in a database and if yes, how?**

**Ans:**  BLOB stands for Binary Large Object, which is a datatype that is generally used to hold images, audio & video files or some binary executables.

This datatype has the capacity of holding data up to 4 GB.

**Q #28) What do you understand by database schema and what does it hold?**

**Ans:**  Schema is a collection of database objects owned by a database user who can create or manipulate new objects within this schema.

The schema can contain any DB objects like table, view, indexes, clusters, stored procs, functions etc.

**Q #29) What is a Data Dictionary and how can it be created?**

**Ans:**  Whenever a new database is created, a database specific data dictionary gets created by the system. This dictionary maintains all the metadata related to the database and owned by the SYS user.

It has a set of read-only tables and views and it is physically stored in the SYSTEM tablespace.

**Q #30) What is a View and how is it different from a table?**

**Ans:**  A view is a user-defined database object that is used to store the results of a SQL query, which can be referenced later. Views do not store this data physically but as a virtual table, hence it can be referred as a logical table.

A table can hold data but not SQL Query results whereas View can save the query results, which can be used in another SQL Query as a whole.

The table can be updated or deleted while Views cannot be done so.

**Q #31) What is meant by a deadlock situation?**

**Ans:**  Deadlock is a situation when two or more users are simultaneously waiting for the data, which is locked by each other and hence, results in all blocked user sessions.

**Q #32) What is meant by an index?**

**Ans:**  An index is a schema object, which is created to search the data efficiently within the table. Indexes are usually created on certain columns of the table, which are accessed the most.

Indexes can be clustered or non-clustered.

**Q#33) What is a Role in Oracle database?**

**Ans:**  Giving access to individual objects to the individual users is a tough administrative task. In order to make this job easy, a group of common privileges is created in a database, which is known as Role. The role, once created can be assigned to or revoked from the users by using Grant & Revoke command.

**Syntax:**

CREATE ROLE READ_TABLE_ROLE;  
GRANT SELECT ON EMP TO READ_TABLE_ROLE;  
GRANT READ_TABLE_ROLE TO USER1;  
REVOKE READ_TABLE_ROLE FROM USER1;

**Q #34) What are the attributes that are found in a cursor?**

**Ans: A cursor has various attributes as mentioned below:**

**(i) %FOUND**:

-   Returns INVALID_CURSOR if the cursor has been declared but closed.
-   Returns NULL if fetch has not happened but the cursor is open only.
-   Returns TRUE if the rows are fetched successfully and FALSE if no rows are returned.

**(ii) NOT FOUND**:

-   Returns INVALID_CURSOR if the cursor has been declared but closed.
-   Returns NULL if fetch has not happened but the cursor is open only.
-   Returns FALSE if rows are fetched successfully and TRUE if no rows are returned

**(iii) %ISOPEN**: Returns TRUE if the cursor is OPEN else FALSE

**(iv) %ROWCOUNT**: Returns the count of fetched rows.

**Q #35) Why do we use %ROWTYPE & %TYPE in PLSQL?**

**Ans:**  %ROWTYPE & %TYPE are the attributes in PL/SQL which can inherit the datatypes of a table defined in a database. The purpose of using these attributes is to provide data independence and integrity.

If any of the datatypes or precision gets changed in the database, PL/SQL code gets updated automatically with the data type changes.

%TYPE is used for declaring a variable which needs to have the same data type as of a table column.

While %ROWTYPE will be used to define a complete row of record having a structure similar to the structure of a table.

**Q #36) Why do we create Stored Procedures & Functions in PL/SQL and how are they different?**

**Ans:**  A stored procedure is a set of SQL statements that are written to perform a specific task. These statements can be saved as a group in the database with an assigned name and can be shared with different programs if permissions are there to access the same.

Functions are again subprograms that are written to perform specific tasks but there are differences between both of them.

  
Stored Procedures

Functions  

SPs may or may not return a value and can return multiple values as well.

Function will always return only single value.

SPs can include DML statements like insert, update & delete.

We cannot use DML statements in a function.

SPs can call functions.

Functions cannot call stored procedures.

SPs support exception handling using Try/Catch block.

Functions does not support Try/Catch block.

**Q #37) What are the parameters that we can pass through a stored procedure?**

**Ans:**  We can pass IN, OUT & INOUT parameters through a stored procedure and they should be defined while declaring the procedure itself.

**Q #38) What is a trigger and what are its types?**

**Ans:**  A trigger is a stored program which is written in such a way that it gets executed automatically when some event occurs. This event can be any DML or a DDL operation.

**PL/SQL supports two types of triggers:**

-   Row Level
-   Statement Level

**Q #39) How will you distinguish a global variable with a local variable in PL/SQL?**

**Ans:**  Global variable is the one, which is defined at the beginning of the program and survives until the end.

It can be accessed by any methods or procedures within the program, while the access to the local variable is limited to the procedure or method where it is declared.

**Q #40) What are the packages in PL SQL?**

**Ans:** A Package is a group of related database objects like stored procs, functions, types, triggers, cursors etc. that are stored in Oracle database. It is a kind of library of related objects which can be accessed by multiple applications if permitted.

PL/SQL Package structure consists of 2 parts: package specification & package body.

### Conclusion

Hope the above set of questions would have helped you to get a glimpse of what Oracle is all about.

Even if you have a thorough knowledge of all the basic concepts, the way in which you present it in the interview matters a lot. Hence stay cool and face the interview confidently without any hesitation.



## Another Set



## Oracle PL/SQL Interview Questions And Answers

##### Q1. What is the difference between PL SQL and SQL?

**Ans.**

### PL Sql vs Sql

Comparison

**SQL**

**PL/SQL**

Execution

Single command at a time

Block of code

Application

Source of data to be displayed

Application created by data aquired by SQL

Structures include

DDL and DML based queries and commands

Includes procedures, functions, etc

Recommended while

Performing CRUD operations on data

Creating applications to display data obtained using sql

Compatibility with each other

SQL can be embedded into PL/SQL

PL/SQL cant be embedded in SQL

##### Q2. What is SQL and also describe types of SQL statements?

**Ans:** SQL stands for  [Structured Query Language](https://en.wikipedia.org/wiki/SQL "What is SQL?"). SQL is a language used to communicate with the server to access, manipulate and control data.

There are 5 different types of SQL statements.

1.  Data Retrieval: SELECT
    
2.  Data Manipulation Language (DML): INSERT, UPDATE, DELETE, MERGE
    
3.  Data Definition Language (DDL): CREATE, ALTER, DROP, RENAME, TRUNCATE.
    
4.  Transaction Control Statements: COMMIT, ROLLBACK, SAVEPOINT
    
5.  Data Control Language (DCL): GRANT, REVOKE
    

##### Q3. What is an alias in SQL statements?

**Ans:**  Alias is a user-defined alternative name given to the column or table. By default column alias headings appear in upper case. Enclose the alias in a double quotation marks (“ “) to make it case sensitive. “**AS**” Keyword before the alias name makes the SELECT clause easier to read.

For ex: Select emp_name AS name from employee; (Here AS is a keyword and “name” is an alias).

##### Q4. What is a Literal? Give an example where it can be used?

**Ans:**  A Literal is a string that can contain a character, a number, or a date that is included in the SELECT list and that is not a column name or a column alias. Date and character literals must be enclosed within single quotation marks (‘ ‘), number literals need not.

For ex: Select last_name||’is a’||job_id As “emp details” from employee; (Here “is a” is a literal).

##### Q5. What is a difference between SQL and iSQL*Plus?

SQL Vs iSQL*Plus

**SQL**

**iSQL*Plus**

Is a Language

Is an Environment

Character and date columns heading are left-justified and number column headings are right-justified.

Default heading justification is in Centre.

Cannot be Abbreviated (short forms)

Can be Abbreviated

Does not have a continuation character

Has a dash (-) as a continuation character if the command is longer than one line

Use Functions to perform some formatting

Use commands to format data

##### Q6. Define the order of Precedence used in executing SQL statements.

Order of Precedence used in executing SQL statements

**Order Evaluated**

**Operator**

1

Arithmetic operators (*, /, +, -)

2

Concatenation operators (||)

3

Comparison conditions

4

Is[NOT] NULL, LIKE, [NOT] IN

5

[NOT] BETWEEN

6

NOT Logical condition

7

AND logical condition

8

OR logical condition

##### Q7. What are SQL functions? Describe in brief different types of SQL functions?

**Ans:**  SQL Functions are very powerful feature of SQL. SQL functions can take arguments but always return some value.  
There are two distinct types of SQL functions:

**1) Single-Row functions:**  These functions operate on a single row to give one result per row.

Types of Single-Row functions:

1.  Character
2.  Number
3.  Date
4.  Conversion
5.  General

**2) Multiple-Row functions:**  These functions operate on groups of rows to give one result per group of rows.

Types of Multiple-Row functions:

1.  AVG
2.  COUNT
3.  MAX
4.  MIN
5.  SUM
6.  STDDEV
7.  VARIANCE

**[Learn: [Frequently Asked SQL Interview Questions & Answers](https://mindmajix.com/sql-interview-questions "SQL Interview Questions") ]**

##### Q8. Explain character, number and date function in detail?

**Ans:  Character functions:** accept character input and return both character and number values. Types of character function are:

a) Case-Manipulation Functions: LOWER, UPPER, INITCAP

b) Character-Manipulation Functions: CONCAT, SUBSTR, LENGTH, INSTR, LPAD/RPAD, TRIM, REPLACE

**Number Functions:** accept Numeric input and return numeric values. Number Functions are: ROUND, TRUNC and MOD

**Date Functions:** operates on values of the Date data type. (All date functions return a value of DATE data type except the MONTHS_BETWEEN Function, which returns a number. Date Functions are MONTHS_BETWEEN, ADD_MONTHS, NEXT_DAY, LAST_DAY, ROUND, TRUNC.

##### Q9. What is a Dual Table?

**Ans:** Dual table is owned by the user SYS and can be accessed by all users. It contains one column**Dummy**  and one row with the value  **X.**  The Dual Table is useful when you want to return a value only once. The value can be a constant, pseudocolumn or expression that is not derived from a table with user data.

**[Learn:  [SQL Server Interview Questions](https://mindmajix.com/sql-server-interview-questions "SQL Server Interview Questions")]**

##### Q10. Explain Conversion function in detail?

**Ans:** Conversion Functions converts a value from one data type to another. Conversion functions are of two types:

Implicit Data type conversion:

1.  VARCHAR2 or CHAR To NUMBER, DATE
2.  NUMBER To VARCHAR2
3.  DATE To VARCHAR2

Explicit data type conversion:

1.  TO_NUMBER
2.  TO_CHAR
3.  TO_DATE

**TO_NUMBER**  function is used to convert Character string to Number format. TO_NUMBER function use fx modifier. Format: TO_NUMBER ( char[, ‘ format_model’] ). fx modifier specifies the exact matching for the character argument and number format model of TO_NUMBER function.

**TO_CHAR**  function is used to convert NUMBER or DATE data type to CHARACTER format. TO_CHAR Function use fm element to remove padded blanks or suppress leading zeros. TO_CHAR Function formats:TO_CHAR (date, ‘format_model’).Format model must be enclosed in single quotation marks and is case sensitive.

For ex: Select TO_CHAR (hiredate, ‘MM/YY’) from employee.

**TO_DATE**  function is used to convert Character string to date format. TO_DATE function use fx modifier which specifies the exact matching for the character argument and date format model of TO_DATE function. TO_DATE function format: TO_DATE ( char[, ‘ format_model’] ).

For ex: Select TO_DATE (‘may 24 2007’,’mon dd rr’) from dual;

### Oracle SQL Interview Questions And Answers

##### Q11. Describe different types of General Function used in SQL?

**Ans:**  General functions are of following types:

1.  **NVL:**  Converts a null value to an actual value. NVL (exp1, exp2) .If exp1 is null then NVL function return value of exp2.
2.  **NVL2:**  If exp1 is not null, nvl2 returns exp2, if exp1 is null, nvl2 returns exp3. The argument exp1 can have any data type. NVL2 (exp1, exp2, exp3)
3.  **NULLIF:**  Compares two expressions and returns null if they are equal or the first expression if they are not equal. NULLIF (exp1, exp2)
4.  **COALESCE:**  Returns the first non-null expression in the expression list. COALESCE (exp1, exp2… expn). The advantage of the COALESCE function over NVL function is that the COALESCE function can take multiple alternative values.
5.  **Conditional Expressions:**  Provide the use of IF-THEN-ELSE logic within a SQL statement. Example: CASE Expression and DECODE Function.

##### Q12. What is difference between COUNT (*), COUNT (expression), COUNT (distinct expression)? (Where expression is any column name of Table)

**Ans:** COUNT (*): Returns number of rows in a table including duplicates rows and rows containing null values in any of the columns.

COUNT (EXP): Returns the number of non-null values in the column identified by expression.

COUNT (DISTINCT EXP): Returns the number of unique, non-null values in the column identified by expression.

##### Q13. What is a Sub Query? Describe its Types?

**Ans:** A sub query is a SELECT statement that is embedded in a clause of another SELECT statement. Sub query can be placed in WHERE, HAVING and FROM clause.

_Guidelines for using sub queries:_

1.  Enclose sub queries within parenthesis
2.  Place sub queries on the right side of the comparison condition.
3.  Use Single-row operators with single-row sub queries and Multiple-row operators with multiple-row sub queries.

**Types of sub queries:**

1.  **Single-Row Sub query:** Queries that return only one row from the Inner select statement. Single-row comparison operators are: =, >, >=, <, <=, <>
2.  **Multiple-Row Sub query:** Queries that return more than one row from the inner Select statement. There are also multiple-column sub queries that return more than one column from the inner select statement. Operators includes: IN, ANY, ALL.

##### Q14. What is difference between ANY and ALL operators?

**Ans:** ANY Operator compares value to each value returned by the subquery. ANY operator has a synonym SOME operator.

> ANY means more than the minimum.

< ANY means less than the maximum

= ANY is equivalent to IN operator.

#### Subscribe to our youtube channel to get new updates..!

ALL Operator compares value to every value returned by the subquery.

> ALL means more than the maximum

< ALL means less than the minimum

<> ALL is equivalent to NOT IN condition.

##### Q15. What is a MERGE statement?

**Ans:** The MERGE statement inserts or updates rows in one table, using data from another table. It is useful in data warehousing applications.

**[Learn:  [SQL Server Interview Questions For Experienced](https://mindmajix.com/sql-server-interview-questions-for-2-5-years "SQL Server Interview Questions for Experienced")]**

##### PL/SQL Interview Questions for Experienced Professionals - (1/2/3 Years)

###### Q16. What is a difference between “VERIFY” and “FEEDBACK” command?

**Ans:** VERIFY Command: Use VERIFY Command to confirm the changes in the SQL statement (Old and New values). Defined with SET VERIFY ON/OFF.

Feedback Command: Displays the number of records returned by a query.

##### Q17. What is the use of Double Ampersand (&&) in SQL Queries? Give example?

**Ans:** Use “&&” if you want to reuse the variable value without prompting the user each time.

For ex: Select empno, ename, &&column_name from employee order by &column_name;

##### Q18. What are Joins and how many types of Joins are there?

**Ans:** Joins are used to retrieve data from more than one table.

There are 5 different types of joins.

types of Joins

**Oracle 8i and Prior**

**SQL: 1999 (9i)**

Equi Join

Natural/Inner Join

Outer Join

Left Outer/ Right Outer/ Full Outer Join

Self Join

Join ON

Non-Equi Join

Join USING

Cartesian Product

Cross Join

##### Q19. Explain all Joins used in Oracle 8i?

**Ans:** Cartesian Join: When a Join condition is invalid or omitted completely, the result is a Cartesian product, in which all combinations of rows are displayed. To avoid a Cartesian product, always include a valid join condition in a “where” clause. To Join ‘N’ tables together, you need a minimum of N-1 Join conditions.

For ex: to join four tables, a minimum of three joins is required. This rule may not apply if the table has a concatenated primary key, in which case more than one column is required to uniquely identify each row.

**Equi Join:** This type of Join involves primary and foreign key relation. Equi Join is also called Simple or Inner Joins.

**Non-Equi Joins:** A Non-Equi Join condition containing something other than an equality operator. The relationship is obtained using an operator other than equal operator (=).The conditions such as <= and >= can be used, but BETWEEN is the simplest to represent Non-Equi Joins.

**Outer Joins:** Outer Join is used to fetch rows that do not meet the join condition. The outer join operator is the plus sign (+), and it is placed on the side of the join that is deficient in information. The Outer Join operator can appear on only one side of the expression, the side that has information missing. It returns those rows from one table that has no direct match in the other table. A condition involving an Outer Join cannot use IN and OR operator.

**Self Join:**  Joining a table to itself.

### Advanced PL/SQL Interview Questions For Experienced

##### Q20. Explain all Joins used in Oracle 9i and later release?

**Ans:** **Cross Join:** Cross Join clause produces the cross-product of two tables. This is same as a Cartesian product between the two tables.

**Natural Joins:**  Is used to join two tables automatically based on the columns which have matching data types and names, using the keyword NATURAL JOIN. It is equal to the Equi-Join. If the columns have the same names but different data types, than the Natural Join syntax causes an error.

**Join with the USING clause:**  If several columns have the same names but the data types do not match, than the NATURAL JOIN clause can be modified with the USING clause to specify the columns that should be used for an equi Join. Use the USING clause to match only one column when more than one column matches. Do not use a table name or alias in the referenced columns. The NATURAL JOIN clause and USING clause are mutually exclusive.

For ex: Select a.city, b.dept_name from loc a Join dept b USING (loc_id) where loc_id=10;

**Joins with the ON clause:** Use the ON clause to specify a join condition. The ON clause makes code easy to understand. ON clause is equals to Self Joins. The ON clause can also be used to join columns that have different names.

**Left/ Right/ Full Outer Joins:** Left Outer Join displays all rows from the table that is Left to the LEFT OUTER JOIN clause, right outer join displays all rows from the table that is right to the RIGHT OUTER JOIN clause, and full outer join displays all rows from both the tables either left or right to the FULL OUTER JOIN clause.

**[Learn:  [SQL Server  DBA Interview Questions](https://mindmajix.com/sql-server-dba-interview-questions "SQL Server DBA Interview Questions")]**

##### Q21. What is a difference between Entity, Attribute and Tuple?

**Ans:**  Entity: A significant thing about which some information is required. For ex: EMPLOYEE (table). Attribute: Something that describes the entity. For ex: empno, empname, empaddress (columns). Tuple: A row in a relation is called Tuple.

##### Q22. What is a Transaction? Describe common errors can occur while executing any Transaction?

**Ans:**  Transaction consists of a collection of DML statements that forms a logical unit of work.

The common errors that can occur while executing any transaction are:

The violation of constraints.

1.  Data type mismatch.
2.  Value too wide to fit in column.
3.  The system crashes or Server gets down.
4.  The session Killed.
5.  Locking take place. Etc.

##### Q23. What is locking in SQL? Describe its types?

**Ans:**  Locking prevents destructive interaction between concurrent transactions. Locks held until Commit or Rollback. Types of locking are:

**Implicit Locking:** Occurs for all SQL statements except SELECT.

**Explicit Locking:**  Can be done by user manually.

Further there are two locking methods:

1.  _Exclusive_: Locks out other users
2.  _Share_: Allows other users to access

##### Q24. What is a difference between Commit, Rollback and Savepoint?

-   COMMIT: Ends the current transaction by making all pending data changes permanent.
-   ROLLBACK: Ends the current transaction by discarding all pending data changes.
-   SAVEPOINT: Divides a transaction into smaller parts. You can rollback the transaction till a particular named savepoint.

##### PLSQL Interview Questions for 4 Years Experience

##### Q25. What are the advantages of COMMIT and ROLLBACK statements?

**Ans:** Advantages of COMMIT and ROLLBACK statements are:

-   Ensure data consistency
-   Can preview data changes before making changes permanent.
-   Group logically related operations.

###### Q26. Describe naming rules for creating a Table?

**Ans:**  Naming rules to be consider for creating a table are:

1.  Table name must begin with a letter,
2.  Table name can be 1-30 characters long,
3.  Table name can contain only A-Z, a-z, 0-9,_, $, #.
4.  Table name cannot duplicate the name of another object owned by the same user.
5.  Table name cannot be an oracle server reserved word.

###### Q27. What is a DEFAULT option in a table?

**Ans:**  A column can be given a default value by using the DEFAULT option. This option prevents null values from entering the column if a row is inserted without a value for that column. The DEFAULT value can be a literal, an expression, or a SQL function such as SYSDATE and USER but the value cannot be the name of another column or a pseudo column such as NEXTVAL or CURRVAL.

###### Q28. What is a difference between USER TABLES and DATA DICTIONARY?

**Ans:**  USER TABLES: Is a collection of tables created and maintained by the user. Contain USER information. DATA DICTIONARY: Is a collection of tables created and maintained by the Oracle Server. It contains database information. All data dictionary tables are owned by the SYS user.

###### Q29. Describe few Data Types used in SQL?

**Ans:**  Data Types is a specific storage format used to store column values. Few data types used in SQL are:

1.  VARCHAR2(size): Minimum size is ‘1’ and Maximum size is ‘4000’
2.  CHAR(size): Minimum size is ‘1’and Maximum size is ‘2000’
3.  NUMBER(P,S): " Precision" can range from 1 to 38 and the “Scale” can range from -84 to 127.
4.  DATE
5.  LONG: 2GB
6.  CLOB: 4GB
7.  RAW (size): Maximum size is 2000
8.  LONG RAW: 2GB
9.  BLOB: 4GB
10.  BFILE: 4GB
11.  ROWID: A 64 base number system representing the unique address of a row in the table.

###### Q30. In what scenario you can modify a column in a table?

[](https://mindmajix.com/oracle-pl-sql-training#curriculum)

### Oracle PL SQL Certification Training!

Explore Curriculum

**Ans:**  During modifying a column:

1.  You can increase the width or precision of a numeric column.
2.  You can increase the width of numeric or character columns.
3.  You can decrease the width of a column only if the column contains null values or if the table has no rows.
4.  You can change the data type only if the column contains null values.
5.  You can convert a CHAR column to the VARCHAR2 data type or convert a VARCHAR2 column to the CHAR data type only if the column contains null values or if you do not change the size.

###### Q31. Describe few restrictions on using “LONG” data type?

**Ans:**  A LONG column is not copied when a table is created using a sub query. A LONG column cannot be included in a GROUP BY or an ORDER BY clause. Only one LONG column can be used per table. No constraint can be defined on a LONG column.

###### Q32. What is a SET UNUSED option?

**Ans:**  SET UNUSED option marks one or more columns as unused so that they can be dropped when the demand on system resources is lower. Unused columns are treated as if they were dropped, even though their column data remains in the table’s rows. After a column has been marked as unused, you have no access to that column. A select * query will not retrieve data from unused columns. In addition, the names and types of columns marked unused will not be displayed during a DESCRIBE, and you can add to the table a new column with the same name as an unused column. The SET UNUSED information is stored in the USER_UNUSED_COL_TABS dictionary view.

##### PLSQL Interview Questions for 5 Years Experience

##### Q33. What is a difference between Truncate and Delete?

**Ans:** The main difference between Truncate and Delete is as below:

SQL Truncate Vs SQL Delete

TRUNCATE

DELETE

Removes all rows from a table and releases storage space used by that table.

Removes all rows from a table but does not release storage space used by that table.

TRUNCATE Command is faster.

DELETE command is slower.

Is a DDL statement and cannot be Rollback.

Is a DDL statement and can be Rollback.

Database Triggers do not fire on TRUNCATE.

Database Triggers fire on DELETE.

###### Q34. What is a main difference between CHAR and VARCHAR2?

**Ans:**  CHAR pads blank spaces to a maximum length, whereas VARCHAR2 does not pad blank spaces.

###### Q35. What are Constraints? How many types of constraints are there?

**Ans:**  Constraints are used to prevent invalid data entry or deletion if there are dependencies. Constraints enforce rules at the table level. Constraints can be created either at the same time as the table is created or after the table has been created. Constraints can be defined at the column or table level. Constraint defined for a specific table can be viewed by looking at the USER-CONSTRAINTS data dictionary table. You can define any constraint at the table level except NOT NULL which is defined only at column level. There are 5 types of constraints:

1.  Not Null Constraint
2.  Unique Key Constraint
3.  Primary Key Constraint
4.  Foreign Key Constraint
5.  Check Key Constraint.

### Oracle PL SQL Technical interview questions

##### Q36. Describe types of Constraints in brief?

**Ans:  NOT NULL: NOT NULL**  Constraint ensures that the column contains no null values.

**UNIQUE KEY:** UNIQUE Key Constraint ensures that every value in a column or set of columns must be unique, that is, no two rows of a table can have duplicate values in a specified column or set of columns. If the UNIQUE constraint comprises more than one column, that group of columns is called a Composite Unique Key. There can be more than one Unique key on a table. Unique Key Constraint allows the input of Null values. Unique Key automatically creates index on the column it is created.

**PRIMARY KEY:**  Uniquely identifies each row in the Table. Only one PRIMARY KEY can be created for each table but can have several UNIQUE constraints. PRIMARY KEY ensures that no column can contain a NULL value. A Unique Index is automatically created for a PRIMARY KEY column. PRIMARY KEY is called a Parent key.

**FOREIGN KEY:** Is also called Referential Integrity Constraint. FOREIGN KEY is one in which a column or set of columns take references of the Primary/Unique key of same or another table. FOREIGN KEY is called a child key. A FOREIGN KEY value must match an existing value in the parent table or be null.

**CHECK KEY:** Defines a condition that each row must satisfy. A single column can have multiple CHECK Constraints. During CHECK constraint following expressions is not allowed:

1) References to CURRVAL, NEXTVAL, LEVEL and ROWNUM Pseudo columns.

2) Calls to SYSDATE, UID, USER and USERENV Functions

###### Q37. What is the main difference between Unique Key and Primary Key?

**Ans:** The main difference between Unique Key and Primary Key are:

Unique Vs Primary Key

**Unique Key**

**Primary Key**

A table can have more than one Unique Key.

A table can have only one Primary Key.

Unique key column can store NULL values.

Primary key column cannot store NULL values.

Uniquely identify each value in a column.

Uniquely identify each row in a table.

##### Q38. What is a difference between ON DELETE CASCADE and ON DELETE SET NULL?

**Ans:** ON DELETE CASCADE Indicates that when the row in the parent table is deleted, the dependent rows in the child table will also be deleted. ON DELETE SET NULL Coverts foreign key values to null when the parent value is removed. Without the ON DELETE CASCADE or the ON DELETE SET NULL options, the row in the parent table cannot be deleted if it is referenced in the child table.

Explore Oracle PL SQL Sample Resumes! Download & Edit for Free!**[Download Now!](https://mindmajix.com/oracle-pl-sql-sample-resumes)**

###### Q39. What is a Candidate Key?

**Ans:** The columns in a table that can act as a Primary Key are called Candidate Key.

###### Q40. What are Views and why they are used?

**Ans:**  A View logically represents subsets of data from one or more table. A View is a logical table based on a table or another view. A View contains no data of its own but is like a window through which data from tables can be viewed or changed. The tables on which a view is based are called Base Tables. The View is stored as a SELECT statement in the data dictionary. View definitions can be retrieved from the data dictionary table: USER_VIEWS.

Views are used:

-   To restrict data access
-   To make complex queries easy
-   To provide data Independence
-   Views provide groups of user to access data according to their requirement.

##### Oracle PL/SQL Interview Questions And Answers For 7 Years Experience

##### Q41. What is a difference between Simple and Complex Views?

**Ans:**  The main differences between two views are:

Simple Views Vs Complex Views

**Simple View**

**Complex View**

Derives data from only one table.

Derives data from many tables.

Contains no functions or group of data

Contain functions or groups of data.

Can perform DML operations through the view.

Does not always allow DML operations through the view.

##### Q42. What are the restrictions of DML operations on Views?

**Ans:**  Few restrictions of DML operations on Views are:

You cannot DELETE a row if the View contains the following:

1.  Group Functions
2.  A Group By clause
3.  The Distinct Keyword
4.  The Pseudo column ROWNUM Keyword.

[](https://mindmajix.com/oracle-pl-sql-training#training_dates)

#### Upcoming Batches - Oracle PL SQL Training!

-   24
    
    **SEPT**
    
    Tuesday  
    6:30 AM IST
    
-   26
    
    **SEPT**
    
    Thursday  
    7:00 AM IST
    
-   29
    
    **SEPT**
    
    Sunday  
    6:30 AM IST
    
-   03
    
    **OCT**
    
    Thursday  
    6:30 AM IST
    

  
More Batches

You cannot MODIFY data in a View if it contains the following:

1.  Group Functions
2.  A Group By clause
3.  The Distinct Keyword
4.  The Pseudo column ROWNUM Keyword.
5.  Columns defined by expressions (Ex; Salary * 12)

You cannot INSERT data through a view if it contains the following:

###### Q43. What is PL/SQL, Why do we need PL/SQL instead of SQL, Describe your experience working with PLSQL and What are the difficulties faced while working with PL SQL and How did you overcome?

1.  PL/SQL is a procedural language extension with SQL Language.
2.  Oracle introduced PL/SQL
3.  It is a combination of SQL and Procedural Statements and used for creating applications.
4.  Basically PL/SQL is a block structure programming language whenever we are submitting PL/SQL
5.  Blocks then all SQL statements are executing separately by using sql engine and also all procedure statements are executed separately.
6.  Explain your current and previous projects along with your roles and responsibilities, mention some of the challenging difficulties you've faced in your project while working with PL/SQL.

###### Q44. What are the different functionalities of a Trigger ?

**Ans:**  Trigger is also same as stored procedure & also it will automatically invoked whenever DML operation performed against table or view.

**There are two types of triggers supported by PL/SQL**

1.  Statement Level Trigger.
2.  Row Level Trigger

**Statement Level Trigger:** In statement level trigger, trigger body is executed only once for DML statement.

**Row Level Trigger:** In row level trigger, trigger body is executed for each row DML statements. It is the reason, we are employing each row clause and internally stored DML transaction in trigger specification, these qualifiers :old, :new, are also called as records type variables.  
These qualifiers are used in trigger specification & trigger body.

**Synatx**:  
:old.column_name  
**Synatx**:  
:new column_name

When we are use this qualifiers in trigger specification then we are not allowed to use “:” in forms of the qualifiers names.

###### Q45. Write a PL/SQL Program which raise a user defined exception on thursday?

**Ans:**

1

2

3

4

5

6

7

8

9

10

11

12

`declare`

`a exception`

`begin`

`If to_char(sysdate, ‘DY)=’THU’`

`then`

`raise a;`

`end` `if``;`

`exception`

`when a then`

`dbms_output.put_line(‘my exception raised on thursday’);`

`end`

`;`

**Output**: my exception raised on thursday

###### Q46.Write a PL/SQL program to retrieve emp table and then display the salary?

**Ans:**

1

2

3

4

5

6

7

`declare`

`v_sal number(10);`

`begin select max(sal)intr v_sal;`

`from emp;`

`dbms_output.put_line(v.sal);`

`end;`

`/`

**(or)**

1

2

3

4

5

6

7

8

9

10

11

`declare`

`A number(10);`

`B number(10);`

`C number(10);`

`begin`

`a:=70;`

`b:=30;`

`c:=greatest+(a,b);`

`dbms_output.put_line(c);`

`end;`

`/`

**Output**:70

##### PLSQL Interview Questions for 8 Years Experience

##### Q47. Write a PL/SQL cursor program which is used to calculate total salary from emp table without using sum() function?

**Ans:**

1

2

3

4

5

6

7

8

9

10

11

12

13

14

15

16

17

`Declare`

`cursor c1 is select sal from emp;`

`v_sal number(10);`

`n.number(10):=0;`

`begin`

`open c1;`

`loop`

`fetch c1 into v_sal;`

`exit when c1%not found;`

`n:=n+v_sal;`

`end loop;`

`dbms_output.put_line(‘tool salary is’||’ ‘ ||n);`

`close c1;`

`end;`

`/`

`Output: total salary is: 36975`

##### Q48. Write a PL/SQL cursor program to display all employee names and their salary from emp table by using % not found attributes?

**Ans:**

1

2

3

4

5

6

7

8

9

10

11

12

13

14

`Declare`

`Cursor c1 is select ename, sal from emp;`

`v_ename varchar2(10);`

`v_sal number(10);`

`begin`

`open c1;`

`loop`

`fetch c1 into v_ename, v_sal;`

`exist when c1 % notfound;`

`dbms_output.put_line(v_name ||’ ‘||v_sal);`

`end loop;`

`close c1;`

`end;`

`/`

##### Q49. What is Mutating Trigger?

**Ans:**

-   Into a row level trigger based on a table trigger body cannot read data from same table and also we cannot perform DML operation on same table.
-   If we are trying this oracle server returns mutating error oracle-4091: table is mutating.
-   This error is called mutating error, and this trigger is called mutating trigger, and table is called mutating table.
-   Mutating errors are not occurred in statement level trigger because through these statement level trigger when we are performing DML operations automatically data committed into the database, whereas in row level trigger when we are performing transaction data is not committed and also again we are reading this data from the same table then only mutating errors is occurred.

###### Oracle PL/SQL Interview Questions and Answers for 10 Years Experience

##### Q50. What is Triggering Events (or) Trigger Predicate Clauses?

**Ans:**  If we want to perform multiple operations in different tables then we must use triggering events within trigger body. These are inserting, updating, deleting clauses. These clauses are used in statement, row-level trigger. These triggers are also called as trigger predicate clauses.

**Syntax**:

1

2

3

4

`If inserting then stmts;`

`else` `if` `updating then stmts;`

`else` `if` `deleting then stmts;`

`end` `if``;`

##### Q51. What is Discard File?

**Ans:**

-   This file extension is .dsc
-   Discard file we must specify within control file by using discard file clause.
-   Discard file also stores reflected record based on when clause condition within control file. This condition must be satisfied into table tablename clause.

##### Q52. What is REF CURSOR (or) CURSOR VARIABLE (or) DYNAMIC CURSOR ?

**Ans:**

Oracle 7.2 introduced ref cursor, This is an user defined type which is used to process multiple records and also this is a record by record process.

In static cursor database servers executes only one select statement at a time for a single active set area where in ref cursor database servers executes number of select statement dynamically for a single active set area that's why those cursor are also called as dynamically cursor.

Generally we are not allowed to pass static cursor as parameters to use subprograms where as we can also pass ref cursor as parameter to the subprograms because basically refcursor is an user defined type in oracle we can also pass all user defined type as parameter to the subprograms.

  
Generally static cursor does not return multiple record into client application where as ref cursor are allowed to return multiple records into client application (Java, .Net, php, VB, C++).

This is an user defined type so we are creating it in 2 steps process i.e first we are creating type then only we are creating variable from that type that’s why this is also called as cursor variable.

##### **Q53. What are The Types of Ref Cursors?**

**Ans:**  In all databases having 2 ref cursors.

1.  Strong ref cursor
2.  Weak ref cursor

Strong ref cursor is a ref cursor which have return type, whereas weak ref cursor has no return type.

**Syntax**:

1

2

`Type typename is ref cursor` `return` `record type data type;`

`Variable Name typename`

**Syntax**

1

2

`Type typename is ref cursor`

`Variable Name typename;`

In Weak ref cursor we must specify select statement by using open for clause this clause is used in executable section of the PL/SQL block.

**Syntax**:

1

`Open ref cursor varname` `for` `SELECT * FROM table_name condition;`

###### **Q54. What is Difference Between trim, delete collection method?**

**Ans:**

1

2

3

4

5

6

7

8

9

10

11

12

13

14

15

16

17

18

19

20

21

22

23

24

25

26

27

`SQL> declare`

`type t1 is table of number(10);`

`v_t t1;=t1(10,20,30,40,50,60);`

`beign`

`v_t.trim(2);`

`dbms_output.put_line(‘after deleting last two elements’);`

`for` `i` `in` `v_t.first.. V_t.last`

`loop`

`dbms_output.put_line(v_t(i));`

`End loop;`

`vt.``delete``(2);`

`dbms_output.put_line(‘after deleting second element;);`

`for` `i` `in` `v_t.first..v_t.last`

`loop`

`If v_t.exists(i) then`

`dbms_output.put_line(v_t(i));`

`end` `if``;`

`end loop;`

`end;`

`/`

###### **Q55. What is Overloading Procedures?**

**Ans:**  Overload is refers to same name can be used for different purpose, in oracle we can also implement overloading procedure through package. Overloading procedure having same name with different type or different number of parameters.

###### **Q56. What is Global Variables?**

**Ans:**  In oracle we are declaring global variables in Package Specification only.

###### **Q57. What is Forward Declaration?**

**Ans:**  In oracle declaring procedures within package body is called forward declaring generally before we are calling private procedures into public procedure first we must implements private into public procedure first we must implements private procedure within body otherwise use a forward declaration within package body.

###### **Q58. What is Invalid_number, Value_Error?**

**Ans:**  In oracle when we try to convert “string type to number type” or” data string into data type” then oracle server returns two types of errors.  
1. Invalid.number  
2. Value_error (or) numeric_error

**a) Invalid_number:**  
When PL/SQL block have a SQL statements and also those SQL statements try to convert string type to number type or data string into data type then oracle server returns an error: ora-1722-Invalid Number  
For handling this error oracle provides number exception Invalid_number exceptionname.

**Example**:

1

2

3

4

5

6

`begin`

`Insert`

`intoemp(empno, ename, sal)`

`values(1,’gokul’, ‘abc’)`

`exception when invalid_number then dbms_output.put_line(‘insert proper data only’);`

`end;/`

**b)value_error**  
Whenever PL/SQL block having procedural statements and also those statements find to convert string type to number type then oracle servers returns an error: ora-6502:numeric or value error: character to number conversion error  
For handling this error oracle provided exception value_error exception name

**Example**:

1

2

3

4

5

6

7

`begin`

`declare z number(10);`

`begin`

`z:= ‘&x’ + ‘&y’;`

`dbms_output.put_line(z);`

`exception when value_error then dbms_output.put_line(‘enter numeric data value` `for` `x & y only’);`

`end;/`

  
**Output**:

1

2

3

4

5

6

7

8

`Enter value` `for` `x:3`

`Enter value` `for` `y:2`

`z:=5`

`Enter value` `for` `x:a`

`Enter value` `for` `y:b`

`Error:enter numeric data value` `for` `x & y only.`

##### Q59. What is Flashback Query?

**Ans:**

-   Flashback query are handle by Database Administrator only flashback queries along allows content of the table to be retrieved with reference to specific point of time by using as of clause that is flashback queries retrieves clause that is flashback queries retrieves accidental data after committing the transaction also.
-   Flashback queries generally uses undo file that is flashback queries retrieve old data before committing the transaction oracle provide two method for flashback queries

**Method1**: using timestamp  
**Method2**: using scn number

### Newly Updated Oracle PL/SQL Interview Questions 2019

##### 1) Explain what PL/SQL package consist of?

**Ans:** PL/SQL consists of two major parts, they are: package specification and package body.

Package specification: it acts as a public interface for your application which includes procedures, types, etc.

Package Body: It contains the code which required to implement the Package Specification

##### 2) Explain what the benefits of PL/SQL Packages are?

**Ans:** These are the benefits of PL/SQL Packages

-   We can store functions and procedures in a single unit called package.
    
-   Packages provides security to grant privileges.
    
-   Functions and procedures, within the package, shares a common variable among them.
    
-   Packages support even if the functions are overloaded.
    
-   Packages enhance the performance even when the multiple objects loaded into memory.
    

##### 3) explain different methods to trace the PL/SQL code?

**Ans:** Tracing code is a necessary technique to test the performance of the code during runtime. We have different methods in PL/SQL to trace the code, which are,

-   DBMS_ TRACE
    
-   DBMS_ APPLICATION_INFO
    
-   Tkproof utilities and trcsess
    
-   DBMS_SESSION and DBMS_MONITOR
    

##### 4) What does it mean by PL/SQL Cursors?

**Ans:**  In PL/SQL to retrieve and process more it requires a special resource, and that resource is known as Cursor. A cursor is defined as a pointer to the context area. Context area is an area of memory which contains information and SQL statements for processing the statements.

##### 5) what is the difference between Implicit and Explicit Cursors?

Ans: Implicit cursor used in PL/SQL to declare, all SQL data manipulation statements. An implicit cursor is used to declare SQL statements such as open, close, fetch etc.

An explicit cursor is a cursor and which is explicitly designed to select the statement with the help of a cursor. This explicit cursor is used to execute the multirow select function. An explicit function is used PL/SQL to execute tasks such as update, insert, delete, etc.

##### 6) what is a trigger?

**Ans:**  It is a program in PL/SQL, stored in the database and executed instantly before or after the UPDATE, INSERT and DELETE commands.

##### 7) what are the uses of database triggers?

**Ans:**  Triggers are programs which are automatically fired or executed when some events happen and are used for:

-   To implement complex security authorizations.
    
-   To drive column values.
    
-   To maintain duplicate tables.
    
-   To implement complex business rules.
    
-   To bring transparency in log events.
    

###### 8) Name the two exceptions in PL/SQL?

**Ans:**  Error handling part of PL/SQL is called an exception. We have two types of exceptions, and they are User defined and predefined.

###### 9) which command is used to delete the package?

**Ans:**  To delete the ‘Package’ in PL/SQL we use DROP PACKAGE command.

###### 10) what is the process for PL/SQL compilation?

**Ans:**  Compilation process consists of syntax check, bind and p-code generation. It checks the errors in PL/SQL code while compiling. Once all errors are corrected, a storage address allocated to a variable which stores this data. This process is called as binding. P-Code consists of list of rules for the PL/SQL engine. It is stored in the database and triggered when next time it is used.

**[https://www.oracletutorial.com/plsql-tutorial/]**(https://www.oracletutorial.com/plsql-tutorial/)


## Oracle SQL Tuning

## Tuning individual Oracle SQL statements

The acronym SQL stands for Structured Query Language. SQL is an industry standard database query language that was adopted in the mid-1980s. It should not be confused with commercial products such as Microsoft SQL Server or open source products such as MySQL, both of which use the acronym as part of the title of their products.  
  
There are three major decisions that the SQL optimizer must make during the optimization of a SQL statement:  
  
-  **Access method:** Oracle has several choices of the "best" way to access data.  
  
-  **Join method:** Oracle must decode between nested loops joins, hash join, etc.  
  
- **Join order:**  The database has choices about the  [best table join order](http://www.dba-oracle.com/t_table_join_order.htm).  
  
Let's take a closer look at the high level process of SQL tuning:

## Do this before you start individual SQL statement tuning

This broad-brush approach can save thousands of hours of tedious SQL tuning because you can hundreds of queries at once. Remember, you MUST do this first, else later changes to the optimizer parameters or statistics may un-tune your SQL.

![](http://www.dba-oracle.com/images/sql_tuning_hierarchy.gif)

Remember, you must ALWAYS start with system-level SQL tuning, else later changes might undo your tuned execution plans:

-   **Optimize the server kernel** - You must always tune your disk and network I/O subsystem (RAID, DASD bandwidth, network) to optimize the I/O time, network packet size and dispatching frequency.  
    
-   **Adjusting your optimizer statistics**  - You must always collect and store optimizer statistics to allow the optimizer to learn more about the distribution of your data to take more intelligent execution plans. Also, histograms can hypercharge SQL in cases of determining optimal table join order, and when making access decisions on skewed WHERE clause predicates.  
    
-   **Adjust optimizer parameters**  - Optimizer optimizer_mode,  _optimizer_index_caching_, o_ptimizer_index_cost_adj_.  
    
-   **Optimize your instance**  - Your choice of  _db_block_size,_  _db_cache_size_, and OS parameters (db_file_multiblock_read_count, cpu_count, &c), can influence SQL performance.  
    
-   **Tune your SQL Access workload with physical indexes and materialized views**  - Just as the SQLAccess advisor recommends missing indexes and missing materialized views, you should always optimize your SQL workload with indexes, especially function-based indexes, a Godsend for SQL tuning.

> > 12c Note: The Oracle 12c SQL Performance Analyzer (SPA), is primarily designed to speed up the holistic SQL tuning process.  
> >   
> > Once you create a workload (called a SQL Tuning Set, or STS), Oracle will repeatedly execute the workload, using sophisticated predictive models (using a regression testing approach) to accurately identify the salient changes to SQL execution plans, based on your environmental changes. Using SPA, we can predict the impact of system changes on a workload, and we can forecast changes in response times for SQL after making any change, like parameter changes, schema changes, hardware changes, OS changes, or Oracle upgrades.

Once the environment, instance, and objects have been tuned, the Oracle administrator can focus on what is probably the single most important aspect of tuning an Oracle database: tuning the individual SQL statements. In this final article in my series on Oracle tuning, I will share some general guidelines for tuning individual SQL statements to improve Oracle performance.

## **Oracle SQL tuning goals**

Oracle SQL tuning is a phenomenally complex subject. Entire books have been written about the nuances of Oracle SQL tuning; however, there are some general guidelines that every Oracle DBA follows in order to improve the performance of their systems. Again, see the book "[Oracle Tuning: The Definitive Reference](http://www.rampant-books.com/book_1002_oracle_tuning_definitive_reference_2nd_ed.htm)", for complete details.  
  
The goals of SQL tuning focus on improving the execution plan to fetch the rows with the smallest number of database "touches" (LIO buffer gets and PIO physical reads).

-   **Remove unnecessary large-table full-table scans**—Unnecessary full-table scans cause a huge amount of unnecessary I/O and can drag-down an entire database. The tuning expert first evaluates the SQL based on the number of rows returned by the query. The most common tuning remedy for unnecessary full-table scans is adding indexes. Standard b-tree indexes can be added to tables, and bitmapped and function-based indexes can also eliminate full-table scans. In some cases, an unnecessary full-table scan can be forced to use an index by adding an index hint to the SQL statement.  
    
-   **Cache small-table full-table scans**—In cases where a full-table scan is the fastest access method, the administrator should ensure that a dedicated data buffer is available for the rows. In Oracle8 and beyond, a small table can be cached by forcing it into the KEEP pool.  
    
-   **Verify optimal index usage**—Oracle sometimes has a choice of indexes, and the tuning professional must examine each index and ensure that Oracle is using the proper index.  
    
-   **Materialize your aggregations and summaries for static tables**  - One features of the Oracle [SQLAccess advisor](http://www.dba-oracle.com/tips_rittman_dbazine_sqlaccess.htm)  is recommendations for new indexes and suggestions for materialized views. Materialized views pre-join tables and pre-summarize data, a real silver bullet for data mart reporting databases where the data is only updated daily. Again, see the book "[Oracle Tuning: The Definitive Reference](http://www.rampant-books.com/book_1002_oracle_tuning_definitive_reference_2nd_ed.htm)", for complete details on SQL tuning with materialized views.

These are the goals of SQL tuning in a nutshell. However, they are deceptively simple, and to effectively meet them, we need to have a through understanding of the internals of Oracle SQL. Let's begin with an overview of the Oracle SQL optimizers.

## **Oracle SQL optimizers**

One of the first things the Oracle DBA looks at is the default optimizer mode for the database. The Oracle initialization parameters offer many cost-based optimizer modes as well as the deprecated yet useful rule-based hint:

The cost-based optimizer uses 'statistics' that are collected from the table using the 'analyze table' command. Oracle uses these metrics about the tables in order to intelligently determine the most efficient way of servicing the SQL query. It is important to recognize that in many cases, the cost-based optimizer may not make the proper decision in terms of the speed of the query. The cost-based optimizer is constantly being improved, but there are still many cases in which the rule-based optimizer will result in faster Oracle queries.

Prior to Oracle 10g, Oracle's default optimizer mode was called 'choose.' In the choose optimizer mode, Oracle will execute the rule-based optimizer if there are no statistics present for the table; it will execute the cost-based optimizer if statistics are present. The danger with using the choose optimizer mode is that problems can occur in cases where one Oracle table in a complex query has statistics and the other tables do not.

Starting in Oracle 10g, the default optimizer mode is all_rows, favoring full-table scans over index access. The all_rows optimizer mode is designed to minimize computing resources and it favors full-table scans. Index access (_first_rows_n_) adds additional I/O overhead, but they return rows faster, back to the originating query:

![](http://www.dba-oracle.com/images/ani_oracle_full_table_scans.gif)  
_Full-table scans touch all data blocks_

Hence, many OLTP shops will choose  [first_rows](http://www.dba-oracle.com/art_orafaq_oracle_first_rows_sql_optimization.htm), first_rows_100 or first_rows_10, asking Oracle to use indexes to reduce block touches:

![](http://www.dba-oracle.com/images/ani_oracle_index_access.gif)  
_Index scans return rows fast by doing additional I/O_

**Note:** Staring in Oracle9i release 2, the Oracle performance tuning guide says that the _first_rows_ optimizer mode has been deprecated and to use  _first_rows_n_ instead.

When only some tables contain CBO statistics, Oracle will use the cost-based optimization and estimate statistics for the other tables in the query at runtime. This can cause significant slowdown in the performance of the individual query.

In sum, the Oracle database administrator will always try changing the optimizer mode for queries as the very first step in Oracle tuning. The foremost tenet of Oracle SQL tuning is avoiding the dreaded full-table scan. One of the hallmarks of an inefficient SQL statement is the failure of the SQL statement to use all of the indexes that are present within the Oracle database in order to speed up the query.  
  
Of course, there are times when a full-table scan is appropriate for a query, such as when you are doing aggregate operations such as a sum or an average, and the majority of the rows within the Oracle table must be read to get the query results. The task of the SQL tuning expert is to evaluate each full-table scan and see if the performance can be improved by adding an index.

In most Oracle systems, a SQL statement will be retrieving only a small subset of the rows within the table. The Oracle optimizers are programmed to check for indexes and to use them whenever possible to avoid excessive I/O. However, if the formulation of a query is inefficient, the cost-based optimizer becomes confused about the best access path to the data, and the cost-based optimizer will sometimes choose to do a full-table scan against the table. Again, the general rule is for the Oracle database administrator to interrogate the SQL and always look for full-table scans.

For the full story, see my book "[Oracle Tuning: The Definitive Reference](http://www.rampant-books.com/book_1002_oracle_tuning_definitive_reference_2nd_ed.htm)" for details on choosing the right optimizer mode.

**A strategic plan for Oracle SQL tuning**  
  
Many people ask where they should start when tuning Oracle SQL. Tuning Oracle SQL is like fishing. You must first fish in the Oracle library cache to extract SQL statements and rank the statements by their amount of activity.  
  
**Step 1: Identify high-impact SQL**  
  
The SQL statements will be ranked according the number of executions and will be tuned in this order. The executions column of the  _v$sqlarea_  view and the  _stats$sql_summary_  or the  _[dba_hist_sql_summary](http://www.dba-oracle.com/oracle10g_tuning/t_awr_statspack_table_names.htm)_  table can be used to locate the most frequently used SQL. Note that we can display SQL statements by:

-   **Rows processed**: Queries that process a large number of rows will have high I/O and may also have impact on the TEMP tablespace.  
    
-   **Buffer gets**: High buffer gets may indicate a resource-intensive query.  
    
-   **Disk reads**: High disk reads indicate a query that is causing excessive I/O.  
    
-   **Memory KB**: The memory allocation of a SQL statement is useful for identifying statements that are doing in-memory table joins.  
    
-   **CPU secs**: This identifies the SQL statements that use the most processor resources.  
    
-   **Sorts**: Sorts can be a huge slowdown, especially if they're being done on a disk in the TEMP tablespace.  
    
-   **Executions**: The more frequently executed SQL statements should be tuned first, since they will have the greatest impact on overall performance.

  
**Step 2: Determine the execution plan for SQL**  
  
As each SQL statement is identified, it will be 'explained' to determine its existing execution plan. There are a host of third-party tools on the market that show the execution plan for SQL statements. The most common way of determining the execution plan for a SQL statement is to use Oracle's explain plan utility. By using explain plan, the Oracle DBA can ask Oracle to parse the statement and display the execution class path without actually executing the SQL statement.  
  
To see the output of an explain plan, you must first create a 'plan table.' Oracle provides a script in $ORACLE_HOME/rdbms/admin called utlxplan.sql. Execute utlxplan.sql and create a public synonym for the plan_table:  
  
sqlplus > @utlxplan  
Table created.  
  
sqlplus > create public synonym plan_table for sys.plan_table;  
Synonym created.

Most relational databases use an explain utility that takes the SQL statement as input, runs the SQL optimizer, and outputs the access path information into a plan_table, which can then be interrogated to see the access methods. Listing 1 runs a complex query against a database.

  
EXPLAIN PLAN SET STATEMENT_ID = 'test1' FOR

SET STATEMENT_ID = 'RUN1'

INTO plan_table

FOR

SELECT 'T'||plansnet.terr_code, 'P'||detplan.pac1

|| detplan.pac2 || detplan.pac3, 'P1', sum(plansnet.ytd_d_ly_tm),

sum(plansnet.ytd_d_ty_tm),

sum(plansnet.jan_d_ly),

sum(plansnet.jan_d_ty),

FROM plansnet, detplan

WHERE

plansnet.mgc = detplan.mktgpm

AND

detplan.pac1 in ('N33','192','195','201','BAI',

'P51','Q27','180','181','183','184','186','188',

'198','204','207','209','211')

GROUP BY 'T'||plansnet.terr_code, 'P'||detplan.pac1 || detplan.pac2 || detplan.pac3;

This syntax is piped into the SQL optimizer, which will analyze the query and store the plan information in a row in the plan table identified by RUN1. Please note that the query will not execute; it will only create the internal access information in the plan table. The plan tables contains the following fields:

-   **operation**: The type of access being performed. Usually table access, table merge, sort, or index operation  
    
-   **options**: Modifiers to the operation, specifying a full table, a range table, or a join  
    
-   **object_name**: The name of the table being used by the query component  
    
-   **Process ID**: The identifier for the query component  
    
-   **Parent_ID**: The parent of the query component. Note that several query components may have the same parent.

Now that the plan_table has been created and populated, you may interrogate it to see your output by running the following query in Listing 2.

> plan.sql - displays contents of the explain plan table
> 
> SET PAGES 9999;
> 
> SELECT lpad(' ',2*(level-1))||operation operation,
> 
> options,
> 
> object_name,
> 
> position
> 
> FROM plan_table
> 
> START WITH id=0
> 
> AND
> 
> statement_id = 'RUN1'
> 
> CONNECT BY prior id = parent_id
> 
> AND
> 
> statement_id = 'RUN1';

Listing 3 shows the output from the plan table shown in Listing 1. This is the execution plan for the statement and shows the steps and the order in which they will be executed.

SQL> @list_explain_plan

OPERATION

-------------------------------------------------------------------------------------

OPTIONS OBJECT_NAME POSITION

------------------------------ -------------------------------------------------------

SELECT STATEMENT

SORT

GROUP BY 1

CONCATENATION 1

NESTED LOOPS 1

TABLE ACCESS FULL PLANSNET 1

TABLE ACCESS BY ROWID DETPLAN 2

INDEX RANGE SCAN DETPLAN_INDEX5 1

NESTED LOOPS

  
From this output, we can see the dreaded TABLE ACCESS FULL on the PLANSNET table. To diagnose the reason for this full-table scan, we return to the SQL and look for any plansnet columns in the WHERE clause. There, we see that the plansnet column called 'mgc' is being used as a join column in the query, indicating that an index is necessary on plansnet.mgc to alleviate the full-table scan.  
  
While the plan table is useful for determining the access path to the data, it does not tell the entire story. The configuration of the data is also a consideration. The SQL optimizer is aware of the number of rows in each table (the cardinality) and the presence of indexes on fields, but it is not aware of data distribution factors such as the number of expected rows returned from each query component.  

**Step 3: Tune the SQL statement**  
  
For those SQL statements that possess a sub-optimal execution plan, the SQL will be tuned by one of the following methods:  

-   Adding SQL 'hints' to modify the execution plan  
    
-   ## [Re-write SQL with Global Temporary Tables](http://www.dba-oracle.com/t_tuning_global_temporary_tables.htm)
    
-   Rewriting the SQL in PL/SQL. For certain queries this can result in more than a 20x performance improvement. The SQL would be replaced with a call to a PL/SQL package that contained a stored procedure to perform the query.

**Using hints to tune Oracle SQL**  
  
Among the most common tools for tuning SQL statements are hints. A hint is a directive that is added to the SQL statement to modify the access path for a SQL query.

**Troubleshooting tip!** For testing, you can quickly test the effect of another optimizer parameter value at the query level without using an 'alter session' command, using the new _opt_param_ SQL hint:

select /*+ opt_param('optimizer_mode','first_rows_10') */ col1, col2 . . .

select /*+ opt_param('optimizer_index_cost_adj',20) */ col1, col2 . .

Oracle publishes many dozens of SQL hints, and hints become increasingly more complicated through the various releases of Oracle and on into Oracle.

Note: Hints are only used for de-bugging and you should adjust your optimizer statistics to make the CBO replicate the hinted SQL. Let's look at the most common hints to improve tuning:

-   Mode hints: first_rows_10, first_rows_100
-   [Oracle leading and ordered hints](http://www.dba-oracle.com/t_leading_hint.htm) Also see how to tune  [table join order with histograms](http://www.dba-oracle.com/oracle_tips_all_columns_histograms.htm)  
    
-   Dynamic sampling: dynamic_sampling  
    
-   ## [Oracle SQL undocumented tuning hints](http://www.dba-oracle.com/art_otn_cbo_p7.htm) - Guru's only
    
-   The cardinality hint  
    

-   **Self-order the table joins** - If you find that Oracle is joining the tables together in a sub-optimal order, you can use the ORDERED hint to force the tables to be joined in the order that they appear in the FROM clause. See
    
-   **Try a first_rows_n hint.** Oracle has two cost-based optimizer modes,  _first_rows_n_ and  _all_rows_. The first_rows mode will execute to begin returning rows as soon as possible, whereas the all_rows mode is designed to optimize the resources on the entire query before returning rows.
    
      
    SELECT /*+ first_rows */  
      
      
    **A case study in SQL tuning**  
      
    One of the historic problems with SQL involves formulating SQL queries. Simple queries can be written in many different ways, each variant of the query producing the same result, but with widely different access methods and query speeds.  
      
    For example, a simple query such as 'What students received an A last semester'' can be written in three ways, as shown in below, each returning an identical result.
    

**A standard join:**  

SELECT *

FROM STUDENT, REGISTRATION

WHERE

STUDENT.student_id = REGISTRATION.student_id

AND

REGISTRATION.grade = 'A';  

A nested query:

  
SELECT *

FROM STUDENT

WHERE

student_id =

(SELECT student_id

FROM REGISTRATION

WHERE

grade = 'A'

);

  
**A correlated subquery:**

  
SELECT *

FROM STUDENT

WHERE

0 <

(SELECT count(*)

FROM REGISTRATION

WHERE

grade = 'A'

AND

student_id = STUDENT.student_id

);

Let's wind up with a review of the basic components of a SQL query and see how to optimize a query for remote execution.  
  
**Tips for writing more efficient SQL**  
  
Space doesn't permit me to discuss every detail of Oracle tuning, but I can share some general rules for writing efficient SQL in Oracle regardless of the optimizer that is chosen. These rules may seem simplistic but following them in a diligent manner will generally relieve more than half of the SQL tuning problems that are experienced:

-   **Rewrite complex subqueries with temporary tables**  - Oracle created the global temporary table (GTT) and the SQL WITH operator to help divide-and-conquer complex SQL sub-queries (especially those with with WHERE clause subqueries, SELECT clause scalar subqueries and FROM clause in-line views). [Tuning SQL with temporary tables](http://www.dba-oracle.com/t_temporary_tables_sql.htm)  (and materializations in the WITH clause) can result in amazing performance improvements.  
    
-   **Use minus instead of EXISTS subqueries** - Some  [say that](http://www.dbpd.com/vault/9801xtra.htm)  using the minus operator instead of NOT IN and NOT Exists will result in a faster execution plan.  
    
-   **Use SQL analytic functions** - The Oracle analytic functions can do multiple aggregations (e.g. rollup by cube) with a single pass through the tables, making them very fast for reporting SQL.
-   **Re-write NOT EXISTS and NOT EXISTS subqueries as outer joins** - In  [many cases](http://www.dba-oracle.com/oracle_tips_subq_rewrite.htm)  of NOT queries (but ONLY where a column is defined as NULL), you can re-write the uncorrelated subqueries into outer joins with IS NULL tests. Note that this is a non-correlated sub-query, but it could be re-written as an outer join.

> > select book_key from book  
> > where  
> > book_key NOT IN (select book_key from sales);

> Below we combine the outer join with a NULL test in the WHERE clause without using a sub-query, giving a faster execution plan.
> 
> > select b.book_key from book b, sales s  
> > where  
> > b.book_key = s.book_key(+)  
> > and  
> > s.book_key IS NULL;

-   **Index your NULL values**  - If you have SQL that frequently tests for NULL, consider creating an [index on NULL values](http://www.dba-oracle.com/oracle_tips_null_idx.htm). To get around the optimization of SQL queries that choose NULL column values (i.e.  _where emp_name IS NULL_), we can create a function-based index using the null value built-in SQL function to index only on the NULL columns.  
    
-   **Leave column names alone**  - Never do a calculation on an indexed column unless you have a matching  [function-based index](http://www.dba-oracle.com/oracle_tips_index_scan_fbi_sql.htm)  (a.k.a. FBI). Better yet, re-design the schema so that common where clause predicates do not need transformation with a BIF:

> > where salary*5 > :myvalue  
> > where substr(ssn,7,4) = "1234"  
> > where to_char(mydate,mon) = "january"

-   **Avoid the use of NOT IN or HAVING**. Instead, a NOT EXISTS subquery may run faster (when appropriate).  
    
-   **Avoid the LIKE predicate**  = Always replace a "like" with an equality, when appropriate.  
    
-   **Never mix data types**  - If a WHERE clause column predicate is numeric, do not to use quotes. For char index columns, always use quotes. There are mixed data type predicates:

> > where cust_nbr = "123"  
> > where substr(ssn,7,4) = 1234

-   **Use decode and case** - Performing complex aggregations with the "decode" or "case" functions can minimize the number of times a table has to be selected.  
    
-   **Don't fear full-table scans** - Not all OLTP queries are optimal when they uses indexes. If your query will return a large percentage of the table rows, a full-table scan may be faster than an index scan. [This depends](http://www.dba-oracle.com/art_orafaq_oracle_sql_tune_table_scan.htm)  on many factors, including your configuration (values for  _db_file_multiblock_read_count_,  _db_block_size),_ query parallelism and the number of table/index blocks in the buffer cache.  
    
-   **Use those aliases**  - Always use table aliases when referencing columns.


## Oracle PL/SQL Tuning

## How PL/SQL Optimizes Your Programs

In releases prior to 10_g_, the PL/SQL compiler translated your code to machine code without applying many changes for performance. Now, PL/SQL uses an optimizing compiler that can rearrange code for better performance.

You do not need to do anything to get the benefits of this new optimizer. It is enabled by default. In rare cases, if the overhead of the optimizer makes compilation of very large applications take too long, you might lower the optimization by setting the initialization parameter  `PLSQL_OPTIMIZE_LEVEL=1`  instead of its default value 2. In even rarer cases, you might see a change in exception behavior, either an exception that is not raised at all, or one that is raised earlier than expected. Setting  `PL_SQL_OPTIMIZE_LEVEL=0`  prevents the code from being rearranged at all.

## When to Tune PL/SQL Code

The information in this chapter is especially valuable if you are responsible for:

-   Programs that do a lot of mathematical calculations. You will want to investigate the datatypes  `PLS_INTEGER`,  `BINARY_FLOAT`, and  `BINARY_DOUBLE`.
    
-   Functions that are called from PL/SQL queries, where the functions might be executed millions of times. You will want to look at all performance features to make the function as efficient as possible, and perhaps a function-based index to precompute the results for each row and save on query time.
    
-   Programs that spend a lot of time processing  `INSERT`,  `UPDATE`, or  `DELETE`  statements, or looping through query results. You will want to investigate the  `FORALL`  statement for issuing DML, and the  `BULK COLLECT INTO`  and  `RETURNING BULK COLLECT INTO`  clauses for queries.
    
-   Older code that does not take advantage of recent PL/SQL language features. (With the many performance improvements in Oracle Database 10_g_, any code from earlier releases is a candidate for tuning.)
    
-   Any program that spends a lot of time doing PL/SQL processing, as opposed to issuing DDL statements like  `CREATE TABLE`  that are just passed directly to SQL. You will want to investigate native compilation. Because many built-in database features use PL/SQL, you can apply this tuning feature to an entire database to improve performance in many areas, not just your own code.
    

Before starting any tuning effort, benchmark the current system and measure how long particular subprograms take. PL/SQL in Oracle Database 10_g_  includes many automatic optimizations, so you might see performance improvements without doing any tuning.

## Guidelines for Avoiding PL/SQL Performance Problems

When a PL/SQL-based application performs poorly, it is often due to badly written SQL statements, poor programming practices, inattention to PL/SQL basics, or misuse of shared memory.

### Avoiding CPU Overhead in PL/SQL Code

  
**Make SQL Statements as Efficient as Possible**  

PL/SQL programs look relatively simple because most of the work is done by SQL statements. Slow SQL statements are the main reason for slow execution.

If SQL statements are slowing down your program:

-   Make sure you have appropriate indexes. There are different kinds of indexes for different situations. Your index strategy might be different depending on the sizes of various tables in a query, the distribution of data in each query, and the columns used in the  `WHERE`  clauses.
    
-   Make sure you have up-to-date statistics on all the tables, using the subprograms in the  `DBMS_STATS`  package.
    
-   Analyze the execution plans and performance of the SQL statements, using:
    
    -   `EXPLAIN`  `PLAN`  statement
        
    -   SQL Trace facility with  `TKPROF`  utility
        
    -   Oracle Trace facility
        
-   Rewrite the SQL statements if necessary. For example, query hints can avoid problems such as unnecessary full-table scans.
    

For more information about these methods, see  _[Oracle Database Performance Tuning Guide](https://docs.oracle.com/cd/B13789_01/server.101/b10752/toc.htm)_.

Some PL/SQL features also help improve the performance of SQL statements:

-   If you are running SQL statements inside a PL/SQL loop, look at the  `FORALL`  statement as a way to replace loops of  `INSERT`,  `UPDATE`, and  `DELETE`  statements.
    
-   If you are looping through the result set of a query, look at the  `BULK COLLECT`  clause of the  `SELECT INTO`  statement as a way to bring the entire result set into memory in a single operation.
    

  
**Make Function Calls as Efficient as Possible**  

Badly written subprograms (for example, a slow sort or search function) can harm performance. Avoid unnecessary calls to subprograms, and optimize their code:

-   If a function is called within a SQL query, you can cache the function value for each row by creating a function-based index on the table in the query. The  `CREATE INDEX`  statement might take a while, but queries can be much faster.
    
-   If a column is passed to a function within an SQL query, the query cannot use regular indexes on that column, and the function might be called for every row in a (potentially very large) table. Consider nesting the query so that the inner query filters the results to a small number of rows, and the outer query calls the function only a few times:
    
    BEGIN
    -- Inefficient, calls my_function for every row.
       FOR item IN (SELECT DISTINCT(SQRT(department_id)) col_alias FROM employees)
       LOOP
          dbms_output.put_line(item.col_alias);
       END LOOP;
    
    -- Efficient, only calls function once for each distinct value.
       FOR item IN
       ( SELECT SQRT(department_id) col_alias FROM
         ( SELECT DISTINCT department_id FROM employees)
       )
       LOOP
          dbms_output.put_line(item.col_alias);
       END LOOP;
    END;
    /
    

If you use  `OUT`  or  `IN OUT`  parameters, PL/SQL adds some performance overhead to ensure correct behavior in case of exceptions (assigning a value to the  `OUT`  parameter, then exiting the subprogram because of an unhandled exception, so that the  `OUT`  parameter keeps its original value).

If your program does not depend on  `OUT`  parameters keeping their values in such situations, you can add the  `NOCOPY`  keyword to the parameter declarations, so the parameters are declared  `OUT NOCOPY`  or  `IN OUT NOCOPY`.

This technique can give significant speedup if you are passing back large amounts of data in  `OUT`  parameters, such as collections, big  `VARCHAR2`  values, or LOBs.

This technique also applies to member subprograms of object types. If these subprograms modify attributes of the object type, all the attributes are copied when the subprogram ends. To avoid this overhead, you can explicitly declare the first parameter of the member subprogram as  `SELF IN OUT NOCOPY`, instead of relying on PL/SQL's implicit declaration  `SELF IN OUT`.

  
**Make Loops as Efficient as Possible**  

Because PL/SQL applications are often built around loops, it is important to optimize the loop itself and the code inside the loop:

-   Move initializations or computations outside the loop if possible.
    
-   To issue a series of DML statements, replace loop constructs with  `FORALL`  statements.
    
-   To loop through a result set and store the values, use the  `BULK COLLECT`  clause on the query to bring the query results into memory in one operation.
    
-   If you have to loop through a result set more than once, or issue other queries as you loop through a result set, you can probably enhance the original query to give you exactly the results you want. Some query operators to explore include  `UNION`,  `INTERSECT`,  `MINUS`, and  `CONNECT BY`.
    
-   You can also nest one query inside another (known as a subselect) to do the filtering and sorting in multiple stages. For example, instead of calling a PL/SQL function in the inner  `WHERE`  clause (which might call the function once for each row of the table), you can filter the result set to a small set of rows in the inner query, and call the function in the outer query.
    

  
**Don't Duplicate Built-in String Functions**  

PL/SQL provides many highly optimized string functions such as  `REPLACE`,  `TRANSLATE`,  `SUBSTR`,  `INSTR`,  `RPAD`, and  `LTRIM`. The built-in functions use low-level code that is more efficient than regular PL/SQL.

If you use PL/SQL string functions to search for regular expressions, consider using the built-in regular expression functions, such as  `REGEXP_SUBSTR`.

  
**Reorder Conditional Tests to Put the Least Expensive First**  

PL/SQL stops evaluating a logical expression as soon as the result can be determined (known as short-circuit evaluation).

When evaluating multiple conditions separated by  `AND`  or  `OR`, put the least expensive ones first. For example, check the values of PL/SQL variables before testing function return values, because PL/SQL might be able to skip calling the functions.

  
**Minimize Datatype Conversions**  

At run time, PL/SQL converts between different datatypes automatically. For example, assigning a  `PLS_INTEGER`  variable to a  `NUMBER`  variable results in a conversion because their internal representations are different.

Avoiding implicit conversions can improve performance. Use literals of the appropriate types: character literals in character expressions, decimal numbers in number expressions, and so on.

In the example below, the integer literal  `15`  must be converted to an Oracle  `NUMBER`  before the addition. The floating-point literal  `15.0`  is represented as a  `NUMBER`, avoiding the need for a conversion.

DECLARE
   n NUMBER;
   c CHAR(5);
BEGIN
   n := n + 15;      -- converted implicitly; slow
   n := n + 15.0;    -- not converted; fast
   c := 25;          -- converted implicitly; slow
   c := TO_CHAR(25); -- converted explicitly; still slow
   c := '25';        -- not converted; fast
END;
/

Minimizing conversions might mean changing the types of your variables, or even working backward and designing your tables with different datatypes. Or, you might convert data once (such as from an  `INTEGER`  column to a  `PLS_INTEGER`  variable) and use the PL/SQL type consistently after that.

  
**Use PLS_INTEGER or BINARY_INTEGER for Integer Arithmetic**  

When you need to declare a local integer variable, use the datatype  `PLS_INTEGER`, which is the most efficient integer type.  `PLS_INTEGER`  values require less storage than  `INTEGER`  or  `NUMBER`  values, and  `PLS_INTEGER`  operations use machine arithmetic.

The  `BINARY_INTEGER`  datatype is just as efficient as  `PLS_INTEGER`  for any new code, but if you are running the same code on Oracle9_i_  or Oracle8_i_  databases,  `PLS_INTEGER`  is faster.

The datatype  `NUMBER`  and its subtypes are represented in a special internal format, designed for portability and arbitrary scale and precision, not performance. Even the subtype  `INTEGER`  is treated as a floating-point number with nothing after the decimal point. Operations on  `NUMBER`  or  `INTEGER`  variables require calls to library routines.

Avoid constrained subtypes such as  `INTEGER`,  `NATURAL`,  `NATURALN`,  `POSITIVE`,  `POSITIVEN`, and  `SIGNTYPE`  in performance-critical code. Variables of these types require extra checking at run time, each time they are used in a calculation.

  
**Use BINARY_FLOAT and BINARY_DOUBLE for Floating-Point Arithmetic**  

The datatype  `NUMBER`  and its subtypes are represented in a special internal format, designed for portability and arbitrary scale and precision, not performance. Operations on  `NUMBER`  or  `INTEGER`  variables require calls to library routines.

The  `BINARY_FLOAT`  and  `BINARY_DOUBLE`  types can use native machine arithmetic instructions, and are more efficient for number-crunching applications such as scientific processing. They also require less space in the database.

These types do not always represent fractional values precisely, and handle rounding differently than the  `NUMBER`  types. These types are less suitable for financial code where accuracy is critical.

### Avoiding Memory Overhead in PL/SQL Code

  
**Be Generous When Declaring Sizes for VARCHAR2 Variables**  

You might need to allocate large  `VARCHAR2`  variables when you are not sure how big an expression result will be. You can actually conserve memory by declaring  `VARCHAR2`  variables with large sizes, such as 32000, rather than estimating just a little on the high side, such as by specifying a size such as 256 or 1000. PL/SQL has an optimization that makes it easy to avoid overflow problems and still conserve memory. Specify a size of 2000 or more characters for the  `VARCHAR2`  variable; PL/SQL waits until you assign the variable, then only allocates as much storage as needed.

  
**Group Related Subprograms into Packages**  

When you call a packaged subprogram for the first time, the whole package is loaded into the shared memory pool. Subsequent calls to related subprograms in the package require no disk I/O, and your code executes faster. If the package is aged out of memory, it must be reloaded if you reference it again.

You can improve performance by sizing the shared memory pool correctly. Make sure it is large enough to hold all frequently used packages but not so large that memory is wasted.

  
**Pin Packages in the Shared Memory Pool**  

You can "pin" frequently accessed packages in the shared memory pool, using the supplied package  `DBMS_SHARED_POOL`. When a package is pinned, it is not aged out by the least recently used (LRU) algorithm that Oracle normally uses. The package remains in memory no matter how full the pool gets or how frequently you access the package.

For more information on the  `DBMS_SHARED_POOL`  package, see  _[PL/SQL Packages and Types Reference](https://docs.oracle.com/cd/B13789_01/appdev.101/b10802/toc.htm)_.

## Profiling and Tracing PL/SQL Programs

As you develop larger and larger PL/SQL applications, it becomes more difficult to isolate performance problems. PL/SQL provides a Profiler API to profile run-time behavior and to help you identify performance bottlenecks. PL/SQL also provides a Trace API for tracing the execution of programs on the server. You can use Trace to trace the execution by subprogram or exception.

### Using The Profiler API: Package DBMS_PROFILER

The Profiler API is implemented as PL/SQL package  `DBMS_PROFILER`, which provides services for gathering and saving run-time statistics. The information is stored in database tables, which you can query later. For example, you can learn how much time was spent executing each PL/SQL line and subprogram.

To use the Profiler, you start the profiling session, run your application long enough to get adequate code coverage, flush the collected data to the database, then stop the profiling session.

The Profiler traces the execution of your program, computing the time spent at each line and in each subprogram. You can use the collected data to improve performance. For instance, you might focus on subprograms that run slowly.

For information about the  `DBMS_PROFILER`  subprograms, see  _[PL/SQL Packages and Types Reference](https://docs.oracle.com/cd/B13789_01/appdev.101/b10802/toc.htm)_.

  
**Analyzing the Collected Performance Data**  

The next step is to determine why more time was spent executing certain code segments or accessing certain data structures. Find the problem areas by querying the performance data. Focus on the subprograms and packages that use up the most execution time, inspecting possible performance bottlenecks such as SQL statements, loops, and recursive functions.

  
**Using Trace Data to Improve Performance**  

Use the results of your analysis to rework slow algorithms. For example, due to an exponential growth in data, you might need to replace a linear search with a binary search. Also, look for inefficiencies caused by inappropriate data structures, and, if necessary, replace those data structures.

### Using The Trace API: Package DBMS_TRACE

With large, complex applications, it becomes difficult to keep track of calls between subprograms. By tracing your code with the Trace API, you can see the order in which subprograms execute. The Trace API is implemented as PL/SQL package  `DBMS_TRACE`, which provides services for tracing execution by subprogram or exception.

To use Trace, you start the tracing session, run your application, then stop the tracing session. As the program executes, trace data is collected and stored in database tables.

For information about the  `DBMS_TRACE`  subprograms, see  _[PL/SQL Packages and Types Reference](https://docs.oracle.com/cd/B13789_01/appdev.101/b10802/toc.htm)_.

  
**Controlling the Trace**  

Tracing large applications can produce huge amounts of data that are difficult to manage. Before starting Trace, you can optionally limit the volume of data collected by selecting specific subprograms for trace data collection.

In addition, you can choose a tracing level. For example, you can choose to trace all subprograms and exceptions, or you can choose to trace selected subprograms and exceptions.

## Reducing Loop Overhead for DML Statements and Queries (FORALL, BULK COLLECT)

PL/SQL sends SQL statements such as DML and queries to the SQL engine for execution, and SQL returns the result data to PL/SQL. You can minimize the performance overhead of this communication between PL/SQL and SQL by using the PL/SQL language features known collectively as bulk SQL. The  `FORALL`  statement sends  `INSERT`,  `UPDATE`, or  `DELETE`  statements in batches, rather than one at a time. The  `BULK COLLECT`  clause brings back batches of results from SQL. If the DML statement affects four or more database rows, the use of bulk SQL can improve performance considerably.

The assigning of values to PL/SQL variables in SQL statements is called  **binding**. PL/SQL binding operations fall into three categories:

-   **in-bind**  When a PL/SQL variable or host variable is stored in the database by an  `INSERT`  or  `UPDATE`  statement.
    
-   **out-bind**  When a database value is assigned to a PL/SQL variable or a host variable by the  `RETURNING`  clause of an  `INSERT`,  `UPDATE`, or  `DELETE`  statement.
    
-   **define**  When a database value is assigned to a PL/SQL variable or a host variable by a  `SELECT`  or  `FETCH`  statement.
    

Bulk SQL uses PL/SQL collections, such as varrays or nested tables, to pass large amounts of data back and forth in a single operation. This process is known as  **bulk binding**. If the collection has 20 elements, bulk binding lets you perform the equivalent of 20  `SELECT`,  `INSERT`,  `UPDATE`, or  `DELETE`  statements using a single operation. Queries can pass back any number of results, without requiring a  `FETCH`  statement for each row.

To speed up  `INSERT`,  `UPDATE`, and  `DELETE`  statements, enclose the SQL statement within a PL/SQL  `FORALL`  statement instead of a loop construct.

To speed up  `SELECT`  statements, include the  `BULK`  `COLLECT INTO`  clause in the  `SELECT`  statement instead of using  `INTO`.

For full details of the syntax and restrictions for these statements, see  ["FORALL Statement"](https://docs.oracle.com/cd/B13789_01/appdev.101/b10807/13_elems021.htm#i34324)  and  ["SELECT INTO Statement"](https://docs.oracle.com/cd/B13789_01/appdev.101/b10807/13_elems045.htm#i36066).

### Using the FORALL Statement

The keyword  `FORALL`  lets you run multiple DML statements very efficiently. It can only repeat a single DML statement, unlike a general-purpose  `FOR`  loop.

For full syntax and restrictions, see  ["FORALL Statement"](https://docs.oracle.com/cd/B13789_01/appdev.101/b10807/13_elems021.htm#i34324).

The SQL statement can reference more than one collection, but  `FORALL`  only improves performance where the index value is used as a subscript.

Usually, the bounds specify a range of consecutive index numbers. If the index numbers are not consecutive, such as after you delete collection elements, you can use the  `INDICES OF`  or  `VALUES OF`  clause to iterate over just those index values that really exist.

The  `INDICES OF`  clause iterates over all of the index values in the specified collection, or only those between a lower and upper bound.

The  `VALUES OF`  clause refers to a collection that is indexed by  `BINARY_INTEGER`  or  `PLS_INTEGER`  and whose elements are of type  `BINARY_INTEGER`  or  `PLS_INTEGER`. The  `FORALL`  statement iterates over the index values specified by the elements of this collection.

**_Example 11-1 Issuing DELETE Statements in a Loop_**

This  `FORALL`  statement sends all three  `DELETE`  statements to the SQL engine at once:

CREATE TABLE employees2 AS SELECT * FROM employees;
DECLARE
   TYPE NumList IS VARRAY(20) OF NUMBER;
   depts NumList := NumList(10, 30, 70);  -- department numbers
BEGIN
   FORALL i IN depts.FIRST..depts.LAST
      DELETE FROM employees2 WHERE department_id = depts(i);
   COMMIT;
END;
/
DROP TABLE employees2;

**_Example 11-2 Issuing INSERT Statements in a Loop_**

The following example loads some data into PL/SQL collections. Then it inserts the collection elements into a database table twice: first using a  `FOR`  loop, then using a  `FORALL`  statement. The  `FORALL`  version is much faster.

CREATE TABLE parts1 (pnum INTEGER, pname VARCHAR2(15));
CREATE TABLE parts2 (pnum INTEGER, pname VARCHAR2(15));
DECLARE
  TYPE NumTab IS TABLE OF parts1.pnum%TYPE INDEX BY PLS_INTEGER;
  TYPE NameTab IS TABLE OF parts1.pname%TYPE INDEX BY PLS_INTEGER;
  pnums  NumTab;
  pnames NameTab;
  iterations CONSTANT PLS_INTEGER := 500;
  t1 INTEGER;  t2 INTEGER;  t3 INTEGER;
BEGIN
  FOR j IN 1..iterations LOOP  -- load index-by tables
     pnums(j) := j;
     pnames(j) := 'Part No. ' || TO_CHAR(j);
  END LOOP;
  t1 := dbms_utility.get_time;
  FOR i IN 1..iterations LOOP  -- use FOR loop
     INSERT INTO parts1 VALUES (pnums(i), pnames(i));
  END LOOP;
  t2 := dbms_utility.get_time;
  FORALL i IN 1..iterations  -- use FORALL statement
     INSERT INTO parts2 VALUES (pnums(i), pnames(i));
  t3 := dbms_utility.get_time;
  dbms_output.put_line('Execution Time (secs)');
  dbms_output.put_line('---------------------');
  dbms_output.put_line('FOR loop: ' || TO_CHAR((t2 - t1)/100));
  dbms_output.put_line('FORALL:   ' || TO_CHAR((t3 - t2)/100));
  COMMIT;
END;
/
DROP TABLE parts1;
DROP TABLE parts2;

Executing this block should show that the loop using  `FORALL`  is much faster.

**_Example 11-3 Using FORALL with Part of a Collection_**

The bounds of the  `FORALL`  loop can apply to part of a collection, not necessarily all the elements:

CREATE TABLE employees2 AS SELECT * FROM employees;
DECLARE
   TYPE NumList IS VARRAY(10) OF NUMBER;
   depts NumList := NumList(5,10,20,30,50,55,57,60,70,75);
BEGIN
   FORALL j IN 4..7  -- use only part of varray
      DELETE FROM employees2 WHERE department_id = depts(j);
   COMMIT;
END;
/
DROP TABLE employees2;

**_Example 11-4 Using FORALL with Non-Consecutive Index Values_**

You might need to delete some elements from a collection before using the collection in a  `FORALL`  statement. The  `INDICES OF`  clause processes sparse collections by iterating through only the remaining elements.

You might also want to leave the original collection alone, but process only some elements, process the elements in a different order, or process some elements more than once. Instead of copying the entire elements into new collections, which might use up substantial amounts of memory, the  `VALUES OF`  clause lets you set up simple collections whose elements serve as "pointers" to elements in the original collection.

The following example creates a collection holding some arbitrary data, a set of table names. Deleting some of the elements makes it a sparse collection that would not work in a default  `FORALL`  statement. The program uses a  `FORALL`  statement with the  `INDICES OF`  clause to insert the data into a table. It then sets up two more collections, pointing to certain elements from the original collection. The program stores each set of names in a different database table using  `FORALL`  statements with the  `VALUES OF`  clause.

-- Create empty tables to hold order details
CREATE TABLE valid_orders (cust_name VARCHAR2(32), amount NUMBER(10,2));
CREATE TABLE big_orders AS SELECT * FROM valid_orders WHERE 1 = 0;
CREATE TABLE rejected_orders AS SELECT * FROM valid_orders WHERE 1 = 0;

DECLARE
-- Make collections to hold a set of customer names and order amounts.

   SUBTYPE cust_name IS valid_orders.cust_name%TYPE;
   TYPE cust_typ IS TABLe OF cust_name;
   cust_tab cust_typ;

   SUBTYPE order_amount IS valid_orders.amount%TYPE;
   TYPE amount_typ IS TABLE OF NUMBER;
   amount_tab amount_typ;

-- Make other collections to point into the CUST_TAB collection.
   TYPE index_pointer_t IS TABLE OF PLS_INTEGER;
   big_order_tab index_pointer_t := index_pointer_t();
   rejected_order_tab index_pointer_t := index_pointer_t();

   PROCEDURE setup_data IS BEGIN
      -- Set up sample order data, including some invalid orders and some 'big' orders.
      cust_tab := cust_typ('Company 1','Company 2','Company 3','Company 4', 'Company 5');
      amount_tab := amount_typ(5000.01, 0, 150.25, 4000.00, NULL);
   END;

BEGIN
   setup_data();

   dbms_output.put_line('--- Original order data ---');
   FOR i IN 1..cust_tab.LAST LOOP
      dbms_output.put_line('Customer #' || i || ', ' || cust_tab(i) || ': $' || amount_tab(i));
   END LOOP;

-- Delete invalid orders (where amount is null or 0).
   FOR i IN 1..cust_tab.LAST LOOP
      IF amount_tab(i) is null or amount_tab(i) = 0 THEN
         cust_tab.delete(i);
         amount_tab.delete(i);
      END IF;
   END LOOP;

   dbms_output.put_line('--- Data with invalid orders deleted ---');
   FOR i IN 1..cust_tab.LAST LOOP
      IF cust_tab.EXISTS(i) THEN
         dbms_output.put_line('Customer #' || i || ', ' || cust_tab(i) || ': $' || amount_tab(i));
      END IF;
   END LOOP;

-- Since the subscripts of our collections are not consecutive, we use
-- FORALL...INDICES OF to iterate through the actual subscripts, rather than 1..COUNT.
   FORALL i IN INDICES OF cust_tab
      INSERT INTO valid_orders(cust_name, amount) VALUES(cust_tab(i), amount_tab(i));

-- Now let's process the order data differently. We'll extract 2 subsets
-- and store each subset in a different table.

   setup_data(); -- Initialize the CUST_TAB and AMOUNT_TAB collections again.

   FOR i IN cust_tab.FIRST .. cust_tab.LAST LOOP
      IF amount_tab(i) IS NULL OR amount_tab(i) = 0 THEN
         rejected_order_tab.EXTEND; -- Add a new element to this collection.
         rejected_order_tab(rejected_order_tab.LAST) := i; -- And record the subscript from the original collection.
      END IF;
      IF amount_tab(i) > 2000 THEN
         big_order_tab.EXTEND; -- Add a new element to this collection.
         big_order_tab(big_order_tab.LAST) := i; -- And record the subscript from the original collection.
      END IF;
   END LOOP;

-- Now it's easy to run one DML statement on one subset of elements, and another DML statement on a different subset.

   FORALL i IN VALUES OF rejected_order_tab
      INSERT INTO rejected_orders VALUES (cust_tab(i), amount_tab(i));

   FORALL i IN VALUES OF big_order_tab
      INSERT INTO big_orders VALUES (cust_tab(i), amount_tab(i));

   COMMIT;
END;
/
-- Verify that the correct order details were stored.
SELECT cust_name "Customer", amount "Valid order amount"  FROM valid_orders;
SELECT cust_name "Customer", amount "Big order amount"  FROM big_orders;
SELECT cust_name "Customer", amount "Rejected order amount"  FROM rejected_orders;

DROP TABLE valid_orders;
DROP TABLE big_orders;
DROP TABLE rejected_orders;

  
**How FORALL Affects Rollbacks**  

In a  `FORALL`  statement, if any execution of the SQL statement raises an unhandled exception, all database changes made during previous executions are rolled back. However, if a raised exception is caught and handled, changes are rolled back to an implicit savepoint marked before each execution of the SQL statement. Changes made during previous executions are  _not_  rolled back. For example, suppose you create a database table that stores department numbers and job titles, as follows. Then, you change the job titles so that they are longer. The second  `UPDATE`  fails because the new value is too long for the column. Because we handle the exception, the first  `UPDATE`  is not rolled back and we can commit that change.

CREATE TABLE emp2 (deptno NUMBER(2), job VARCHAR2(18));
DECLARE
   TYPE NumList IS TABLE OF NUMBER;
   depts NumList := NumList(10, 20, 30);
BEGIN
   INSERT INTO emp2 VALUES(10, 'Clerk');
   INSERT INTO emp2 VALUES(20, 'Bookkeeper');  -- Lengthening this job title causes an exception.
   INSERT INTO emp2 VALUES(30, 'Analyst');
   COMMIT;

   FORALL j IN depts.FIRST..depts.LAST -- Run 3 UPDATE statements.
      UPDATE emp2 SET job = job || ' (Senior)' WHERE deptno = depts(j);
         -- raises a "value too large" exception
EXCEPTION
   WHEN OTHERS THEN
      dbms_output.put_line('Problem in the FORALL statement.');
      COMMIT; -- Commit results of successful updates.
END;
/
DROP TABLE emp2;

#### Counting Rows Affected by FORALL with the %BULK_ROWCOUNT Attribute

The cursor attributes  `SQL%FOUND`,  `SQL%ISOPEN`,  `SQL%NOTFOUND`, and  `SQL%ROWCOUNT`, return useful information about the most recently executed DML statement.

The  `SQL`  cursor has one composite attribute,  `%BULK_ROWCOUNT`, for use with the  `FORALL`  statement. This attribute works like an associative array:  `SQL%BULK_ROWCOUNT(`_`i`_`)`  stores the number of rows processed by the  _i_th execution of an  `INSERT`,  `UPDATE`  or  `DELETE`  statement. For example:

CREATE TABLE emp2 AS SELECT * FROM employees;
DECLARE
   TYPE NumList IS TABLE OF NUMBER;
   depts NumList := NumList(30, 50, 60);
BEGIN
   FORALL j IN depts.FIRST..depts.LAST
      DELETE FROM emp2 WHERE department_id = depts(j);
-- How many rows were affected by each DELETE statement?
   FOR i IN depts.FIRST..depts.LAST
   LOOP
      dbms_output.put_line('Iteration #' || i || ' deleted ' ||
         SQL%BULK_ROWCOUNT(i) || ' rows.');
   END LOOP;
END;
/
DROP TABLE emp2;

The  `FORALL`  statement and  `%BULK_ROWCOUNT`  attribute use the same subscripts. For example, if  `FORALL`  uses the range  `5..10`, so does  `%BULK_ROWCOUNT`. If the  `FORALL`  tatement uses the  `INDICES OF`  clause to process a sparse collection,  `%BULK_ROWCOUNT`  has corresponding sparse subscripts. If the  `FORALL`  statement uses the  `VALUES OF`  clause to process a subset of elements,  `%BULK_ROWCOUNT`  has subscripts corresponding to the values of the elements in the index collection. If the index collection contains duplicate elements, so that some DML statements are issued multiple times using the same subscript, then the corresponding elements of  `%BULK_ROWCOUNT`  represent the sum of all rows affected by the DML statement using that subscript. (For examples showing how to interpret  `%BULK_ROWCOUNT`  when using the  `INDICES OF`  and  `VALUES OF`  clauses, see the PL/SQL sample programs at  `[http://otn.oracle.com/tech/pl_sql/](http://otn.oracle.com/tech/pl_sql/)`.)

`%BULK_ROWCOUNT`  is usually equal to 1 for inserts, because a typical insert operation affects only a single row. For the  `INSERT ... SELECT`  construct,  `%BULK_ROWCOUNT`  might be greater than 1. For example, the  `FORALL`  statement below inserts an arbitrary number of rows for each iteration. After each iteration,  `%BULK_ROWCOUNT`  returns the number of items inserted:

CREATE TABLE emp_by_dept AS SELECT employee_id, department_id
   FROM employees WHERE 1 = 0;
DECLARE
  TYPE dept_tab IS TABLE OF departments.department_id%TYPE;
  deptnums dept_tab;
BEGIN
  SELECT department_id BULK COLLECT INTO deptnums FROM departments;

  FORALL i IN 1..deptnums.COUNT
     INSERT INTO emp_by_dept
        SELECT employee_id, department_id FROM employees
           WHERE department_id = deptnums(i);

  FOR i IN 1..deptnums.COUNT LOOP
-- Count how many rows were inserted for each department; that is,
-- how many employees are in each department.
     dbms_output.put_line('Dept '||deptnums(i)||': inserted '||
                          SQL%BULK_ROWCOUNT(i)||' records');
  END LOOP;

  dbms_output.put_line('Total records inserted =' || SQL%ROWCOUNT);
END;
/
DROP TABLE emp_by_dept;

You can also use the scalar attributes  `%FOUND`,  `%NOTFOUND`, and  `%ROWCOUNT`  after running a  `FORALL`  statement. For example,  `%ROWCOUNT`  returns the total number of rows processed by all executions of the SQL statement.

`%FOUND`  and  `%NOTFOUND`  refer only to the last execution of the SQL statement. You can use  `%BULK_ROWCOUNT`  to infer their values for individual executions. For example, when  `%BULK_ROWCOUNT(i)`  is zero,  `%FOUND`  and  `%NOTFOUND`  are  `FALSE`  and  `TRUE`, respectively.

#### Handling FORALL Exceptions with the %BULK_EXCEPTIONS Attribute

PL/SQL provides a mechanism to handle exceptions raised during the execution of a  `FORALL`  statement. This mechanism enables a bulk-bind operation to save information about exceptions and continue processing.

To have a bulk bind complete despite errors, add the keywords  `SAVE`  `EXCEPTIONS`  to your  `FORALL`  statement after the bounds, before the DML statement.

All exceptions raised during the execution are saved in the cursor attribute  `%BULK_EXCEPTIONS`, which stores a collection of records. Each record has two fields:

-   `%BULK_EXCEPTIONS(i).ERROR_INDEX`  holds the "iteration" of the  `FORALL`  statement during which the exception was raised.
    
-   `%BULK_EXCEPTIONS(i).ERROR_CODE`  holds the corresponding Oracle error code.
    

The values stored by  `%BULK_EXCEPTIONS`  always refer to the most recently executed  `FORALL`  statement. The number of exceptions is saved in  `%BULK_EXCEPTIONS.COUNT`. Its subscripts range from 1 to  `COUNT`.

You might need to work backward to determine which collection element was used in the iteration that caused an exception. For example, if you use the  `INDICES OF`  clause to process a sparse collection, you must step through the elements one by one to find the one corresponding to  `%BULK_EXCEPTIONS(i).ERROR_INDEX`. If you use the  `VALUES OF`  clause to process a subset of elements, you must find the element in the index collection whose subscript matches  `%BULK_EXCEPTIONS(i).ERROR_INDEX`, and then use that element's value as the subscript to find the erroneous element in the original collection. (For examples showing how to find the erroneous elements when using the  `INDICES OF`  and  `VALUES OF`  clauses, see the PL/SQL sample programs at  `[http://otn.oracle.com/tech/pl_sql/](http://otn.oracle.com/tech/pl_sql/)`.)

If you omit the keywords  `SAVE`  `EXCEPTIONS`, execution of the  `FORALL`  statement stops when an exception is raised. In that case,  `SQL%BULK_EXCEPTIONS.COUNT`  returns 1, and  `SQL%BULK_EXCEPTIONS`  contains just one record. If no exception is raised during execution,  `SQL%BULK_EXCEPTIONS.COUNT`  returns 0.

**_Example 11-5 Bulk Operation That Continues Despite Exceptions_**

The following example shows how you can perform a number of DML operations, without stopping if some operations encounter errors:

CREATE TABLE emp2 AS SELECT * FROM employees;
DECLARE
   TYPE NumList IS TABLE OF NUMBER;
-- The zeros in this list will cause divide-by-zero errors.
   num_tab NumList := NumList(10,0,11,12,30,0,20,199,2,0,9,1);
   errors  NUMBER;
   dml_errors EXCEPTION;
   PRAGMA exception_init(dml_errors, -24381);
BEGIN
-- SAVE EXCEPTIONS means don't stop if some DELETEs fail.
   FORALL i IN num_tab.FIRST..num_tab.LAST SAVE EXCEPTIONS
      DELETE FROM emp2 WHERE salary > 500000/num_tab(i);
-- If any errors occurred during the FORALL SAVE EXCEPTIONS,
-- a single exception is raised when the statement completes.
EXCEPTION
  WHEN dml_errors THEN -- Now we figure out what failed and why.
   errors := SQL%BULK_EXCEPTIONS.COUNT;
   dbms_output.put_line('Number of DELETE statements that failed: ' || errors);
   FOR i IN 1..errors LOOP
      dbms_output.put_line('Error #' || i || ' occurred during '||
         'iteration #' || SQL%BULK_EXCEPTIONS(i).ERROR_INDEX);
      dbms_output.put_line('Error message is ' ||
         SQLERRM(-SQL%BULK_EXCEPTIONS(i).ERROR_CODE));
   END LOOP;
END;
/
DROP TABLE emp2;

In this example, PL/SQL raised the predefined exception  `ZERO_DIVIDE`  when  `i`  equaled 2, 6, 10. After the  `FORALL`  statement,  `SQL%BULK_EXCEPTIONS.COUNT`  returned 3, and the contents of  `SQL%BULK_EXCEPTIONS`  were (2,1476), (6,1476), and (10,1476). To get the Oracle error message (which includes the code), we negated the value of  `SQL%BULK_EXCEPTIONS(i).ERROR_CODE`  and passed the result to the error-reporting function  `SQLERRM`, which expects a negative number. Here is the output:

  
`Number of errors is 3`  
`Error 1 occurred during iteration 2`  
`Oracle error is ORA-01476: divisor is equal to zero`  
`Error 2 occurred during iteration 6`  
`Oracle error is ORA-01476: divisor is equal to zero`  
`Error 3 occurred during iteration 10`  
`Oracle error is ORA-01476: divisor is equal to zero`

### Retrieving Query Results into Collections with the BULK COLLECT Clause

Using the keywords  `BULK`  `COLLECT`  with a query is a very efficient way to retrieve the result set. Instead of looping through each row, you store the results in one or more collections, in a single operation. You can use these keywords in the  `SELECT`  `INTO`  and  `FETCH`  `INTO`  statements, and the  `RETURNING`  `INTO`  clause.

With the BULK COLLECT clause, all the variables in the  `INTO`  list must be collections. The table columns can hold scalar or composite values, including object types. The following example loads two entire database columns into nested tables:

DECLARE
   TYPE NumTab IS TABLE OF employees.employee_id%TYPE;
   TYPE NameTab IS TABLE OF employees.last_name%TYPE;
   enums NumTab;   -- No need to initialize the collections.
   names NameTab;  -- Values will be filled in by the SELECT INTO.
   PROCEDURE print_results IS
   BEGIN
      dbms_output.put_line('Results:');
      FOR i IN enums.FIRST .. enums.LAST
      LOOP
         dbms_output.put_line('  Employee #' || enums(i) || ': ' ||
            names(i));
      END LOOP;
   END;
BEGIN
   SELECT employee_id, last_name -- Retrieve data for 10 arbitrary employees.
      BULK COLLECT INTO enums, names
      FROM employees WHERE ROWNUM < 11;
-- The data has all been brought into memory by BULK COLLECT.
-- No need to FETCH each row from the result set.
   print_results;

   SELECT employee_id, last_name -- Retrieve approximately 20% of all rows
      BULK COLLECT INTO enums, names
      FROM employees SAMPLE (20);
   print_results;
END;
/

The collections are initialized automatically. Nested tables and associative arrays are extended to hold as many elements as needed. If you use varrays, all the return values must fit in the varray's declared size. Elements are inserted starting at index 1, overwriting any existing elements.

Since the processing of the  `BULK COLLECT INTO`  clause is similar to a  `FETCH`  loop, it does not raise a  `NO_DATA_FOUND`  exception if no rows match the query. You must check whether the resulting nested table or varray is null, or if the resulting associative array has no elements.

To prevent the resulting collections from expanding without limit, you can use the pseudocolumn  `ROWNUM`  to limit the number of rows processed. Or, you can use the  `SAMPLE`  clause to retrieve a random sample of rows.

DECLARE
   TYPE SalList IS TABLE OF emp.sal%TYPE;
   sals SalList;
BEGIN
-- Limit the number of rows to 100.
   SELECT sal BULK COLLECT INTO sals FROM emp
      WHERE ROWNUM <= 100;
-- Retrieve 10% (approximately) of the rows in the table.
   SELECT sal BULK COLLECT INTO sals FROM emp SAMPLE 10;

END;
/

You can process very large result sets by fetching a specified number of rows at a time from a cursor, as shown in the following sections.

#### Examples of Bulk-Fetching from a Cursor

**_Example 11-6 Bulk-Fetching from a Cursor Into One or More Collections_**

You can fetch from a cursor into one or more collections:

DECLARE
   TYPE NameList IS TABLE OF employees.last_name%TYPE;
   TYPE SalList IS TABLE OF employees.salary%TYPE;
   CURSOR c1 IS SELECT last_name, salary FROM employees WHERE salary > 10000;
   names NameList;
   sals  SalList;
   TYPE RecList IS TABLE OF c1%ROWTYPE;
   recs RecList;

   PROCEDURE print_results IS
   BEGIN
      dbms_output.put_line('Results:');
      IF names IS NULL OR names.COUNT = 0 THEN
         RETURN; -- Don't print anything if collections are empty.
      END IF;
      FOR i IN names.FIRST .. names.LAST
      LOOP
         dbms_output.put_line('  Employee ' || names(i) || ': $' ||
            sals(i));
      END LOOP;
   END;
BEGIN
   dbms_output.put_line('--- Processing all results at once ---');
   OPEN c1;
   FETCH c1 BULK COLLECT INTO names, sals;
   CLOSE c1;
   print_results;

   dbms_output.put_line('--- Processing 7 rows at a time ---');
   OPEN c1;
   LOOP
      FETCH c1 BULK COLLECT INTO names, sals LIMIT 7;
      EXIT WHEN c1%NOTFOUND;
      print_results;
   END LOOP;
-- Loop exits when fewer than 7 rows are fetched. Have to
-- process the last few. Need extra checking inside PRINT_RESULTS
-- in case it is called when the collection is empty.
   print_results;
   CLOSE c1;

   dbms_output.put_line('--- Fetching records rather than columns ---');
   OPEN c1;
   FETCH c1 BULK COLLECT INTO recs;
   FOR i IN recs.FIRST .. recs.LAST
   LOOP
-- Now all the columns from the result set come from a single record.
      dbms_output.put_line('  Employee ' || recs(i).last_name || ': $'
         || recs(i).salary);
   END LOOP;
END;
/

**_Example 11-7 Bulk-Fetching from a Cursor Into a Collection of Records_**

You can fetch from a cursor into a collection of records:

DECLARE
   TYPE DeptRecTab IS TABLE OF dept%ROWTYPE;
   dept_recs DeptRecTab;
   CURSOR c1 IS
      SELECT deptno, dname, loc FROM dept WHERE deptno > 10;
BEGIN
   OPEN c1;
   FETCH c1 BULK COLLECT INTO dept_recs;
END;
/

#### Limiting the Rows for a Bulk FETCH Operation with the LIMIT Clause

The optional  `LIMIT`  clause, allowed only in bulk  `FETCH`  statements, limits the number of rows fetched from the database.

In the example below, with each iteration of the loop, the  `FETCH`  statement fetches ten rows (or less) into index-by table  `empnos`. The previous values are overwritten.

DECLARE
   TYPE NumTab IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
   CURSOR c1 IS SELECT empno FROM emp;
   empnos NumTab;
   rows   NATURAL := 10;
BEGIN
   OPEN c1;
   LOOP
      /* The following statement fetches 10 rows (or less). */
      FETCH c1 BULK COLLECT INTO empnos LIMIT rows;
      EXIT WHEN c1%NOTFOUND;
      ...
   END LOOP;
   CLOSE c1;
END;
/

#### Retrieving DML Results into a Collection with the RETURNING INTO Clause

You can use the  `BULK`  `COLLECT`  clause in the  `RETURNING`  `INTO`  clause of an  `INSERT`,  `UPDATE`, or  `DELETE`  statement:

CREATE TABLE emp2 AS SELECT * FROM employees;
DECLARE
   TYPE NumList IS TABLE OF employees.employee_id%TYPE;
   enums NumList;
   TYPE NameList IS TABLE OF employees.last_name%TYPE;
   names NameList;
BEGIN
   DELETE FROM emp2 WHERE department_id = 30
      RETURNING employee_id, last_name BULK COLLECT INTO enums, names;
   dbms_output.put_line('Deleted ' || SQL%ROWCOUNT || ' rows:');
   FOR i IN enums.FIRST .. enums.LAST
   LOOP
      dbms_output.put_line('Employee #' || enums(i) || ': ' || names(i));
   END LOOP;
END;
/
DROP TABLE emp2;

#### Using FORALL and BULK COLLECT Together

You can combine the  `BULK`  `COLLECT`  clause with a  `FORALL`  statement. The output collections are built up as the  `FORALL`  statement iterates.

In the following example, the  `EMPNO`  value of each deleted row is stored in the collection  `ENUMS`. The collection  `DEPTS`  has 3 elements, so the  `FORALL`  statement iterates 3 times. If each  `DELETE`  issued by the  `FORALL`  statement deletes 5 rows, then the collection  `ENUMS`, which stores values from the deleted rows, has 15 elements when the statement completes:

CREATE TABLE emp2 AS SELECT * FROM employees;
DECLARE
   TYPE NumList IS TABLE OF NUMBER;
   depts NumList := NumList(10,20,30);
   TYPE enum_t IS TABLE OF employees.employee_id%TYPE;
   TYPE dept_t IS TABLE OF employees.department_id%TYPE;
   e_ids enum_t;
   d_ids dept_t;
BEGIN
   FORALL j IN depts.FIRST..depts.LAST
      DELETE FROM emp2 WHERE department_id = depts(j)
         RETURNING employee_id, department_id BULK COLLECT INTO e_ids, d_ids;
   dbms_output.put_line('Deleted ' || SQL%ROWCOUNT || ' rows:');
   FOR i IN e_ids.FIRST .. e_ids.LAST
   LOOP
      dbms_output.put_line('Employee #' || e_ids(i) || ' from dept #' || d_ids(i));
   END LOOP;
END;
/
DROP TABLE emp2;

The column values returned by each execution are added to the values returned previously. If you use a  `FOR`  loop instead of the  `FORALL`  statement, the set of returned values is overwritten by each  `DELETE`  statement.

You cannot use the  `SELECT`  ...  `BULK`  `COLLECT`  statement in a  `FORALL`  statement.

#### Using Host Arrays with Bulk Binds

Client-side programs can use anonymous PL/SQL blocks to bulk-bind input and output host arrays. This is the most efficient way to pass collections to and from the database server.

Host arrays are declared in a host environment such as an OCI or a Pro*C program and must be prefixed with a colon to distinguish them from PL/SQL collections. In the example below, an input host array is used in a  `DELETE`  statement. At run time, the anonymous PL/SQL block is sent to the database server for execution.

DECLARE
   ...
BEGIN
   -- assume that values were assigned to the host array
   -- and host variables in the host environment
   FORALL i IN :lower..:upper
      DELETE FROM employees WHERE department_id = :depts(i);
   COMMIT;
END;
/

## Writing Computation-Intensive Programs in PL/SQL

The  `BINARY_FLOAT`  and  `BINARY_DOUBLE`  datatypes make it practical to write PL/SQL programs to do number-crunching, for scientific applications involving floating-point calculations. These datatypes behave much like the native floating-point types on many hardware systems, with semantics derived from the IEEE-754 floating-point standard.

The way these datatypes represent decimal data make them less suitable for financial applications, where precise representation of fractional amounts is more important than pure performance.

The  `PLS_INTEGER`  and  `BINARY_INTEGER`  datatypes are PL/SQL-only datatypes that are more efficient than the SQL datatypes  `NUMBER`  or  `INTEGER`  for integer arithmetic. You can use  `PLS_INTEGER`  to write pure PL/SQL code for integer arithmetic, or convert  `NUMBER`  or  `INTEGER`  values to  `PLS_INTEGER`  for manipulation by PL/SQL.

In previous releases,  `PLS_INTEGER`  was more efficient than  `BINARY_INTEGER`. Now, they have similar performance, but you might still prefer  `PLS_INTEGER`  if your code might be run under older database releases.

Within a package, you can write overloaded versions of procedures and functions that accept different numeric parameters. The math routines can be optimized for each kind of parameter (`BINARY_FLOAT`,  `BINARY_DOUBLE`,  `NUMBER`,  `PLS_INTEGER`), avoiding unnecessary conversions.

The built-in math functions such as  `SQRT`,  `SIN`,  `COS`, and so on already have fast overloaded versions that accept  `BINARY_FLOAT`  and  `BINARY_DOUBLE`  parameters. You can speed up math-intensive code by passing variables of these types to such functions, and by calling the  `TO_BINARY_FLOAT`  or  `TO_BINARY_DOUBLE`  functions when passing expressions to such functions.

## Tuning Dynamic SQL with EXECUTE IMMEDIATE and Cursor Variables

Some programs (a general-purpose report writer for example) must build and process a variety of SQL statements, where the exact text of the statement is unknown until run time. Such statements probably change from execution to execution. They are called  _dynamic_  SQL statements.

Formerly, to execute dynamic SQL statements, you had to use the supplied package  `DBMS_SQL`. Now, within PL/SQL, you can execute any kind of dynamic SQL statement using an interface called  _native dynamic SQL_. The main PL/SQL features involved are the  `EXECUTE IMMEDIATE`  statement and cursor variables (also known as  `REF CURSOR`s).

Native dynamic SQL code is more compact and much faster than calling the  `DBMS_SQL`  package. The following example declares a cursor variable, then associates it with a dynamic  `SELECT`  statement:

DECLARE
   TYPE EmpCurTyp IS REF CURSOR;
   emp_cv   EmpCurTyp;
   my_ename VARCHAR2(15);
   my_sal   NUMBER := 1000;
   table_name VARCHAR2(30) := 'employees';
BEGIN
   OPEN emp_cv FOR 'SELECT last_name, salary FROM ' || table_name ||
      ' WHERE salary > :s' USING my_sal;
   CLOSE emp_cv;
END;
/

For more information, see Chapter 7.

## Tuning PL/SQL Procedure Calls with the NOCOPY Compiler Hint

By default,  `OUT`  and  `IN`  `OUT`  parameters are passed by value. The values of any  `IN OUT`  parameters are copied before the subprogram is executed. During subprogram execution, temporary variables hold the output parameter values. If the subprogram exits normally, these values are copied to the actual parameters. If the subprogram exits with an unhandled exception, the original parameters are unchanged.

When the parameters represent large data structures such as collections, records, and instances of object types, this copying slows down execution and uses up memory. In particular, this overhead applies to each call to an object method: temporary copies are made of all the attributes, so that any changes made by the method are only applied if the method exits normally.

To avoid this overhead, you can specify the  `NOCOPY`  hint, which allows the PL/SQL compiler to pass  `OUT`  and  `IN`  `OUT`  parameters by reference. If the subprogram exits normally, the behavior is the same as normal. If the subprogram exits early with an exception, the values of  `OUT`  and  `IN OUT`  parameters (or object attributes) might still change. To use this technique, ensure that the subprogram handles all exceptions.

The following example asks the compiler to pass  `IN`  `OUT`  parameter  `MY_STAFF`  by reference, to avoid copying the varray on entry to and exit from the subprogram:

DECLARE
   TYPE Staff IS VARRAY(200) OF Employee;
   PROCEDURE reorganize (my_staff IN OUT NOCOPY Staff) IS ...
BEGIN
   NULL;
END;
/

The following example loads 25,000 records into a local nested table, which is passed to two local procedures that do nothing. A call to the procedure that uses  `NOCOPY`  takes much less time.

DECLARE
   TYPE EmpTabTyp IS TABLE OF employees%ROWTYPE;
   emp_tab EmpTabTyp := EmpTabTyp(NULL);  -- initialize
   t1 NUMBER;
   t2 NUMBER;
   t3 NUMBER;
   PROCEDURE get_time (t OUT NUMBER) IS
     BEGIN t := dbms_utility.get_time; END;
   PROCEDURE do_nothing1 (tab IN OUT EmpTabTyp) IS
     BEGIN NULL; END;
   PROCEDURE do_nothing2 (tab IN OUT NOCOPY EmpTabTyp) IS
     BEGIN NULL; END;
BEGIN
   SELECT * INTO emp_tab(1) FROM employees WHERE employee_id = 100;
   emp_tab.EXTEND(49999, 1);  -- copy element 1 into 2..50000
   get_time(t1);
   do_nothing1(emp_tab);  -- pass IN OUT parameter
   get_time(t2);
   do_nothing2(emp_tab);  -- pass IN OUT NOCOPY parameter
   get_time(t3);
   dbms_output.put_line('Call Duration (secs)');
   dbms_output.put_line('--------------------');
   dbms_output.put_line('Just IN OUT: ' || TO_CHAR((t2 - t1)/100.0));
   dbms_output.put_line('With NOCOPY: ' || TO_CHAR((t3 - t2))/100.0);
END;
/

### Restrictions on NOCOPY

The use of  `NOCOPY`  increases the likelihood of parameter aliasing. For more information, see "Understanding Subprogram Parameter Aliasing".

Remember,  `NOCOPY`  is a hint, not a directive. In the following cases, the PL/SQL compiler ignores the  `NOCOPY`  hint and uses the by-value parameter-passing method; no error is generated:

-   The actual parameter is an element of an associative array. This restriction does not apply if the parameter is an entire associative array.
    
-   The actual parameter is constrained, such as by scale or  `NOT`  `NULL`. This restriction does not apply to size-constrained character strings. This restriction does not extend to constrained elements or attributes of composite types.
    
-   The actual and formal parameters are records, one or both records were declared using  `%ROWTYPE`  or  `%TYPE`, and constraints on corresponding fields in the records differ.
    
-   The actual and formal parameters are records, the actual parameter was declared (implicitly) as the index of a cursor  `FOR`  loop, and constraints on corresponding fields in the records differ.
    
-   Passing the actual parameter requires an implicit datatype conversion.
    
-   The subprogram is called through a database link or as an external procedure.
    

## Compiling PL/SQL Code for Native Execution

You can speed up PL/SQL procedures by compiling them into native code residing in shared libraries. The procedures are translated into C code, then compiled with your usual C compiler and linked into the Oracle process.

You can use this technique with both the supplied Oracle packages, and procedures you write yourself. Procedures compiled this way work in all server environments, such as the shared server configuration (formerly known as multi-threaded server) and Oracle Real Application Clusters.

  
**Before You Begin**  

If you are a first-time user of native PL/SQL compilation, try it first with a test database, before proceeding to a production environment.

Always back up your database before configuring the database for PL/SQL native compilation. If you find that the performance benefit is outweighed by extra compilation time, it might be faster to restore from a backup than to recompile everything in interpreted mode.

Some of the setup steps require DBA authority. You must change the values of some initialization parameters, and create a new directory on the database server, preferably near the data files for the instance. The database server also needs a C compiler; on a cluster, the compiler is needed on each node. Even if you can test out these steps yourself on a development machine, you will generally need to consult with a DBA and enlist their help to use native compilation on a production server.

Contact your system administrator to ensure that you have the required C compiler on your operating system, and find the path for its location. Use a text editor such as vi to open the file  `$ORACLE_HOME/plsql/spnc_commands`, and make sure the command templates are correct. Generally, you should not need to make any changes here, just confirm that the setup is correct.

  
**Determining Whether to Use PL/SQL Native Compilation**  

PL/SQL native compilation provides the greatest performance gains for computation-intensive procedural operations. Examples of such operations are data warehouse applications, and applications with extensive server-side transformations of data for display. In such cases, expect speed increases of up to 30%.

Because this technique cannot do much to speed up SQL statements called from PL/SQL, it is most effective for compute-intensive PL/SQL procedures that do not spend most of their time executing SQL. You can test to see how much performance gain you can get by enabling PL/SQL native compilation.

It takes longer to compile program units with native compilation than to use the default interpreted mode. You might turn off native compilation during the busiest parts of the development cycle, where code is being frequently recompiled.

When you have decided that you will have significant performance gains in database operations using PL/SQL native compilation, Oracle Corporation recommends that you compile the whole database using the  `NATIVE`  setting. Compiling all the PL/SQL code in the database means you see the speedup in your own code, and in calls to all the built-in PL/SQL packages.

  
**How PL/SQL Native Compilation Works**  

If you do not use native compilation, each PL/SQL program unit is compiled into an intermediate form, machine-readable code (m-code). The m-code is stored in the database dictionary and interpreted at run time.

With PL/SQL native compilation, the PL/SQL statements are turned into C code that bypasses all the runtime interpretation, giving faster runtime performance.

PL/SQL uses the command file  `$ORACLE_HOME/plsql/spnc_commands`, and the supported operating system C compiler and linker, to compile and link the resulting C code into shared libraries. The shared libraries are stored inside the data dictionary, so that they can be backed up automatically and are protected from being deleted. These shared library files are copied to the filesystem and are loaded and run when the PL/SQL subprogram is invoked. If the files are deleted from the filesystem while the database is shut down, or if you change the directory that holds the libraries, they are extracted again automatically.

Although PL/SQL program units that just call SQL statements might see little or no speedup, natively compiled PL/SQL is always at least as fast as the corresponding interpreted code. The compiled code makes the same library calls as the interpreted code would, so its behavior is exactly the same.

  
**Format of the spnc_commands File**  

The spnc_commands file, in the  `$ORACLE_HOME/plsql`  directory, contains the templates for the commands to compile and link each program. Some special names such as  `%(src)`  are predefined, and are replaced by the corresponding filename. The variable  `$(ORACLE_HOME)`  is replaced by the location of the Oracle home directory. You can include comment lines, starting with a  `#`  character. The file contains comments that explain all the special notation.

The  `spnc_commands`  file contains a predefined path for the C compiler, depending on the particular operating system. (One specific compiler is supported on each operating system.) In most cases, you should not need to change this path, but you might if you the system administrator has installed it in another location.

  
**System-Level Initialization Parameters for PL/SQL Native Compilation**  

The following table lists the initialization parameters you must set before using PL/SQL native compilation. They can be set only at the system level, not by an  `ALTER SESSION`  command. You cannot use variables such as  `ORACLE_HOME`  in the values; use the full path instead.

  

****Note**:**

The examples in this section for setting system parameters for PL/SQL native compilation assume a system using a server parameter file (SPFILE).

If you use a text initialization parameter file (PFILE, or  `init`_`sid`_`.ora`), ensure that you change parameters in your initialization parameter file, as indicated in the following table.

  

**Parameter**

**Characteristics**

PLSQL_NATIVE_LIBRARY_DIR

The full path and directory name used to store the shared libraries that contain natively compiled PL/SQL code.

In accordance with optimal flexible architecture (OFA) rules, Oracle Corporation recommends that you create the shared library directory as a subdirectory where the data files are located.

For security reasons, only the users  `oracle`  and  `root`  should have write privileges for this directory.

PLSQL_NATIVE_LIBRARY_SUBDIR_COUNT

The number of subdirectories in the directory specified by the parameter  `PLSQL_NATIVE_LIBRARY_DIR`.

Optional; use if the number of natively compiled program units exceeds 15000. If you need to set this option, refer to the section  ["Setting Up PL/SQL Native Library Subdirectories"](https://docs.oracle.com/cd/B13789_01/appdev.101/b10807/12_tune.htm#i54161).

  

  
**Session-Level Initialization Parameter for PL/SQL Native Compilation**  

The parameter  `PLSQL_CODE_TYPE`  determines whether PL/SQL code is natively compiled or interpreted. The default setting is  `INTERPRETED`. To enable PL/SQL native compilation, set the value of  `PLSQL_CODE_TYPE`  to  `NATIVE`.

If you compile the whole database as  `NATIVE`, Oracle Corporation recommends that you set  `PLSQL_CODE_TYPE`  at the system level.

Use the following syntax to set this parameter:

alter session set plsql_code_type='NATIVE';
alter session set plsql_code_type='INTERPRETED';
alter system set plsql_code_type='NATIVE';
alter system set plsql_code_type='INTERPRETED';

-     
    
    **See Also:**
    
    _[Oracle Database Reference](https://docs.oracle.com/cd/B13789_01/server.101/b10755/toc.htm)_  for complete details about the initialization parameters and data dictionary views.
    
      
    

  
**Setting Up and Using PL/SQL Native Compilation**  

To speed up one or more subprograms through native compilation:

1.  Set up the  `PLSQL_NATIVE_LIBRARY_DIR`  initialization parameter, and optionally the  `PLSQL_NATIVE_LIBRARY_SUBDIR_COUNT`  initialization parameter, as described above.
    
2.  Use the  `ALTER SYSTEM`  or  `ALTER SESSION`  command, or update your initialization file, to set the parameter  `PLSQL_CODE_TYPE`  to the value  `NATIVE`.
    
3.  Compile one or more subprograms, using one of these methods:
    
    -   Use  `CREATE OR REPLACE`  to create or recompile the subprogram.
        
    -   Use the  `ALTER PROCEDURE`,  `ALTER FUNCTION`, or  `ALTER PACKAGE`  command with the  `COMPILE`  option to recompile the subprogram or the entire package. (You can also use the clause  `PLSQL_CODE_TYPE = NATIVE`  with the  `ALTER`  statements to affect specific subprograms without changing the initialization parameter for the whole session.)
        
    -   Drop the subprogram and create it again.
        
    -   Run one of the SQL*Plus scripts that creates a set of Oracle-supplied packages.
        
    -   Create a database using a preconfigured initialization file with  `PLSQL_CODE_TYPE=NATIVE`. During database creation, the  `UTLIRP`  script is run to compile all the Oracle-supplied packages.
        
4.  To be sure that the process worked, you can query the data dictionary to see that a procedure is compiled for native execution. To check whether an existing procedure is compiled for native execution or not, you can query the data dictionary views  `USER_PLSQL_OBJECT_SETTINGS, DBA_PLSQL_OBJECT_SETTINGS`, and  `ALL_PLSQL_OBJECT_SETTINGS`. For example, to check the status of the procedure  `MY_PROC`, you could enter:
    
    CREATE OR REPLACE PROCEDURE my_proc AS BEGIN NULL; END;
    /
    SELECT plsql_code_type FROM user_plsql_object_settings WHERE name = 'MY_PROC';
    DROP PROCEDURE my_proc;
    
    The  `CODE_TYPE`  column has a value of  `NATIVE`  for procedures that are compiled for native execution, and  `INTERPRETED`  otherwise.
    

After the procedures are compiled and turned into shared libraries, they are automatically linked into the Oracle process. You do not need to restart the database, or move the shared libraries to a different location. You can call back and forth between stored procedures, whether they are all interpreted, all compiled for native execution, or a mixture of both.

  
**Dependencies, Invalidation and Revalidation**  

This recompilation happens automatically invalidated, such as when a table that it depends on is re-created.

If an object on which a natively compiled PL/SQL subprogram depends changes, the subprogram is invalidated. The next time the same subprogram is called, the database recompiles the subprogram automatically. Because the  `PLSQL_CODE_TYPE`  setting is stored inside the library unit for each subprogram, the automatic recompilation uses this stored setting for code type.

The stored settings are only used when recompiling as part of revalidation. If a PL/SQL subprogram is explicitly compiled through the SQL commands "`create or replace`" or "`alter...compile`", the current session setting is used.

The generated shared libraries are stored in the database, in the  `SYSTEM`  tablespace. The first time a natively compiled procedure is executed, the corresponding shared library is copied from the database to the directory specified by the initialization parameter  `PLSQL_NATIVE_LIBRARY_DIR`.

  
**Setting Up Databases for PL/SQL Native Compilation**  

Use the procedures in this section to set up an entire database for PL/SQL native compilation. The performance benefits apply to all the built-in PL/SQL packages, which are used for many database operations.

  
**Creating a New Database for PL/SQL Native Compilation**  

If you use Database Configuration Assistant, use it to set the initialization parameters required for PL/SQL native compilation, as described in the preceding section,  ["System-Level Initialization Parameters for PL/SQL Native Compilation"](https://docs.oracle.com/cd/B13789_01/appdev.101/b10807/12_tune.htm#i54167).

To find the supported C compiler on your operating system. refer to the table "Precompilers and Tools Restrictions and Requirements" in the installation guide for your operating system. Determine from your system administrator where it is located on your system. You will need to check that this path is correct in the  `spnc_commands`  file.

Determine if you have so many PL/SQL program units that you need to set the initialization parameter  `PLSQL_NATIVE_DIR_SUBDIR_COUNT`, and create PL/SQL native library subdirectories if necessary. By default, PL/SQL program units are kept in one directory. If the number of program units exceeds 15,000, the operating system begins to impose performance limits. To work around this problem, Oracle Corporation recommends that you spread the PL/SQL program units among subdirectories.

If you have set up a test database, use this SQL query to determine how many PL/SQL program units you are using:

select count (*) from DBA_PLSQL_OBJECTS;

If the count returned by this query is greater than 15,000, complete the procedure described in the section  ["Setting Up PL/SQL Native Library Subdirectories"](https://docs.oracle.com/cd/B13789_01/appdev.101/b10807/12_tune.htm#i54161).

  
**Modifying an Existing Database for PL/SQL Native Compilation**  

To natively compile an existing database, complete the following procedure:

1.  Contact your system administrator to ensure that you have the required C compiler on your operating system, and find the path for its location. Use a text editor such as vi to open the file  `spnc_commands`, and make sure the command templates are correct.
    
2.  As the  `oracle`  user, create the PL/SQL native library directory for each Oracle database.
    
      
    
    **Note:**
    
    You must set up PL/SQL libraries for each Oracle database. Shared libraries (`.so`  and  `.dll`  files) are logically connected to the database. They cannot be shared between databases. If you set up PL/SQL libraries to be shared, the databases will be corrupted.
    
    Create a directory in a secure place, in accordance with OFA rules, to prevent  `.so`  and  `.dll`  files from unauthorized access.
    
    In addition, ensure that the compiler executables used for PL/SQL native compilation are writable only by a properly secured user.
    
    The original copies of the shared libraries are stored inside the database, so they are backed up automatically with the database.
    
      
    
3.  Using SQL, set the initialization parameter  `PLSQL_NATIVE_LIBRARY_DIR`  to the full path to the PL/SQL native library.
    
    For example, if the path to the PL/SQL native library directory is  `/oracle/oradata/mydb/natlib`, enter the following:
    
    alter system set plsql_native_library_dir='/oracle/oradata/mydb/natlib'
    
4.  Determine if you need to set the initialization parameter  `PLSQL_NATIVE_DIR_SUBDIR_COUNT`, and create PL/SQL native library subdirectories if necessary.
    
    By default, PL/SQL program units are kept in one directory. However, if the number of program units exceeds 15000, then the operating system begins to impose performance limits. To work around this problem, Oracle Corporation recommends that you spread the PL/SQL program units in subdirectories.
    
    If you have an existing database that you will migrate to the new installation, or if you have set up a test database, use the following SQL query to determine how many PL/SQL program units you are using:
    
    select count (*) from DBA_PLSQL_OBJECTS;
    
    If the count returned by this query is greater than 15,000, complete the procedure described in the following section,  ["Setting Up PL/SQL Native Library Subdirectories"](https://docs.oracle.com/cd/B13789_01/appdev.101/b10807/12_tune.htm#i54161).
    
5.  Set the remaining required initialization parameters as listed in the table in the preceding section  ["System-Level Initialization Parameters for PL/SQL Native Compilation"](https://docs.oracle.com/cd/B13789_01/appdev.101/b10807/12_tune.htm#i54167).
    
6.  Create the following stored procedure to confirm that PL/SQL native compilation is enabled:
    
    CREATE OR REPLACE PROCEDURE Hello AS
    BEGIN
       dbms_output.put_line ( 'This output is from a natively compiled procedure.' );
    END Hello;
    /
    
7.  Run the stored procedure:
    
    CALL Hello();
    
    If the program does not return the expected output, contact Oracle Support for assistance. (Remember to  `SET SERVEROUTPUT ON`  in SQL*Plus before running the procedure.)
    
8.  Recompile all the PL/SQL subprograms in the database. The script  `$ORACLE_HOME/admin/utlirp.sql`  is typically used here.
    

  
**Setting Up PL/SQL Native Library Subdirectories**  

If you need to set up PL/SQL native library subdirectories, use the following procedure:

1.  Create subdirectories sequentially in the form of d0, d1, d2, d3...dx, where x is the total number of directories. Oracle Corporation recommends that you use a script for this task. For example, you might run a PL/SQL block like the following, save its output to a file, then run that file as a shell script:
    
    BEGIN
      FOR j IN 0..999
      LOOP
        dbms_output.put_line ( 'mkdir d' || TO_CHAR(j) );
      END LOOP;
    END;
    /
    
2.  Set the initialization parameter  `PLSQL_NATIVE_DIR_COUNT`  to the number of subdirectories you have created. For example, if you created 1000 subdirectories, enter the following SQL statement in SQL*Plus:
    
    alter system set plsql_native_library_subdir_count=1000;
    

**_Example 11-8 Compiling a PL/SQL Procedure for Native Execution_**

alter session set plsql_code_type='NATIVE';
CREATE OR REPLACE PROCEDURE hello_native
AS
BEGIN
  dbms_output.put_line('Hello world.');
  dbms_output.put_line('Today is ' || TO_CHAR(SYSDATE) || '.');
END;
/
select plsql_code_type from user_plsql_object_settings
   where name = 'HELLO_NATIVE';
alter session set plsql_code_type='INTERPRETED';

The procedure is immediately available to call, and runs as a shared library directly within the Oracle process. If any errors occur during compilation, you can see them using the  `USER_ERRORS`  view or the  `SHOW ERRORS`  command in SQL*Plus.

  
**Limitations of Native Compilation**  

-   Debugging tools for PL/SQL do not handle procedures compiled for native execution.
    
-   When many procedures and packages (typically, over 15000) are compiled for native execution, the large number of shared objects in a single directory might affect system performance. See  ["Setting Up PL/SQL Native Library Subdirectories"](https://docs.oracle.com/cd/B13789_01/appdev.101/b10807/12_tune.htm#i54161)  for a workaround.
    

  
**Real Application Clusters and PL/SQL Native Compilation**  

Because any node might need to compile a PL/SQL subprogram, each node in the cluster needs a C compiler and correct settings and paths in the  `$ORACLE_HOME/plsql/spnc_commands`  file.

When you use PLSQL native compilation in a Real Application Clusters (RAC) environment, the original copies of the shared library files are stored in the databases, and these files are automatically propagated to all nodes in the cluster. You do not need to do any copying of libraries for this feature to work.

The reason for using a server parameter file (SPFILE) in the examples in this section, is to make sure that all nodes of a RAC cluster use the same settings for the parameters that control PL/SQL native compilation.

## Setting Up Transformation Pipelines with Table Functions

This section describes how to chain together special kinds of functions known as table functions. These functions are used in situations such as data warehousing to apply multiple transformations to data.

Major topics covered are:

-   [Overview of Table Functions](https://docs.oracle.com/cd/B13789_01/appdev.101/b10807/12_tune.htm#i52954)
    
-   [Writing a Pipelined Table Function](https://docs.oracle.com/cd/B13789_01/appdev.101/b10807/12_tune.htm#i53109)
    

### Overview of Table Functions

Table functions are functions that produce a collection of rows (either a nested table or a varray) that can be queried like a physical database table or assigned to a PL/SQL collection variable. You can use a table function like the name of a database table, in the  `FROM`  clause of a query, or like a column name in the  `SELECT`  list of a query.

A table function can take a collection of rows as input. An input collection parameter can be either a collection type (such as a  `VARRAY`  or a PL/SQL table) or a  `REF CURSOR`.

Execution of a table function can be parallelized, and returned rows can be streamed directly to the next process without intermediate staging. Rows from a collection returned by a table function can also be  **pipelined**—that is, iteratively returned as they are produced instead of in a batch after all processing of the table function's input is completed.

Streaming, pipelining, and parallel execution of table functions can improve performance:

-   By enabling multi-threaded, concurrent execution of table functions
    
-   By eliminating intermediate staging between processes
    
-   By improving query response time: With non-pipelined table functions, the entire collection returned by a table function must be constructed and returned to the server before the query can return a single result row. Pipelining enables rows to be returned iteratively, as they are produced. This also reduces the memory that a table function requires, as the object cache does not need to materialize the entire collection.
    
-   By iteratively providing result rows from the collection returned by a table function as the rows are produced instead of waiting until the entire collection is staged in tables or memory and then returning the entire collection.
    

**_Example 11-9 Example: Querying a Table Function_**

The following example shows a table function  `GetBooks`  that takes a  `CLOB`  as input and returns an instance of the collection type  `BookSet_t`. The  `CLOB`  column stores a catalog listing of books in some format (either proprietary or following a standard such as XML). The table function returns all the catalogs and their corresponding book listings.

The collection type  `BookSet_t`  is defined as:

CREATE TYPE Book_t AS OBJECT ( name VARCHAR2(100), author VARCHAR2(30), abstract VARCHAR2(1000));
/
CREATE TYPE BookSet_t AS TABLE OF Book_t;
/
-- The CLOBs are stored in a table Catalogs:
CREATE TABLE Catalogs ( name VARCHAR2(30), cat CLOB );

Function  `GetBooks`  is defined as follows:

CREATE FUNCTION GetBooks(a CLOB) RETURN BookSet_t;
/

The query below returns all the catalogs and their corresponding book listings.

SELECT c.name, Book.name, Book.author, Book.abstract
  FROM Catalogs c, TABLE(GetBooks(c.cat)) Book;

**_Example 11-10 Example: Assigning the Result of a Table Function_**

The following example shows how you can assign the result of a table function to a PL/SQL collection variable. Because the table function is called from the  `SELECT`  list of the query, you do not need the  `TABLE`  keyword.

CREATE TYPE numset_t AS TABLE OF NUMBER;
/

CREATE FUNCTION f1(x number) RETURN numset_t PIPELINED IS
BEGIN
  FOR i IN 1..x LOOP
    PIPE ROW(i);
  END LOOP;
  RETURN;
END;
/

-- pipelined function in from clause
select * from table(f1(3));

### Using Pipelined Table Functions for Transformations

A pipelined table function can accept any argument that regular functions accept. A table function that accepts a  `REF CURSOR`  as an argument can serve as a transformation function. That is, it can use the  `REF CURSOR`  to fetch the input rows, perform some transformation on them, and then pipeline the results out.

For example, the following code sketches the declarations that define a  `StockPivot`  function. This function converts a row of the type (`Ticker, OpenPrice, ClosePrice`) into two rows of the form (`Ticker, PriceType, Price`). Calling  `StockPivot`  for the row (`"ORCL", 41, 42`) generates two rows: (`"ORCL", "O", 41`) and (`"ORCL", "C", 42`).

Input data for the table function might come from a source such as table  `StockTable`:

CREATE TABLE StockTable (
  ticker VARCHAR(4),
  open_price NUMBER,
  close_price NUMBER
);

Here are the declarations. See  ["Returning Results from Table Functions"](https://docs.oracle.com/cd/B13789_01/appdev.101/b10807/12_tune.htm#i53120)  for the function bodies.

-- Create the types for the table function's output collection
-- and collection elements
CREATE TYPE TickerType AS OBJECT
(
  ticker VARCHAR2(4),
  PriceType VARCHAR2(1),
  price NUMBER
);
/

CREATE TYPE TickerTypeSet AS TABLE OF TickerType;
/

-- Define the ref cursor type

CREATE PACKAGE refcur_pkg IS
  TYPE refcur_t IS REF CURSOR RETURN StockTable%ROWTYPE;
END refcur_pkg;
/

-- Create the table function

CREATE FUNCTION StockPivot(p refcur_pkg.refcur_t) RETURN TickerTypeSet
PIPELINED ... ;
/

Here is an example of a query that uses the  `StockPivot`  table function:

SELECT * FROM TABLE(StockPivot(CURSOR(SELECT * FROM StockTable)));

In the query above, the pipelined table function  `StockPivot`  fetches rows from the  `CURSOR`  subquery  `SELECT * FROM StockTable`, performs the transformation, and pipelines the results back to the user as a table. The function produces two output rows (collection elements) for each input row.

Note that when a  `CURSOR`  subquery is passed from SQL to a  `REF CURSOR`  function argument as in the example above, the referenced cursor is already open when the function begins executing.

### Writing a Pipelined Table Function

You declare a pipelined table function by specifying the  `PIPELINED`  keyword. This keyword indicates that the function returns rows iteratively. The return type of the pipelined table function must be a collection type, such as a nested table or a varray. You can declare this collection at the schema level or inside a package. Inside the function, you return individual elements of the collection type.

For example, here are declarations for two pipelined table functions. (The function bodies are shown in later examples.)

CREATE FUNCTION GetBooks(cat CLOB) RETURN BookSet_t
  PIPELINED IS ...;
/

CREATE FUNCTION StockPivot(p refcur_pkg.refcur_t) RETURN TickerTypeSet
  PIPELINED IS...;
/

### Returning Results from Table Functions

In PL/SQL, the  `PIPE ROW`  statement causes a table function to pipe a row and continue processing. The statement enables a PL/SQL table function to return rows as soon as they are produced. (For performance, the PL/SQL runtime system provides the rows to the consumer in batches.) For example:

CREATE FUNCTION StockPivot(p refcur_pkg.refcur_t) RETURN TickerTypeSet
PIPELINED IS
  out_rec TickerType := TickerType(NULL,NULL,NULL);
  in_rec p%ROWTYPE;
BEGIN
  LOOP
    FETCH p INTO in_rec;
    EXIT WHEN p%NOTFOUND;
    -- first row
    out_rec.ticker := in_rec.Ticker;
    out_rec.PriceType := 'O';
    out_rec.price := in_rec.OpenPrice;
    PIPE ROW(out_rec);
    -- second row
    out_rec.PriceType := 'C';
    out_rec.Price := in_rec.ClosePrice;
    PIPE ROW(out_rec);
  END LOOP;
  CLOSE p;
  RETURN;
END;
/

In the example, the  `PIPE ROW(out_rec)`  statement pipelines data out of the PL/SQL table function.  `out_rec`  is a record, and its type matches the type of an element of the output collection.

The  `PIPE ROW`  statement may be used only in the body of pipelined table functions; an error is raised if it is used anywhere else. The  `PIPE ROW`  statement can be omitted for a pipelined table function that returns no rows.

A pipelined table function must have a  `RETURN`  statement that does not return a value. The  `RETURN`  statement transfers the control back to the consumer and ensures that the next fetch gets a  `NO_DATA_FOUND`  exception.

Because table functions pass control back and forth to a calling routine as rows areproduced, there is a restriction on combining table functions and  `PRAGMA AUTONOMOUS_TRANSACTION`. If a table function is part of an autonomous transaction, it must  `COMMIT`  or  `ROLLBACK`  before each  `PIPE ROW`  statement, to avoid an error in the calling subprogram.

Oracle has three special SQL datatypes that enable you to dynamically encapsulate and access type descriptions, data instances, and sets of data instances of any other SQL type, including object and collection types. You can also use these three special types to create  **anonymous**  (that is, unnamed) types, including anonymous collection types. The types are  `SYS.ANYTYPE`,  `SYS.ANYDATA`, and  `SYS.ANYDATASET`. The  `SYS.ANYDATA`  type can be useful in some situations as a return value from table functions.

  

**See Also:**

_[PL/SQL Packages and Types Reference](https://docs.oracle.com/cd/B13789_01/appdev.101/b10802/toc.htm)_  for information about the interfaces to the  `ANYTYPE`,  `ANYDATA`, and  `ANYDATASET`  types and about the  `DBMS_TYPES`  package for use with these types.

  

### Pipelining Data Between PL/SQL Table Functions

With serial execution, results are pipelined from one PL/SQL table function to another using an approach similar to co-routine execution. For example, the following statement pipelines results from function  `g`  to function  `f`:

SELECT * FROM TABLE(f(CURSOR(SELECT * FROM TABLE(g()))));

Parallel execution works similarly except that each function executes in a different process (or set of processes).

### Querying Table Functions

Pipelined table functions are used in the  `FROM`  clause of  `SELECT`  statements. The result rows are retrieved by Oracle iteratively from the table function implementation. For example:

SELECT x.Ticker, x.Price
FROM TABLE(StockPivot(  CURSOR(SELECT * FROM StockTable))) x
WHERE x.PriceType='C';

  

**Note:**

A table function returns a collection. In some cases, such as when the top-level query uses  `SELECT *`  and the query refers to a PL/SQL variable or a bind variable, you may need a  `CAST`  operator around the table function to specify the exact return type.

  

### Optimizing Multiple Calls to Table Functions

Multiple invocations of a table function, either within the same query or in separate queries result in multiple executions of the underlying implementation. By default, there is no buffering or reuse of rows.

For example,

SELECT * FROM TABLE(f(...)) t1, TABLE(f(...)) t2
  WHERE t1.id = t2.id;

SELECT * FROM TABLE(f());
SELECT * FROM TABLE(f());

If the function always produces the same result value for each combination of values passed in, you can declare the function  `DETERMINISTIC`, and Oracle automatically buffers rows for it. If the function is not really deterministic, results are unpredictable.

### Fetching from the Results of Table Functions

PL/SQL cursors and ref cursors can be defined for queries over table functions. For example:

OPEN c FOR SELECT * FROM TABLE(f(...));

Cursors over table functions have the same fetch semantics as ordinary cursors.  `REF CURSOR`  assignments based on table functions do not have any special semantics.

However, the SQL optimizer will not optimize across PL/SQL statements. For example:

DECLARE
    r SYS_REFCURSOR;
BEGIN
    OPEN r FOR SELECT * FROM TABLE(f(CURSOR(SELECT * FROM tab)));
    SELECT * BULK COLLECT INTO rec_tab FROM TABLE(g(r));
END;
/

does not execute as well as:

SELECT * FROM TABLE(g(CURSOR(SELECT * FROM
  TABLE(f(CURSOR(SELECT * FROM tab))))));

This is so even ignoring the overhead associated with executing two SQL statements and assuming that the results can be pipelined between the two statements.

### Passing Data with Cursor Variables

You can pass a set of rows to a PL/SQL function in a  `REF CURSOR`  parameter. For example, this function is declared to accept an argument of the predefined weakly typed  `REF CURSOR`  type  `SYS_REFCURSOR`:

FUNCTION f(p1 IN SYS_REFCURSOR) RETURN ... ;

Results of a subquery can be passed to a function directly:

SELECT * FROM TABLE(f(CURSOR(SELECT empno FROM tab)));

In the example above, the  `CURSOR`  keyword is required to indicate that the results of a subquery should be passed as a  `REF CURSOR`  parameter.

A predefined weak  `REF CURSOR`  type  `SYS_REFCURSOR`  is also supported. With  `SYS_REFCURSOR`, you do not need to first create a  `REF CURSOR`  type in a package before you can use it.

To use a strong  `REF CURSOR`  type, you still must create a PL/SQL package and declare a strong  `REF CURSOR`  type in it. Also, if you are using a strong  `REF CURSOR`  type as an argument to a table function, then the actual type of the  `REF CURSOR`  argument must match the column type, or an error is generated. Weak  `REF CURSOR`  arguments to table functions can only be partitioned using the  `PARTITION BY ANY`  clause. You cannot use range or hash partitioning for weak  `REF CURSOR`  arguments.

**_Example 11-11 Example: Using Multiple REF CURSOR Input Variables_**

PL/SQL functions can accept multiple  `REF CURSOR`  input variables:

CREATE FUNCTION g(p1 pkg.refcur_t1, p2 pkg.refcur_t2) RETURN...
  PIPELINED ... ;
/

Function  `g`  can be invoked as follows:

SELECT * FROM TABLE(g(CURSOR(SELECT employee_id FROM tab),
  CURSOR(SELECT * FROM employees));

You can pass table function return values to other table functions by creating a  `REF CURSOR`  that iterates over the returned data:

SELECT * FROM TABLE(f(CURSOR(SELECT * FROM TABLE(g(...)))));

**_Example 11-12 Example: Explicitly Opening a REF CURSOR for a Query_**

You can explicitly open a  `REF CURSOR`  for a query and pass it as a parameter to a table function:

DECLARE
  r SYS_REFCURSOR;
  rec ...;
BEGIN
  OPEN r FOR SELECT * FROM TABLE(f(...));
  -- Must return a single row result set.
  SELECT * INTO rec FROM TABLE(g(r));
END;
/

In this case, the table function closes the cursor when it completes, so your program should not explicitly try to close the cursor.

**_Example 11-13 Example: Using a Pipelined Table Function as an Aggregate Function_**

A table function can compute aggregate results using the input ref cursor. The following example computes a weighted average by iterating over a set of input rows.

DROP TABLE gradereport;
CREATE TABLE gradereport (student VARCHAR2(30), subject VARCHAR2(30), weight NUMBER, grade NUMBER);

INSERT INTO gradereport  VALUES('Mark', 'Physics', 4, 4);
INSERT INTO  gradereport VALUES('Mark','Chemistry', 4,3);
INSERT INTO gradereport VALUES('Mark','Maths', 3,3);
INSERT INTO gradereport VALUES('Mark','Economics', 3,4);

CREATE OR replace TYPE gpa AS TABLE OF NUMBER;
/

CREATE OR replace FUNCTION weighted_average(input_values
sys_refcursor)
RETURN gpa PIPELINED IS
  grade NUMBER;
  total NUMBER := 0;
  total_weight NUMBER := 0;
  weight NUMBER := 0;
BEGIN
-- The function accepts a ref cursor and loops through all the input rows.
  LOOP
     FETCH input_values INTO weight, grade;
     EXIT WHEN input_values%NOTFOUND;
-- Accumulate the weighted average.
     total_weight := total_weight + weight;
     total := total + grade*weight;
  END LOOP;
  PIPE ROW (total / total_weight);
-- The function returns a single result.
  RETURN;
END;
/
show errors;

-- The result comes back as a nested table with a single row.
-- COLUMN_VALUE is a keyword that returns the contents of a nested table.
select weighted_result.column_value from
   table( weighted_average( cursor(select weight,grade from gradereport) ) ) weighted_result;

### Performing DML Operations Inside Table Functions

To execute DML statements, declare a table function with the  `AUTONOMOUS_TRANSACTION`  pragma, which causes the function to execute in a new transaction not shared by other processes:

CREATE FUNCTION f(p SYS_REFCURSOR) return CollType PIPELINED IS
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN NULL; END;
/

During parallel execution, each instance of the table function creates an independent transaction.

### Performing DML Operations on Table Functions

Table functions cannot be the target table in  `UPDATE`,  `INSERT`, or  `DELETE`  statements. For example, the following statements will raise an error:

UPDATE F(CURSOR(SELECT * FROM tab)) SET col = value;
INSERT INTO f(...) VALUES ('any', 'thing');

However, you can create a view over a table function and use  `INSTEAD OF`  triggers to update it. For example:

CREATE VIEW BookTable AS
  SELECT x.Name, x.Author
  FROM TABLE(GetBooks('data.txt')) x;

The following  `INSTEAD OF`  trigger is fired when the user inserts a row into the  `BookTable`  view:

CREATE TRIGGER BookTable_insert
INSTEAD OF INSERT ON BookTable
REFERENCING NEW AS n
FOR EACH ROW
BEGIN
  ...
END;
/
INSERT INTO BookTable VALUES (...);

`INSTEAD OF`  triggers can be defined for all DML operations on a view built on a table function.

### Handling Exceptions in Table Functions

Exception handling in table functions works just as it does with regular functions.

Some languages, such as C and Java, provide a mechanism for user-supplied exception handling. If an exception raised within a table function is handled, the table function executes the exception handler and continues processing. Exiting the exception handler takes control to the enclosing scope. If the exception is cleared, execution proceeds normally.

An unhandled exception in a table function causes the parent transaction to roll back.

[http://www.dba-oracle.com/plsql/](http://www.dba-oracle.com/plsql/)



## SQL Server migration

You will be not able to migrate from SQL 2000 to 2012 directly. Migration from 2000 to 2012 requires an intermediate step.

SQL Server 2012 supports upgrade from only the following versions: SQL 2005 SP4 or SQL 2008 SP2 or SQL 2008 R2 SP1.

If you try to restore a backup database from SQL Server 2000, you will get the error number 3169 with the following message:  _The database was backed up on a server running version %ls. That version is incompatible with this server, which is running version %ls. Either restore the database on a server that supports the backup, or use a backup that is compatible with this server._

You will have to make the migration in two steps:

**- Step 1:**  Make a first migration from SQL 2000 to SQL 2008 for instance. You need to be SQL 2000 SP4, then follow this step :  [Migration SQL Server 2000 to SQL Server 2008  
](http://blogs.technet.com/b/mdegre/archive/2009/07/21/migration-sql-server-2000-to-sql-server-2008.aspx "Migration SQL Server 2000 to SQL Server 2008")  
**- Step 2:**  Make a second migration from SQL Server 2008 to 2012.

  
I invite you to read the following bol :

-  [Supported Version and Edition Upgrades](http://msdn.microsoft.com/en-us/library/ms143393.aspx)  
-  [Use Upgrade Advisor to Prepare for Upgrades](http://msdn.microsoft.com/en-us/library/ms144256.aspx)  
-  [Breaking Changes to Database Engine Features in SQL Server 2012](http://technet.microsoft.com/en-us/library/ms143179(SQL.110).aspx)

Note :  [SQL Server Upgrade Advisor](http://www.microsoft.com/en-us/download/details.aspx?id=29065 "SQL Server&nbsp; Upgrade Advisor")  is available for 2012 : Microsoft Upgrade Advisor analyzes instances of SQL Server 2005, SQL Server 2008, SQL Server 2008 R2 in preparation for upgrading to SQL Server 2012. Upgrade Advisor identifies feature and configuration changes that might affect your upgrade, and it provides links to documentation that describes each identified issue and how to resolve it.

## SQL Server 2000 to 2008

The purpose of this post is to explain the minimum required for the migration of Microsoft SQL Server 2000 to Microsoft SQL Server 2008. You can use the same principles to migrate a Microsoft SQL Server 2005 to Microsoft SQL Server 2008.  
  
  
**1. SQL server 2008 upgrade advisor**  
  
**1.1. Before migrating**  
Microsoft provides a tool called "Microsoft SQL Server 2008 Upgrade Advisor" to alert you of any changes in design between Microsoft SQL Server 2000/2005 and SQL Server 2008. It is strongly recommended to run this software before migration.  
  
**1.2. Download**  
You can download this tool from the link below:  
  
“Download the Microsoft SQL Server 2008 Upgrade Advisor. Upgrade Advisor analyzes instances of SQL Server 2000 and SQL Server 2005 to help you prepare for upgrades to SQL Server 2008.”  
[http://www.microsoft.com/downloads/details.aspx?familyid=F5A6C5E9-4CD9-4E42-A21C-7291E7F0F852&displaylang=en](http://www.microsoft.com/downloads/details.aspx?familyid=F5A6C5E9-4CD9-4E42-A21C-7291E7F0F852&displaylang=en)  
  
After installation, a new tab appears in: Start>> All Programs >> Microsoft SQL server 2008 >> SQL Server 2008 Upgrade Advisor  
  
**1.3. Report before migration**  
Run “SQL Server 2008 Upgrade Advisor”.  
Then click on “Launch Upgrade Advisor Analysis Wizard”.  
Then click on "Detect". The tool will automatically select the components installed on your platform.  
It is also interesting to give a trace profiler tool containing a representative of your business so that it detects all the elements that would longer supported or recommended in Microsoft SQL Server 2008.  
![migration SQL Server 2005 to SQL Server 2008](https://msdnshared.blob.core.windows.net/media/TNBlogsFS/prod.evol.blogs.technet.com/telligent.evolution.components.attachments/13/7693/00/00/03/26/56/48/migration_SQL2000_to_SQL2008.png)  
  
Then configure the connection to your SQL server 2000 instance. After a few minutes a report will be generated with warning or points on which you must bring your attention. These items may include Full Text Search, replication, objects that no longer exist or have been modified in the new version, plans to maintain ...  
The tool will provide two other types of information:  
1. Objects affected  
2. Advice you can find a workaround or fix the problem.  
  
Sample report provided by the tool:  
![migration SQL Server 2005 to SQL Server 2008](https://msdnshared.blob.core.windows.net/media/TNBlogsFS/prod.evol.blogs.technet.com/telligent.evolution.components.attachments/13/7693/00/00/03/26/56/50/image003.png)  
  
**3. Migration with the database restore method**  
  
**3.1. Restoring a database SQL server 2000**  
On your new instance Microsoft SQL Server 2008, connect to Management Studio 2008. Then click on the "Restore Database". Then follow the instructions.  
  
RESTORE (Transact-SQL)  
[http://msdn.microsoft.com/en-us/library/ms186858.aspx](http://msdn.microsoft.com/en-us/library/ms186858.aspx)  
  
How to: Restore a Database Backup (SQL Server Management Studio)  
[http://msdn.microsoft.com/en-us/library/ms177429.aspx](http://msdn.microsoft.com/en-us/library/ms177429.aspx)  
  
**3.2. Compatibility Level SQL 2000/ SQL 2008**  
If you restore your database in SQL Server 2000 SQL Server 2008, the level of compatibility will default mode "SQL Server 2000 (80).  
To know the level of compatibility, Make a right click on the name of the database>> "Property"  
Then in the dialog "Database Properties", click "Options"  
  
![migration SQL Server 2005 to SQL Server 2008](https://msdnshared.blob.core.windows.net/media/TNBlogsFS/prod.evol.blogs.technet.com/telligent.evolution.components.attachments/13/7693/00/00/03/26/56/52/image005.png)  
  
To enjoy all the new features in the new engine SQL server 2008, you must change the compatibility level to 100.  
  
To know the differences between compatibility 80, 90 or 100, I invite you to read the following article  
[http://msdn.microsoft.com/en-us/library/bb510680.aspx](http://msdn.microsoft.com/en-us/library/bb510680.aspx)  
  
sp_dbcmptlevel (Transact-SQL)  
[http://msdn.microsoft.com/en-us/library/ms178653.aspx](http://msdn.microsoft.com/en-us/library/ms178653.aspx)  
  
  
**3.3. Transferring SQL Server logins and Windows**  
There are different ways to migrate your users  
- SQL Server Intégration Services, with component « "transfer Login task ».  
- SQL Server Management Studio, with “Copy Database Wizard”  
- You can also draw on examples of script between SQL Server 2000 and 2005 KB Article [http://support.microsoft.com/kb/246133](http://support.microsoft.com/kb/246133)  
  
**3.4. SQL Server Agent jobs**  
You can migrate your SQL Server Agent jobs using Enterprise Manager 2000. You can find more detail in the documentation below:  
  
How to script jobs using Transact-SQL (Enterprise Manager)  
[http://msdn.microsoft.com/en-us/library/aa177024(SQL.80).aspx](http://msdn.microsoft.com/en-us/library/aa177024(SQL.80).aspx)  
  
**3.5. Other components**  
You must also reconfigure the components such as SQL database Mail extended stored procedures, linked servers...  
  
**3.6. Update statistics**  
It is recommended that, after having committed or changed the compatibility mode to 100, execute the stored procedure: sp_updatestats  
  
The procedure allows sp_updatestats system to recalculate the statistics and make an update for all the statistics on each table in your base data. To avoid errors related to the statistics of the previous version.  
  
sp_updatestats (Transact-SQL)  
[http://msdn.microsoft.com/en-us/library/ms173804.aspx](http://msdn.microsoft.com/en-us/library/ms173804.aspx)


## Transaction Isolation Level

_Transaction isolation levels_  are a measure of the extent to which transaction isolation succeeds. In particular, transaction isolation levels are defined by the presence or absence of the following phenomena:

-   **Dirty Reads**  A  _dirty read_  occurs when a transaction reads data that has not yet been committed. For example, suppose transaction 1 updates a row. Transaction 2 reads the updated row before transaction 1 commits the update. If transaction 1 rolls back the change, transaction 2 will have read data that is considered never to have existed.
    
-   **Nonrepeatable Reads**  A  _nonrepeatable read_  occurs when a transaction reads the same row twice but gets different data each time. For example, suppose transaction 1 reads a row. Transaction 2 updates or deletes that row and commits the update or delete. If transaction 1 rereads the row, it retrieves different row values or discovers that the row has been deleted.
    
-   **Phantoms**  A  _phantom_  is a row that matches the search criteria but is not initially seen. For example, suppose transaction 1 reads a set of rows that satisfy some search criteria. Transaction 2 generates a new row (through either an update or an insert) that matches the search criteria for transaction 1. If transaction 1 reexecutes the statement that reads the rows, it gets a different set of rows.
    

The four transaction isolation levels (as defined by SQL-92) are defined in terms of these phenomena. In the following table, an "X" marks each phenomenon that can occur.

The following table describes simple ways that a DBMS might implement the transaction isolation levels.

Important

Most DBMSs use more complex schemes than these to increase concurrency. These examples are provided for illustration purposes only. In particular, ODBC does not prescribe how particular DBMSs isolate transactions from each other.

Transaction isolation

Possible implementation

Read uncommitted

Transactions are not isolated from each other. If the DBMS supports other transaction isolation levels, it ignores whatever mechanism it uses to implement those levels. So that they do not adversely affect other transactions, transactions running at the Read Uncommitted level are usually read-only.

Read committed

The transaction waits until rows write-locked by other transactions are unlocked; this prevents it from reading any "dirty" data.  
  
The transaction holds a read lock (if it only reads the row) or write lock (if it updates or deletes the row) on the current row to prevent other transactions from updating or deleting it. The transaction releases read locks when it moves off the current row. It holds write locks until it is committed or rolled back.

Repeatable read

The transaction waits until rows write-locked by other transactions are unlocked; this prevents it from reading any "dirty" data.  
  
The transaction holds read locks on all rows it returns to the application and write locks on all rows it inserts, updates, or deletes. For example, if the transaction includes the SQL statement  **SELECT * FROM Orders**, the transaction read-locks rows as the application fetches them. If the transaction includes the SQL statement  **DELETE FROM Orders WHERE Status = 'CLOSED'**, the transaction write-locks rows as it deletes them.  
  
Because other transactions cannot update or delete these rows, the current transaction avoids any nonrepeatable reads. The transaction releases its locks when it is committed or rolled back.

Serializable

The transaction waits until rows write-locked by other transactions are unlocked; this prevents it from reading any "dirty" data.  
  
The transaction holds a read lock (if it only reads rows) or write lock (if it can update or delete rows) on the range of rows it affects. For example, if the transaction includes the SQL statement  **SELECT * FROM Orders**, the range is the entire Orders table; the transaction read-locks the table and does not allow any new rows to be inserted into it. If the transaction includes the SQL statement  **DELETE FROM Orders WHERE Status = 'CLOSED'**, the range is all rows with a Status of "CLOSED"; the transaction write-locks all rows in the Orders table with a Status of "CLOSED" and does not allow any rows to be inserted or updated such that the resulting row has a Status of "CLOSED".  
  
Because other transactions cannot update or delete the rows in the range, the current transaction avoids any nonrepeatable reads. Because other transactions cannot insert any rows in the range, the current transaction avoids any phantoms. The transaction releases its lock when it is committed or rolled back.

It is important to note that the transaction isolation level does not affect a transaction's ability to see its own changes; transactions can always see any changes they make. For example, a transaction might consist of two  **UPDATE**  statements, the first of which raises the pay of all employees by 10 percent and the second of which sets the pay of any employees over some maximum amount to that amount. This succeeds as a single transaction only because the second  **UPDATE**  statement can see the results of the first.
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTEyMDI3MTU3OF19
-->