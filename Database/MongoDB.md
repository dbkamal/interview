# MongoDB

## Benefit of globally define object_id

By default, MongoDB generates a unique  [ObjectID](https://docs.mongodb.com/manual/reference/method/ObjectId/ "Object ID")  identifier that is assigned to the _id field in a new document before writing that document to the database. In many cases the default unique identifiers assigned by MongoDB will meet application requirements. Benefit:

1. works well on distributed system
2. handle heavy traffic
3. no id collision as across all the database this object_id is unique
4. 

However, in some cases an application may need to create custom unique identifiers, such as:

-   The application may require unique identifiers with a precise number of digits. For example, unique 12 digit identifiers might be required for bank account or credit card numbers.
-   Unique identifiers may need to be generated in a monotonically increasing and continuous sequential order.
-   Unique identifiers may need to be independent of a specific database vendor.

Due to the multi-threaded and distributed nature of modern applications, it is not always a straightforward task to generate unique identifiers that satisfy application requirements.

[https://www.mongodb.com/blog/post/generating-globally-unique-identifiers-for-use-with-mongodb](https://www.mongodb.com/blog/post/generating-globally-unique-identifiers-for-use-with-mongodb)

**During custome creation of _Object_id_, it returns 12byte of _id. However, if we convert the _id to string, it will take 24bytes of storage for each _id.**

## Mongoose vs MongoDB

Mongoose is an object document modeling (ODM) layer that sits on top of Node's MongoDB driver. If your coming from SQL, it's similar to an ORM for a relational database. While it's not required to use Mongoose with the Mongo, here are four reasons why using Mongoose with MongoDB is generally a good idea.

### 1. Schemas

MongoDB is a denormalized NoSQL database. This makes it inherently schema-less as documents have varying sets of fields with different data types. While this provides your data model with flexibility as it evolves over time, it can be difficult to cope with coming from a SQL background. Mongoose defines a schema for your data models so your documents follow a specific structure with pre-defined data types.

### 2. Validation

Mongoose has built in validation for schema definitions. This saves you from writing a bunch of validation code that you have to otherwise write with the MongoDB driver. By simply including things like  required:true  in your schema definitions, Mongoose provides out-of-the-box validations for your collections (including data types).

### 3. Instance Methods

Mongoose provides optional pre and post save operations for data models. This makes it easy to define hooks and custom functionality on successful reads/writes etc. You can also define custom methods that act on a particular instance (or document). While you can achieve similar functionality with the native MongoDB driver, Mongoose makes it easier to define and organize such methods within your schema definition.

### 4. Returning results

Mongoose makes returning updated documents or query results easier. A prime example can be found with  update  queries. While the native driver returns an object with a success flag and the number of documents modified, Mongoose returns the updated object itself so you can easily work with the results.

### Reasons not to use Mongoose?

There are few reasons not to use Mongoose with MongoDB (especially if you are just getting started). For more advanced queries, it can be argued that Mongoose makes things more difficult and can slow performance. Advocates of the native MongoDB driver also argue that bringing ODM to a denormalized design entirely defeats the purpose of a NoSQL database.

### Conclusion

Despite the arguments against using Mongoose, it remains one of the most popular ODM tools for Mongo. If you are coming from a SQL background then using Mongoose will make the transition into a NoSQL environment much easier. It will also save you time writing your own validations and instance methods and is highly recommended for smaller DBs and basic Mongo operations.

## Notes

MongoDB retryable writes make only one retry attempt. This helps address transient network errors and replica set elections, but not persistent network errors. If the driver cannot find a healthy primary in the destination replica set or sharded cluster shard, the drivers wait serverSelectionTimeoutMS milliseconds to determine the new primary before retrying. Retryable writes do not address instances where the failover period exceeds serverSelectionTimeoutMS.  
  
ReadConcern: The readConcern option allows you to control the consistency and isolation properties of the data read from replica sets and replica set shards. Through the effective use of write concerns and read concerns, you can adjust the level of consistency and availability guarantees as appropriate, such as waiting for stronger consistency guarantees, or loosening consistency requirements to provide higher availability.  
  
Using MongoDB Zones, DBAs can build tiered storage solutions that support the data lifecycle, with frequently-used data stored in memory, less-used data stored on the server, and archived data taken offline at the proper time.  
  
MongoDB used mongoimport and mongoexport for moving data into and out of a database. For migration from one MongoDB to another, use the mongodump and mongorestore tools.  
  
Continuous Availability with MongoDB : Just as your power supply has a battery backup or series of backups, so your MongoDB software can be configured with data redundancy and system redundancies that guarantee continuous availability. Journaling. Journal entries are recovered when data is recovered, making for a durable database that can be restored quickly. With WiredTiger and MMAPv1, journals are compressed to keep storage space down. Data redundancies. MongoDB maintains multiple replicas of the database for instant failover in the event of a crash. While the failed dataset is repaired or replaced, MongoDB keeps operating. Replica data sets make it possible to upgrade without any downtime, and to conduct maintenance and tuning while the database is operating. Replication. MongoDB replica data sets can be employed across data centers to protect against human and natural disasters. DBAs can specify the level of persistence they want associated with data writes. The guarantee can be set at written, journaled, replicated, or even custom configurations for replication of specific data to specific replicas.  
  
One of the benefits of using MongoDB is the ease of scaling your databases as your needs grow. The technique used is called sharding, by which MongoDB automatically balances data across shards, or replica data sets, so that there is always capacity and rarely a bottleneck. There are many ways to shard a database that correspond to the way the data is stored and retrieved. Sharding by range organizes the database along a key value that is contained in all the documents in the database. Hash sharding uses an MD5 of the shard key value to determine the shard distribution. MongoDB Zones is a sharding program that makes it easier to see and manipulate sharding policies. MongoDB 3.4 has new helpers that work with Zones for better control over sharding.  
  
Monitoring. It’s important to establish performance baselines so your monitoring can let you know how actual performance is departing from what is expected or required. Ops Manager tracks over 100 metrics in easy-to-customize dashboards that tell you at a glance how your database is performing. Dashboards can be mapped to permissions, restricting visibility into data sets. Custom alerts can be designed to trigger warnings whenever performance is outside of a desired parameter.  
  
Backup and recovery. You need a backup-and-recovery plan for your MongoDB systems to guarantee system uptime, that you don’t lose any data, and that you satisfy regulatory requirements for data backup and storage. With Ops Manager and Cloud Manager, your data is continuously backed up just seconds behind the master database. If there is a problem, you can restore to an exact point in time. The mongodump tool bundled with MongoDB can perform live backups, scheduled backups, or point-in-time backups.  
  
Design Schema:  
  
So, even at this basic level, there is more to think about when designing a MongoDB schema than when designing a comparable relational schema. You need to consider two factors:  
  
Will the entities on the “N” side of the One-to-N ever need to stand alone?  
What is the cardinality of the relationship: is it one-to-few; one-to-many; or one-to-squillions?  
Based on these factors, you can pick one of the three basic One-to-N schema designs:  
  
Embed the N side if the cardinality is one-to-few and there is no need to access the embedded object outside the context of the parent object  
Use an array of references to the N-side objects if the cardinality is one-to-many or if the N-side objects should stand alone for any reasons  
Use a reference to the One-side in the N-side objects if the cardinality is one-to-squillions  
  
Intermediate: Two-Way Referencing  
  
If you want to get a little bit fancier, you can combine two techniques and include both styles of reference in your schema, having both references from the “one” side to the “many” side and references from the “many” side to the “one” side.  
  
For an example, let’s go back to that task-tracking system. There’s a “people” collection holding Person documents, a “tasks” collection holding Task documents, and a One-to-N relationship from Person -> Task. The application will need to track all of the Tasks owned by a Person, so we will need to reference Person -> Task. This design has all of the advantages and disadvantages of the “One-to-Many” schema, but with some additions. Putting in the extra ‘owner’ reference into the Task document means that its quick and easy to find the Task’s owner, but it also means that if you need to reassign the task to another person, you need to perform two updates instead of just one. Specifically, you’ll have to update both the reference from the Person to the Task document, and the reference from the Task to the Person. (And to the relational gurus who are reading this – you’re right: using this schema design means that it is no longer possible to reassign a Task to a new Person with a single atomic update. This is OK for our task-tracking system: you need to consider if this works with your particular use case.)  
  
[https://www.youtube.com/watch?v=mWPxSPgLRh0](https://www.youtube.com/watch?v=mWPxSPgLRh0)  
  
[https://www.mongodb.com/blog/post/the-modern-application-stack-part-5-using-reactjs-es6-and-jsx-to-build-a-ui-the-rise-of-mern](https://www.mongodb.com/blog/post/the-modern-application-stack-part-5-using-reactjs-es6-and-jsx-to-build-a-ui-the-rise-of-mern)  
  
Denormalizing only makes sense when there’s an high ratio of reads to updates. If you’ll be reading the denormalized data frequently, but updating it only rarely, it often makes sense to pay the price of slower updates – and more complex updates – in order to get more efficient queries. As updates become more frequent relative to queries, the savings from denormalization decrease.  
  
[https://kb.objectrocket.com/mongo-db/creating-stored-procedures-in-mongodb-with-stored-javascript-341](https://kb.objectrocket.com/mongo-db/creating-stored-procedures-in-mongodb-with-stored-javascript-341)  
  
[https://blog.stvmlbrn.com/2019/02/20/automatically-refreshing-data-in-react.html](https://blog.stvmlbrn.com/2019/02/20/automatically-refreshing-data-in-react.html)  
  
[https://statsbot.co/blog/building-mongodb-dashboard-using-node.js?86](https://statsbot.co/blog/building-mongodb-dashboard-using-node.js?86)  
  
[https://cube.dev/](https://cube.dev/)  
  
  
Must Read  
  
[https://www.datavail.com/blog/mongodb-best-practices/](https://www.datavail.com/blog/mongodb-best-practices/)  
[https://docs.mongodb.com/manual/reference/read-concern/](https://docs.mongodb.com/manual/reference/read-concern/)  
[https://dzone.com/articles/mongodb-atlas-best-practices-part-3](https://dzone.com/articles/mongodb-atlas-best-practices-part-3)  
[https://www.youtube.com/watch?v=l1XRggQQ3NM](https://www.youtube.com/watch?v=l1XRggQQ3NM)
<!--stackedit_data:
eyJoaXN0b3J5IjpbMjg2MjE0NDc1LDE0MzEwOTcyNDIsLTE3MT
IyMjIzOTgsODY5MTI4MDgwLDE1NTM2OTExOTUsNDE2MjU4MTE3
LC0yMDg4NzQ2NjEyXX0=
-->