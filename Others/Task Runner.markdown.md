# Grunt

1. [https://scotch.io/tutorials/a-simple-guide-to-getting-started-with-grunt](https://scotch.io/tutorials/a-simple-guide-to-getting-started-with-grunt)
2. [https://semaphoreci.com/community/tutorials/getting-started-with-grunt-js](https://semaphoreci.com/community/tutorials/getting-started-with-grunt-js)
## Why use a task runner?

In one word: automation. The less work you have to do when performing repetitive tasks like minification, compilation, unit testing, linting, etc, the easier your job becomes. After you've configured it through a  [Gruntfile](https://gruntjs.com/sample-gruntfile), a task runner can do most of that mundane work for you—and your team—with basically zero effort.

## Why use Grunt?

The Grunt ecosystem is huge and it's growing every day. With literally hundreds of plugins to choose from, you can use Grunt to automate just about anything with a minimum of effort. If someone hasn't already built what you need, authoring and publishing your own Grunt plugin to npm is a breeze.


# Build Automation

[https://en.wikipedia.org/wiki/Build_automation](https://en.wikipedia.org/wiki/Build_automation)
[https://en.wikipedia.org/wiki/List_of_build_automation_software](https://en.wikipedia.org/wiki/List_of_build_automation_software)
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTExNzM2MjI2MjUsLTE5MjQ1Mjc0NzAsLT
k0NTIxNDM1NiwtMTU2OTM3ODI5NF19
-->