## Nginx

### Installing NGINX Plus AMIs on Amazon EC2

To quickly set up an NGINX Plus environment on AWS:

1.  Follow the instructions in  [Getting Started with Amazon EC2 Linux Instances](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/EC2_GetStarted.html)  to sign up on AWS and get more information about EC2 itself.
    
2.  Proceed to the product page for the appropriate AMI at the AWS Marketplace, and launch the AMI.
    
    -   [NGINX Plus – Amazon Linux AMI (HVM)](https://aws.amazon.com/marketplace/pp/B00UU272MM/ref=_ptnr_doc_)
    -   [NGINX Plus – Red Hat Enterprise Linux 7 AMI (HVM)](https://aws.amazon.com/marketplace/pp/B0162SZWME/ref=_ptnr_doc_)
    -   [NGINX Plus – Red Hat Enterprise Linux 6 AMI (HVM)](https://aws.amazon.com/marketplace/pp/B0162SZXW8/ref=_ptnr_doc_)
    -   [NGINX Plus – Ubuntu AMI (HVM)](https://aws.amazon.com/marketplace/pp/B00UU270AG/ref=_ptnr_doc_)
    
    Click the  **Continue to Subscribe**  button to proceed to the  **Launch on EC2**  page.
    
3.  Select the type of launch by clicking the appropriate tab (1‑Click Launch,  **Manual Launch**, or  **Service Catalog**). Choose the desired options for billing, instance size, and so on, and click the  Accept Software Terms…  button.
    
4.  When configuring the firewall rules, add a rule to accept web traffic on TCP ports 80 and 443 (this happens automatically if you launch from the  1-Click Launch  tab).
    
5.  As soon as the new EC2 instance launches, NGINX Plus starts automatically and serves a default  **index.html**  page. To view the page, use a web browser to access the public DNS name of the new instance. You can also check the status of the NGINX Plus server by logging into the EC2 instance and running this command:
    
    /etc/init.d/nginx status

[https://docs.nginx.com/nginx/admin-guide/load-balancer/http-load-balancer/](https://docs.nginx.com/nginx/admin-guide/load-balancer/http-load-balancer/)

[https://medium.com/@nishankjaintdk/setting-up-a-node-js-app-on-a-linux-ami-on-an-aws-ec2-instance-with-nginx-59cbc1bcc68c](https://medium.com/@nishankjaintdk/setting-up-a-node-js-app-on-a-linux-ami-on-an-aws-ec2-instance-with-nginx-59cbc1bcc68c)

[https://www.valentinog.com/blog/socket-react/](https://www.valentinog.com/blog/socket-react/)
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTU2NzE2MzU0NSwxMDc1MDk0NDQsLTIwMz
QxMTU3NDMsLTEwMDYwODU2NDYsOTgxNDIyMDA3XX0=
-->