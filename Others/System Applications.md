# NGINX

How to install Nginx on Ubuntu machine : [https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-18-04](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-18-04)

### Nginx performance scale

[https://www.nginx.com/blog/inside-nginx-how-we-designed-for-performance-scale/](https://www.nginx.com/blog/inside-nginx-how-we-designed-for-performance-scale/)

# Amazon Elastic Load Balancer

Elastic Load Balancing automatically distributes incoming application traffic across multiple targets, such as Amazon EC2 instances, containers, IP addresses, and Lambda functions. It can handle the varying load of your application traffic in a single Availability Zone or across multiple Availability Zones. Elastic Load Balancing offers three types of load balancers that all feature the high availability, automatic scaling, and robust security necessary to make your applications fault tolerant.

**Application Load Balancer** Application Load Balancer is best suited for load balancing of HTTP and HTTPS traffic and provides advanced request routing targeted at the delivery of modern application architectures, including microservices and containers. Operating at the individual request level (Layer 7), Application Load Balancer routes traffic to targets within Amazon Virtual Private Cloud (Amazon VPC) based on the content of the request.

**Network Load Balancer** Network Load Balancer is best suited for load balancing of Transmission Control Protocol (TCP), User Datagram Protocol (UDP) and Transport Layer Security (TLS) traffic where extreme performance is required. Operating at the connection level (Layer 4), Network Load Balancer routes traffic to targets within Amazon Virtual Private Cloud (Amazon VPC) and is capable of handling millions of requests per second while maintaining ultra-low latencies. Network Load Balancer is also optimized to handle sudden and volatile traffic patterns.

**Classic Load Balancer** Classic Load Balancer provides basic load balancing across multiple Amazon EC2 instances and operates at both the request level and connection level. Classic Load Balancer is intended for applications that were built within the EC2-Classic network.

## Benefits

**Highly available**
Elastic Load Balancing automatically distributes incoming traffic across multiple targets – Amazon EC2 instances, containers, IP addresses, and Lambda functions – in multiple Availability Zones and ensures only healthy targets receive traffic. Elastic Load Balancing can also load balance across a Region, routing traffic to healthy targets in different Availability Zones. The Amazon Elastic Load Balancing Service Level Agreement commitment is 99.99% availability for a load balancer.

**Secure**
Elastic Load Balancing works with Amazon Virtual Private Cloud (VPC) to provide robust security features, including integrated certificate management, user-authentication, and SSL/TLS decryption. Together, they give you the flexibility to centrally manage TLS settings and offload CPU intensive workloads from your applications.

**Elastic**
Elastic Load Balancing is capable of handling rapid changes in network traffic patterns. Additionally, deep integration with Auto Scaling ensures sufficient application capacity to meet varying levels of application load without requiring manual intervention.

**Flexible**
Elastic Load Balancing also allows you to use IP addresses to route requests to application targets. This offers you flexibility in how you virtualize your application targets, allowing you to host more applications on the same instance. This also enables these applications to have individual security groups and use the same network port to further simplify inter-application communication in microservice-based architecture.

**Robust monitoring & auditing**
Elastic Load Balancing allows you to monitor your applications and their performance in real time with Amazon CloudWatch metrics, logging, and request tracing. This improves visibility into the behavior of your applications, uncovering issues and identifying performance bottlenecks in your application stack at the granularity of an individual request.

**Hybrid load balancing**
Elastic Load Balancing offers ability to load balance across AWS and on-premises resources using the same load balancer. This makes it easy for you to migrate, burst, or failover on-premises applications to the cloud.

## Use Cases

### Achieve better fault tolerance for your applications

Elastic Load Balancing provides fault tolerance for your applications by automatically balancing traffic across targets – Amazon EC2 instances, containers, IP addresses, and Lambda functions – in multiple Availability Zones while ensuring only healthy targets receive traffic. If all of your targets in a single Availability Zone are unhealthy, Elastic Load Balancing will route traffic to healthy targets in other Availability Zones. Once targets have returned to a healthy state, load balancing will automatically resume to the original targets.

### Automatically load balance your containerized applications

With enhanced container support for Elastic Load Balancing, you can now load balance across multiple ports on the same Amazon EC2 instance. You can also take advantage of deep integration with the Amazon EC2 Container Service (ECS), which provides a fully-managed container offering. Simply register a service with a load balancer, and ECS transparently manages the registration and de-registration of Docker containers. The load balancer automatically detects the port and dynamically reconfigures itself.

### Automatically scale your applications

Elastic Load Balancing provides confidence that your applications will scale to the demands of your customers. With the ability to trigger Auto Scaling for your Amazon EC2 instance fleet when latency of any one of your EC2 instances exceeds a preconfigured threshold, your applications will always be ready to serve the next customer request.

### Using Elastic Load Balancing in your Amazon Virtual Private Cloud (Amazon VPC)

Elastic Load Balancing makes it easy to create an internet-facing entry point into your VPC or to route request traffic between tiers of your application within your VPC. You can assign security groups to your load balancer to control which ports are open to a list of allowed sources. Because Elastic Load Balancing is integrated with your VPC, all of your existing Network Access Control Lists (ACLs) and Routing Tables continue to provide additional network controls.

When you create a load balancer in your VPC, you can specify whether the load balancer is internet-facing (default) or internal. If you select internal, you do not need to have an internet gateway to reach the load balancer, and the private IP addresses of the load balancer will be used in the load balancer’s DNS record.

### Hybrid load balancing with Elastic Load Balancing

Elastic Load Balancing offers ability to load balance across AWS and on-premises resources using the same load balancer. For example, if you need to distribute application traffic across both AWS and on-premises resources, you can achieve this by registering all the resources to the same target group and associating the target group with a load balancer. Alternatively, you can use DNS-based weighted load balancing across AWS and on-premises resources using two load balancers, with one load balancer for AWS and other for on-premises resources.

You can also use hybrid load balancing to benefit separate applications where one is in a VPC and the other is in an on-premises location. Simply put the VPC targets in one target group and the on-premises targets in another target group, and then use content based routing to route traffic to each target group.

### Invoking Lambda functions over HTTP(S)

Elastic Load Balancing supports invoking Lambda functions to serve HTTP(S) requests. This enables users to access serverless applications from any HTTP client, including web browsers. You can register Lambda functions as targets and leverage the support for content-based routing rules in Application Load Balancers to route requests to different Lambda functions. You can use an Application Load Balancer as a common HTTP endpoint for applications that use servers and serverless computing. You can build an entire website using Lambda functions or combine EC2 instances, containers, on-premises servers and Lambda functions to build applications.

## How ELB Works?

[https://docs.aws.amazon.com/elasticloadbalancing/latest/userguide/how-elastic-load-balancing-works.html](https://docs.aws.amazon.com/elasticloadbalancing/latest/userguide/how-elastic-load-balancing-works.html)

A load balancer accepts incoming traffic from clients and routes requests to its registered targets (such as EC2 instances) in one or more Availability Zones. The load balancer also monitors the health of its registered targets and ensures that it routes traffic only to healthy targets. When the load balancer detects an unhealthy target, it stops routing traffic to that target, and then resumes routing traffic to that target when it detects that the target is healthy again.

You configure your load balancer to accept incoming traffic by specifying one or more  _listeners_. A listener is a process that checks for connection requests. It is configured with a protocol and port number for connections from clients to the load balancer and a protocol and port number for connections from the load balancer to the targets.

Elastic Load Balancing supports three types of load balancers: Application Load Balancers, Network Load Balancers, and Classic Load Balancers. There is a key difference between the way you configure these load balancers. With Application Load Balancers and Network Load Balancers, you register targets in target groups, and route traffic to the target groups. With Classic Load Balancers, you register instances with the load balancer.

### Availability Zones and Load Balancer Nodes

When you enable an Availability Zone for your load balancer, Elastic Load Balancing creates a load balancer node in the Availability Zone. If you register targets in an Availability Zone but do not enable the Availability Zone, these registered targets do not receive traffic. Note that your load balancer is most effective if you ensure that each enabled Availability Zone has at least one registered target.

We recommend that you enable multiple Availability Zones. (Note that with an Application Load Balancer, we require you to enable multiple Availability Zones.) With this configuration, if one Availability Zone becomes unavailable or has no healthy targets, the load balancer can continue to route traffic to the healthy targets in another Availability Zone.

After you disable an Availability Zone, the targets in that Availability Zone remain registered with the load balancer, but the load balancer will not route traffic to them.

# AWS CloudFront

# AWS EC2

Amazon Elastic Compute Cloud (Amazon EC2) is a web service that provides secure, resizable compute capacity in the cloud. It is designed to make web-scale cloud computing easier for developers.

Amazon EC2’s simple web service interface allows you to obtain and configure capacity with minimal friction. It provides you with complete control of your computing resources and lets you run on Amazon’s proven computing environment. Amazon EC2 reduces the time required to obtain and boot new server instances to minutes, allowing you to quickly scale capacity, both up and down, as your computing requirements change. Amazon EC2 changes the economics of computing by allowing you to pay only for capacity that you actually use. Amazon EC2 provides developers the tools to build failure resilient applications and isolate them from common failure scenarios.

## Benefits

### ELASTIC WEB-SCALE COMPUTING

Amazon EC2 enables you to increase or decrease capacity within minutes, not hours or days. You can commission one, hundreds, or even thousands of server instances simultaneously. You can also use  [Amazon EC2 Auto Scaling](https://aws.amazon.com/ec2/autoscaling/?sc_channel=ba&sc_campaign=ec2-autoscaling&sc_country=mult&sc_geo=mult&sc_category=mult&sc_outcome=aware)  to maintain availability of your EC2 fleet and automatically scale your fleet up and down depending on its needs in order to maximize performance and minimize cost. To scale multiple services, you can use  [AWS Auto Scaling](https://aws.amazon.com/autoscaling/?sc_channel=ba&sc_campaign=aws-autoscaling&sc_country=mult&sc_geo=mult&sc_category=mult&sc_outcome=aware).

### COMPLETELY CONTROLLED

You have complete control of your instances including root access and the ability to interact with them as you would any machine. You can stop any instance while retaining the data on the boot partition, and then subsequently restart the same instance using web service APIs. Instances can be rebooted remotely using web service APIs, and you also have access to their console output.

### FLEXIBLE CLOUD HOSTING SERVICES

You have the choice of multiple instance types, operating systems, and software packages. Amazon EC2 allows you to select a configuration of memory, CPU, instance storage, and the boot partition size that is optimal for your choice of operating system and application. For example, choice of operating systems includes numerous Linux distributions and [Microsoft Windows Server](https://aws.amazon.com/windows/).

### INTEGRATED

Amazon EC2 is integrated with most AWS services such as Amazon Simple Storage Service (Amazon S3), Amazon Relational Database Service (Amazon RDS), and Amazon Virtual Private Cloud (Amazon VPC) to provide a complete, secure solution for computing, query processing, and cloud storage across a wide range of applications.

To use Amazon EC2, you simply:

-   Select a pre-configured, templated Amazon Machine Image (AMI) to get up and running immediately. Or create an AMI containing your applications, libraries, data, and associated configuration settings.
-   Configure security and network access on your Amazon EC2 instance.
-   Choose which instance type(s) you want, then start, terminate, and monitor as many instances of your AMI as needed, using the web service APIs or the variety of management tools provided.
-   Determine whether you want to run in multiple locations, utilize static IP endpoints, or attach persistent block storage to your instances.
-   Pay only for the resources that you actually consume, like instance-hours or data transfer.

### Bare Metal instances

Amazon EC2 bare metal instances provide your applications with direct access to the processor and memory of the underlying server. These instances are ideal for workloads that require access to hardware feature sets (such as Intel® VT-x), or for applications that need to run in non-virtualized environments for licensing or support requirements. Bare metal instances are built on the Nitro system, a collection of AWS-built hardware offload and hardware protection components that come together to securely provide high performance networking and storage resources to EC2 instances. Bare metal instances are EC2 instances and thus offer the same robust security, reliability, capacity elasticity, and support for different operating systems and software packages as other virtual EC2 instances. You can also use bare metal instances with AWS services such as Amazon Virtual Private Cloud (VPC), Elastic Block Store (EBS), Elastic Load Balancing (ELB) and more.

### Flexible Storage Options

Different Amazon EC2 workloads can have vastly different storage requirements. Beyond the built-in instance storage, we also have  [Amazon Elastic Block Store (Amazon EBS)](https://aws.amazon.com/ebs/)  and  [Amazon Elastic File System (Amazon EFS)](https://aws.amazon.com/efs/)  to suit other  [cloud storage](https://aws.amazon.com/what-is-cloud-storage/)  workload requirements.

Amazon EBS provides persistent, highly available, consistent, low-latency block storage volumes for use with Amazon EC2 instances. Each Amazon EBS volume is automatically replicated within its Availability Zone to protect you from component failure, offering high availability and durability. It is designed for application managers who need to tune workloads for capacity, performance and cost.

Amazon EFS provides simple, scalable, persistent, fully managed  [cloud file storage](https://aws.amazon.com/what-is-cloud-file-storage/)  for shared access. Designed for high availability and durability across multiple Availability Zones it delivers a file system interface with standard file system access semantics, grows and shrinks capacity automatically, and provides application managers with high throughput and consistently low latencies at petabyte scale.

### Elastic IP Addresses

Elastic IP addresses are static IP addresses designed for dynamic  [cloud computing](https://aws.amazon.com/what-is-cloud-computing/). An Elastic IP address is associated with your account not a particular instance, and you control that address until you choose to explicitly release it. Unlike traditional static IP addresses, however, Elastic IP addresses allow you to mask instance or Availability Zone failures by programmatically remapping your public IP addresses to any instance in your account. Rather than waiting on a data technician to reconfigure or replace your host, or waiting for DNS to propagate to all of your customers, Amazon EC2 enables you to engineer around problems with your instance or software by quickly remapping your Elastic IP address to a replacement instance.

### What is the difference between using the local instance store and Amazon Elastic Block Store (Amazon EBS) for the root device?

When you launch your Amazon EC2 instances you have the ability to store your root device data on Amazon EBS or the local instance store. By using Amazon EBS, data on the root device will persist independently from the lifetime of the instance. This enables you to stop and restart the instance at a subsequent time, which is similar to shutting down your laptop and restarting it when you need it again.

Alternatively, the local instance store only persists during the life of the instance. This is an inexpensive way to launch instances where data is not stored to the root device. For example, some customers use this option to run large web sites where each instance is a clone to handle web traffic.

### How do I load and store my systems with Amazon EC2?

Amazon EC2 allows you to set up and configure everything about your instances from your operating system up to your applications. An Amazon Machine Image (AMI) is simply a packaged-up environment that includes all the necessary bits to set up and boot your instance. Your AMIs are your unit of deployment. You might have just one AMI or you might compose your system out of several building block AMIs (e.g., webservers, appservers, and databases). Amazon EC2 provides a number of tools to make creating an AMI easy. Once you create a custom AMI, you will need to bundle it. If you are bundling an image with a root device backed by Amazon EBS, you can simply use the bundle command in the AWS Management Console. If you are bundling an image with a boot partition on the instance store, then you will need to use the AMI Tools to upload it to Amazon S3. Amazon EC2 uses Amazon EBS and Amazon S3 to provide reliable, scalable storage of your AMIs so that we can boot them when you ask us to do so.

Or, if you want, you don’t have to set up your own AMI from scratch. You can choose from a number of globally available AMIs that provide useful instances. For example, if you just want a simple Linux server, you can choose one of the standard Linux distribution AMIs.

# AWS DynamoDB

[https://www.youtube.com/watch?v=HaEPXoXVf2k](https://www.youtube.com/watch?v=HaEPXoXVf2k)

# Cassandra

# Apache Kafka

# Service Discovery

# ZooKeeper

# Message Queue / RabbitMQ

# Hadoop

# Apache Spark

# MapReduce

# HDFS & HBase

# Redis

# Memcache

# GraphQL

# PostgreSQL

# MySQL

# Oracle Database

[https://www.youtube.com/watch?v=EqYB7mp_U3I&list=PL3Hc8S1SG2AQnk6Nu57m2JLkWxorMAsmE](https://www.youtube.com/watch?v=EqYB7mp_U3I&list=PL3Hc8S1SG2AQnk6Nu57m2JLkWxorMAsmE)

# PL/SQL

# Oracle RAC

# MS SQL Server

# ETL

# Data Warehousing

# Docker

# Kubernetes

# Chef

# Ansible

# Jenkins

# Travis CI

# CI/CD

CP -> Consistency with Partition Tolerance  
  
Chubby, Doozer - Paxos  
ZooKeeper - Zab  
Consul, etcd - Raft
  
Message failure in a distributed system:  
Delayed, Dropped, Delivered out of order, Duplicated

# Session Management

A centralized session management data store provides consistent user experience, better session durability, and high availability.

There are various ways to manage user sessions including storing those sessions locally to the node responding to the HTTP request or designating a layer in your architecture which can store those sessions in a scalable and robust manner. Common approaches used include utilizing Sticky sessions or using a Distributed Cache for your session management. These approaches are described below.

### Sticky Sessions with Local Session Caching

Sticky sessions, also known as  _session affinity_, allow you to route a site user to the particular web server that is managing that individual user’s session. The session’s validity can be determined by a number of methods, including a client-side cookies or via configurable duration parameters that can be set at the load balancer which routes requests to the web servers.

Some advantages with utilizing sticky sessions are that it’s cost effective due to the fact you are storing sessions on the same web servers running your applications and that retrieval of those sessions is generally fast because it eliminates network latency. A drawback for using storing sessions on an individual node is that in the event of a failure, you are likely to lose the sessions that were resident on the failed node. In addition, in the event the number of your web servers change, for example a scale-up scenario, it’s possible that the traffic may be unequally spread across the web servers as active sessions may exist on particular servers. If not mitigated properly, this can hinder the scalability of your applications.

### Distributed Session Management

In order to address scalability and to provide a shared data storage for sessions that can be accessible from any individual web server, you can abstract the HTTP sessions from the web servers themselves. A common solution to for this is to leverage an  [In-Memory Key/Value store](https://aws.amazon.com/elasticache/)  such as  [Redis](https://aws.amazon.com/redis/)  and  [Memcached](https://aws.amazon.com/memcached/).  
  
While Key/Value data stores are known to be extremely fast and provide sub-millisecond latency, the added network latency and added cost are the drawbacks. An added benefit of leveraging Key/Value stores is that they can also be utilized to cache any data, not just HTTP sessions, which can help boost the overall performance of your applications.  
  
A consideration when choosing a distributed cache for session management is determining how many nodes may be needed in order to manage the user sessions. Generally speaking, this decision can be determined by how much traffic is expected and/or how much risk is acceptable. In a distributed session cache, the sessions are divided by the number of nodes in the cache cluster. In the event of a failure, only the sessions that are stored on the failed node are affected. If reducing risk is more important than cost, adding additional nodes to further reduce the percent of stored sessions on each node may be ideal even when fewer nodes are sufficient.  
  
Another consideration may be whether or not the sessions need to be replicated or not. Some key/value stores offer replication via read replicas. In the event of a node failure, the sessions would not be entirely lost. Whether replica nodes are important in your individual architecture may inform as to which key/value store should be used.  [ElastiCache](https://aws.amazon.com/elasticache/)  offerings for In-Memory key/value stores include  [ElastiCache for Redis](https://aws.amazon.com/elasticache/redis/), which can support replication, and  [ElastiCache for Memcached](https://aws.amazon.com/elasticache/memcached/)  which does not support replication.  
  
There are a number of ways to store sessions in Key/Value stores. Many application frameworks provide libraries which can abstract some of the integration plumbing required to GET/SET those sessions in memory. In other cases, you can write your own session handler to persist the sessions directly.

![caching-session-management-diagram-v2](https://d1.awsstatic.com/product-marketing/caching-session-management-diagram-v2.c6856e6de83c4222dbc4853d9ff873f5542a86d8.PNG)


<!--stackedit_data:
eyJoaXN0b3J5IjpbMTEyMDk1MDc1MSwtNjM4MzE4Mzc2LDQ1OT
U4NTY5Nyw1MjYxMTMyMTMsLTE5MjI3MDkxOTIsLTM0NzM2NzM5
OCwtMTIzNTA2MTg3NSwtNjM3NzQyMDU1LC0xMTg5NDEzMzU4LD
IwMDM5OTA3NDAsLTE4NjkwNDE1MTAsMTEyMzM3MjQzOSwtNDI4
MDE4NTQ4LC0xNTQxODUxODI1LDEyNzc5MDU5ODUsNTI5ODEzOT
YxLDU3OTQ3ODM1MiwtMTYyNjQ0MTAzMSwtMjEwNDg5NjUzMSwt
MTc4Njk4Njg1Nl19
-->