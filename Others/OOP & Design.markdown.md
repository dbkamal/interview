# OOPS Basic
Object-oriented programming (OOP) is a style of programming that focuses on using objects to design and build applications. Contrary to procedure-oriented programming where programs are designed as blocks of statements to manipulate data, OOP organizes the program to combine data and functionality and wrap it inside something called an “Object”.

If you have never used an object-oriented programming language before, you will need to learn a few basic concepts before you can begin writing any code. This chapter will introduce some basic concepts of OOP:

-   **Objects:**  Objects represent a real-world entity and the basic building block of OOP. For example, an Online Shopping System will have objects such as shopping cart, customer, product item, etc.
    
-   **Class:**  Class is the prototype or blueprint of an object. It is a template definition of the attributes and methods of an object. For example, in the Online Shopping System, the Customer object will have attributes like shipping address, credit card, etc., and methods for placing an order, canceling an order, etc.
The four principles of object-oriented programming are encapsulation, abstraction, inheritance, and polymorphism.

-   **Encapsulation:**  Encapsulation is the mechanism of binding the data together and hiding it from the outside world. Encapsulation is achieved when each object keeps its state private so that other objects don’t have direct access to its state. Instead, they can access this state only through a set of public functions.
    
-   **Abstraction:**  Abstraction can be thought of as the natural extension of encapsulation. It means hiding all but the relevant data about an object in order to reduce the complexity of the system. In a large system, objects talk to each other, which makes it difficult to maintain a large code base; abstraction helps by hiding internal implementation details of objects and only revealing operations that are relevant to other objects.
    
-   **Inheritance:**  Inheritance is the mechanism of creating new classes from existing ones.
    
-   **Polymorphism:**  Polymorphism (from Greek, meaning “many forms”) is the ability of an object to take different forms and thus, depending upon the context, to respond to the same message in different ways. Take the example of a chess game; a chess piece can take many forms, like bishop, castle, or knight and all these pieces will respond differently to the ‘move’ message.

## OO Analysis and Design

OO Analysis and Design is a structured method for analyzing and designing a system by applying object-oriented concepts. This design process consists of an investigation into the objects constituting the system. It starts by first identifying the objects of the system and then figuring out the interactions between various objects.

The process of OO analysis and design can be described as:

1.  Identifying the objects in a system;
2.  Defining relationships between objects;
3.  Establishing the interface of each object; and,
4.  Making a design, which can be converted to executables using OO languages.

We need a standard method/tool to document all this information; for this purpose we use UML. UML can be considered as the successor of object-oriented (OO) analysis and design. UML is powerful enough to represent all the concepts that exist in object-oriented analysis and design. UML diagrams are a representation of object-oriented concepts only. Thus, before learning UML, it is essential to understand OO concepts.

Let’s find out how we can model using UML.

## UML

UML stands for Unified Modeling Language and is used to model the Object-Oriented Analysis of a software system. UML is a way of visualizing and documenting a software system by using a collection of diagrams, which helps engineers, businesspeople, and system architects understand the behavior and structure of the system being designed.

Benefits of using UML:

1.  Helps develop a quick understanding of a software system.
2.  UML modeling helps in breaking a complex system into discrete pieces that can be easily understood.
3.  UML’s graphical notations can be used to communicate design decisions.
4.  Since UML is independent of any specific platform or language or technology, it is easier to abstract out concepts.
5.  It becomes easier to hand the system over to a new team.

**Types of UML Diagrams:**  The current UML standards call for 14 different kinds of diagrams. These diagrams are organized into two distinct groups: structural diagrams and behavioral or interaction diagrams. As the names suggest, some UML diagrams analyze and depict the structure of a system or process, whereas others describe the behavior of the system, its actors, and its building components. The different types are broken down as follows:

**Structural UML diagrams**

-   Class diagram
-   Object diagram
-   Package diagram
-   Component diagram
-   Composite structure diagram
-   Deployment diagram
-   Profile diagram

**Behavioral UML diagrams**

-   Use case diagram
-   Activity diagram
-   Sequence diagram
-   State diagram
-   Communication diagram
-   Interaction overview diagram
-   Timing diagram

In this course, we will be focusing on the following UML diagrams:

-   **Use Case Diagram:**  Used to describe a set of user scenarios, this diagram, illustrates the functionality provided by the system. [https://www.youtube.com/watch?v=zid-MVo7M-E](https://www.youtube.com/watch?v=zid-MVo7M-E)
    
-   **Class Diagram:**  Used to describe structure and behavior in the use cases, this diagram provides a conceptual model of the system in terms of entities and their relationships.
    
-   **Activity Diagram:**  Used to model the functional flow-of-control between two or more class objects.
    
-   **Sequence Diagram:**  Used to describe interactions among classes in terms of an exchange of messages over time.


### Use Case Diagram

Use case diagrams describe a set of actions (called use cases) that a system should or can perform in collaboration with one or more external users of the system (called actors). Each use case should provide some observable and valuable result to the actors.

1.  Use Case Diagrams describe the high-level functional behavior of the system.
2.  It answers what system does from the user point of view.
3.  Use case answers ‘What will the system do?’ and at the same time tells us ‘What will the system NOT do?’.

A use case illustrates a unit of functionality provided by the system. The primary purpose of the use case diagram is to help development teams visualize the functional requirements of a system, including the relationship of “actors” to the essential processes, as well as the relationships among different use cases.

To illustrate a use case on a use case diagram, we draw an oval in the middle of the diagram and put the name of the use case in the center of the oval. To show an actor (indicating a system user) on a use-case diagram, we draw a stick figure to the left or right of the diagram.

# Approach for System Design Interview

Ref: [http://blog.gainlo.co/index.php/2015/10/22/8-things-you-need-to-know-before-system-design-interviews/](http://blog.gainlo.co/index.php/2015/10/22/8-things-you-need-to-know-before-system-design-interviews/)

There are also several other things you’d better be familiar although it’s possible that they may not be covered in your interview.

-   Abstraction. It’s a very important topic for system design interview. You should be clear about how to abstract a system, what is visible and invisible from other components, and what is the logic behind it. Object oriented programming is also important to know.
-   Database. You should be clear about those basic concepts like relational database. Knowing about No-SQL might be a plus depends on your level (new grads or experienced engineers).
-   Network. You should be able to explain clearly what happened when you type “gainlo.co” in your browser, things like DNS lookup, HTTP request should be clear.
-   Concurrency. It will be great if you can recognize concurrency issue in a system and tell the interviewer how to solve it. Sometimes this topic can be very hard, but knowing about basic concepts like race condition, dead lock is the bottom line.
-   Operating system. Sometimes your discussion with the interviewer can go very deeply and at this point it’s better to know how OS works in the low level.
-   Machine learning (optional). You don’t need to be an expert, but again some basic concepts like feature selection, how ML algorithm works in general are better to be familiar with.

Remember, the point is here asking you to learn all these stuff from scratch, which may take you more than a year. What really matters is the basic concepts behind each topic. For instance, it’s totally okay if you can’t implement neural network in the interview, but you should be able to explain it within a sentence.

## Basic Strategy

This is the general strategy for solving a system design problem and ways to explain to the interviewer. The worst case is always jumping into details immediately, which can only make things in a mess.

Instead, it’s always good to start with high-level ideas and then figure out details step by step, so this should be a top-down approach. Why? Because many system design questions are very general and there’s no way to solve it without a big picture.

Let’s use Youtube recommendation system as an example. I might first divide this into front-end and backend (the interviewer may only ask for backend or a specific part, but I’ll cover the whole system to give you an idea). For backend, the flow can be 3 steps: collect user data (like videos he watched, location, preferences etc.), offline pipeline that generating the recommendation, and store and serve the data to front-end. And then, we can jump into each detailed components.

For user data, we can list features that we think are relevant to videos a user may like. For pipeline, we can discuss how to train the dataset etc.. We can go even deeper. Since Youtube has a huge dataset, the offline pipeline must run over a huge number of data, then MapReduce or Hadoop might be used.

We can continue this analysis infinitely by going deeper and deeper, but the idea I want to explain here is that you should always have a big picture.

Another tip here is modularization. Like I illustrated above, it’s better to divide a system into different components, which is also a good practice in real life projects. Modularization not only can make your design much clearer to both yourself and the interviewer, but also make testing much easier.

# Design eCommerce Website (Amazon)

Features:
- Product + Quantity (Warehouse maintenance)
- Separate product based on category and sort product based on rating/preference
- Product details -> images/videos -> user review + rating
- Cart and purchasing
- User management - user credentials, preferences, credit card and address
- Shipping and backend ordering
- Notification
- User recommendation
- Product search option

Technical Features:
- Highly consistent and available
- Minimum latency
- Handle concurrency
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTc3Mjg1NDM5NiwxODA5NTU0MTUzLDk5Nz
kwMTcwOCwtMTg5NzkwMzQyNCwxNzMzNDc0NTkyLC0xNzU5NTQ2
NTk1XX0=
-->