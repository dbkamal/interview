# RabbitMQ Best Practicess

[https://www.cloudamqp.com/blog/2017-12-29-part1-rabbitmq-best-practice.html](https://www.cloudamqp.com/blog/2017-12-29-part1-rabbitmq-best-practice.html)

Some applications require really high throughput while other applications are publishing batch jobs that can be delayed for a while. The goal when designing your system should be to maximize combinations of performance and availability that make sense for your specific application. Bad architecture design decisions or client-side bugs, can damage your broker or affect your throughput.

Your publisher might be halted or the server might crash due to high memory usage. This article series focuses on best practice for RabbitMQ. Dos and don'ts mixed with best practice for two different usage categories; high availability and high performance (high throughput). We will, among other things, discuss queue size, common mistakes, lazy queues, prefetch values, connections and channels, HiPE and number of nodes in a cluster. Those are all generally best practice rules, based on the experience we have gained while working with RabbitMQ.

## Queues

### Keep your queue short if possible

Many messages in a queue can put a heavy load on RAM usage. In order to free up RAM, RabbitMQ start flushing (page out) messages to disk. This process deteriorates queueing speed. The page out process usually takes time and blocks the queue from processing messages when there are many messages to page out. Many messages might affect the performance of the broker negatively.

It is also time-consuming to restart a cluster with many messages since the index has to be rebuilt. It also takes time to sync messages between nodes in the cluster after a restart.

### Enable lazy queues to get predictable performance

A feature called  _lazy queues_  was added in RabbitMQ 3.6. Lazy queues are queues where the messages are automatically stored to disk. Messages are only loaded into memory when they are needed. With lazy queues, the messages go straight to disk and thereby the RAM usage is minimized, but the throughput time will take longer time.

We have seen that lazy queues create a more stable cluster, with better  **predictable performance.**  Your messages will not, without a warning, get flushed to disk. You will not suddenly be taken by a performance hit. If you are sending a lot of messages at once (e.g. processing batch jobs) or if you think that your consumers will not keep up with the speed of the publishers all the time, we recommend you to enable lazy queues.

_Please note that you should disable lazy queues if you require really high performance - if you’re queues always are short, or set a max-length policy._

### Limit queue size, with TTL or max-length

Another thing that could be recommended for applications that often get hit by spikes of messages, and where throughput is more important than anything else, is to set a max-length on the queue. This keeps the queue short by discarding messages from the head of the queue so that it’s never getting larger than the max-length setting.

### Number of queues

Queues are single-threaded in RabbitMQ, and one queue can handle up to about 50k messages/s. You will achieve better throughput on a multi-core system if you have multiple queues and consumers. You will achieve optimal throughput if you have as many queues as cores on the underlying node(s).

The RabbitMQ management interface collects and calculates metrics for every queue in the cluster. This might slow down the server if you have thousands upon thousands of active queues and consumers. The CPU and RAM usage may also be affected negatively if you have too many queues.

### Split your queues over different cores

Queue performance is limited to one CPU core. You will, therefore, get better performance if you split your queues into different cores, and also into different nodes if you have a RabbitMQ cluster.

RabbitMQ queues are bound to the node where they were first declared. Even if you create a cluster of RabbitMQ brokers, all messages routed to a specific queue will go to the node where that queue lives. You can manually split your queues evenly between nodes, but the downside is that you need to remember where your queue is located.

We recommend two plugins that will help you if you have multiple nodes or a single node cluster with multiple cores.

**Consistent hash exchange plugin**

The consistent hash exchange plugin allows you to use an exchange to load-balance messages between queues. Messages sent to the exchange are consistently and equally distributed across many queues, based on the routing key of the message. The plugin creates a hash of the routing key and spread the messages out between queues that have a binding to that exchange. It could quickly become problematically to do this manually, without adding too much information about numbers of queues and their bindings into the publisher.

The consistent hash exchange plugin can be used if you need to get maximum use of many cores in your cluster. Note that it’s important to consume from all queues. Read more about the consistent hash exchange plugin  [here.](https://github.com/rabbitmq/rabbitmq-consistent-hash-exchange)

**RabbitMQ sharding**

The RabbitMQ sharding plugin does the partitioning of queues automatically for you, i.e., once you define an exchange as sharded, then the supporting queues are automatically created on every cluster node and messages are sharded across them. RabbitMQ sharding shows one queue to the consumer, but it could be many queues running behind it in the background. The RabbitMQ Sharding plugin gives you a centralized place to where you can send your messages, plus load balancing across many nodes, by adding queues to the other nodes in the cluster. Read more about  [RabbitMQ Sharding here.](https://github.com/rabbitmq/rabbitmq-sharding)

### Don’t set your own names on temporary queues

Giving a queue a name is important when you want to share the queue between producers and consumers, but it's not important if you are using temporary queues. Instead, you should let the server choose a random queue name instead of making up your own names - or modify the RabbitMQ policies.

### [](https://www.cloudamqp.com/blog/2017-12-29-part1-rabbitmq-best-practice.html#autodelete-queues-you-are-not-using)Auto-delete queues you are not using

Client connections can fail and potentially leave unused resources (queues) behind, and leaving to many queues behind might affect performance. There are three ways delete a queue automatically.

You can set a  **TTL policy**  on the queue. E.g., a TTL policy of 28 days deletes queues that haven't been consumed from for 28 days.

An  **auto-delete**  queue is deleted when its last consumer has canceled or when the channel/connection is closed (or when it has lost the TCP connection with the server).

An  **exclusive queue**  can only be used (consumed from, purged, deleted, etc) by its declaring connection. Exclusive queues are deleted when their declaring connection is closed or gone (e.g., due to underlying TCP connection loss).

### [](https://www.cloudamqp.com/blog/2017-12-29-part1-rabbitmq-best-practice.html#set-limited-use-of-priority-queues)Set limited use of priority queues

Each priority level uses an internal queue on the Erlang VM, which takes up some resources. In most use cases it is sufficent to have no more than 5 priority levels.

##   
Payload - RabbitMQ message size and types

A common question is how to handle the payload (message size) of messages sent to RabbitMQ. You should of course not send very large files in messages, but messages per second is a way larger bottleneck than the message size it self. Sending multiple small messages might be a bad alternative. A better idea could be to bundle them into one larger message and let the consumer split it up. However, if you bundle multiple messages you need to keep in mind that this might affect the processing time. If one of the bundled messages fails - do you need to re-process them all? How you set this up depends on bandwidth and your architecture.

## [](https://www.cloudamqp.com/blog/2017-12-29-part1-rabbitmq-best-practice.html#connections-and-channels)Connections and channels

Each connection uses about 100 KB of RAM (and even more, if TLS is used). Thousands of connections can be a heavy burden on a RabbitMQ server. In the worst case, the server can crash due to out-of-memory. The AMQP protocol has a mechanism called channels that “multiplexes” a single TCP connection. It is recommended that each process only creates one TCP connection, and uses multiple channels in that connection for different threads. Connections should also be long-lived. The handshake process for an AMQP connection is quite involved and requires at least 7 TCP packets (more if TLS is used).

Instead, channels can be opened and closed more frequently, if required. Even channels should be long-lived if possible, e.g. reuse the same channel per thread of publishing. Don’t open a channel for each time you are publishing. Best practise is to reuse connections and multiplex a connection between threads with channels. You should ideally only have one connection per process, and then use a channel per thread in your application.

### Don’t share channels between threads

You should also make sure that you don’t share channels between threads as most clients don’t make channels thread-safe (because it would have a serious negative effect on the performance impact).

### [](https://www.cloudamqp.com/blog/2017-12-29-part1-rabbitmq-best-practice.html#don%E2%80%99t-open-and-close-connections-or-channels-repeatedly)Don’t open and close connections or channels repeatedly

Make sure that you don’t share channels between threads as most clients don’t make channels thread-safe (because it would have a serious negative effect on the performance impact).

### [](https://www.cloudamqp.com/blog/2017-12-29-part1-rabbitmq-best-practice.html#separate-connections-for-publisher-and-consumer)Separate connections for publisher and consumer

Separate connections for publisher and consumer to get high throughput. RabbitMQ can apply back pressure on the TCP connection when the publisher is sending too many messages to the server to handle. If you consume on the same TCP connection the server might not receive the message acknowledgements from the client. Thus, the consume performance will be affected too. And with lower consume speed the server will be overwhelmed.

### [](https://www.cloudamqp.com/blog/2017-12-29-part1-rabbitmq-best-practice.html#a-large-number-of-connections-and-channels-might-affect-the-rabbitmq-management-interface-performance)A large number of connections and channels might affect the RabbitMQ management interface performance

Another effect of having a large number of connections and channels is the performance of the RabbitMQ management interface. For each connection and channel performance, metrics have to be collected, analyzed and displayed.

## Acknowledgements and Confirms

Messages in transit might get lost in an event of a connection failure, and such a message might need to be retransmitted. Acknowledgements let the server and clients know when to retransmit messages. The client can either ack the message when it receives it, or when the client has completely processed the message. Acknowledgement has a performance impact, so for fastest possible throughput, manual acks should be disabled.

A consuming application that receives important messages should not acknowledge messages until it has finished whatever it needs to do with them so that unprocessed messages (worker crashes, exceptions etc) don't go missing.

Publish confirm, is the same thing, but for publishing. The server acks when it has received a message from a publisher. Publish confirm also has a performance impact. However, one should keep in mind that it’s required if the publisher needs at-least-once processing of messages.

### [](https://www.cloudamqp.com/blog/2017-12-29-part1-rabbitmq-best-practice.html#unacknowledged-messages)Unacknowledged messages

All unacknowledged messages have to reside in RAM on the servers. If you have too many unacknowledged messages you will run out of memory. An efficient way to limit unacknowledged messages is to limit how many messages your clients prefetch. Read more about prefetch in the prefetch section.

## Persistent messages and durable queues

If you cannot afford to lose any messages, make sure that your queue is declared as “durable” and your messages are sent with delivery mode "persistent".

In order to avoid losing messages in the broker, you need to be prepared for broker restarts, broker hardware failure, or broker crashes. To ensure that messages and broker definitions survive restarts, we need to ensure that they are on disk. Messages, exchanges, and queues that are not durable and persistent will be lost during a broker restart.

Persistent messages are heavier as they have to be written to disk. Keep in mind that lazy queues will have the same effect on performance, even though you are sending transient messages. For high performance - use transient messages.

## TLS and AMQPS

You can connect to RabbitMQ over AMQPS, which is the AMQP protocol wrapped in TLS. TLS has a performance impact since all traffic has to be encrypted and decrypted. For maximum performance we recommend using VPC peering instead, then the traffic is private and isolated without involving the AMQP client/server.

At CloudAMQP we configure the RabbitMQ servers to only accept and prioritize fast but secure encryption ciphers.

Resumed from "Prefetch"

[https://www.cloudamqp.com/blog/2017-12-29-part1-rabbitmq-best-practice.html](https://www.cloudamqp.com/blog/2017-12-29-part1-rabbitmq-best-practice.html)


<!--stackedit_data:
eyJoaXN0b3J5IjpbNzQ1MzIyNDc0LDQzNjg2OTk0NSwtOTk4Mz
E0Nzk4LDgwODgyNjQ1NCwxNjcyNjA2MTUsLTY2MjcyMjE3Mywt
NjcxNTE1NTU4XX0=
-->