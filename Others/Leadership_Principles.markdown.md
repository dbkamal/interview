# Amazon LP

## Customer Obsession

Leaders start with the customer and work backwards. They work vigorously to earn and keep customer trust. Although leaders pay attention to competitors, they obsess over customers.

## Ownership

Leaders are owners. They think long term and don’t sacrifice long-term value for short-term results. They act on behalf of the entire company, beyond just their own team. They never say “that’s not my job."

## Invent and Simplify

Leaders expect and require innovation and invention from their teams and always find ways to simplify. They are externally aware, look for new ideas from everywhere, and are not limited by “not invented here." As we do new things, we accept that we may be misunderstood for long periods of time.

## Are Right, A Lot

Leaders are right a lot. They have strong judgment and good instincts. They seek diverse perspectives and work to disconfirm their beliefs.

## Learn and Be Curious

Leaders are never done learning and always seek to improve themselves. They are curious about new possibilities and act to explore them.

## Hire and Develop the Best

Leaders raise the performance bar with every hire and promotion. They recognize exceptional talent, and willingly move them throughout the organization. Leaders develop leaders and take seriously their role in coaching others. We work on behalf of our people to invent mechanisms for development like Career Choice.

## Insist on the Highest Standards

Leaders have relentlessly high standards — many people may think these standards are unreasonably high. Leaders are continually raising the bar and drive their teams to deliver high quality products, services, and processes. Leaders ensure that defects do not get sent down the line and that problems are fixed so they stay fixed.

## Think Big

Thinking small is a self-fulfilling prophecy. Leaders create and communicate a bold direction that inspires results. They think differently and look around corners for ways to serve customers.

## Bias for Action

Speed matters in business. Many decisions and actions are reversible and do not need extensive study. We value calculated risk taking.

## Frugality

Accomplish more with less. Constraints breed resourcefulness, self-sufficiency, and invention. There are no extra points for growing headcount, budget size, or fixed expense.

## Earn Trust

Leaders listen attentively, speak candidly, and treat others respectfully. They are vocally self-critical, even when doing so is awkward or embarrassing. Leaders do not believe their or their team’s body odor smells of perfume. They benchmark themselves and their teams against the best.

## Dive Deep

Leaders operate at all levels, stay connected to the details, audit frequently, and are skeptical when metrics and anecdote differ. No task is beneath them.

## Have Backbone; Disagree and Commit

Leaders are obligated to respectfully challenge decisions when they disagree, even when doing so is uncomfortable or exhausting. Leaders have conviction and are tenacious. They do not compromise for the sake of social cohesion. Once a decision is determined, they commit wholly.

## Deliver Results

Leaders focus on the key inputs for their business and deliver them with the right quality and in a timely fashion. Despite setbacks, they rise to the occasion and never settle.

## Common QA
### Tell me about yourself

`Passion`: I'm a Software engineer with 9+ years of exp working on IT service company mainly as a full stack developer and web developer. I have designed and developed various applications, software component, software prototype from scratch.

`experience`: have worked on multiple platform, from front end and to back ends application and developed highly scalable and reliable web application using multiple framework & technology like React, NodeJS, Java, JavaScript, Spring. Have strong understanding on the various software architecture and methodologies and help client to make more stable software by designing and developing the databases, middleware, DevOPS and Cloud environment. Also, have worked on many projects migrating software compoment from one platform to another to make the software more robust and highly available as per business need. Applied the industry best practices on the application and trains end users and team member about the application, system design and cuting edge technologies.

`next`: I'm currently pursing master degree on computer science on machine learning apart from working full time job. I'm looking for a role that will allow me to leverage my experience and skills to the company. I love solving critical problems and produces optimal result. Can easily work with various cross functional team and help them reduce the application bottleneck.

### Why should I hire you
`passion & exp`: have always loved finding working on a new team of developers where i can help to solve any challenges, any system bottlenecks or developing a software component or prototype from scatch based on my experience and skillset. Can work with customer to help them making robust application by guiding or providing the industry best practices on front end, back end, databases, devOPS and cloud system. As I mostly worked at client site for developing and managing the customer application based on their needs, so I can utilize my experience here as well: how to handle customer request, prioritize the customer needs.

`some acheivements`

I'm a quick learner, so I can learn a new skillset within a short period of time to migitate any knowledge gap if essential to perform a certain/critical task within a team. Working with different highly professional also will help me to gain more understanding on the applications.

### Why did you want to move into a new company?

I love working here but want to take more responsibilities and also looking for more opportunies to work on ML/AI system and/or highly available and scalable system which can handle millions/billions of users

`another answer`: I enjoyed my current job but wanted to take more responsibilities and also looking for more opportunies to work on truely scalable system which can handle millions / billions user. So far, I work with highly scalable and available system but the number of concurrent and total user base is somewhat limited to 10K. Also, I wanted to work more on challenging projects which will help me gaining more knowledge in various aspects.

### Have you ever not delivered the work promised?

`Situation`: When I joined the company after finishing my college, had very little exposure with the IT service industry and how the real world programming happens. My first project was to maintain production RDBMS databases and apply bug fixes on the existing PL/SQL codebases which had almost 100+ packages
`Task` I had to find the wrong database validation code within those packages, but due to lack of testing knowledge, I was not able to remove all the wrong validation which made the ETL process running long hours than the usual running time. The end result, I was not able to deliver the project within the timeline.
`Action` So, I had to go back to the codebase again and focus more on the unit testing and parallely I tried to learn as much as possible how to do better unit testing on the PL/SQL code
`Result` This was a really a learning experience for me to make sure proper testing in place prior to move the code into live production and I was able to push my changes within a week time and customer was somewhat happy that I made it.

### Have you ever improved anything thinking from the consumer's perspective? Was this delivered? What was the consumer feedback?

### Have you ever had a conflict with your manager? How did you resolve it?


### Notes
[https://medium.com/@scarletinked/are-you-the-leader-were-looking-for-interviewing-at-amazon-8301d787815d](https://medium.com/@scarletinked/are-you-the-leader-were-looking-for-interviewing-at-amazon-8301d787815d)

- I added one functionality that which help customer quickly get the top K issues while navigating the system. I convince my client that it would be very helpful for the end user if we do something like this out of the box
- earn customer trust
- take ownership of what you are doing
- help out other engineer/team/person to solve any kind of problems
- help team to fix bugs rather than just test the features as the team had terrible critical deadline and also I documented the bug and fix processes
- reduce or remove code for better simplicty, more productivity and optimized solutions
- we are never done
- Our goal is never to deliver to requirements — to only meet our goals. At all times — once you look past our official project goals — our goal is to improve. Our goal is to never accept that something is broken, to never feel that anything less than a perfect product is acceptable.
- Then how did you fix the lead generation tool?

“_Actually we really didn’t have the time to maintain it or rebuild it, but they really needed a tool to track their work. I realized that our bug tracking system had most of the features they needed. Certainly wasn’t perfect, but I explained how they could use it, and it worked out fine. One less system to maintain._”

- > How did you reduce the memory footprint?
> 
> “_It’s funny, we had to restart our system every few months because of a memory leak. I’d just joined the team, and the mystery annoyed me. So I spent a few hours reading very carefully through our memory management code, and realized that we incorrectly double-parsing in one of our helper functions. Not only did it save on memory, but CPU as well. It’d been there for years._”

I love when I ask questions of people, and they can go 4 or 5 levels deep, and keep getting more excited because the details are actually interesting to them.

**For every project, find out couple of optimization you did, convince customer, disagree with product manager, help other team fixing the bug (what bug, how did you fix it), example of taking ownership, your mistake and how are overcome it, what you are doing/thinking improving the current model/customer experience/, how are you helping customer to have more cheaper simpler solution**

### What are you currently doing on a project?

I've been working on a project in last two years where I migrated the multi-page application to a single page app mainly on React and move the on-premise servers on the AWS platform. The customer has lots of legacy software component mainly developed with Spring MVC and other older tech stack. So, rather than upgrading the software component we proposed a relatively fresh and better web application which can help end user ultimately. So, there are lots of new tech stack involved to develop the application as the current model uses NodeJS in the backend.

[https://dev.to/aizkhaj/what-was-your-most-challenging-experience-in-a-project-youve-worked-on-and-why-49pk](https://dev.to/aizkhaj/what-was-your-most-challenging-experience-in-a-project-youve-worked-on-and-why-49pk)

[https://www.quora.com/As-a-software-engineer-developer-what-was-the-most-difficult-problem-you-faced-and-how-did-you-handle-it-and-what-was-the-result](https://www.quora.com/As-a-software-engineer-developer-what-was-the-most-difficult-problem-you-faced-and-how-did-you-handle-it-and-what-was-the-result)
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTkwOTg0MjE1OSwtNzQ3NzgxMzM4LC0xMz
IzNzkwMjk1LDE2MDE3MTg2OTUsNDYwMjAyODUsLTEzNTIyNDEy
NzksLTk5OTk0NTcxOSwtMjAyNzIyMDYxNSwtMTM4MjE3MTUxMC
wxMzc5NTExNDA1LDExMzY5NzIxOSwxNjM3NDE5OTgxLC0xMjc1
MzM1MjY1LDcwOTc5NDQ0NCwtMTY3MjgzNzc1MCwtMTM4MjcyOD
c4MywtNzk2ODc2ODQsLTgzNDI3MjA4MSwtMjk0NTY5MTk4LC0x
MjQzNjM4Nzg5XX0=
-->