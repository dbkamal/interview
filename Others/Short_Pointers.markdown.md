**MD5** 
- MD5 (Message-Digest Algorithm) is a cryptographic hash function whose main purpose is to verify that a file has been unaltered.
- Instead of confirming that two sets of data are identical by comparing the raw data, MD5 does this by producing a checksum on both sets and then comparing the checksums to verify that they're the same.
- MD5 hashes are 128-bits in length

**Checksum**
- A checksum is the outcome of running an algorithm, called a cryptographic hash function, on a piece of data, usually a single file. 
- Comparing the checksum that you generate from your version of the file, with the one provided by the source of the file, helps ensure that your copy of the file is genuine and error free.
- [https://www.lifewire.com/what-does-checksum-mean-2625825](https://www.lifewire.com/what-does-checksum-mean-2625825)
- Use case: if you download a large file from website, how do you know that the file downloaded properly? What if a few bits were dropped during the download and the file you have on your computer right now isn't _exactly_ what was intended? This is where comparing checksums can put your mind at ease. Assuming the website you downloaded the file from provides the checksum data alongside the file to be downloaded, you can then use a checksum calculator to produce a checksum from your downloaded file.

** base-64 **
- In computer science, Base64 is a group of binary-to-text encoding schemes that represent binary data in an ASCII string format by translating it into a radix-64 representation.
- Each Base64 digit represents exactly 6 bits of data
- base64 incluses [a-z], [A-Z], [0 - 9], [- .]

**SSH**
- Secure Shell (SSH) is a protocol and a method for secure remote login from one computer to another
- providing secure access for users and automated processes
- interactive and automated file transfers and issue remote commands
- managing network infrastructure and other mission-critical system components

**How SSH works**
The protocol works in the client-server model, which means that the connection is established by the SSH client connecting to the SSH server. The SSH client drives the connection setup process and uses public key cryptography to verify the identity of the SSH server. After the setup phase the SSH protocol uses strong symmetric encryption and hashing algorithms to ensure the privacy and integrity of the data that is exchanged between the client and server.

The figure below presents a simplified setup flow of a secure shell connection. Read more [https://www.ssh.com/ssh/protocol/](https://www.ssh.com/ssh/protocol/)

![How does the SSH protocol work?](https://www.ssh.com/s/how-does-ssh-protocol-work-920x272-SWKuhzNV.png)

**Symmetric encryption**
- Symmetric encryption is a type of encryption where only one key (a secret key) is used to both encrypt and decrypt electronic information. 
- Symmetric encryption must exchange the key so that it can be used in the decryption process. 
- This encryption method differs from asymmetric encryption where a pair of keys, one public and one private, is used to encrypt and decrypt messages.
**Asymmetric encryption**
- Asymmetric encryption uses two keys to encrypt a plain text and public keys are exchanged over the Internet or a large network which is use to encrypt the message
- The second private key is kept a secret only within the server.
- A message that is encrypted using a public key can only be decrypted using a private key, while also, a message encrypted using a private key can be decrypted using a public key

 **Website Performance best practices**
 - `Critical render path:`Discuss in the below sections
 - Client side optimized code
 - Progressive webapps
 - `Minimize files:` use any build tool (like webpack) to minimize the file by removing whitespaces and extra character before sending over to the internet. Use difference sized images based on the media size. Also use appropriate image files and compress them before sending. Use `@media queries in css`
 - `Minimize delivery over the network:` try to use less number of library into your application and try to utilize the standard function what HTML, CSS and JavaScript provides. For example, rather than using Bootstrap for styling, you can use flexbox and standard CSS grid for your website unless you plan to use maximum functionality from Bootstrap. Same goes for JavaScript. If you are trying to use couple of functionality from a library, its better to write your own which could save lots of uncessary library files to download.
 - CDN
 - Load balancing
 - Caching
 - DB scaling
 - GZip

**Critical Rendering Path**
Ref:
1. [https://developers.google.com/web/fundamentals/performance/critical-rendering-path/](https://developers.google.com/web/fundamentals/performance/critical-rendering-path/)

- Optimizing for performance is all about understanding what happens in these intermediate steps between receiving the HTML, CSS, and JavaScript bytes and the required processing to turn them into rendered pixels - that's the **critical rendering path**.
- Once browser receives the HTML and CSS files, HTML markup is transformed into a Document Object Model (DOM); CSS markup is transformed into a CSS Object Model (CSSOM). DOM and CSSOM are independent data structures
- Every time the browser processes HTML markup, it goes through all of the steps: convert bytes to characters, identify tokens, convert tokens to nodes, and build the DOM tree. This entire process can take some time, especially if we have a large amount of HTML to process.
- While the browser was constructing the DOM of our simple page, it encountered a link tag in the head section of the document referencing an external CSS stylesheet: `style.css`. Anticipating that it needs this resource to render the page, it immediately dispatches a request for this resource
- Load JS file asyncronously `<script async>` or `<script defer`

**Profile**
[https://learning.oreilly.com/library/view/high-performance-python/9781449361747/ch02.html](https://learning.oreilly.com/library/view/high-performance-python/9781449361747/ch02.html)

**Columnar Database vs RDBMS**

While a relational database is optimized for storing rows of data, typically for transactional applications, a columnar database is optimized for fast retrieval of columns of data, typically in analytical applications. Column-oriented storage for database tables is an important factor in analytic query performance because it drastically reduces the overall disk I/O requirements and reduces the amount of data you need to load from disk.
Like other NoSQL databases, column-oriented databases are designed to scale “out” using distributed clusters of low-cost hardware to increase throughput, making them ideal for data warehousing and Big Data processing.

Ref: [https://www.youtube.com/watch?v=8KGVFB3kVHQ](https://www.youtube.com/watch?v=8KGVFB3kVHQ)

**Database Choice**

[https://www.youtube.com/watch?v=v5e_PasMdXc](https://www.youtube.com/watch?v=v5e_PasMdXc)

**Instagram Architecture**

1. [https://www.youtube.com/watch?v=hnpzNAPiC0E](https://www.youtube.com/watch?v=hnpzNAPiC0E)
2. [http://highscalability.com/blog/2011/12/6/instagram-architecture-14-million-users-terabytes-of-photos.html](http://highscalability.com/blog/2011/12/6/instagram-architecture-14-million-users-terabytes-of-photos.html)

**High availability and scalable reads in PostgreSQL**

[https://blog.timescale.com/blog/scalable-postgresql-high-availability-read-scalability-streaming-replication-fb95023e2af/](https://blog.timescale.com/blog/scalable-postgresql-high-availability-read-scalability-streaming-replication-fb95023e2af/)

**Web socket vs HTTP long pulling**

1. [https://dev.to/moz5691/websocket-vs-long-polling-http-412f](https://dev.to/moz5691/websocket-vs-long-polling-http-412f)
2. [https://www.pubnub.com/blog/http-long-polling/](https://www.pubnub.com/blog/http-long-polling/)

**Canary Deploy**

[https://rollout.io/blog/canary-deployment/](https://rollout.io/blog/canary-deployment/)
<!--stackedit_data:
eyJoaXN0b3J5IjpbNDg2MDY0MjMsLTE3NDY0MDUyOTEsLTM2OD
k4NzU5NywxNDA3ODUzOTI1LDY5MDE5MTcwMywxMzY0OTM4MDI3
LC0xNjUxMTAwODIyLC0xMjY1NzU4OTgwLC0xOTY4NDcwMDYxLD
EyOTgwMjYzNTMsMTUzMjY2NzAwMSwtMTc0Njc1NDI4MSwyMDMw
NzgyOTU0LDMxODc2OTA0MCw0Nzc3OTQ1MDgsNzcyOTQwMjYsOD
Y4MzUwNDEzLDE3NDY1Njc5NTgsNzU5NTkzNTAyLC03MTY5NTg2
Nl19
-->