# System Design

Ref: [https://lethain.com/introduction-to-architecting-systems-for-scale/](https://lethain.com/introduction-to-architecting-systems-for-scale/)

## Latency vs Throughput

_Latency_  is the time required to perform some action or to produce some result. Latency is measured in units of time -- hours, minutes, seconds, nanoseconds or clock periods.

_Throughput_  is the number of such actions executed or results produced per unit of time. This is measured in units of whatever is being produced (cars, motorcycles, I/O samples, memory words, iterations) per unit of time. The term "memory bandwidth" is sometimes used to specify the throughput of memory systems.

## Scalability

### Vertical and Horizontal Scaling

Horizontal scaling means that you scale by adding more machines into your pool of resources whereas Vertical scaling means that you scale by adding more power (CPU, RAM) to an existing machine.

An easy way remember this is to think of a machine on a server rack, we add more machines across the horizontal direction and add more resources to a machine in the vertical direction.

**Horizontal Scaling** also referred to as "scale-out" is basically the addition of more machines or setting up a cluster or a distributed environment for your software system. This usually requires a load-balancer program which is a middle-ware component in the standard 3 tier client-server architectural model.

Load-Balancer is responsible to distribute user requests (load) among the various back-end systems/machines/nodes in the cluster. Each of these back-end machines run a copy of your software and hence capable of servicing requests. This is just one of the various functions that load balancer may be performing. Another very common responsibility is "health-check" where the load balancer uses the "ping-echo" protocol or exchanges heartbeat messages with all the servers to ensure they are up and running fine.

Load-Balancer distributes load by maintaining state of each machine -- how many requests are being served by each machine, which machine is idle, which machine is over-loaded with queued requests etc. So the load balancing algorithm considers such things before redirecting the request to an appropriate server machine. It also takes into account the network overhead and might choose the server in the nearest data-center provided it is available to service the requests.

The request-response can also be done in 2 different ways:

- Load Balancer always acts as an intermediary program for every response - In this case, once the request has been handed over to the server by the load balancer, any response from the server to the user will go through the load balancer. So the server machines that are actually servicing the request will never directly interface with the user machine running the client application. The machine hosting the load balancer program will be handling all the requests/responses to and from the user. 

- Load Balancer does not act as an intermediary for the responses coming from the server machine - In this case, once the server has received the request from load-balancer, it bypasses the load balancer and communicates it responses directly to the client. Setting up a cluster and load-balancer as a front-end interface to the client application does not really complete our scale-out architecture and design. There are still lots of critical questions to be answered and a number of key design decisions to be made which will affect the overall properties of our system.

We first need to identify our business goals and areas where we would like to add value. These goals will give rise to various requirements. We should then ask ourselves various questions with respect to different systemic properties.

Will such a design address our performance requirements ? What performance characteristics do we care about ? Is it the overall system throughput where we are interested in serving maximum number of requests in any given time ? Or is it the system's response time where we design for sending back the response to client in as less time as possible ? Both these and many other types of performance characteristics are related to each other. Will such a design address our availability requirements ? Is the system fault-tolerant ? If so, what is the degree of it ? Is such a design reliable ? Does it impact the correctness ? We should not forget that 100% correctness is an implicit goal of any system. Are we really meeting our scalability goals ? Might be achieving the short-term or immediate ones, but what is going to happen in the long run ? All these kinds of requirements should have quantifiable measures associated with them.

We should then make important design decisions by questioning ourselves, developing prototypes, and refining the design.

Firstly, is using load-balancer the only approach to distribute load and horizontally scale the system ? Do the various back-end servers or nodes communicate with each other ? If yes, then how does the system address the situation where one or more nodes go down -- permanently or temporarily ? If yes, then how does the system address the situation where the network connecting the nodes is down, but all the nodes are up and running ? Most importantly, do we have to differentiate between these two situations ? How ? Whether or not the back-end nodes communicate with each other, does our system need to maintain consistent data across all the nodes ? What level of consistency do we care about ? Is it that At any point in time, data across all the nodes should be consistent. Or later some point in time, data across all the nodes will be consistent. If so, then what is this "later" ? When and how do all the nodes converge to a consistent state ? How will we achieve "total-order" of operations across all the nodes ? Do we have a global clock ? If we are relying on every node's local clock, then how do we synchronize the clocks of all machines. They can easily seem to regress or a machine with an out of order clock might join the cluster. As a consequence, we may ignore the latest data, and consider old/stale data as the latest one. What cluster setup do we have to design for ? Is it a "replica" cluster, where data on each node is replicated to some or every other node. In case of former, what is the replication factor, and how do we decide it ? Or is it a sharded cluster where cluster is divided into various shards or units. A shard is a designated group of nodes. Each shard takes care of a particular partition of data. Data across shards is not replicated, but each shard can adopt replication strategy within itself. Whatever distributed system we design, it should ideally be able to answer the above and many other similar questions. All this is what makes a distributed system so interesting and challenging to design and implement.

**Vertical Scaling -**  also referred to as "scale-up" approach is an attempt to increase the capacity of a single machine : By adding more processing power By adding more storage More memory.

What is important here is to understand the differences between these 2 scaling approaches, identify what suits our requirements, and see if the application really fits the model we choose.

As you would have understood by now, horizontal scaling comes with overhead in the form of cluster setup, management, and maintenance costs and complexities. The design gets increasingly complex and programming model changes.

So simply throwing in new hardware and adding more nodes or machines is not the way to start. First, see if the requirements can be met by increasing the capacity or tuning characteristics of a single machine. If not, then go with the scale-out approach or a combination of both.

Ref: [https://medium.com/system-designing-interviews/approach-a-system-design-interview-f3594e243730](https://medium.com/system-designing-interviews/approach-a-system-design-interview-f3594e243730)

### Overview of Scalability, Horizontal Scaling, and Vertical Scaling

Scaling a database can become necessary for a variety of reasons:

1.  Support a higher volume of users
2.  Provide better performance for existing users
3.  Store a larger volume of data
4.  Improve system availability
5.  Geographic dispersion (generally related to performance and availability)

Options for scaling a database can be grouped into two major categories: Vertical and Horizontal. Scaling vertically generally describes adding capacity to the existing database instance(s) without increasing the number of servers or database instances. Horizontal Scaling, on the other hand, refers the alternative approach of adding servers and database instances.

### Scaling Vertically

The easiest way to increase the capacity of a MySQL database is to upgrade the server hardware. Most often, this means adding CPU and/or RAM, but can also include disk I/O capacity. Scaling vertically can be done by upgrading existing server hardware or by moving to a new server.

1.  Easiest, simplest option for scaling your MySQL database
2.  Doesn’t improve availability (db is still running on a single box)
3.  Good for boosting CPU and RAM, but limited potential for improving network I/O or disk I/O
4.  Beefy hardware is expensive
5.  There is a finite limit to the capacity achievable with vertical scaling

### Scaling Horizontally

For projects with requirements for high availability or failover, adding additional servers can be the right solution. Horizontal scaling can be done with a variety of configurations: Active-Passive, Master-Slave, Cluster, or Sharding.

### Active-Passive

With two database instances, one is the active instance and the other is the passive. All queries (reads and writes) are directed to the active instance. Write operations are replicated to the passive instance to keep it in sync. In the event of failure of the active instance, query and update traffic is redirected to the passive instance. For more information on MySQL replication, look here:  [http://dev.mysql.com/doc/refman/5.1/en/replication.html](https://www.oshyn.com/blogs/2011/november/For%20more%20information%20on%20MySQL%20replication,%20look%20here:%20http://dev.mysql.com/doc/refman/5.1/en/replication.html).

1.  Improves availability and failover
2.  Doesn’t improve capacity or performance (over a single instance of the same size)
3.  Redirecting traffic from the active instance to the passive can be done automatically (recommended) with a load-balancer or switch, or can be done manually using a VIP or internal DNS configuration change

![](https://cdn-images-1.medium.com/max/1600/1*jQmHEBJoRkGLerlDkasGWg.gif)

### Master-Slave

Using two or more database instances, one instance is the master and the others are slaves. All writes are routed to the master instance, then replicated to the slaves. Read operations are routed to the slave instances. In the event of failure of the master, one of the slave instances must be designated as the new master and update operations rerouted.

1.  An unlimited number of slave instances can be added, but only one instance can be the master
2.  Great for applications with mostly READ operations
3.  Good for addressing I/O (network or disk) bottlenecks
4.  Good for geographic dispersion
5.  Requires application logic to separate read and write operations
6.  Replication is asynchronous, so instances are not guaranteed to be in sync
7.  Master instance can be scaled vertically

**Question:** Design the data structures for a very large social network. Describe how you would design an algorithm to show the connection, or path, between two people (e g , Me -> Kumar-> Verma-> Atul-> You)?

**Approach**: Forget that we’re dealing with millions of users at first Design this for the simple case.

We can construct a graph by assuming every person is a node and if there is an edge between two nodes, then the two people are friends with each other class Person { Person[] friends; // Other info }

If I want to find the connection between two people, I would start with one person and do a simple breadth first search But… oh no! Millions of users! When we deal with large social network, means that our simple Person data structure from above doesn’t quite work — our friends may not live on the same machine as us.

Instead, we can replace our list of friends with a list of their IDs, and traverse as follows:

1 For each friend ID: int machineID = lookupMachineForUserID(id);

2. Person friend = lookupFriend(machineID, ID);

There are more optimizations and follow up questions here than we could possibly discuss, but here are just a few thoughts:

**Optimization**: Reduce Machine Jumps, Jumping from one machine to another is expensive Instead of randomly jumping from machine to machine with each friend, try to batch these jumps — e g , if 5 of my friends live on one machine, I should look them up all at once Optimization: Smart Division of People and Machines People are much more likely to be friends with people who live in the same country as them Rather than randomly dividing people up across machines, try to divvy them up by country, city, state, etc This will reduce the number of jumps

**Question**: Breadth First Search usually requires “marking” a node as visited. How do you do that in this case?

**Approach**: Usually, in BFS, we mark a node as visited by setting a flag visited in its node class. Here, we don’t want to do that (there could be multiple searches going on at the same time, so it’s bad to just edit our data) In this case, we could mimic the marking of nodes with a hash table to lookup a node id and whether or not it’s been visited.

**Other Follow-Up Questions:**  In the real world, servers fail, How does this affect you? How could you take advantage of caching? Do you search until the end of the graph (infinite)? How do you decide when to give up?

In real life, some people have more friends of friends than others, and are therefore more likely to make a path between you and someone else How could you use this data to pick where you start traversing?

## Sharding

Ref: 
[https://www.digitalocean.com/community/tutorials/understanding-database-sharding](https://www.digitalocean.com/community/tutorials/understanding-database-sharding)

1. [https://www.acodersjourney.com/database-sharding/](https://www.acodersjourney.com/database-sharding/)
2. [https://www.youtube.com/watch?v=5faMjKuB9bc&t=17s](https://www.youtube.com/watch?v=5faMjKuB9bc&t=17s)
3. [https://medium.com/system-designing-interviews/system-design-chapter-2-sharding-484960c18f6](https://medium.com/system-designing-interviews/system-design-chapter-2-sharding-484960c18f6)
4. [https://instagram-engineering.com/sharding-ids-at-instagram-1cf5a71e5a5c](https://instagram-engineering.com/sharding-ids-at-instagram-1cf5a71e5a5c)

### What is Sharding or Data Partitioning?

Sharding (also known as Data Partitioning) is the process of splitting a large dataset into many small partitions which are placed on different machines. Each partition is known as a "shard".

Each shard has the same database schema as the original database. Most data is distributed such that each row appears in exactly one shard. The combined data from all shards is the same as the data from the original database.

The two illustrations below shows a system with no sharding and another with a simple sharding scheme.

[![No Sharding](https://i1.wp.com/www.acodersjourney.com/wp-content/uploads/2018/08/No-Sharding.jpg?resize=800%2C228&ssl=1 "No Sharding")](https://i1.wp.com/www.acodersjourney.com/wp-content/uploads/2018/08/No-Sharding.jpg)

No Sharding

[![Simple Sharding Scheme](https://i2.wp.com/www.acodersjourney.com/wp-content/uploads/2018/08/Simple-Sharding-Scheme.jpg?resize=800%2C344&ssl=1 "Simple Sharding Scheme")](https://i2.wp.com/www.acodersjourney.com/wp-content/uploads/2018/08/Simple-Sharding-Scheme.jpg)

Note that the sharded architecture is transparent to the client application. The client application keeps on talking to the database shards(partitions) as if it was talking to a single database.

### What scalability problems are solved by Sharding?

As more users are onboarded on your system, you'll experience performance degradation with a single database server architecture . Your read queries and updates will start become slower and your network bandwidth may be start to saturate . You'll probably start running out of disk space on your database server at some point.

Sharding helps to fix all the above issues by distributing data across a cluster of machines. In theory, you can have a huge number of shards thereby providing virtually unlimited horizontal scaling for your database.

### Is each shard located on a different machine ?

Each shard may be located on the same machine (coresident) or on different machines(remote).

The motivation for co-resident partitioning is to reduce the size of individual indexes and reduce the amount of I/O (input/output) that is needed to update records.

The motivation for remote partitioning is to increase the bandwidth of access to data by having more RAM in which to store data, by avoiding disk access, or by having more network interfaces and disk I/O channels available.

### What are some common Sharding or Data Partitioning Schemes ?

There are four common sharding strategies:

#### 1. Horizontal or Range Based Sharding

In this case, the data is split based on the  **_value ranges_**  that are inherent in each entity. For example, the if you store the contact info for your online customers, you might choose to store the info for customers whose last name starts with A-H on one shard, while storing the rest on another shard.

The disadvantage of this scheme is that the last names of the customers may not be evenly distributed. You might have a lot more customers whose names fall in the range of A-H than customers whose last name falls in the range I-Z. In that case, your first shard will be experiencing a much heavier load than the second shard and can become a system bottleneck.

Nevertheless, the benefit of this approach is that it's the simplest sharding scheme available. Each shard also has the same schema as the original database. Your application layer is relatively simple because in most scenarios, you'll not need to combine data from multiple shards to answer any query.

It works well for relative non static data -- for example to store the contact info for students in a college because the data is unlikely to see huge churn.

[![Horizontal Sharding](https://i0.wp.com/www.acodersjourney.com/wp-content/uploads/2018/08/Horizontal-Sharding.jpg?resize=800%2C585&ssl=1 "Horizontal Sharding")](https://i0.wp.com/www.acodersjourney.com/wp-content/uploads/2018/08/Horizontal-Sharding.jpg)

#### 2. Vertical Sharding

In this case, different features of an entity will be placed in different shards on different machines. For example, in a LinkedIn like application, an user might have a profile, a list of connection and a set of articles he has authored. In Vertical sharding scheme , we might place the various user profiles on one shard, the connections on a second shard and the articles on a third shard.

The main benefit of this scheme is that you can handle the critical part of your data (for examples User Profiles) differently from the not so critical part of your data (for example, blog posts) and build different replication and consistency models around it.

The two main disadvantages of vertical sharding scheme are as follows:

1.  Depending on your system, your application layer might need to combine data from multiple shards to answer a query. For example, a profile view request will need to combine data from the User Profile, Connections and Articles shard. This increases the development and operational complexity of the system.
2.  If your Site/system experiences additional growth then it may be necessary to further shard a feature specific database across multiple servers.

[![Vertical Sharding](https://i1.wp.com/www.acodersjourney.com/wp-content/uploads/2018/08/Vertical-Sharding.jpg?resize=800%2C588&ssl=1 "Vertical Sharding")](https://i1.wp.com/www.acodersjourney.com/wp-content/uploads/2018/08/Vertical-Sharding.jpg)

#### 3. Key or hash based sharding

In this case, an entity has a value ( Eg. IP address of a client application) which can be used as an input to a hash function and a resultant hash value generated. This hash value determines which database server(shard) to use.

As a simple example, imagine you have 4 database servers and each request contained an application id which was incremented by 1 every time a new application is registered.

In this case, you can simply perform a modulo operation on the application id with the number 4 and take the remainder to determine which server the application data should be placed on.

[![Consistent Hashing - Sharding data across several database servers](https://i0.wp.com/www.acodersjourney.com/wp-content/uploads/2017/08/Data-Sharding-Based-on-Modulo-number-of-servers.jpg?resize=800%2C444&ssl=1 "Data Sharding Based on Modulo number of servers")](https://i0.wp.com/www.acodersjourney.com/wp-content/uploads/2017/08/Data-Sharding-Based-on-Modulo-number-of-servers.jpg)

The main drawback of this method is that elastic load balancing ( dynamically adding/removing database servers) becomes very difficult and expensive.

For example, if we wanted to add 6 more servers, majority of the keys would need to be remapped and migrated to new servers. Also, the hash function will need to be changed from modulo 4 to modulo 10.

While the migration of data is in effect , neither the new nor the old hash function is fully valid. So in effect, a large number of the requests cannot be serviced and you'll incur a downtime till the migration completes.

This problem is easily solved by Consistent hashing.

#### 4. Directory based sharding

Directory based shard partitioning involves placing a **_lookup service_**  in front of the sharded databases. The lookup service knows the current partitioning scheme and keeps a map of each entity and which database shard it is stored on. The lookup service is usually implemented as a webservice.

The client application first queries the lookup service to figure out the shard (database partition) on which the entity resides/should be placed. Then it queries / updates the shard returned by the lookup service.

**What does this loose coupling buy us ?**

**​**It enables us to  **_solve the elastic scaling problem_**  described in the previous section without using ​Consistent Hashing.

Here's how: In the previous example, we had 4 database servers and a hash function that performed a modulo 4 operation on the application ids. Now, if we wanted to add 6 more database servers without incurring any downtime, we'll need to do the following steps:

1.  Keep the modulo 4 hash function in the lookup service .
2.  Determine the data placement based on the new hash function - modulo 10.
3.  Write a script to copy all the data based on #2 into the six new shards and possibly on the 4 existing shards. Note that it does not delete any existing data on the 4 existing shards.
4.  Once the copy is complete, change the hash function to modulo 10 in the lookup service
5.  Run a cleanup script to purge unnecessary data from 4 existing shards based on step#2. The reason being that the purged data is now existing on other shards.
6. There are two practical considerations which needs to be solved on a per system basis:

1.  While the migration is happening, the users might still be updating their data. Options include putting the system in read-only mode or placing new data in a separate server that is placed into correct shards once migration is done.
2.  The copy and cleanup scripts might have an effect on system performance during the migration. It can be circumvented by using system cloning and elastic load balancing - but both are expensive.

[![Directory Based Sharding](https://i1.wp.com/www.acodersjourney.com/wp-content/uploads/2018/08/Directory-Based-Sharding.jpg?resize=800%2C520&ssl=1 "Directory Based Sharding")](https://i1.wp.com/www.acodersjourney.com/wp-content/uploads/2018/08/Directory-Based-Sharding.jpg)

#### What Drives the Need for Database Sharding?

-   You can scale the system out by adding further shards running on additional storage nodes.
-   A system can use off-the-shelf hardware rather than specialized and expensive computers for each storage node.
-   You can reduce contention and improve performance by balancing the workload across shards.
-   In the cloud, shards can be located physically close to the users that’ll access the data.

#### Practicalities of Database Sharding

If Database Sharding is highly scalable, less costly, and improves performance, why hasn’t adoption of the technology been more widespread? Is it feasible for your organization?

The reality is that Database Sharding is a very useful technology, but like other approaches, there are many factors to consider that ensure a successful implementation. Further, there are some limitations and Database Sharding will not work well for every type of business application. This chapter discusses these critical considerations and how they can be addressed.

#### Challenges:

Due to the distributed nature of individual databases, a number of key elements must be taken into account:

1.  **Avoidance of cross-shard joins :** In a sharded system, queries or other statements that use inner-joins that span shards are highly inefficient and difficult to perform.
2.  **Auto-increment key management-** Typical auto-increment functionality provided by database management systems generate a sequential key for each new row inserted into the database. This is fine for a single database application, but when using Database Sharding, keys must be managed across all shards in a coordinated fashion.
3. **_Reliability_**_-_ The database tier is often the single most critical element in any reliability design, and therefore an implementation of Database Sharding is no exception. In fact, due to the distributed nature of multiple shard databases, the criticality of a well-designed approach is even greater. To ensure a fault-tolerant and reliable approach, the following items are required:

-   Automated backups of individual Database Shards.
-   Database Shard redundancy, ensuring at least 2 “live” copies of each shard are available in the event of an outage or server failure. This requires a high-performance, efficient, and reliable replication mechanism.
-   Cost-effective hardware redundancy, both within and across servers.
-   Automated failover when an outage or server failure occurs.
-   Disaster Recovery site management.

#### Summary:

Sharding comes in many forms. In the most basic sense, it describes breaking up a large database into many smaller databases. Sharding can include strategies like carving off tables, or cutting up tables vertically (by columns). I’m mostly referring to the strategy of “horizontal table partitioning” — dividing large tables by row. This is a relatively exotic configuration and generally used only by the most demanding applications.

1.  Good strategy for handling extreme database loads
2.  Easiest/most appropriate when most load is accessing a few tables and JOIN operations across shards are not required
3.  Success depends largely of sharding strategy and shard sizing
4.  May require painful periodic shard resizing
5.  Very high complexity
6.  Improves availability — The impact of a shard failure is small (affects only a portion of data), but sharding must be coupled with an additional strategy (like active-passive) to provide complete high-availability

## Load-Balancing

Ref:
1. [https://medium.com/system-designing-interviews/system-design-chapter-3-load-balancing-e1c89148e37](https://medium.com/system-designing-interviews/system-design-chapter-3-load-balancing-e1c89148e37)
2. [https://www.educative.io/collection/page/5668639101419520/5649050225344512/5747976207073280](https://www.educative.io/collection/page/5668639101419520/5649050225344512/5747976207073280)

Load Balancer (LB) is another critical component of any distributed system. It helps to spread the traffic across a cluster of servers to improve responsiveness and availability of applications, websites or databases. LB also keeps track of the status of all the resources while distributing requests. If a server is not available to take new requests or is not responding or has elevated error rate, LB will stop sending traffic to such a server.

Typically a load balancer sits between the client and the server accepting incoming network and application traffic and distributing the traffic across multiple backend servers using various algorithms. By balancing application requests across multiple servers, a load balancer reduces individual server load and prevents any one application server from becoming a single point of failure, thus improving overall application availability and responsiveness.

To utilize full scalability and redundancy, we can try to balance the load at each layer of the system. We can add LBs at three places:

-   Between the user and the web server
-   Between web servers and an internal platform layer, like application servers or cache servers
-   Between internal platform layer and database.

#### Benefits of Load Balancing

-   Users experience faster, uninterrupted service. Users won’t have to wait for a single struggling server to finish its previous tasks. Instead, their requests are immediately passed on to a more readily available resource.
-   Service providers experience less downtime and higher throughput. Even a full server failure won’t affect the end user experience as the load balancer will simply route around it to a healthy server.
-   Load balancing makes it easier for system administrators to handle incoming requests while decreasing wait time for users.
-   Smart load balancers provide benefits like predictive analytics that determine traffic bottlenecks before they happen. As a result, the smart load balancer gives an organization actionable insights. These are key to automation and can help drive business decisions.
-   System administrators experience fewer failed or stressed components. Instead of a single device performing a lot of work, load balancing has several devices perform a little bit of work.

#### Load Balancing Algorithm

**How does the load balancer choose the backend server?**  
Load balancers consider two factors before forwarding a request to a backend server. They will first ensure that the server they choose is actually responding appropriately to requests and then use a pre-configured algorithm to select one from the set of healthy servers. We will discuss these algorithms shortly.

**Health Checks**  - Load balancers should only forward traffic to “healthy” backend servers. To monitor the health of a backend server, “health checks” regularly attempt to connect to backend servers to ensure that servers are listening. If a server fails a health check, it is automatically removed from the pool, and traffic will not be forwarded to it until it responds to the health checks again.

There is a variety of load balancing methods, which use different algorithms for different needs.

-   **Least Connection Method**  — This method directs traffic to the server with the fewest active connections. This approach is quite useful when there are a large number of persistent client connections which are unevenly distributed between the servers.
-   **Least Response Time Method**  — This algorithm directs traffic to the server with the fewest active connections and the lowest average response time.
-   **Least Bandwidth Method**  - This method selects the server that is currently serving the least amount of traffic measured in megabits per second (Mbps).
-   **Round Robin Method**  — This method cycles through a list of servers and sends each new request to the next server. When it reaches the end of the list, it starts over at the beginning. It is most useful when the servers are of equal specification and there are not many persistent connections.
-   **Weighted Round Robin Method**  — The weighted round-robin scheduling is designed to better handle servers with different processing capacities. Each server is assigned a weight (an integer value that indicates the processing capacity). Servers with higher weights receive new connections before those with less weights and servers with higher weights get more connections than those with less weights.
-   **IP Hash**  — Under this method, a hash of the IP address of the client is calculated to redirect the request to a server.

#### Redundant Load Balancer

The load balancer can be a single point of failure; to overcome this, a second load balancer can be connected to the first to form a cluster. Each LB monitors the health of the other and, since both of them are equally capable of serving traffic and failure detection, in the event the main load balancer fails, the second load balancer takes over.

![](https://www.educative.io/api/collection/5668639101419520/5649050225344512/page/5747976207073280/image/5761673931522048.png)

#### What are the different types of load balancers?

Load balancing can be achieved in three ways:

1.  By using Software Load balancers in clients which requests data from a list of servers
2.  By using Software Load balancers in services layer
3.  By using Hardware Load balancers in services layer

#### Software Load Balancers in Clients

This is probably the cheapest way to implement load balancing . In this case, all logic for your load balancing resides on the client application. On startup, the client application (Eg. A mobile phone app) is provided with a list of web servers / application servers it can communicate with. The client app picks the first one in the list and requests data from the server. If a failure is detected persistently (after a configurable number of retries), it marks the first server as unavailable and picks another server from the list to request data from.

#### Software Load Balancers in Services

Software load balancers are pieces of software that receives a set of requests and routes them according to a set of rules. Unlike hardware load balancers, software load balancers do not require any specific type of hardware – they can be installed on any windows or linux machines. One has the option of either using an off-the-shelf software load balancer like  [HA Proxy](https://www.digitalocean.com/community/tutorials/how-to-use-haproxy-to-set-up-http-load-balancing-on-an-ubuntu-vps)  or writing their own custom software for Load balancing specific workload types. For example, when designing the Authentication platform for Microsoft Office365, we wrote a custom load balancer to load balance Active Directory Queries.

### Hardware Load Balancers

Hardware Load balancer device (HLD) is a physical device used to distribute web traffic across a cluster of network servers. HLDs basically present a virtual server address to the outside world and when client applications attempt to connect, it would forward the connection on the most appropriate real server doing bi-directional network address translation (NAT). HLDs, also known as Layer 4-7 Routers are typically able to load balance not only HTTP and HTTPS traffic, but also TCP and UDP traffics. For example, TCP traffic to a database cluster can be spread across all servers by a HLD.

The load balancer could control exactly which server received which connection and employed “health monitors” of increasing complexity to ensure that the application server (a real, physical server) was responding as needed; if not, it would automatically stop sending traffic to that server until it produced the desired response (indicating that the server was functioning properly).

HLDs, while remarkably flexible in terms of the type and scale of load balancing they perform, are expensive to acquire and configure. Because of this reason, most online service providers use HLDs at the first entry point of user requests into their infrastructure and then use internal software load balancers to route data behind their infrastructure wall.

For example, SharePoint online (back in 2012) had one  [F5 Big-IP Hardware Load Balancer](https://f5.com/products/deployment-methods/hardware)  in front of our web servers and used software load balancing in it’s application servers for load balancing across active directory instances and databases.

### What are the benefits of using load balancing?

Using a Load Balancer as the gateway to your internal cluster of web servers has the following benefits:
1. **Facilitate Zero-downtime rolling updates to web servers:**  This is done by effectively taking a web server (due for maintainance) out of the load balancer pool, waiting for all the active connections to “drain i.e. service requeests in progress” and then safely shutting down the server. This way, no client requests in flight are dropped and you can perform patching/maiantenance on the web servers without affecting you high availability SLA.
2. **Facilitate immediate increase in capacity:**  Adding more web servers to DNS for load balacing purposes takes time to propagate. DNS is basically an Eventually Consistent system. However, with Load balancers (hardware or software), as soon as you add a new server, it can start servicing the client requests immediately. Thus, you can increase your capacity at the flick of a switch)
3. **Enhance Fault Tolerance:** Load balancers enable a fault web server instance to be immediately taken out of rotation by removing it from the load balancer pool. This is much better than having to remove the server from DNS which takes time and during that window, the DNS will still be sending traffic to the faulty web server which will fail the client requests.  
4. **Reduce load on web servers through SSL termination:**  SSL offloading ( a.k.a SSL termination) is a load balancer feature that allows you to handle all SSL encryption/ decryption work on the load balancer and use un-encrypted connections internally between the load balancer and web servers. This removes a significant load of the web servers who no longer have to absorb the overhead of traffic encryption/decryption. It’s also possible to provide SSL acceleration using specialized hardware installed on the load balancer. Please check out https://kemptechnologies.com/solutions/ssl-acceleration-solutions/
5. **Facilitate Just In Time Load Balancing:** If your web servers are hosted in the cloud via AWS or Azure, you can add new workloads (web servers and front ends) depending on the load your system is experiencing. If you use the  [elastic load balancer (ELB)](https://aws.amazon.com/elasticloadbalancing/)  in AWS or the  [cloud load balancer](https://azure.microsoft.com/en-us/services/load-balancer/) in Azure, the scaling can happen automatically and just in time to accomodate your increasing/decreasing traffic. This automatic load balancing has three benefits – no downtime and low latency for your customers, no IT maintenance for the load balancer since it’s hosted in AWS or Azure cloud and cost savings because the system scales down automatically when traffic reduces.

#### Summary
In summary, you’ll almost ALWAYS be asked some sort of scalability question in system design interviews for which you’ll need to use a load balancer. The key things to remember from this article are:

-   Load balancing enables elastic scalability and redundancy (you can have many copies of the same data). Elastic scalability improves performance and throughput of data. Redundancy improves availability and also helps in backup/restore of service in case a few servers fail.

-   Load balancers can be placed at any software layer – refer to the section above for details.

-   Load balancers can be implemented in hardware or software. Many companies uses both at different scale points in their system.

## Caching

Ref:
1. [https://medium.com/system-designing-interviews/system-design-chapter-4-caching-b59a4cf83f10](https://medium.com/system-designing-interviews/system-design-chapter-4-caching-b59a4cf83f10)
2. [https://www.educative.io/collection/page/5668639101419520/5649050225344512/5643440998055936](https://www.educative.io/collection/page/5668639101419520/5649050225344512/5643440998055936)
3. [https://www.youtube.com/watch?v=U3RkDLtS7uY](https://www.youtube.com/watch?v=U3RkDLtS7uY)


Load balancing helps you scale horizontally across an ever-increasing number of servers, but caching will enable you to make vastly better use of the resources you already have as well as making otherwise unattainable product requirements feasible. Caches take advantage of the locality of reference principle: recently requested data is likely to be requested again. They are used in almost every layer of computing: hardware, operating systems, web browsers, web applications, and more. A cache is like short-term memory: it has a limited amount of space, but is typically faster than the original data source and contains the most recently accessed items. Caches can exist at all levels in architecture, but are often found at the level nearest to the front end where they are implemented to return data quickly without taxing downstream levels.

### Application server cache

Placing a cache directly on a request layer node enables the local storage of response data. Each time a request is made to the service, the node will quickly return local cached data if it exists. If it is not in the cache, the requesting node will query the data from disk. The cache on one request layer node could also be located both in memory (which is very fast) and on the node’s local disk (faster than going to network storage).

What happens when you expand this to many nodes? If the request layer is expanded to multiple nodes, it’s still quite possible to have each node host its own cache. However, if your load balancer randomly distributes requests across the nodes, the same request will go to different nodes, thus increasing cache misses. Two choices for overcoming this hurdle are global caches and distributed caches

###  Content Distribution Network (CDN)

Ref: [http://www.travelblogadvice.com/technical/the-differences-between-push-and-pull-cdns/](http://www.travelblogadvice.com/technical/the-differences-between-push-and-pull-cdns/)

CDNs are a kind of cache that comes into play for sites serving large amounts of static media. In a typical CDN setup, a request will first ask the CDN for a piece of static media; the CDN will serve that content if it has it locally available. If it isn’t available, the CDN will query the back-end servers for the file, cache it locally, and serve it to the requesting user.

If the system we are building isn’t yet large enough to have its own CDN, we can ease a future transition by serving the static media off a separate subdomain (e.g.  [static.yourservice.com](http://static.yourservice.com/)) using a lightweight HTTP server like Nginx, and cut-over the DNS from your servers to a CDN later.

[https://www.youtube.com/watch?v=farO15_0NUQ](https://www.youtube.com/watch?v=farO15_0NUQ)

### Cache Invalidation

While caching is fantastic, it does require some maintenance for keeping cache coherent with the source of truth (e.g., database). If the data is modified in the database, it should be invalidated in the cache; if not, this can cause inconsistent application behavior.

Solving this problem is known as cache invalidation; there are three main schemes that are used:

**Write-through cache:**  Under this scheme, data is written into the cache and the corresponding database at the same time. The cached data allows for fast retrieval and, since the same data gets written in the permanent storage, we will have complete data consistency between the cache and the storage. Also, this scheme ensures that nothing will get lost in case of a crash, power failure, or other system disruptions.

Although, write through minimizes the risk of data loss, since every write operation must be done twice before returning success to the client, this scheme has the disadvantage of higher latency for write operations.

**Write-around cache:**  This technique is similar to write through cache, but data is written directly to permanent storage, bypassing the cache. This can reduce the cache being flooded with write operations that will not subsequently be re-read, but has the disadvantage that a read request for recently written data will create a “cache miss” and must be read from slower back-end storage and experience higher latency.

**Write-back cache:**  Under this scheme, data is written to cache alone and completion is immediately confirmed to the client. The write to the permanent storage is done after specified intervals or under certain conditions. This results in low latency and high throughput for write-intensive applications, however, this speed comes with the risk of data loss in case of a crash or other adverse event because the only copy of the written data is in the cache.

###  Cache eviction policies

Following are some of the most common cache eviction policies:

1.  First In First Out (FIFO): The cache evicts the first block accessed first without any regard to how often or how many times it was accessed before.
2.  Last In First Out (LIFO): The cache evicts the block accessed most recently first without any regard to how often or how many times it was accessed before.
3.  Least Recently Used (LRU): Discards the least recently used items first.
4.  Most Recently Used (MRU): Discards, in contrast to LRU, the most recently used items first.
5.  Least Frequently Used (LFU): Counts how often an item is needed. Those that are used least often are discarded first.
6.  Random Replacement (RR): Randomly selects a candidate item and discards it to make space when necessary.


A cache’s **eviction policy tries to predict which entries are most likely to be used again**in the near future, thereby maximizing the hit ratio. The Least Recently Used (LRU) policy is perhaps the most popular due to its simplicity, good runtime performance, and a decent hit rate in common workloads. Its ability to predict the future is limited to the history of the entries residing in the cache, preferring to give the last access the highest priority by guessing that it is the most likely to be reused again soon.

### Distributed cache

When the system gets to certain scale, we need to distribute the cache to multiple machines.

The general strategy is to keep a hash table that maps each resource to the corresponding machine. Therefore, when requesting resource A, from this hash table we know that machine M is responsible for cache A and direct the request to M. At machine M, it works similar to local cache discussed above. Machine M may need to fetch and update the cache for A if it doesn’t exist in memory. After that, it returns the cache back to the original server.

[**Memcached**](http://memcached.org/)  is a simple in-memory key-value store, which primary use case is shared cache for several processes within the server, or for occasionally starting and dying processes (e. g. how PHP processes behind Apache server used to do).  
What it Does

![](https://cdn-images-1.medium.com/max/1200/0*7htyNNVUsSWOvxuR.png)

**memcached**  allows you to take memory from parts of your system where you have more than you need and make it accessible to areas where you have less than you need.

memcached also allows you to make better use of your memory. If you consider the diagram to the right, you can see two deployment scenarios:

1.  Each node is completely independent (top).
2.  Each node can make use of memory from other nodes (bottom).

The first scenario illustrates the classic deployment strategy, however you’ll find that it’s both wasteful in the sense that the total cache size is a fraction of the actual capacity of your web farm, but also in the amount of effort required to keep the cache consistent across all of those nodes.

With memcached, you can see that all of the servers are looking into the same virtual pool of memory. This means that a given item is always stored and always retrieved from the same location in your entire web cluster.

Also, as the demand for your application grows to the point where you need to have more servers, it generally also grows in terms of the data that must be regularly accessed. A deployment strategy where these two aspects of your system scale together just makes sense.

The illustration to the right only shows two web servers for simplicity, but the property remains the same as the number increases. If you had fifty web servers, you’d still have a usable cache size of 64MB in the first example, but in the second, you’d have 3.2GB of usable cache.

Of course, you aren’t required to use your web server’s memory for cache. Many memcached users have dedicated machines that are built to only be memcached servers.

### Concurrency

Concurrent access to a cache is viewed as a difficult problem because in  **most policies every access is a write to some shared state**. The traditional solution is to guard the cache with a single lock. This might then be improved through lock striping by splitting the cache into many smaller independent regions. Unfortunately that tends to have a limited benefit due to hot entries causing some locks to be more contented than others. When contention becomes a bottleneck the next classic step has been to  **update only per entry metadata**  and use either a random sampling or a FIFO-based eviction policy. Those techniques can have great read performance, poor write performance, and difficulty in choosing a good victim.

An alternative is to  **borrow an idea from database theory where writes are scaled by using a commit log**. Instead of mutating the data structures immediately, the  **updates are written to a log and replayed in asynchronous batches**. This same idea can be applied to a cache by performing the hash table operation, recording the operation to a buffer, and scheduling the replay activity against the policy when deemed necessary. The policy is still guarded by a lock, or a try lock to be more precise, but shifts contention onto appending to the log buffers instead.

### **Search Engine Caching**

Search engines are essential services to find the content on the Web. Commercial search engines like Yahoo! have over a hundred billion documents indexed, which map to petabytes of data. Searching through such an enormous amount of data is not trivial, especially when serving a large num- ber of queries concurrently. Thus, search engines rely upon systems comprising large numbers of machines grouped in clusters by functionality, such as index servers, document servers, and caches.

In a typical search engine, there are five types of data items that are accessed or generated during the search process: query results, precomputed scores, posting lists, precomputed intersections of posting lists, and documents.

### Query processing overview

Web search engines are composed of multiple replicas of large search clusters. Each query is assigned to an individual search cluster, based on the current workload of clusters or based on a hash of the query string. A search cluster is composed of many nodes over which the documents are partitioned. Each node builds and maintains an index over its local document collection. All nodes in the cluster contribute to processing of a query.

Query processing involves a number of steps: issuing the query to search nodes, computing a partial result ranking in all nodes, merging partial rankings to obtain a global top-_k_  result set, computing snippets for the top-_k_documents, and generating the final result page.

![](https://cdn-images-1.medium.com/max/1600/1*6b7e7PGJ71z8_a0UaXtqYA.jpeg)

#### Five-level static caching

Herein, we describe a five-level cache architecture for static caching in search engines. Steps mentioned in value column refers to figure 1 for your reference.

![](https://cdn-images-1.medium.com/max/1600/1*fq3eMWMow6-S-05ejXHnvg.png)

Below figure illustrates the interaction between different cache components.

![](https://cdn-images-1.medium.com/max/1600/1*gmhfOUqQ15WRJZWU5QgGOQ.jpeg)

### System Global Caching and Consistency

**Scenario**: Lets take an example of Twitter. There is a huge cache which gets updated frequently. For example: if person Foo tweets and it has followers all across the globe. Ideally all the caches across all PoP needs to get updated. i.e. they should remain in sync.

How does replication across datacenter (PoP) work for realtime caches ? What tools/technologies are preferred ? What are potential issues here in this system design ?

**Solution**: I would tackle the problem from a slightly different angle: when a user posts something, that something goes in a distributed storage (not necessarily a cache) that is already redundant across multiple geographies.I would also presume that, in the interest of performance, these nodes are eventually consistent.

Now the caching. I would not design a system that takes care of synchronising all the caches each time someone does something. I would rather implement caching at the service level. Imagine a small service residing in a geographically distributed cluster. Each time a user tries to fetch data, the service checks its local cache — if it is a miss, it reads the tweets from the storage and puts a portion of them in a cache (subject to eviction policies). All subsequent accesses, if any, would be cached at a local level.

In terms of design precautions:

-   Carefully consider the AZ(Availability Zone) topology in order to ensure sufficient bandwidth and low latency
-   Cache at the local level in order to avoid useless network trips
-   Cache updates don’t happen from the centre to the periphery; cache is created when a cache miss happens

## Database Replication

Ref: [https://www.brianstorti.com/replication/](https://www.brianstorti.com/replication/)

Replicating a database can make our applications faster and increase our tolerance to failures, but there are a lot of different options available and each one comes with a price tag. It’s hard to make the right choice if we do not understand how the tools we are using work, and what are the guarantees they provide (or, more importantly, do _not_ provide), and that’s what I want to explore here.

### Why we need Database Replication

When we say we want to replicate something, it means we want to keep a copy of the same data in multiple places. In the case of databases, that can mean a copy of the entire database, which is the most common scenario, or just some parts of it (e.g. a set of tables). These multiple locations where we will keep the data are usually connected by a network, and that’s the origin of most of our headaches, as you will see in a bit.

The reason for wanting that will be one or more of the following:

-   You want to keep the data closer to your users so you can save the travel time. Remember, no matter how fast your database is, the data still needs to travel from the computer that started the request to the server where the database is, and then back again. You can optimize the heck out of your database, but you cannot optimize the laws of physics.
    
-   You want to scale the number of machines serving requests. At some point a single server will not be able to handle the number of clients it needs to serve. In that case, having several databases with the same data helps you serve more clients. That’s what we call scaling  _horizontally_  (as opposed to_vertically_, which means having a more powerful machine).
    
-   You want to be safe in case of failures (that will happen). Imagine you have your data in single database server and that server catches fire, then what happens? I am sure you have some sort of backup (right?!), but your backup will a) take some time to be restored and b) probably be  _at least_  a couple of hours old. Not cool. Having a replica means you can just start sending your requests to this server while you are solving the fire situation, and maybe no one will even notice that something bad happened.

### Asynchronous replication

When we talk about replication, we are basically saying that when I write some data in a given node  `A`, this same data also needs to be written in node  `B`  (and maybe  `C`  and  `D`  and  `E`  and…), but we need to decide  _how_  this replication will happen, and what are the guarantees that we need. As always, it’s all about trade-offs. Let’s explore our options.

The first option is to be happy to send a confirmation back to the client as soon as the node that received the message has successfully written the data, and_then_ send this message to the replicas (that may or may not be alive). It works somewhat like this:

![](https://www.brianstorti.com/assets/images/replication/async.png)

This looks great, we don’t notice any performance impact as the replication happens in the background, after we already got a response, and if the replica is dead or slow we won’t even notice it, as the data was already sent back to the client. Life is good.

There are (at least) two main issues with asynchronous replication. The first is that we are weakening our durability guarantees, and the other is that we are exposed to replication lags. We will talk about replication lag later, let’s focus on the durability issue first.

Our problem here is that if the node that received this write request fails before it can replicate this change to the replicas, the data is lost, even though we sent a confirmation to the client.

### Synchronous replication

As you might expect, synchronous replication basically means that we will  _first_replicate the data, and then send a confirmation to the client. So when the client gets the confirmation we can be sure that the data is replicated and safe (well, it’s never 100% safe, all of our data centers can, in theory, explode at the same time, but it’s safe enough).

![](https://www.brianstorti.com/assets/images/replication/sync.png)

The price we need to pay is: Performance and availability.

The performance penalty is due to the fact that we need to  _wait_  for these - potentially - slow replicas to do their thing and send us a confirmation before we can tell the client that everything is going to be fine. As these replicas are usually distributed geographically, and potentially very far from each other, this takes more time than we would like to wait.

The second issue is availability. If one of the replicas (remember, we can have many!) is down or we cannot reach it for some reason, we simply cannot write any data. You should always plan for failures, and network partitions are more common than we imagine.

There’s some middle ground. Some databases and replication tools allow us to define a number of followers to replicate synchronously, and the others just use the asynchronous approach. This is sometimes called  _semi-synchronous replication_.

As an example, in  `Postgres`  you can define a configuration called  [`synchronous_standby_names`](https://www.postgresql.org/docs/9.6/static/runtime-config-replication.html)  to specify which replicas will receive the updates synchronously, and the other replicas will just receive them asynchronously.

### Single leader replication (Master-Slave Architecture )

The most common replication topology is to have a single leader, that then replicate the changes to all the followers.

In this setup, the clients always send writes (in the case of databases,  `INSERT`,  `UPDATE`  and  `DELETE`  queries) to the leader, and never to a follower. These followers can, however, answer read queries.

![](https://www.brianstorti.com/assets/images/replication/single-leader.png)

The main benefit of having a single leader is that we avoid conflicts caused by concurrent writes. All the clients are writing to the same server, so the coordination is easier. If we instead allow clients to write to 2 different servers at the same time, we need to somehow resolve the conflict that will happen if they both try to change the same  _object_, with different values (more on that later).

So, what are the problems that we need to keep in mind if we decide to go with the single leader approach? The first one is that we need to make sure that just one node is able to handle all the writes. Although we can split the read work across the entire cluster, all the writes are going to a single server, and if your application is very write-intensive that might be a problem. Keep in mind though, that most applications read a lot more data than they write, so you need to analyze if that’s really a problem for you.

Another problem is that you will need to pay the latency price on writes. Remember our colleagues in Asia? Well, when they want to update some data, that query will still need to travel the globe before they get a response.

Lastly, although this is not really a problem just for single leader replication, you need to think about what will happen when the leader node dies. Is the entire system going to stop working? Will it be available just for reads (from the replicas), but not for writes? Is there a process to  _elect_  a new leader (i.e. promoting one of the replicas to a leader status)? Is this election process automated or will it need someone to tell the system who is the new king in town?

At first glance it seems like the best approach is to just have an automatic failover strategy, that will elect a new leader and everything will keep working wonderfully. That, unfortunately, is easier said than done.

#### Challenges of Automatic failover process

Let me list _some_ of the challenges in implementing this automatic failover strategy.

- The first question we need to ask is: How can we be sure that the leader is dead? And the answer is: We probably can’t.
- Challenge number two: You need to decide who is the new leader. You have all these followers, living in an anarchy, and they need to somehow agree on who should be the new leader.

Alright, you detected that the leader is really dead and selected a new leader, now you need to somehow tell the clients to start sending writes to this new leader, instead of the dead one. This is a **_request routing_ problem**, and we can also approach it from several different angles. For example, you can allow clients to send writes to any node, and have these nodes redirect this request to the leader. Or you can have a _routing layer_ that receives this messages and redirect them to the appropriate node.

This is also what happens when there’s a network partition and we end up with what appears to be two isolated clusters, each one with its own leader, as each part of this cluster cannot see the other, and therefore thinks they are all dead.

As you can see, automatic failovers are not simple. There are a lot of things to take into consideration, and for that reason sometimes it’s better to have a human manually perform this procedure. Of course, if your leader database dies at 7pm and there’s no one on-call, it might not be the best solution to wait until tomorrow morning, so, as always, trade-offs.

### Multi leader replication

So, we talked a lot about single leader replication, now let’s discuss an alternative, and also explore its own challenges and try to identify scenarios where it might make sense to use it.

The main reason to consider a multi leader approach is that is solves some of the problems that we face when we have just one leader node. Namely, we have more than one node handling writes, and these writes can be performed by databases that are closer to the clients.

![](https://www.brianstorti.com/assets/images/replication/multi-leader.png)

If your application needs to handle a very high number of writes, it might make sense to split that work across multiple leaders. Also, if the latency price to write in a database that is very far is too high, you could have one leader in each location (for example, one in North America, one in Europe and another in Asia).

Another good use case is when you need to support offline clients, that might be writing to their own (leader) database, and these writes need to be synchronized with the rest of the databases once this client gets online again.

The main problem you will face with multiple leaders accepting writes is that you need some way to solve conflicts. For example, let’s say you have a database constraint to ensure that your users’ emails are unique. If two clients write to two different leaders that are not yet in sync, both writes will succeed in their respective leaders, but we will have problems when we try to replicate that data. Let’s talk a bit more about these conflicts.

Let’s use as an example an application to manage the projects in your company. You can ensure that all the updates in the projects related to the American office are sent to the leader in North America, and all the European projects are written to the leader in Europe. This way you can avoid conflicts, as the writes to the same projects will be sent to the same leader. Also, if we assume that the clients updating these projects will probably be in their respective offices (e.g. people in the New York office will update the American projects, that will be sent to the leader in North America), we can ensure that they are accessing a database geographically close to them.

#### DDL replication

Handing DDLs (changes in the structure of the database, like adding/removing a column) can also be tricky in a multi leader scenario. It’s, in some sense, also a conflict issue, we cannot change the database structure while other nodes are still writing to the old structure, so we usually need to get a global database lock, wait until all the pending replications take place, and then execute this DDL. In the meantime, all the writes will either be blocked or fail.

### Leaderless Replication

Another idea that was popularized by Amazon’s  [DynamoDB](https://aws.amazon.com/dynamodb/)  (although it first appeared some decades ago) is to simply have no leaders, every replica can accept writes. The basic idea is that clients will send writes not only to one replica, but to several (or, in some cases, to all of them).

![](https://www.brianstorti.com/assets/images/replication/leaderless.png)

The client sends this write request concurrently to several replicas, and as soon as it gets a confirmation from some of them (we will talk about how many are “some” in a bit) it can consider that write a success and move on.

One advantage we have here is that we can tolerate node failures more easily. Think about what would happen in a scenario where we had to send a write to a single leader and for some reason that leader didn’t respond. The write would fail and we would need to start a failover process to elect a new leader that could start receiving writes again. No leaders, no failover, and if you remember what we’ve talked about failovers, you can probably see why this can be a big deal.

But, again, there is no free lunch, so let’s take a look at the price tag here.

What happens if, say, your write succeeds in 2 replicas, but fails in 1 (maybe that server was being rebooted when you sent the write request)?  
You now have 2 replicas with the new value and 1 with the old value. Remember, these replicas are not talking to each other, there’s no leader handling any kind of synchronization.

![](https://www.brianstorti.com/assets/images/replication/leaderless-stale.png)

Now if you read from this replica, BOOM, you get stale data.

To deal with this problem, a client will not read data from one replica, but also send requests to several replicas concurrently (like it did for writes). The replicas then return their values, and also some kind of version number, that the clients can use to decide which value it should use, and which it should discard.

We still have a problem, though. One of the replicas still has the old value, and we need to somehow synchronize it with the rest of the replicas (after all, replication is the process of keeping the  _same_  data in several places).

There are usually two ways to do that: We can make the client responsible for this update, or we can have another process that is responsible just for finding differences in the data and fixing them.

Making the client fix it is conceptually simple, when the client reads data from several nodes and detects that one of them is stale, it sends a write request with the correct value. This is usually called  _read repair_.

The other solution, having a background process fixing the data, really depends on the database implementation, and there are several ways to do that, depending on how the data is stored. For example,  `DynamoDB`  has an  _anti-entropy_process using Merkle trees.

### Quorums

So we said we need to send the write/read requests to “some” replicas. There are good ways to define how many are enough, and what we are compromising (and gaining) if we decide to decrease this number.

Let’s first talk about the most obvious problematic scenario, when we require just one successful response to consider a value written, and also read from just one replica. From there we can expand the problem to more realistic scenarios.

![](https://www.brianstorti.com/assets/images/replication/leaderless-write-one-replica.png)

As there is no synchronization between these replicas, we will read stale values every time we send a read request to a node other than the only one that succeeded.

Now let’s imagine we have 5 nodes and require a successful write in 2 of them, and also read from 2. Well, we will have the exact same problem. If we write to nodes  `A`  and  `B`  and read from nodes  `C`  and  `D`, we will always get stale data.

What we need is some way to guarantee that at least one of the nodes that we are reading from is a node that received the write, and that’s what quorums are.

For example, if we have 5 replicas and require that 3 of them accept the write, and also read from 3 replicas, we can be sure that  _at least_  one of these replicas that we are reading from accepted the write and therefore has the most recent data. There’s always an overlap.

Most databases allow us to configure how many replicas need to accept a write (`w`) and how many we want to read from (`r`). A good rule of thumb is to always have  `w + r > number of replicas`.

Now you can start playing with these numbers. For example, if your application writes to the database very rarely, but reads very frequently, maybe you can set`w = number of replicas`  and  `n = 1`. What that means is that writes need to be confirmed by every replica, but you can then read from just one of them as you are sure every replica has the latest value. Of course, you are then making your writes slower and less available, as just a single replica failure will prevent any write from happening, so you need to measure your specific needs and what is the right balance.

### Replication Lag

In a leader-based replication, as we have seen, writes need to be sent to a leader, but reads can be performed by any replica. When we have applications that are mostly reading from the database, and writing a lot less often (which is the most common case), it can be tempting to add many replicas to handle all these read requests, creating what can be called a  _read scaling architecture_. Not only that, but we can have many replicas geographically close to our clients to also improve latency.

The more replicas we have, though, the harder it is to use synchronous replication, as the probability of one these many nodes being down when we need to replicate an update increases, and our availability decreases. The only feasible solution in this case is to use asynchronous replication, that is, we can still perform updates even if a node is not responding, and when this replica is back up it should catch up with the leader.

We’ve already discussed the benefits and challenges in using synchronous and asynchronous replication, so I’ll not talk about that again, but assuming we are replicating updates asynchronously, we need to be aware of the problems we can have with the replication lag, or, in other words, the delay between the time an update is applied in the leader node and the time it’s applied in a given replica.

![](https://www.brianstorti.com/assets/images/replication/replication-lag.png)

If a client reads from this replica during this period, it will receive outdated information, because the latest update(s) were not applied yet. In other words, if you send the same query to 2 different server, you may get 2 different answers. As you may remember when we talked about the CAP theorem, this breaks the  _consistency_  guarantee. This is just temporary, though, eventually all the nodes replicas will get this update, and if you stop writing new data, they will all end up being identical. This is what we call  _eventual consistency_.

In theory there is no limit for how long it will take to a replica to be consistent with its leader (the only guarantee we have is that  _eventually_  it will be), but in practice we usually expect this to happen fairly quickly, maybe in a couple of milliseconds.

Unfortunately, we cannot expect that to always be the case, and we need to plan for the worst. Maybe the network is slow, or the server is operating near capacity and is not replicating the updates as fast as we’d except, and this replication lag can increase. Maybe it will increase a couple of seconds, maybe minutes. What happens then?

Well, the first step is to understand the guarantees we need to provide. For example, is it really a problem that, when facing an issue that increases the lag, it will take 30 seconds for your friend to be able to see that last cat picture you just posted on Facebook? Probably not.

### Read-your-Write Consistency

The most common problem we can have with asynchronous replicas is when a client sends a write to the leader, and shortly after tries to read that same value from a replica. If this read happens before the leader had enough time to replicate the update, it will look like the write didn’t actually work.

![](https://www.brianstorti.com/assets/images/replication/read-your-writes.png)

So, although it might not be a big issue if a client doesn’t see other clients’ updates right away, it’s pretty bad if they don’t see their own writes. This is what is called  _read-your-writes consistency_, we want to make sure that a client never reads the database in a state it was before it performed a write.

Let’s talk about some techniques that can be used to achieve this type of consistency.

A simple solution is to actually read from the leader when we are trying to read something that the user might have changed. For example, if we are implementing something like Twitter, we can read other people’s timeline from a replica (as the user will not be able to write/change it), but when viewing their own timeline, read from the leader, to ensure we don’t miss any update.

Now, if there are lots of things that can be changed by every user, that doesn’t really work very well, as we would end up sending all the reads to the leader, defeating the whole purpose of having replicas, so in this case we need a different strategy.

Another technique that can be used is to track the timestamp of the last write request and for the next, say, 10 seconds, send all the read requests to the leader. Then you need to find the right balance here, because if there is a new write every 9 seconds you will also end up sending all of your reads to the leader. Also, you will probably want to monitor the replication lag to make sure replicas that fall more than 10 seconds behind stop receiving requests until they catch up.

Then there are also more sophisticated ways to handle this, that requires more collaboration of your database. For example,  [Berkeley DB](http://www.oracle.com/technetwork/database/database-technologies/berkeleydb/overview/index.html)  will generate a  _commit token_  when you write something to the leader. The client can then send this token to the replica it’s trying to read from, and with this token the replica knows if it’s current enough (i.e. if it has already applied that commit). If so, it can just serve that read without any problem, otherwise it can either block until it receives that update, and then answer the request, or it can just reject it, and the client can try another replica.

As always, there are no right answers here, and I am sure there lots of other techniques that can be used to work around this problem, but you need to know how your system will behave when facing a large replication lag and if_read-your-writes_  is a guarantee you really need to provide, as there are databases and replication tools that will simply ignore this issue.

### Monotonic Reads consistency

This is a fancy name to say that we don’t want clients to see time moving backwards: If I read from a replica that has already applied commits 1, 2 and 3, I don’t want my next read to go to a replica that only has commits 1 and 2.

Imagine, for example, that I’m reading the comments of a blog post. When I refresh the page to check if there’s any new comment, what actually happens is that the last comment disappears, as if it was deleted. Then I refresh again, and it’s back there. Very confusing.

![](https://www.brianstorti.com/assets/images/replication/monotonic-reads.png)

Although you can still see stale data, what monotonic read guarantees is that if you make several reads to a given value, all the successive reads will be at least as recent as the previous one. Time never moves backwards.

The simplest way to achieve monotonic reads is to make each client send their read requests to the same replica. Different clients can still read from different replicas, but having a given client always (or at least for the duration of a session) connected to the same replica will ensure it never reads data from the past.

Another alternative is to have something similar to the commit token that we talked about in the  _read-your-writes_  discussion. Every time that a client reads from a replica it receives its latest commit token, that is then sent in the next read, that can go to another replica. This replica can then check this commit token to know if it’s eligible to answer that query (i.e. if its own commit token is “greater” than the one received). If that’s not the case, it can wait until more data is replicated before responding, or it can return an error.

### Delayed replicas

We talked about replication lags, some of the problems that we can have when this lag increases too much, and how to deal with these problems, but sometimes we may actually  _want_  this lag. In other words, we want a  _delayed replica_.

We will not really read (or write) from this replica, it will just sit there, lagging behind the leader, maybe by a couple of hours, while no one is using it. So, why would anyone want that?

Well, imagine that you release a new version of your application, and a bug introduced in this release starts deleting all the records from your  `orders`  table. You notice the issue and rollback this release, but the deleted data is gone. Your replicas are not very useful at this point, as all these deletions were already replicated and you have the same messy database replicated. You could start to restore a backup, but if you have a big database you probably won’t have a backup running every couple of minutes, and the process to restore a database can take a lot of time.

That’s were a delayed replica can save the day. Let’s say you have a replica that is always 1 hour behind the leader. As long as you noticed the issue in less than 1 hour (as you probably will when your orders evaporate) you can just start using this replica and, although you will still probably lose some data, the damage could be a lot worse.

A replica will almost never replace a proper backup, but in some cases having a delayed replica can be extremely helpful (as the developer that shipped that bug can confirm).

### Replication under the hood

We talked about several different replication setups, consistency guarantees, benefits and disadvantages of each approach. Now let’s go one level below, and see how one node can actually send its data to another, after all, replication is all about copying bytes from one place to another, right?

#### Statement-based replication

Statement-based replication basically means that one node will send the same statements it received to its replicas. For example, if you send an  `UPDATE foo = bar`  statement to the leader, it will execute this update and send the same instruction to its replicas, that will also execute the update, hopefully getting to the same result.

Although this is a very simple solution, there are some things to be considered here. The main problem is that not every statement is deterministic, meaning that each time you execute them, you can get a different result. Think about functions like  `CURRENT_TIME()`  or  `RANDOM()`, if you simply execute these functions twice in a row, you will get different results, so just letting each replica re-execute them would lead to inconsistent data.

Most databases and replication tools that use statement-based replication (e.g.  `MySQL`  before 5.1) will try to replace these nondeterministic function calls with fixed values to avoid these problems, but it’s hard to account for every case. For example, a user-defined function can be used, or a trigger can be called after an update, and it’s hard to guarantee determinism in these cases.  [`VoltDB`](https://www.voltdb.com/), for instance, uses logical replication but  [requires stored procedures to be deterministic](https://docs.voltdb.com/UsingVoltDB/DesignProc.php#DesignProcDeterminism).

Another important requirement is that we need to make sure that all transactions either commit or abort on every replica, so we don’t have a change being applied in some replicas and not in others.

#### Log Shipping replication

Most databases use a  [log](https://en.wikipedia.org/wiki/Write-ahead_logging)  (an append-only data structure) to provide durability and atomicity (from the  _ACID_  properties). Every change is first written to this log before being applied, so the database can recover in case of crashes during a write operation.

The log describes changes to the database at a very low level, describing, for example, which bytes were changed and where exactly in the disk. It’s not meant to be read by humans, but machines can interpret them pretty efficiently.

The idea of log shipping replication is to transfer these log files to the replicas, that can then apply them to get the exact same result.

The main limitation that we have when shipping these logs is that, as it describes the changes at such a low level, we probably won’t be able to replicate a log generated by a different version of the database, for example, as the way the data is physically stored may have changed.

Another issue is that we cannot use multi-master replication with log shipping, as there’s no way to unify multiple logs into one, and if data is changing in multiple locations at the same time, that would be necessary.

This technique is used by  `Postgres`’  [streaming replication](https://www.postgresql.org/docs/current/static/warm-standby.html)  and also to provide  [incremental backups and Point-in-Time Recovery](https://www.postgresql.org/docs/9.6/static/continuous-archiving.html).

This is also known as  _physical replication_.

#### Row-based replication

Row-based, or  _logical_  replication, is kind of a mix of these two techniques. Instead of shipping the internal log (`WAL`), it uses a different log just for replication. This log can then be decoupled from the storage engine and therefore can be used, in most cases, to replica data across different database versions.

This row-based log will include enough information to uniquely identify a row, and a set of changes that need to be performed.

A benefit of using a row-based approach is that we can, for example, upgrade the database version with zero downtime. We can take one node down to upgrade it, and in the meantime the other replicas handle all the requests, and after it’s back up, with the new version, we do the same thing for the other nodes.

The main disadvantage here when compared to a statement-based replication is that sometimes we need to log a lot more data. For example, if we want to replicate  `UPDATE foo = bar`, and this update changes 100 rows, with the statement-based replication we would log just this simple  `SQL`, while we would need to log all the 100 rows when using the row-based technique. In the same way, if you use an user-defined function that generates a lot of data, all that data needs to be logged, instead of just the function call.

`MySQL`  for example, allows us to define a  [`MIXED`  logging format](https://dev.mysql.com/doc/refman/8.0/en/binary-log-mixed.html), that will switch between statement and row-base replication, trying to use the best technique for each case.

## Apache Cassandra

Ref: 
1. [https://medium.com/netflix-techblog/benchmarking-cassandra-scalability-on-aws-over-a-million-writes-per-second-39f45f066c9e](https://medium.com/netflix-techblog/benchmarking-cassandra-scalability-on-aws-over-a-million-writes-per-second-39f45f066c9e)
2. [https://en.wikipedia.org/wiki/Apache_Cassandra](https://en.wikipedia.org/wiki/Apache_Cassandra)
3. [https://www.youtube.com/watch?v=xQnIN9bW0og](https://www.youtube.com/watch?v=xQnIN9bW0og)

The Apache Cassandra database is the right choice when you need scalability and high availability without compromising performance.  [Linear scalability](http://techblog.netflix.com/2011/11/benchmarking-cassandra-scalability-on.html)  and proven fault-tolerance on commodity hardware or cloud infrastructure make it the perfect platform for mission-critical data. Cassandra's support for replicating across multiple datacenters is best-in-class, providing lower latency for your users and the peace of mind of knowing that you can survive regional outages.

Apache Cassandra is a free and open-source, distributed, wide column store, NoSQL database management system designed to handle large amounts of data across many commodity servers, providing high availability with no single point of failure. Cassandra offers robust support for clusters spanning multiple datacenters, with asynchronous masterless replication allowing low latency operations for all clients.

#### Replication

- Data is replicated automatically
- You pick number of servers called "Replication Factor" RF (RF = 3)
- Data is always replicated to each replica
- If a machine is done, missing data is replaced by hinted handoff

#### Consistency Level

The Cassandra consistency level is defined as the minimum number of Cassandra nodes that must acknowledge a read or write operation before the operation can be considered successful.

Write Consistency Level:

All - A write must be written to the [commit log and memtable](https://docs.datastax.com/en/archived/cassandra/3.0/cassandra/dml/dmlHowDataWritten.html#dmlHowDataWritten__logging-writes-and-memtable-storage) on all replica nodes in the cluster for that partition. Provides the highest consistency and the lowest availability of any other level.

QUORUM - A write must be written to the [commit log and memtable](https://docs.datastax.com/en/archived/cassandra/3.0/cassandra/dml/dmlHowDataWritten.html#dmlHowDataWritten__logging-writes-and-memtable-storage) on a quorum of replica nodes across _all_ datacenters. Used in either single or multiple datacenter clusters to maintain strong consistency across the cluster. Use if you can tolerate some level of failure.

Read Consistency Level:

All - Returns the record after all replicas have responded. The read operation will fail if a replica does not respond. Provides the highest consistency of all levels and the lowest availability of all levels.

QUORUM - Returns the record after a quorum of replicas from all [datacenters](https://docs.datastax.com/en/glossary/doc/glossary/gloss_data_center.html) has responded. Used in either single or multiple datacenter clusters to maintain strong consistency across the cluster. Ensures strong consistency if you can tolerate some level of failure.


#### FAULT TOLERANT

Data is automatically replicated to multiple nodes for fault-tolerance. Replication across multiple data centers is supported. Failed nodes can be replaced with no downtime.

#### DECENTRALIZED

There are no single points of failure. There are no network bottlenecks. Every node in the cluster is identical.

#### SCALABLE

Some of the largest production deployments include Apple's, with over 75,000 nodes storing over 10 PB of data, Netflix (2,500 nodes, 420 TB, over 1 trillion requests per day), Chinese search engine Easou (270 nodes, 300 TB, over 800 million requests per day), and eBay (over 100 nodes, 250 TB).

#### ELASTIC

Read and write throughput both increase linearly as new machines are added, with no downtime or interruption to applications.


## CAP Theorem

The CAP theorem was introduced by Eric Brewer in the year 2000, so it’s not a new idea. The acronym stands for `C`onsistency, `A`vailability and `P`artition Tolerance, and it basically says that, given these 3 properties in a distributed system, you need to choose 2 of them (i.e. you cannot have all 3). In practice, it means you need to choose between consistency and availability when an inevitable partition happens.

![](https://www.brianstorti.com/assets/images/replication/cap.png)

**Consistency**: In the CAP definition, consistency means that all the nodes in a cluster (e.g. all your database servers, leaders and replicas) see the same data at any given point in time. In practice, it means that if you query any of your database servers at the exact same time, you will get the same result back.

**Availability**: It means that reads and writes will always succeed, even if we cannot guarantee that it will have the most recent data. In practice, it means that we will still be able to use one of our databases, even when it cannot talk to the others, and therefore might not have received the latest updates.

**Partition Tolerance**: This means that your system will continue working even if there is a network partition. A network partition means that the nodes in your cluster cannot talk to each other.

![](https://www.brianstorti.com/assets/images/replication/network-partition.png)

## Indexes

Ref: 
1. [https://en.wikipedia.org/wiki/Database_index](https://en.wikipedia.org/wiki/Database_index)
2. [https://medium.com/@nishantnitb/system-design-chapter-5-indexes-in-databases-beb90295dbcf](https://medium.com/@nishantnitb/system-design-chapter-5-indexes-in-databases-beb90295dbcf)

Indexes are well known when it comes to databases. Sooner or later there comes a time when database performance is no longer satisfactory. One of the very first things you should turn to when that happens is database indexing.

The goal of creating an index on a particular table in a database is to make it faster to search through the table and find the row or rows that we want. Indexes can be created using one or more columns of a database table, providing the basis for both rapid random lookups and efficient access of ordered records.

### A library catalog

A library catalog is a register that contains the list of books found in a library. The catalog is organized like a database table generally with four columns: book title, writer, subject, and date of publication. There are usually two such catalogs: one sorted by the book title and one sorted by the writer name. That way, you can either think of a writer you want to read and then look through their books or look up a specific book title you know you want to read in case you don’t know the writer’s name. These catalogs are like indexes for the database of books. They provide a sorted list of data that is easily searchable by relevant information.

Simply saying, an index is a data structure that can be perceived as a table of contents that points us to the location where actual data lives. So when we create an index on a column of a table, we store that column and a pointer to the whole row in the index. Let’s assume a table containing a list of books, the following diagram shows how an index on the ‘Title’ column looks like:

![](https://www.educative.io/api/collection/5668639101419520/5649050225344512/page/5681717746597888/image/5684961520648192.png)

Just like a traditional relational data store, we can also apply this concept to larger datasets. The trick with indexes is that we must carefully consider how users will access the data. In the case of data sets that are many terabytes in size, but have very small payloads (e.g., 1 KB), indexes are a necessity for optimizing data access. Finding a small payload in such a large dataset can be a real challenge, since we can’t possibly iterate over that much data in any reasonable time. Furthermore, it is very likely that such a large data set is spread over several physical devices—this means we need some way to find the correct physical location of the desired data. Indexes are the best way to do this.

### How do Indexes decrease write performance?

An index can dramatically speed up data retrieval but may itself be large due to the additional keys, which slow down data insertion & update.

When adding rows or making updates to existing rows for a table with an active index, we not only have to write the data but also have to update the index. This will decrease the write performance. This performance degradation applies to all insert, update, and delete operations for the table. For this reason, adding unnecessary indexes on tables should be avoided and indexes that are no longer used should be removed. To reiterate, adding indexes is about improving the performance of search queries. If the goal of the database is to provide a data store that is often written to and rarely read from, in that case, decreasing the performance of the more common operation, which is writing, is probably not worth the increase in performance we get from reading.


## Reverse Proxy

Ref: 
1. [https://www.cloudflare.com/learning/cdn/glossary/reverse-proxy/](https://www.cloudflare.com/learning/cdn/glossary/reverse-proxy/)
2. [https://en.wikipedia.org/wiki/Reverse_proxy](https://en.wikipedia.org/wiki/Reverse_proxy)

### What is a reverse proxy?

A reverse proxy is a server that sits in front of web servers and forwards client (e.g. web browser) requests to those web servers. Reverse proxies are typically implemented to help increase  [security](https://www.cloudflare.com/learning/security/what-is-web-application-security/), performance, and reliability. In order to better understand how a reverse proxy works and the benefits it can provide, let’s first define what a proxy server is.

### What’s a proxy server?

A forward proxy, often called a proxy, proxy server, or web proxy, is a server that sits in front of a group of client machines. When those computers make requests to sites and services on the Internet, the proxy server intercepts those requests and then communicates with web servers on behalf of those clients, like a middleman.

For example, let’s name 3 computers involved in a typical forward proxy communication:

-   A: This is a user’s home computer
-   B: This is a forward proxy server
-   C: This is a website’s origin server (where the website data is stored)

![Forward Proxy Flow](https://www.cloudflare.com/img/learning/cdn/glossary/reverse-proxy/forward-proxy-flow.svg)

In a standard Internet communication, computer A would reach out directly to computer C, with the client sending requests to the  [origin server](https://www.cloudflare.com/learning/cdn/glossary/origin-server/)  and the origin server responding to the client. When a forward proxy is in place, A will instead send requests to B, which will then forward the request to C. C will then send a response to B, which will forward the response back to A.

Why would anyone add this extra middleman to their Internet activity? There are a few reasons one might want to use a forward proxy:

-   **To avoid state or institutional browsing restrictions**  - Some governments, schools, and other organizations use firewalls to give their users access to a limited version of the Internet. A forward proxy can be used to get around these restrictions, as they let the user connect to the proxy rather than directly to the sites they are visiting.
-   **To block access to certain content**  - Conversely, proxies can also be set up to block a group of users from accessing certain sites. For example, a school network might be configured to connect to the web through a proxy which enables content filtering rules, refusing to forward responses from Facebook and other social media sites.
-   **To protect their identity online**  - In some cases, regular Internet users simply desire increased anonymity online, but in other cases, Internet users live in places where the government can impose serious consequences to political dissidents. Criticizing the government in a web forum or on social media can lead to fines or imprisonment for these users. If one of these dissidents uses a forward proxy to connect to a website where they post politically sensitive comments, the  [IP address](https://www.cloudflare.com/learning/dns/glossary/what-is-my-ip-address/)  used to post the comments will be harder to trace back to the dissident. Only the IP address of the proxy server will be visible.

### How is a reverse proxy different?

A reverse proxy is a server that sits in front of one or more web servers, intercepting requests from clients. This is different from a forward proxy, where the proxy sits in front of the clients. With a reverse proxy, when clients send requests to the origin server of a website, those requests are intercepted at the  [network edge](https://www.cloudflare.com/learning/serverless/glossary/what-is-edge-computing/)  by the reverse proxy server. The reverse proxy server will then send requests to and receive responses from the origin server.

The difference between a forward and reverse proxy is subtle but important. A simplified way to sum it up would be to say that a forward proxy sits in front of a client and ensures that no origin server ever communicates directly with that specific client. On the other hand, a reverse proxy sits in front of an origin server and ensures that no client ever communicates directly with that origin server.

Once again, let’s illustrate by naming the computers involved:

-   D: Any number of users’ home computers
-   E: This is a reverse proxy server
-   F: One or more origin servers

![Reverse Proxy Flow](https://www.cloudflare.com/img/learning/cdn/glossary/reverse-proxy/reverse-proxy-flow.svg)

Typically all requests from D would go directly to F, and F would send responses directly to D. With a reverse proxy, all requests from D will go directly to E, and E will send its requests to and receive responses from F. E will then pass along the appropriate responses to D.

Below we outline some of the benefits of a reverse proxy:

-   **[Load balancing](https://www.cloudflare.com/learning/cdn/cdn-load-balance-reliability/)**  - A popular website that gets millions of users every day may not be able to handle all of its incoming site traffic with a single origin server. Instead, the site can be distributed among a pool of different servers, all handling requests for the same site. In this case, a reverse proxy can provide a load balancing solution which will distribute the incoming traffic evenly among the different servers to prevent any single server from becoming overloaded. In the event that a server fails completely, other servers can step up to handle the traffic.
-   **Protection from attacks**  - With a reverse proxy in place, a web site or service never needs to reveal the IP address of their origin server(s). This makes it much harder for attackers to leverage a targeted attack against them, such as a  [DDoS attack](https://www.cloudflare.com/learning/ddos/what-is-a-ddos-attack/). Instead the attackers will only be able to target the reverse proxy, such as Cloudflare’s  [CDN](https://www.cloudflare.com/learning/cdn/what-is-a-cdn/), which will have tighter security and more resources to fend off a cyber attack.
-   **[Global Server Load Balancing](https://www.cloudflare.com/learning/cdn/glossary/global-server-load-balancing-gslb/)  (GSLB)**  - In this form of load balancing, a website can be distributed on several servers around the globe and the reverse proxy will send clients to the server that’s geographically closest to them. This decreases the distances that requests and responses need to travel, minimizing load times.
-   **Caching**  - A reverse proxy can also cache content, resulting in faster performance. For example, if a user in Paris visits a reverse-proxied website with web servers in Los Angeles, the user might actually connect to a local reverse proxy server in Paris, which will then have to communicate with an origin server in L.A. The proxy server can then cache (or temporarily save) the response data. Subsequent Parisian users who browse the site will then get the locally cached version from the Parisian reverse proxy server, resulting in much faster performance.
-   **SSL encryption**  - Encrypting and decrypting  [SSL](https://www.cloudflare.com/learning/security/glossary/what-is-ssl/)  (or  [TLS](https://www.cloudflare.com/learning/security/glossary/transport-layer-security-tls/)) communications for each client can be computationally expensive for an origin server. A reverse proxy can be configured to decrypt all incoming requests and encrypt all outgoing responses, freeing up valuable resources on the origin server.

### How to implement a reverse proxy

Some companies build their own reverse proxies, but this requires intensive software and hardware engineering resources, as well as a significant investment in physical hardware. One of the easiest and most cost-effective ways to reap all the benefits of a reverse proxy is by signing up for a CDN service. For example, the  [Cloudflare CDN](https://www.cloudflare.com/cdn/)  provides all the performance and security features listed above, as well as many others.

## CDN

Ref: 
1. [https://www.cloudflare.com/learning/cdn/what-is-a-cdn/](https://www.cloudflare.com/learning/cdn/what-is-a-cdn/)

## Redundency

[https://www.educative.io/collection/page/5668639101419520/5649050225344512/5661458385862656](https://www.educative.io/collection/page/5668639101419520/5649050225344512/5661458385862656)

[Redundancy](https://en.wikipedia.org/wiki/Redundancy_(engineering))  is the duplication of critical components or functions of a system with the intention of increasing the reliability of the system, usually in the form of a backup or fail-safe, or to improve actual system performance. For example, if there is only one copy of a file stored on a single server, then losing that server means losing the file. Since losing data is seldom a good thing, we can create duplicate or redundant copies of the file to solve this problem.

Redundancy plays a key role in removing the single points of failure in the system and provides backups if needed in a crisis. For example, if we have two instances of a service running in production and one fails, the system can failover to the other one.

[Replication](https://en.wikipedia.org/wiki/Replication_(computing))  means sharing information to ensure consistency between redundant resources, such as software or hardware components, to improve reliability,  [fault-tolerance](https://en.wikipedia.org/wiki/Fault_tolerance), or accessibility.

Replication is widely used in many database management systems (DBMS), usually with a master-slave relationship between the original and the copies. The master gets all the updates, which then ripple through to the slaves. Each slave outputs a message stating that it has received the update successfully, thus allowing the sending of subsequent updates.

## SQL vs NoSQL

Ref:
1. [https://www.thegeekstuff.com/2014/01/sql-vs-nosql-db](https://www.thegeekstuff.com/2014/01/sql-vs-nosql-db)
2. [https://www.bmc.com/blogs/sql-vs-nosql/](https://www.bmc.com/blogs/sql-vs-nosql/)

In the world of databases, there are two main types of solutions: SQL and NoSQL (or relational databases and non-relational databases). Both of them differ in the way they were built, the kind of information they store, and the storage method they use.

Relational databases are structured and have predefined schemas like phone books that store phone numbers and addresses. Non-relational databases are unstructured, distributed, and have a dynamic schema like file folders that hold everything from a person’s address and phone number to their Facebook ‘likes’ and online shopping preferences.

### SQL

Relational databases store data in rows and columns. Each row contains all the information about one entity and each column contains all the separate data points. Some of the most popular relational databases are MySQL, Oracle, MS SQL Server, SQLite, Postgres, and MariaDB.

### NoSQL

Following are the most common types of NoSQL:

**Key-Value Stores:**  Data is stored in an array of key-value pairs. The ‘key’ is an attribute name which is linked to a ‘value’. Well-known key-value stores include Redis, Voldemort, and Dynamo.

**Document Databases:**  In these databases, data is stored in documents (instead of rows and columns in a table) and these documents are grouped together in collections. Each document can have an entirely different structure. Document databases include the CouchDB and MongoDB.

**Wide-Column Databases:**  Instead of ‘tables,’ in columnar databases we have column families, which are containers for rows. Unlike relational databases, we don’t need to know all the columns up front and each row doesn’t have to have the same number of columns. Columnar databases are best suited for analyzing large datasets - big names include Cassandra and HBase.

**Graph Databases:**  These databases are used to store data whose relations are best represented in a graph. Data is saved in graph structures with nodes (entities), properties (information about the entities), and lines (connections between the entities). Examples of graph database include Neo4J and InfiniteGraph.

### Difference between SQL and NoSQL

**Storage:**  SQL stores data in tables where each row represents an entity and each column represents a data point about that entity; for example, if we are storing a car entity in a table, different columns could be ‘Color’, ‘Make’, ‘Model’, and so on.

NoSQL databases have different data storage models. The main ones are key-value, document, graph, and columnar. We will discuss differences between these databases below.

**Schema:**  In SQL, each record conforms to a fixed schema, meaning the columns must be decided and chosen before data entry and each row must have data for each column. The schema can be altered later, but it involves modifying the whole database and going offline.

In NoSQL, schemas are dynamic. Columns can be added on the fly and each ‘row’ (or equivalent) doesn’t have to contain data for each ‘column.’

**Querying:**  SQL databases use SQL (structured query language) for defining and manipulating the data, which is very powerful. In a NoSQL database, queries are focused on a collection of documents. Sometimes it is also called UnQL (Unstructured Query Language). Different databases have different syntax for using UnQL.

**Scalability:**  In most common situations, SQL databases are vertically scalable, i.e., by increasing the horsepower (higher Memory, CPU, etc.) of the hardware, which can get very expensive. It is possible to scale a relational database across multiple servers, but this is a challenging and time-consuming process.

On the other hand, NoSQL databases are horizontally scalable, meaning we can add more servers easily in our NoSQL database infrastructure to handle a lot of traffic. Any cheap commodity hardware or cloud instances can host NoSQL databases, thus making it a lot more cost-effective than vertical scaling. A lot of NoSQL technologies also distribute data across servers automatically.

**Reliability or ACID Compliancy (Atomicity, Consistency, Isolation, Durability):**  The vast majority of relational databases are ACID compliant. So, when it comes to data reliability and safe guarantee of performing transactions, SQL databases are still the better bet.

Most of the NoSQL solutions sacrifice ACID compliance for performance and scalability.

For complex queries: SQL databases are good fit for the complex query intensive environment whereas NoSQL databases are not good fit for complex queries. On a high-level, NoSQL don’t have standard interfaces to perform complex queries, and the queries themselves in NoSQL are not as powerful as SQL query language

### Which one to choose?

When it comes to database technology, there’s no one-size-fits-all solution. That’s why many businesses rely on both relational and non-relational databases for different needs. Even as NoSQL databases are gaining popularity for their speed and scalability, there are still situations where a highly structured SQL database may perform better; choosing the right technology hinges on the use case.

### Reasons to use SQL database

Here are a few reasons to choose a SQL database:

1.  We need to ensure ACID compliance. ACID compliance reduces anomalies and protects the integrity of your database by prescribing exactly how transactions interact with the database. Generally, NoSQL databases sacrifice ACID compliance for scalability and processing speed, but for many e-commerce and financial applications, an ACID-compliant database remains the preferred option.
2.  Your data is structured and unchanging. If your business is not experiencing massive growth that would require more servers and if you’re only working with data that is consistent, then there may be no reason to use a system designed to support a variety of data types and high traffic volume.

### Reasons to use NoSQL database

When all the other components of our application are fast and seamless, NoSQL databases prevent data from being the bottleneck. Big data is contributing to a large success for NoSQL databases, mainly because it handles data differently than the traditional relational databases. A few popular examples of NoSQL databases are MongoDB, CouchDB, Cassandra, and HBase.

1.  Storing large volumes of data that often have little to no structure. A NoSQL database sets no limits on the types of data we can store together and allows us to add new types as the need changes. With document-based databases, you can store data in one place without having to define what “types” of data those are in advance.
2.  Making the most of cloud computing and storage. Cloud-based storage is an excellent cost-saving solution but requires data to be easily spread across multiple servers to scale up. Using commodity (affordable, smaller) hardware on-site or in the cloud saves you the hassle of additional software and NoSQL databases like Cassandra are designed to be scaled across multiple data centers out of the box, without a lot of headaches.
3.  Rapid development. NoSQL is extremely useful for rapid development as it doesn’t need to be prepped ahead of time. If you’re working on quick iterations of your system which require making frequent updates to the data structure without a lot of downtime between versions, a relational database will slow you down.

## Distributed Caching

In computing, a distributed cache is an extension of the traditional concept of cache used in a single locale. A distributed cache may span multiple servers so that it can grow in size and in transactional capacity. It is mainly used to store application data residing in database and web session data. The idea of distributed caching[1] has become feasible now because main memory has become very cheap and network cards have become very fast, with 1 Gbit now standard everywhere and 10 Gbit gaining traction. Also, a distributed cache works well on lower cost machines usually employed for web servers as opposed to database servers which require expensive hardware

## Scalability
### Cloning

[https://www.lecloud.net/post/7295452622/scalability-for-dummies-part-1-clones](https://www.lecloud.net/post/7295452622/scalability-for-dummies-part-1-clones)

Public servers of a scalable web service are hidden behind a load balancer. This load balancer evenly distributes load (requests from your users) onto your group/cluster of application servers. That means that if, for example, user Steve interacts with your service, he may be served at his first request by server 2, then with his second request by server 9 and then maybe again by server 2 on his third request.

Steve should always get the same results of his request back, independent what server he “landed on”. That leads to the first golden rule for scalability: every server contains exactly the same codebase and does not store any user-related data, like sessions or profile pictures, on local disc or memory.  
  
Sessions need to be stored in a centralized data store which is accessible to all your application servers. It can be an external database or an external persistent cache, like Redis. An external persistent cache will have better performance than an external database. By external I mean that the data store does not reside on the application servers. Instead, it is somewhere in or near the data center of your application servers.  
  
But what about deployment? How can you make sure that a code change is sent to all your servers without one server still serving old code? This tricky problem is fortunately already solved by the great tool Capistrano. It requires some learning, especially if you are not into Ruby on Rails, but it is definitely both the effort.

After “outsourcing” your sessions and serving the same codebase from all your servers, you can now create an image file from one of these servers (AWS calls this AMI - Amazon Machine Image.) Use this AMI as a “super-clone” that all your new instances are based upon. Whenever you start a new instance/clone, just do an initial deployment of your latest code and you are ready!

### Database

After following  [Part 1](https://www.lecloud.net/post/7295452622/scalability-for-dummies-part-1-clones) of this series, your servers can now horizontally scale and you can already serve thousands of concurrent requests. But somewhere down the road your application gets slower and slower and finally breaks down. The reason: your database. It’s MySQL, isn’t it?

Now the required changes are more radical than just adding more cloned servers and may even require some boldness. In the end, you can choose from 2 paths:

**Path #1** is to stick with MySQL and keep the “beast” running. Hire a database administrator (DBA,) tell him to do master-slave replication (read from slaves, write to master) and upgrade your master server by adding RAM, RAM and more RAM. In some months, your DBA will come up with words like “sharding”, “denormalization” and “SQL tuning” and will look worried about the necessary overtime during the next weeks. At that point every new action to keep your database running will be more expensive and time consuming than the previous one. You might have been better off if you had chosen Path #2 while your dataset was still small and easy to migrate.

**Path #2** means to denormalize right from the beginning and include no more Joins in any database query. You can stay with MySQL, and use it like a NoSQL database, or you can switch to a better and easier to scale NoSQL database like MongoDB or CouchDB. Joins will now need to be done in your application code. The sooner you do this step the less code you will have to change in the future. But even if you successfully switch to the latest and greatest NoSQL database and let your app do the dataset-joins, soon your database requests will again be slower and slower. You will need to introduce a cache.

### Cache

After following  [Part 2](http://www.lecloud.net/post/7994751381/scalability-for-dummies-part-2-database)  of this series, you now have a scalable database solution. You have no fear of storing terabytes anymore and the world is looking fine. But just for you. Your users still have to suffer slow page requests when a lot of data is fetched from the database. The solution is the implementation of a cache.

With “cache” I always mean in-memory caches like  [Memcached](https://t.umblr.com/redirect?z=http%3A%2F%2Fmemcached.org%2F&t=ZDQ0NmEwMDA2MzdmNWRhYmYyZTUwZTY2ZjU3ODQ0NmQyZjIzNWM0ZSxMVTFseGRTRw%3D%3D&b=t%3AeE4iDilbUfNhGIklAbjWYQ&p=https%3A%2F%2Fwww.lecloud.net%2Fpost%2F9246290032%2Fscalability-for-dummies-part-3-cache&m=1)  or  [Redis](https://t.umblr.com/redirect?z=http%3A%2F%2Fredis.io%2F&t=MGIxNmYzNGNkNmVjYjJiMDIzMDI4MTYzY2Q3OTQ2ZTU0NDFiNzEyMSxMVTFseGRTRw%3D%3D&b=t%3AeE4iDilbUfNhGIklAbjWYQ&p=https%3A%2F%2Fwww.lecloud.net%2Fpost%2F9246290032%2Fscalability-for-dummies-part-3-cache&m=1). Please  **never do file-based caching**, it makes cloning and auto-scaling of your servers just a pain.  
  
But back to in-memory caches. A cache is a simple key-value store and it should reside as a buffering layer between your application and your data storage. Whenever your application has to read data it should at first try to retrieve the data from your cache. Only if it’s not in the cache should it then try to get the data from the main data source. Why should you do that? Because  **a cache is lightning-fast**. It holds every dataset in RAM and requests are handled as fast as technically possible. For example, Redis can do several hundreds of thousands of read operations per second when being hosted on a standard server. Also writes, especially increments, are very, very fast. Try that with a database!  
  
  
There are 2 patterns of caching your data. An old one and a new one:  
**  
#1 - Cached Database Queries  
**That’s still the most commonly used caching pattern. Whenever you do a query to your database, you store the result dataset in cache. A hashed version of your query is the cache key. The next time you run the query, you first check if it is already in the cache. The next time you run the query, you check at first the cache if there is already a result. This pattern has several issues. The main issue is the expiration. It is hard to delete a cached result when you cache a complex query (who has not?). When one piece of data changes (for example a table cell) you need to delete all cached queries who may include that table cell. You get the point?

**#2 - Cached Objects**  
That’s my strong recommendation and I always prefer this pattern. In general, see your data as an object like you already do in your code (classes, instances, etc.). Let your class assemble a dataset from your database and then store the complete instance of the class or the assembed dataset in the cache. Sounds theoretical, I know, but just look how you normally code. You have, for example, a class called “Product” which has a property called “data”. It is an array containing prices, texts, pictures, and customer reviews of your product. The property “data” is filled by several methods in the class doing several database requests which are hard to cache, since many things relate to each other. Now, do the following: when your class has finished the “assembling” of the data array, directly store the data array, or better yet the complete instance of the class, in the cache! This allows you to easily get rid of the object whenever something did change and makes the overall operation of your code faster and more logical.  
  
And the best part: it makes asynchronous processing possible! Just imagine an army of worker servers who assemble your objects for you! The application just consumes the latest cached object and nearly never touches the databases anymore!  
  
Some ideas of objects to cache:

-   user sessions (never use the database!)
-   fully rendered blog articles
-   activity streams
-   user<->friend relationships

As you maybe already realized, I am a huge fan of caching. It is easy to understand, very simple to implement and the result is always breathtaking. In general, I am more a friend of Redis than Memcached, because I love the extra database-features of Redis like persistence and the built-in data structures like lists and sets. With Redis and a clever key’ing there may be a chance that you even can get completly rid of a database. But if you just need to cache, take Memcached, because it scales like a charm.

### Asynchonism

This 4th part of the  [series](http://www.lecloud.net/tagged/scalability)  starts with a picture: please imagine that you want to buy bread at your favorite bakery. So you go into the bakery, ask for a loaf of bread, but there is no bread there! Instead, you are asked to come back in 2 hours when your ordered bread is ready. That’s annoying, isn’t it?  
  
To avoid such a “please wait a while” - situation, asynchronism needs to be done. And what’s good for a bakery, is maybe also good for your web service or web app.  

In general, there are two ways / paradigms asynchronism can be done.  
**  
Async #1  
**Let’s stay in the former bakery picture. The first way of async processing is the “bake the breads at night and sell them in the morning” way. No waiting time at the cash register and a happy customer. Referring to a web app this means doing the time-consuming work in advance and serving the finished work with a low request time.  
  
Very often this paradigm is used to turn dynamic content into static content. Pages of a website, maybe built with a massive framework or CMS, are pre-rendered and locally stored as static HTML files on every change. Often these computing tasks are done on a regular basis, maybe by a script which is called every hour by a cronjob. This pre-computing of overall general data can extremely improve websites and web apps and makes them very scalable and performant. Just imagine the scalability of your website if the script would upload these pre-rendered HTML pages to AWS S3 or Cloudfront or another Content Delivery Network! Your website would be super responsive and could handle millions of visitors per hour!  
**  
Async #2  
**Back to the bakery. Unfortunately, sometimes customers has special requests like a birthday cake with “Happy Birthday, Steve!” on it. The bakery can not foresee these kind of customer wishes, so it must start the task when the customer is in the bakery and tell him to come back at the next day. Refering to a web service that means to handle tasks asynchronously.  
  
Here is a typical workflow:  
  
A user comes to your website and starts a very computing intensive task which would take several minutes to finish. So the frontend of your website sends a job onto a job queue and immediately signals back to the user: your job is in work, please continue to the browse the page. The job queue is constantly checked by a bunch of workers for new jobs. If there is a new job then the worker does the job and after some minutes sends a signal that the job was done. The frontend, which constantly checks for new “job is done” - signals, sees that the job was done and informs the user about it. I know, that was a very simplified example.  
  
If you now want to dive more into the details and actual technical design, I recommend you take a look at the first 3 tutorials on the  RabbitMQ website. RabbitMQ is one of many systems which help to implement async processing. You could also use  ActiveMQ or a simple  Redis list. The basic idea is to have a queue of tasks or jobs that a worker can process. Asynchronism seems complicated, but it is definitely worth your time to learn about it and implement it yourself. Backends become nearly infinitely scalable and frontends become snappy which is good for the overall user experience.

## Consistency Pattern

[https://www.youtube.com/watch?v=srOgpXECblk](https://www.youtube.com/watch?v=srOgpXECblk)

With multiple copies of the same data, we are faced with options on how to synchronize them so clients have a consistent view of the data. Recall the definition of consistency from the  [CAP theorem](https://github.com/donnemartin/system-design-primer#cap-theorem)  - Every read receives the most recent write or an error.

### Weak consistency

After a write, reads may or may not see it. A best effort approach is taken.

This approach is seen in systems such as memcached. Weak consistency works well in real time use cases such as VoIP, video chat, and realtime multiplayer games. For example, if you are on a phone call and lose reception for a few seconds, when you regain connection you do not hear what was spoken during connection loss.

### Eventual consistency

After a write, reads will eventually see it (typically within milliseconds). Data is replicated asynchronously.

This approach is seen in systems such as DNS and email. Eventual consistency works well in highly available systems.

### Strong consistency

After a write, reads will see it. Data is replicated synchronously.

This approach is seen in file systems and RDBMSes. Strong consistency works well in systems that need transactions.

## Consistent Hashing

Ref:
1. [https://www.toptal.com/big-data/consistent-hashing](https://www.toptal.com/big-data/consistent-hashing)

## ZooKeeper

[https://www.youtube.com/watch?v=gZj16chk0Ss](https://www.youtube.com/watch?v=gZj16chk0Ss)

## Availability Patterns

There are two main patterns to support high availability: **fail-over** and **replication**

### Fail-over

**Active-passive** With active-passive fail-over, heartbeats are sent between the active and the passive server on standby. If the heartbeat is interrupted, the passive server takes over the active's IP address and resumes service.

The length of downtime is determined by whether the passive server is already running in 'hot' standby or whether it needs to start up from 'cold' standby. Only the active server handles traffic.

Active-passive failover can also be referred to as master-slave failover.

**Active-active** In active-active, both servers are managing traffic, spreading the load between them.

If the servers are public-facing, the DNS would need to know about the public IPs of both servers. If the servers are internal-facing, application logic would need to know about both servers.

Active-active failover can also be referred to as master-master failover.

**Disadvantage(s): failover**
Fail-over adds more hardware and additional complexity.
There is a potential for loss of data if the active system fails before any newly written data can be replicated to the passive.

## Configuration Management
[https://www.youtube.com/watch?v=W49zokvnL2g](https://www.youtube.com/watch?v=W49zokvnL2g)

## HDFS

**Hadoop Distributed File system – HDFS** is the world’s most reliable storage system. HDFS is a Filesystem of Hadoop designed for storing very large files running on a cluster of commodity hardware. It is designed on principle of storage of less number of large files rather than the huge number of small files. Hadoop HDFS also provides fault tolerant storage layer for Hadoop and its other components. HDFS Replication of data helps us to attain this feature. It stores data reliably even in the case of hardware failure. It provides high throughput access to application data by providing the data access in parallel.

[https://www.quora.com/What-is-hdfs](https://www.quora.com/What-is-hdfs)

## Subset
[https://www.youtube.com/watch?v=-yz3FV8WliU](https://www.youtube.com/watch?v=-yz3FV8WliU)

### Network Address Translator (NAT)
[https://www.youtube.com/watch?v=FTUV0t6JaDA](https://www.youtube.com/watch?v=FTUV0t6JaDA)

## Two Phase Commit (2PC)

A two-phase commit is a standardized protocol that ensures that a database commit is implementing in the situation where a commit operation must be broken into two separate parts.

In database management, saving data changes is known as a commit and undoing changes is known as a rollback. Both can be achieved easily using transaction logging when a single server is involved, but when the data is spread across geographically-diverse servers in distributed computing (i.e., each server being an independent entity with separate log records), the process can become more tricky.

A special object, known as a coordinator, is required in a distributed transaction. As its name implies, the coordinator arranges activities and synchronization between distributed servers. The two-phase commit is implemented as follows:

Phase 1 - Each server that needs to commit data writes its data records to the log. If a server is unsuccessful, it responds with a failure message. If successful, the server replies with an OK message.

Phase 2 - This phase begins after all participants respond OK. Then, the coordinator sends a signal to each server with commit instructions. After committing, each writes the commit as part of its log record for reference and sends the coordinator a message that its commit has been successfully implemented. If a server fails, the coordinator sends instructions to all servers to roll back the transaction. After the servers roll back, each sends feedback that this has been completed.

## CQRS

  
CQRS, which means  _Command_  _Query Responsibility Segregation_, comes from CQS (_Command Query Separation_) introduced by Bertrand Meyer in  _Object Oriented Software Construction_. Meyer states that every method should be either a  _query_ or a  _command_.

The difference between CQS and CQRS is that every CQRS object is divided in two objects: one for the query and one for the command.

A command is defined as a method that changes state. On the contrary, a query only returns a value.

The following schema shows a basic implementation of the CQRS pattern inside an application. All messages are sent through commands and events. Let’s take a closer look at this.

![](https://miro.medium.com/max/60/0*BCNT4rhyijg2yhuK.png?q=20)

![](https://miro.medium.com/max/2048/0*BCNT4rhyijg2yhuK.png)

Example of implementation of CQRS pattern

We can clearly see the separation between writing parts and reading ones: the user does a modification on his page, resulting in a command being executed. Once this command is fully handled, an event is dispatched to signal a modification.

### Commands

A command tells our application to do something. Its name always uses the indicative tense, like  _TerminateBusiness_  or  _SendForgottenPasswordEmail_. It’s very important not to confine these names to  _create, change, delete…_  and to really focus on the use cases (see  _CQRS Documents_ at the end of this document for more information).

A command captures the intent of the user. No content in the response is returned by the server, only queries are in charge of retrieving data.

### Queries

Using different  _data stores_ in our application for the command and query parts seems to be a very interesting idea. As Udi Dahan explains very well in his article  _Clarified CQRS_, we could create a user interface oriented database, which would reflect what we need to display to our user. We would gain in both performance and speed.  
Dissociating our data stores (one for the modification of data and one for reading) does not imply the use of relational databases for both of them for instance. Therefore, it would be more thoughtful to use a database that can read our queries fastly.

If we separate our data sources, how can we still make them synchronized? Indeed, our “read” data store is not supposed to be aware that a command has been sent! This is where events come into play.

### Events

An event is a notification of something that  _has_ happened. Like a command, an event must respect a rule regarding names. Indeed, the name of an event always needs to be in the past tense, because we need to notify other parties listening on our event that a command has been completed. For instance,  _UserRegistered_ is a valid event name.  
Events are processed by one or more consumers. Those consumers are in charge of keeping things synchronized in our query store.

Very much like commands, events are messages. The difference with a command is summed up here:  a command is characterized by an action that _must_ happen, and an event by something that _has_ happened.

### Pros and Cons

The different benefits from this segregation are numerous. Here are a few:

-   _Scalability :_ The number of reads being far higher than the number of modifications inside an application, applying the CQRS pattern allows to focus independently on both concerns. One main advantage of this separation is scalability: we can scale our reading part differently from our writing part (allocate more resources, different types of database).
-   _Flexibility_  : it’s easy to update or add on the reading side, without changing anything on the writing side. Data consistency is therefore not altered.

One of the main con is to convince the members of a team that its benefits justify the additional complexity of this solution, as underlines the excellent article called  _CQRS Journey_ (see below)_._

### Usage

The CQRS pattern must be used inside a  _bounded context_ (key notion of  _Domain Driven Development_), or a business component of your application. Indeed, although this pattern has an impact on your code at several places, it must not be at the higher level of your application.

### Event Sourcing

What’s interesting with CQRS is  _event sourcing_. It can be used without application of the CQRS pattern, but if we use CQRS,  _event sourcing_ appears like a must-have.  
E_vent sourcing_ consists in saving every event that is occurring, in a database, and therefore having a back-up of facts. In a design of  _event sourcing_, you cannot delete or modify events, you can only add more. This is beneficial for our business and our information system, because we can know at a specific time what is the status of a command, a user or something else. Also, saving events allows us to rebuild the series of events and gain time in analysis.  
One of the examples given by Greg Young in the conference  _Code on the Beach_, is the balance in a bank account. It can be considered like a column in a table that we update every time money is debited or credited on the account. One other approach would be to store in our database all transactions that enabled us to get to this balance. It then becomes easier to be sure that the indicated amount is correct, because we keep a trace of events that have occurred, without being able to change them.

We will not go into more details on this principle in this article. A very interesting in-depth study is available in the article  _CQRS Journey_  (see below).

### Recap

CQRS is a simple pattern, which enables fantastic possibilities. It consists in separating the reading part from the writing part, with  _queries_ and  _commands._

Many advantages result from its usage, especially in term of flexibility and scaling.

_Event sourcing_  completes the  CQRS pattern by saving the history that determines the current state of our application. This is very useful in domains like accounting, because you get in your database a series of events (like financial transactions for instance) that cannot be modified or deleted.

## Monolithic Architecture

[https://microservices.io/patterns/monolithic.html](https://microservices.io/patterns/monolithic.html)

### Context

You are developing a server-side enterprise application. It must support a variety of different clients including desktop browsers, mobile browsers and native mobile applications. The application might also expose an API for 3rd parties to consume. It might also integrate with other applications via either web services or a message broker. The application handles requests (HTTP requests and messages) by executing business logic; accessing a database; exchanging messages with other systems; and returning a HTML/JSON/XML response. There are logical components corresponding to different functional areas of the application.

### Solution

Build an application with a monolithic architecture. For example:

-   a single Java WAR file.
-   a single directory hierarchy of Rails or NodeJS code

Let’s imagine that you are building an e-commerce application that takes orders from customers, verifies inventory and available credit, and ships them. The application consists of several components including the StoreFrontUI, which implements the user interface, along with some backend services for checking credit, maintaining inventory and shipping orders.

The application is deployed as a single monolithic application. For example, a Java web application consists of a single WAR file that runs on a web container such as Tomcat. A Rails application consists of a single directory hierarchy deployed using either, for example, Phusion Passenger on Apache/Nginx or JRuby on Tomcat. You can run multiple instances of the application behind a load balancer in order to scale and improve availability.

![](https://microservices.io/i/DecomposingApplications.011.jpg)

This solution has a number of benefits:

-   Simple to develop - the goal of current development tools and IDEs is to support the development of monolithic applications
-   Simple to deploy - you simply need to deploy the WAR file (or directory hierarchy) on the appropriate runtime
-   Simple to scale - you can scale the application by running multiple copies of the application behind a load balancer

However, once the application becomes large and the team grows in size, this approach has a number of drawbacks that become increasingly significant:

-   The large monolithic code base intimidates developers, especially ones who are new to the team. The application can be difficult to understand and modify. As a result, development typically slows down. Also, because there are not hard module boundaries, modularity breaks down over time. Moreover, because it can be difficult to understand how to correctly implement a change the quality of the code declines over time. It’s a downwards spiral.
    
-   Overloaded IDE - the larger the code base the slower the IDE and the less productive developers are.
    
-   Overloaded web container - the larger the application the longer it takes to start up. This had have a huge impact on developer productivity because of time wasted waiting for the container to start. It also impacts deployment too.
    
-   Continuous deployment is difficult - a large monolithic application is also an obstacle to frequent deployments. In order to update one component you have to redeploy the entire application. This will interrupt background tasks (e.g. Quartz jobs in a Java application), regardless of whether they are impacted by the change, and possibly cause problems. There is also the chance that components that haven’t been updated will fail to start correctly. As a result, the risk associated with redeployment increases, which discourages frequent updates. This is especially a problem for user interface developers, since they usually need to iterative rapidly and redeploy frequently.
    
-   Scaling the application can be difficult - a monolithic architecture is that it can only scale in one dimension. On the one hand, it can scale with an increasing transaction volume by running more copies of the application. Some clouds can even adjust the number of instances dynamically based on load. But on the other hand, this architecture can’t scale with an increasing data volume. Each copy of application instance will access all of the data, which makes caching less effective and increases memory consumption and I/O traffic. Also, different application components have different resource requirements - one might be CPU intensive while another might memory intensive. With a monolithic architecture we cannot scale each component independently
    
-   Obstacle to scaling development - A monolithic application is also an obstacle to scaling development. Once the application gets to a certain size its useful to divide up the engineering organization into teams that focus on specific functional areas. For example, we might want to have the UI team, accounting team, inventory team, etc. The trouble with a monolithic application is that it prevents the teams from working independently. The teams must coordinate their development efforts and redeployments. It is much more difficult for a team to make a change and update production.
    
-   Requires a long-term commitment to a technology stack - a monolithic architecture forces you to be married to the technology stack (and in some cases, to a particular version of that technology) you chose at the start of development . With a monolithic application, can be difficult to incrementally adopt a newer technology. For example, let’s imagine that you chose the JVM. You have some language choices since as well as Java you can use other JVM languages that inter-operate nicely with Java such as Groovy and Scala. But components written in non-JVM languages do not have a place within your monolithic architecture. Also, if your application uses a platform framework that subsequently becomes obsolete then it can be challenging to incrementally migrate the application to a newer and better framework. It’s possible that in order to adopt a newer platform framework you have to rewrite the entire application, which is a risky undertaking.

## Microservice Architecture

[https://microservices.io/patterns/microservices.html](https://microservices.io/patterns/microservices.html)

Define an architecture that structures the application as a set of loosely coupled, collaborating services. This approach corresponds to the Y-axis of the  [Scale Cube](https://microservices.io/articles/scalecube.html). Each service is:

-   Highly maintainable and testable - enables rapid and frequent development and deployment
-   Loosely coupled with other services - enables a team to work independently the majority of time on their service(s) without being impacted by changes to other services and without affecting other services
-   Independently deployable - enables a team to deploy their service without having to coordinate with other teams
-   Capable of being developed by a small team - essential for high productivity by avoiding the high communication head of large teams

Services communicate using either synchronous protocols such as HTTP/REST or asynchronous protocols such as AMQP. Services can be developed and deployed independently of one another. Each service has its  [own database](https://microservices.io/patterns/data/database-per-service.html)  in order to be decoupled from other services. Data consistency between services is maintained using the  [Saga pattern](https://microservices.io/patterns/data/saga.html)

To learn more about the nature of a service, please  [read this article](http://chrisrichardson.net/post/microservices/general/2019/02/16/whats-a-service-part-1.html).

### Example

Let’s imagine that you are building an e-commerce application that takes orders from customers, verifies inventory and available credit, and ships them. The application consists of several components including the StoreFrontUI, which implements the user interface, along with some backend services for checking credit, maintaining inventory and shipping orders. The application consists of a set of services.

![](https://microservices.io/i/Microservice_Architecture.png)

### Benefits

This solution has a number of benefits:

-   Enables the continuous delivery and deployment of large, complex applications.
    -   Improved maintainability - each service is relatively small and so is easier to understand and change
    -   Better testability - services are smaller and faster to test
    -   Better deployability - services can be deployed independently
    -   It enables you to organize the development effort around multiple, autonomous teams. Each (so called two pizza) team owns and is responsible for one or more services. Each team can develop, test, deploy and scale their services independently of all of the other teams.
-   Each microservice is relatively small:
    -   Easier for a developer to understand
    -   The IDE is faster making developers more productive
    -   The application starts faster, which makes developers more productive, and speeds up deployments
-   Improved fault isolation. For example, if there is a memory leak in one service then only that service will be affected. The other services will continue to handle requests. In comparison, one misbehaving component of a monolithic architecture can bring down the entire system.
-   Eliminates any long-term commitment to a technology stack. When developing a new service you can pick a new technology stack. Similarly, when making major changes to an existing service you can rewrite it using a new technology stack.

### Drawbacks

This solution has a number of drawbacks:

-   Developers must deal with the additional complexity of creating a distributed system:
    -   Developers must implement the inter-service communication mechanism and deal with partial failure
    -   Implementing requests that span multiple services is more difficult
    -   Testing the interactions between services is more difficult
    -   Implementing requests that span multiple services requires careful coordination between the teams
    -   Developer tools/IDEs are oriented on building monolithic applications and don’t provide explicit support for developing distributed applications.
-   Deployment complexity. In production, there is also the operational complexity of deploying and managing a system comprised of many different services.
-   Increased memory consumption. The microservice architecture replaces N monolithic application instances with NxM services instances. If each service runs in its own JVM (or equivalent), which is usually necessary to isolate the instances, then there is the overhead of M times as many JVM runtimes. Moreover, if each service runs on its own VM (e.g. EC2 instance), as is the case at Netflix, the overhead is even higher.

#### When to use the microservice architecture?

One challenge with using this approach is deciding when it makes sense to use it. When developing the first version of an application, you often do not have the problems that this approach solves. Moreover, using an elaborate, distributed architecture will slow down development. This can be a major problem for startups whose biggest challenge is often how to rapidly evolve the business model and accompanying application. Later on, however, when the challenge is how to scale and you need to use functional decomposition, the tangled dependencies might make it difficult to decompose your monolithic application into a set of services.

**API Gateway**

Ref: [https://www.youtube.com/watch?v=vHQqQBYJtLI](https://www.youtube.com/watch?v=vHQqQBYJtLI)

#### How to decompose the application into services?

Another challenge is deciding how to partition the system into microservices. This is very much an art, but there are a number of strategies that can help:

-   [Decompose by business capability](https://microservices.io/patterns/decomposition/decompose-by-business-capability.html)  and define services corresponding to business capabilities.
-   [Decompose by domain-driven design subdomain](https://microservices.io/patterns/decomposition/decompose-by-subdomain.html).
-   Decompose by verb or use case and define services that are responsible for particular actions. e.g. a  `Shipping Service`  that’s responsible for shipping complete orders.
-   Decompose by by nouns or resources by defining a service that is responsible for all operations on entities/resources of a given type. e.g. an  `Account Service`  that is responsible for managing user accounts.

Ideally, each service should have only a small set of responsibilities. (Uncle) Bob Martin talks about designing classes using the  [Single Responsibility Principle (SRP)](http://www.objectmentor.com/resources/articles/srp.pdf). The SRP defines a responsibility of a class as a reason to change, and states that a class should only have one reason to change. It make sense to apply the SRP to service design as well.

Another analogy that helps with service design is the design of Unix utilities. Unix provides a large number of utilities such as grep, cat and find. Each utility does exactly one thing, often exceptionally well, and can be combined with other utilities using a shell script to perform complex tasks.

#### How to maintain data consistency?

In order to ensure loose coupling, each service has its own database. Maintaining data consistency between services is a challenge because 2 phase-commit/distributed transactions is not an option for many applications. An application must instead use the  [Saga pattern](https://microservices.io/patterns/data/saga.html). A service publishes an event when its data changes. Other services consume that event and update their data. There are several ways of reliably updating data and publishing events including  [Event Sourcing](https://microservices.io/patterns/data/event-sourcing.html)  and  [Transaction Log Tailing](https://microservices.io/patterns/data/transaction-log-tailing.html).

#### How to implement queries?

Another challenge is implementing queries that need to retrieve data owned by multiple services.

-   The  [API Composition](https://microservices.io/patterns/data/api-composition.html)  and  [Command Query Responsibility Segregation (CQRS)](https://microservices.io/patterns/data/cqrs.html)  patterns.


![](https://microservices.io/i/PatternsRelatedToMicroservices.jpg)

### Decompose by business capability

You are developing a large, complex application and want to use the  [microservice architecture](https://microservices.io/patterns/microservices.html). The microservice architecture structures an application as a set of loosely coupled services. The goal of the microservice architecture is to accelerate software development by enabling continuous delivery/deployment.

![](https://microservices.io/i/successtriangle.png)

The microservice architecture does this in two ways:

1.  Simplifies testing and enables components to deployed independently
2.  Structures the engineering organization as a collection of small (6-10 members), autonomous teams, each of which is responsible for one or more services

These benefits are not automatically guaranteed. Instead, they can only be achieved by the careful functional decomposition of the application into services.

A service must be small enough to be developed by a small team and to be easily tested. A useful guideline from object-oriented design (OOD) is the Single Responsibility Principle ([SRP](http://www.objectmentor.com/resources/articles/srp.pdf)). The SRP defines a responsibility of a class as a reason to change, and states that a class should only have one reason to change. It make sense to apply the SRP to service design as well and design services that are cohesive and implement a small set of strongly related functions.

The application also be decomposed in a way so that most new and changed requirements only affect a single service. That is because changes that affect multiple services requires coordination across multiple teams, which slows down development. Another useful principle from OOD is the Common Closure Principle (CCP), which states that classes that change for the same reason should be in the same package. Perhaps, for instance, two classes implement different aspects of the same business rule. The goal is that when that business rule changes developers, only need to change code in a small number - ideally only one - of packages. This kind of thinking makes sense when designing services since it will help ensure that each change should impact only one service.

-   The architecture must be stable
-   Services must be cohesive. A service should implement a small set of strongly related functions.
-   Services must conform to the Common Closure Principle - things that change together should be packaged together - to ensure that each change affect only one service
-   Services must be loosely coupled - each service as an API that encapsulates its implementation. The implementation can be changed without affecting clients
-   A service should be testable
-   Each service be small enough to be developed by a “two pizza” team, i.e. a team of 6-10 people
-   Each team that owns one or more services must be autonomous. A team must be able to develop and deploy their services with minimal collaboration with other teams.

#### Solution

Define services corresponding to business capabilities. A business capability is a concept from business architecture modeling. It is something that a business does in order to generate value. A business capability often corresponds to a business object, e.g.

-   _Order Management_  is responsible for orders
-   _Customer Management_  is responsible for customers

Business capabilities are often organized into a multi-level hierarchy. For example, an enterprise application might have top-level categories such as  _Product/Service development_,  _Product/Service delivery_,  _Demand generation_, etc.

#### Examples

The business capabilities of an online store include:

-   Product catalog management
-   Inventory management
-   Order management
-   Delivery management
-   …

The corresponding microservice architecture would have services corresponding to each of these capabilities.

![](https://microservices.io/i/decompose-by-business-capability.png)

#### Resulting Context

This pattern has the following benefits:

-   Stable architecture since the business capabilities are relatively stable
-   Development teams are cross-functional, autonomous, and organized around delivering business value rather than technical features
-   Services are cohesive and loosely coupled

#### Issues

There are the following issues to address:

-   **How to identify business capabilities?**  Identifying business capabilities and hence services requires an understanding of the business. An organization’s business capabilities are identified by analyzing the organization’s purpose, structure, business processes, and areas of expertise. Bounded contexts are best identified using an iterative process. Good starting points for identifying business capabilities are:
    
    -   organization structure - different groups within an organization might correspond to business capabilities or business capability groups.
    -   high-level domain model - business capabilities often correspond to domain objects

### Decompose by Business Domain/Subdomain

Define services corresponding to Domain-Driven Design (DDD) subdomains. DDD refers to the application’s problem space - the business - as the domain. A domain is consists of multiple subdomains. Each subdomain corresponds to a different part of the business.

Subdomains can be classified as follows:

-   Core - key differentiator for the business and the most valuable part of the application
-   Supporting - related to what the business does but not a differentiator. These can be implemented in-house or outsourced.
-   Generic - not specific to the business and are ideally implemented using off the shelf software

#### Examples

The subdomains of an online store include:

-   Product catalog
-   Inventory management
-   Order management
-   Delivery management
-   …

The corresponding microservice architecture would have services corresponding to each of these subdomains.

![](https://microservices.io/i/decompose-by-subdomain.png)

#### Resulting Context

This pattern has the following benefits:

-   Stable architecture since the subdomains are relatively stable
-   Development teams are cross-functional, autonomous, and organized around delivering business value rather than technical features
-   Services are cohesive and loosely coupled

#### Issues

There are the following issues to address:

-   **How to identify the subdomains?**  Identifying subdomains and hence services requires an understanding of the business. Like business capabilities, subdomains are identified by analyzing the business and its organizational structure and identifying the different areas of expertise. Subdomains are best identified using an iterative process. Good starting points for identifying subdomains are:
    
-   organization structure - different groups within an organization might correspond to subdomains
-   high-level domain model - subdomains often have a key domain object

### Self Contained Service

Consider, the FTGO application, which is an online food delivery application. A client of application creates an order by making an HTTP  `POST /orders`  request and expects a response, say, within 600ms. Because the FTGO application uses the  [microservice architecture](https://microservices.io/patterns/microservices.html), the responsibilities that implement order creation are scattered across multiple services. The  `POST`  request is first routed to the  `Order Service`, which must then collaborate with the following services:

-   `Restaurant Service`  - knows the restaurant’s menu and prices
-   `Consumer Service`  - knows the state of the  `Consumer`  that places the order
-   `Kitchen Service`  - creates a  `Ticket`, which tells the chef what to cook
-   `Accounting Service`  - authorizes the consumer’s credit card

The  `Order Service`  could invoke each of these services using synchronous request/response. It might, for example, implement the inter-service communication using REST or gRPC.

![](https://microservices.io/i/Order-Service-synchronous.png)

However, a key drawback of using synchronous request/response is that it reduces availability. That’s because if any of the  `Order Sevice`’s collaborators are unavailable, it will not be able to create the order and must return an error to the client.

An alternative approach is to eliminate all synchronous communication between the  `Order Service`  and its collaborators by using the CQRS and Saga patterns. The  `Order Service`  can use the  [CQRS pattern](https://microservices.io/patterns/data/cqrs.html)  to maintain a replica of the restaurant menu’s and there by eliminate the need to synchronously fetch data from the  `Restaurant Service`. It can validate the order asynchronously by using the  [Saga pattern](https://microservices.io/patterns/data/saga.html). The  `Order Service`  creates an  `Order`  in a  `PENDING`  state and sends back a response to the  `POST /order`. It then completes the creation of the order by communicating asynchronously with the other services.

![](https://microservices.io/i/Order-Service-asynchronous.png)

A key benefit of this approach is that it improves availability. The  `Order Service`  always respond to a  `POST /orders`  request even when one of the other services is unavailable. One drawback, however, of using a saga to complete the creation of the order is that the response to the  `POST`  doesn’t tell the client whether the order was approved. The client must find out by periodically invoking  `GET /orders/{orderId}`.

#### Problem

How should a service collaborate with other services when handling a synchronous request?

#### Forces

-   The microservice architecture often distributes the responsibility of handling a request amongst multiple services
-   An operation is typically required to be highly available with a low response time
-   The availability of an operation is the product of the availabilities of the services that are invoked while handling a request: serviceAvailabilitynumberOfSynchronouslyCollaboratingServices
-   A service can retry a request to a failed collaborator but this increases response time.

#### Solution

Design a service so that it can respond to a synchronous request without waiting for the response from any other service.

![](https://microservices.io/i/SelfContainedService.png)

One way to make a service self-contained is to implement needed functionality as a service module rather than a separate service. We could, for example, merge the  `Order Service`  and  `Restaurant Service`.

Another way to make a service self-contained is for it to collaborate with other services using the  [CQRS](https://microservices.io/patterns/data/cqrs.html)  and the  [Saga](https://microservices.io/patterns/data/saga.html)  patterns. A self-contained service uses the Saga pattern to asynchronously maintain data consistency. It uses the CQRS pattern to maintain a replica of data owned by other services.

#### Example

The  `Order Service`  in the FTGO application described earlier is an example of a self-contained service. The  `createOrder()`  operation, for example, queries a CQRS replica of data owned by the  `Restaurant Service`  to validate and price the order, and then initiates a saga to finish the creation of the order.

#### Resulting Context

This pattern has the following benefits:

-   Improved availability and response time

This pattern has the following drawbacks:

-   Increased cost and complexity of using CQRS
-   Increased complexity of using sagas
-   Less straightforward API when using sagas
-   Larger service due to functionality being implemented in the service instead of as a separate service

SAGA - used by a service self-contained to asynchronously maintain data consistency
CQRS - used by a service self-contained to maintain a replica of data owned by other services

## Microservice Data Management
### Database per services

Let’s imagine you are developing an online store application using the  [Microservice architecture pattern](https://microservices.io/patterns/microservices.html). Most services need to persist data in some kind of database. For example, the  `Order Service`  stores information about orders and the  `Customer Service`  stores information about customers.

![](https://microservices.io/i/customersandorders.png)

What’s the database architecture in a microservices application?

-   Services must be loosely coupled so that they can be developed, deployed and scaled independently
    
-   Some business transactions must enforce invariants that span multiple services. For example, the  `Place Order`  use case must verify that a new Order will not exceed the customer’s credit limit. Other business transactions, must update data owned by multiple services.
    
-   Some business transactions need to query data that is owned by multiple services. For example, the  `View Available Credit`  use must query the Customer to find the  `creditLimit`  and Orders to calculate the total amount of the open orders.
    
-   Some queries must join data that is owned by multiple services. For example, finding customers in a particular region and their recent orders requires a join between customers and orders.
    
-   Databases must sometimes be replicated and sharded in order to scale. See the  [Scale Cube](https://microservices.io/articles/scalecube.html).
    
-   Different services have different data storage requirements. For some services, a relational database is the best choice. Other services might need a NoSQL database such as MongoDB, which is good at storing complex, unstructured data, or Neo4J, which is designed to efficiently store and query graph data.
    

#### Solution

Keep each microservice’s persistent data private to that service and accessible only via its API. A service’s transactions only involve its database.

The following diagram shows the structure of this pattern.

![](https://microservices.io/i/databaseperservice.png)

The service’s database is effectively part of the implementation of that service. It cannot be accessed directly by other services.

There are a few different ways to keep a service’s persistent data private. You do not need to provision a database server for each service. For example, if you are using a relational database then the options are:

-   Private-tables-per-service – each service owns a set of tables that must only be accessed by that service
-   Schema-per-service – each service has a database schema that’s private to that service
-   Database-server-per-service – each service has it’s own database server.

Private-tables-per-service and schema-per-service have the lowest overhead. Using a schema per service is appealing since it makes ownership clearer. Some high throughput services might need their own database server.

It is a good idea to create barriers that enforce this modularity. You could, for example, assign a different database user id to each service and use a database access control mechanism such as grants. Without some kind of barrier to enforce encapsulation, developers will always be tempted to bypass a service’s API and access it’s data directly.

#### Example

The  [FTGO application](https://chrisrichardson.net/post/microservices/patterns/data/2019/07/15/ftgo-database-per-service.html)  is an example of an application that uses this approach. Each service has database credentials that only grant it access its own (logical) database on a shared MySQL server. For more information, see this  [blog post](https://chrisrichardson.net/post/microservices/patterns/data/2019/07/15/ftgo-database-per-service.html).

Using a database per service has the following benefits:

-   Helps ensure that the services are loosely coupled. Changes to one service’s database does not impact any other services.
    
-   Each service can use the type of database that is best suited to its needs. For example, a service that does text searches could use ElasticSearch. A service that manipulates a social graph could use Neo4j.
    

Using a database per service has the following drawbacks:

-   Implementing business transactions that span multiple services is not straightforward. Distributed transactions are best avoided because of the CAP theorem. Moreover, many modern (NoSQL) databases don’t support them.
    
-   Implementing queries that join data that is now in multiple databases is challenging.
    
-   Complexity of managing multiple SQL and NoSQL databases
    

There are various patterns/solutions for implementing transactions and queries that span services:

-   Implementing transactions that span services - use the  [Saga pattern](https://microservices.io/patterns/data/saga.html).
    
-   Implementing queries that span services:
    
    -   [API Composition](https://microservices.io/patterns/data/api-composition.html)  - the application performs the join rather than the database. For example, a service (or the API gateway) could retrieve a customer and their orders by first retrieving the customer from the customer service and then querying the order service to return the customer’s most recent orders.
        
    -   [Command Query Responsibility Segregation (CQRS)](https://microservices.io/patterns/data/cqrs.html)  - maintain one or more materialized views that contain data from multiple services. The views are kept by services that subscribe to events that each services publishes when it updates its data. For example, the online store could implement a query that finds customers in a particular region and their recent orders by maintaining a view that joins customers and orders. The view is updated by a service that subscribes to customer and order events.
        

#### Related patterns

-   [Microservice architecture pattern](https://microservices.io/patterns/microservices.html)  creates the need for this pattern
-   [Saga pattern](https://microservices.io/patterns/data/saga.html)  is a useful way to implement eventually consistent transactions
-   The  [API Composition](https://microservices.io/patterns/data/api-composition.html)  and  [Command Query Responsibility Segregation (CQRS)](https://microservices.io/patterns/data/cqrs.html)  pattern are useful ways to implement queries
-   The  [Shared Database anti-pattern](https://microservices.io/patterns/data/shared-database.html)  describes the problems that result from microservices sharing a database

### Share database accross multiple services

Use a (single) database that is shared by multiple services. Each service freely accesses data owned by other services using local ACID transactions. For example -

The  `OrderService`  and  `CustomerService`  freely access each other’s tables. For example, the  `OrderService`  can use the following ACID transaction ensure that a new order will not violate the customer’s credit limit.

```sql
BEGIN TRANSACTION
…
SELECT ORDER_TOTAL
 FROM ORDERS WHERE CUSTOMER_ID = ?
…
SELECT CREDIT_LIMIT
FROM CUSTOMERS WHERE CUSTOMER_ID = ?
…
INSERT INTO ORDERS …
…
COMMIT TRANSACTION
```

The database will guarantee that the credit limit will not be exceeded even when simultaneous transactions attempt to create orders for the same customer.

**The benefits of this pattern are:**

-   A developer uses familiar and straightforward ACID transactions to enforce data consistency
-   A single database is simpler to operate

**The drawbacks of this pattern are:**

-   Development time coupling - a developer working on, for example, the  `OrderService`  will need to coordinate schema changes with the developers of other services that access the same tables. This coupling and additional coordination will slow down development.
    
-   Runtime coupling - because all services access the same database they can potentially interfere with one another. For example, if long running  `CustomerService`  transaction holds a lock on the  `ORDER`  table then the  `OrderService`  will be blocked.
    
-   Single database might not satisfy the data storage and access requirements of all services.

### Database consistency across multiple services

[https://www.youtube.com/watch?v=WnZ7IcaN_JA&list=PLTyWtrsGknYd0JgqeARypdRy-SX1ORYhs&index=12](https://www.youtube.com/watch?v=WnZ7IcaN_JA&list=PLTyWtrsGknYd0JgqeARypdRy-SX1ORYhs&index=12)

Implement each business transaction that spans multiple services as a saga. A saga is a sequence of local transactions. Each local transaction updates the database and publishes a message or event to trigger the next local transaction in the saga. If a local transaction fails because it violates a business rule then the saga executes a series of compensating transactions that undo the changes that were made by the preceding local transactions.

![](https://microservices.io/i/data/saga.jpg)

There are two ways of coordination sagas:

-   Choreography - each local transaction publishes domain events that trigger local transactions in other services
-   Orchestration - an orchestrator (object) tells the participants what local transactions to execute

![](https://microservices.io/i/data/Saga_Choreography_Flow.001.jpeg)

An e-commerce application that uses this approach would create an order using a choreography-based saga that consists of the following steps:

1.  The  `Order Service`  creates an Order in a  _pending_  state and publishes an  `OrderCreated`  event
2.  The  `Customer Service`  receives the event attempts to reserve credit for that Order. It publishes either a  `Credit Reserved`  event or a  `CreditLimitExceeded`  event.
3.  The  `Order Service`  receives the event and changes the state of the order to either  _approved_  or  _cancelled_

![](https://microservices.io/i/data/Saga_Orchestration_Flow.001.jpeg)

An e-commerce application that uses this approach would create an order using an orchestration-based saga that consists of the following steps:

1.  The  `Order Service`  creates an Order in a  _pending_  state and creates a  `CreateOrderSaga`
2.  The  `CreateOrderSaga`  sends a  `ReserveCredit`  command to the  `Customer Service`
3.  The  `Customer Service`  attempts to reserve credit for that Order and sends back a reply
4.  The  `CreateOrderSaga`  receives the reply and sends either an  `ApproveOrder`  or  `RejectOrder`  command to the  `Order Service`
5.  The  `Order Service`  changes the state of the order to either  _approved_  or  _cancelled_

This pattern has the following benefits:

-   It enables an application to maintain data consistency across multiple services without using distributed transactions

This solution has the following drawbacks:

-   The programming model is more complex. For example, a developer must design compensating transactions that explicitly undo changes made earlier in a saga.

There are also the following issues to address:

-   In order to be reliable, a service must atomically update its database  _and_  publish a message/event. It cannot use the traditional mechanism of a distributed transaction that spans the database and the message broker. Instead, it must use one of the patterns listed below.

#### Related patterns

-   The  [Database per Service pattern](https://microservices.io/patterns/data/database-per-service.html)  creates the need for this pattern
-   The following patterns are ways to  _atomically_  update state  _and_  publish messages/events:
    -   [Event sourcing](https://microservices.io/patterns/data/event-sourcing.html)
    -   [Transactional Outbox](https://microservices.io/patterns/data/transactional-outbox.html)
-   A choreography-based saga can publish events using  [Aggregates](https://microservices.io/patterns/data/aggregate.html)  and  [Domain Events](https://microservices.io/patterns/data/domain-event.html)

#### Learn more

-   Read these  [blog posts on the Saga pattern](https://chrisrichardson.net/post/antipatterns/2019/07/09/developing-sagas-part-1.html)
-   My book  [Microservices patterns](https://microservices.io/book)  describes this pattern in a lot more detail. The book’s  [example application](https://github.com/microservice-patterns/ftgo-application)  implements orchestration-based sagas using the  [Eventuate Tram Sagas framework](https://github.com/eventuate-tram/eventuate-tram-sagas)
-   My  [presentations](https://microservices.io/presentations)  on sagas and asynchronous microservices.

#### Example code

The following examples implement the customers and orders example in different ways:

-   [Choreography-based saga](https://github.com/eventuate-tram/eventuate-tram-examples-customers-and-orders)  where the services publish domain events using the  [Eventuate Tram framework](https://github.com/eventuate-tram/eventuate-tram-core)
-   [Orchestration-based saga](https://github.com/eventuate-tram/eventuate-tram-sagas-examples-customers-and-orders)  where the  `Order Service`  uses a saga orchestrator implemented using the  [Eventuate Tram Sagas framework](https://github.com/eventuate-tram/eventuate-tram-sagas)
-   [Choreography and event sourcing-based saga](https://github.com/eventuate-examples/eventuate-examples-java-customers-and-orders)  where the services publish domain events using the  [Eventuate event sourcing framework](http://eventuate.io/)

### API Composer

Implement a query by defining an  _API Composer_, which invoking the services that own the data and performs an in-memory join of the results.

![](https://microservices.io/i/data/ApiBasedQueryBigPicture.png)

An  [API Gateway](https://microservices.io/patterns/apigateway.html)  often does API composition.

This pattern has the following benefits:

-   It a simple way to query data in a microservice architecture

This pattern has the following drawbacks:

-   Some queries would result in inefficient, in-memory joins of large datasets.

### CQRS - Implement Query on multiple Services

Define a view database, which is a read-only replica that is designed to support that query. The application keeps the replica up to data by subscribing to  [Domain events](https://microservices.io/patterns/data/domain-event.html)  published by the service that own the data.

![](https://microservices.io/i/patterns/data/QuerySideService.png)

This pattern has the following benefits:

-   Supports multiple denormalized views that are scalable and performant
-   Improved separation of concerns = simpler command and query models
-   Necessary in an event sourced architecture

This pattern has the following drawbacks:

-   Increased complexity
-   Potential code duplication
-   Replication lag/eventually consistent views

### Event Sourcing?

How to reliably/atomically update the database and publish messages/events?

A good solution to this problem is to use event sourcing. Event sourcing persists the state of a business entity such an Order or a Customer as a sequence of state-changing events. Whenever the state of a business entity changes, a new event is appended to the list of events. Since saving an event is a single operation, it is inherently atomic. The application reconstructs an entity’s current state by replaying the events.

Applications persist events in an event store, which is a database of events. The store has an API for adding and retrieving an entity’s events. The event store also behaves like a message broker. It provides an API that enables services to subscribe to events. When a service saves an event in the event store, it is delivered to all interested subscribers.

Some entities, such as a Customer, can have a large number of events. In order to optimize loading, an application can periodically save a snapshot of an entity’s current state. To reconstruct the current state, the application finds the most recent snapshot and the events that have occurred since that snapshot. As a result, there are fewer events to replay.

## Example

[Customers and Orders](https://github.com/eventuate-examples/eventuate-examples-java-customers-and-orders)  is an example of an application that is built using Event Sourcing and  [CQRS](https://microservices.io/patterns/data/cqrs.html). The application is written in Java, and uses Spring Boot. It is built using  [Eventuate](http://eventuate.io/), which is an application platform based on event sourcing and CQRS.

The following diagram shows how it persist orders.

![](https://microservices.io/i/storingevents.png)

Instead of simply storing the current state of each order as a row in an  `ORDERS`  table, the application persists each  `Order`  as a sequence of events. The  `CustomerService`  can subscribe to the order events and update its own state.

Event sourcing has several benefits:

-   It solves one of the key problems in implementing an event-driven architecture and makes it possible to reliably publish events whenever state changes.
-   Because it persists events rather than domain objects, it mostly avoids the object‑relational impedance mismatch problem.
-   It provides a 100% reliable  [audit log](https://microservices.io/patterns/observability/audit-logging)  of the changes made to a business entity
-   It makes it possible to implement temporal queries that determine the state of an entity at any point in time.
-   Event sourcing-based business logic consists of loosely coupled business entities that exchange events. This makes it a lot easier to migrate from a monolithic application to a microservice architecture.

Event sourcing also has several drawbacks:

-   It is a different and unfamiliar style of programming and so there is a learning curve.
-   The event store is difficult to query since it requires typical queries to reconstruct the state of the business entities. That is likely to be complex and inefficient. As a result, the application must use  [Command Query Responsibility Segregation (CQRS)](https://microservices.io/patterns/data/cqrs.html)  to implement queries. This in turn means that applications must handle eventually consistent data.

## Transactional outbox/Application Event

How to reliably/atomically update the database and publish messages/events?

A service that uses a relational database inserts messages/events into an  _outbox_  table (e.g.  `MESSAGE`) as part of the local transaction. An service that uses a NoSQL database appends the messages/events to attribute of the record (e.g. document or item) being updated. A separate  _Message Relay_  process publishes the events inserted into database to a message broker.

![](https://microservices.io/i/patterns/data/ReliablePublication.png)

This pattern has the following benefits:

-   The service publishes high-level  [domain events](https://microservices.io/patterns/data/domain-event.html)
-   No 2PC required

This pattern has the following drawbacks:

-   Potentially error prone since the developer might forget to publish the message/event after updating the database.
-   The Message Relay might publish a message more than once. It might, for example, crash after publishing a message but before recording the fact that it has done so. When it restarts, it will then publish the message again. As a result, a message consumer must be idempotent, perhaps by tracking the IDs of the messages that it has already processed. Fortunately, since Message Consumers usually need to be idempotent (because a message broker can deliver messages more than once) this is typically not a problem.

## Transaction Log Tailing

How to publish messages/events into the  _outbox_  in the database to the message broker?

Tail the database transaction log and publish each message/event inserted into the  _outbox_  to the message broker.

![](https://microservices.io/i/patterns/data/TransactionLogMining.png)

The mechanism for trailing the transaction log depends on the database:

-   MySQL binlog
-   Postgres WAL
-   AWS DynamoDB table streams

This pattern has the following benefits:

-   No 2PC
-   Guaranteed to be accurate

This pattern has the following drawbacks:

-   Relatively obscure although becoming increasing common
-   Requires database specific solutions
-   Tricky to avoid duplicate publishing

## Service Component Test

Ref: below link to understand more about the Microservice Testing [https://medium.com/@copyconstruct/testing-microservices-the-sane-way-9bb31d158c16](https://medium.com/@copyconstruct/testing-microservices-the-sane-way-9bb31d158c16)

How do you easily test a service?

-   End to end testing (i.e. tests that launch multiple services) is difficult, slow, brittle, and expensive.

A test suite that tests a service in isolation using **test doubles** for any services that it invokes.

This pattern has the following benefits:

-   Testing a service in isolation is easier, faster, more reliable and cheap

This pattern has the following drawbacks:

-   Tests might pass but the application will fail in production
-   How to ensure that the test doubles always correctly emulate the behavior of the invoked services?

### Test Doubles

Ref: [https://blog.pragmatists.com/test-doubles-fakes-mocks-and-stubs-1a7491dfa3da](https://blog.pragmatists.com/test-doubles-fakes-mocks-and-stubs-1a7491dfa3da)

A  **test double**  is an object that can stand in for a real object in a  **test**, similar to how a stunt  **double**  stands in for an actor in a movie

Now, to really understand the above sentence, you need to know one trivial fact about unit testing. Whenever we test a method in a particular class, that method in most of the cases will depend on objects of other classes. Let us look at an example below:

```java
public void testableMethod (A a, B b) {  
     // does something that needs to be tested  
}
```

Its pretty apparent above that the method we need to test (testableMethod) depends on objects of class A and class B. Our code inside the testable method will be calling some method of class A and class B. While testing our testableMethod, what we should be able to do is pass in dummy objects (again, not the correct terminology, but works for now) of class A and class B, and hard code the response whenever particular methods are called on those dummy objects (test double)

**Fake**

**_Fakes are objects that have working implementations, but not same as production one. Usually they take some shortcut and have simplified version of production code._**

An example of this shortcut, can be an in-memory implementation of Data Access Object or Repository. This fake implementation will not engage database, but will use a simple collection to store data. This allows us to do integration test of services without starting up a database and performing time consuming requests.

![Example of Fake](https://miro.medium.com/max/60/0*snrzYwepyaPu3uC9.png?q=20)

![Example of Fake](https://miro.medium.com/max/2048/0*snrzYwepyaPu3uC9.png)

Apart from testing, fake implementation can come handy for prototyping and spikes. We can quickly implement and run our system with in-memory store, deferring decisions about database design. Another example can be also a fake payment system, that will always return successful payments.

**Stub**

**_Stub is an object that holds predefined data and uses it to answer calls during tests. It is used when we cannot or don’t want to involve objects that would answer with real data or have undesirable side effects._**

An example can be an object that needs to grab some data from the database to respond to a method call. Instead of the real object, we introduced a stub and defined what data should be returned.

![example of Stub](https://miro.medium.com/max/60/0*KdpZaEVy6GNnrUpB.png?q=20)

![example of Stub](https://miro.medium.com/max/2048/0*KdpZaEVy6GNnrUpB.png)

Instead of calling database from Gradebook store to get real students grades, we preconfigure stub with grades that will be returned. We define just enough data to test average calculation algorithm.

**Command Query Separation**

Methods that return some result and do not change the state of the system, are called  **Query**. Method  _averageGrades_, that returns average of student grades is a good example.  
`Double averageGrades(Student student);`

It returns a value and is free of side effects. As we have seen in students grading example, for testing this type of method we use Stubs. We are replacing real functionality to provide values needed for method to perform its job. Then, values returned by the method can be used for assertions.

There is also another category of methods called  **Command**. This is when a method performs some actions, that changes the system state, but we don’t expect any return value from it.  
`void sendReminderEmail(Student student);`

A good practice is to divide an object's methods into those two separated categories.  
This practice was named: Command Query separation by Bertrand Meyer in his book  ["Object Oriented Software Construction"](https://www.amazon.com/Object-Oriented-Software-Construction-Book-CD-ROM/dp/0136291554).

For testing Query type methods we should prefer use of Stubs as we can verify method’s return value. But what about Command type of methods, like method sending an e-mail? How to test them when they do not return any values? The answer is Mock - the last type of test dummy we gonna cover.

**Mock**

**_Mocks are objects that register calls they receive.  
In test assertion we can verify on Mocks that all expected actions were performed._**

We use mocks when we don’t want to invoke production code or when there is no easy way to verify, that intended code was executed. There is no return value and no easy way to check system state change. An example can be a functionality that calls e-mail sending service.  
We don’t want to send e-mails each time we run a test. Moreover, it is not easy to verify in tests that a right email was send. Only thing we can do is to verify the outputs of the functionality that is exercised in our test. In other worlds, verify that e-mail sending service was called.

Similar case is presented in the following example:

![Example of Mock](https://miro.medium.com/max/60/0*k7mwTF60slyMxRlm.png?q=20)

![Example of Mock](https://miro.medium.com/max/2048/0*k7mwTF60slyMxRlm.png)

We don’t want to close real doors to test that security method is working, right? Instead, we place door and window mocks objects in the test code.

After execution of securityOn method, window and door mocks recorded all interactions. This lets us verify that window and door objects were instructed to close themselves. That's all we need to test from SecurityCental perspective.

You may ask how can we tell if door and window will be closed for real if we use mock? The answer is that we can’t. But we don’t care about it. This is not responsibility of SecurityCentral. This is responsibility of Door and Window alone to close itself when they get proper signal. We can test it independently in different unit test.

## Microservice Deployment

**Run multiple instances of different services on a single host (Physical or Virtual machine)**

There are various ways of deploying a service instance on a shared host including:

-   Deploy each service instance as a JVM process. For example, a Tomcat or Jetty instances per service instance.
-   Deploy multiple service instances in the same JVM. For example, as web applications or OSGI bundles.

The drawbacks of this approach include:

-   Risk of conflicting resource requirements
-   Risk of conflicting dependency versions
-   Difficult to limit the resources consumed by a service instance
-   If multiple services instances are deployed in the same process then its difficult to monitor the resource consumption of each service instance. Its also impossible to isolate each instance

**Deploy each single service instance on its own host**

The benefits of this approach include:

-   Services instances are isolated from one another
-   There is no possibility of conflicting resource requirements or dependency versions
-   A service instance can only consume at most the resources of a single host
-   Its straightforward to monitor, manage, and redeploy each service instance

**Package the service as a virtual machine image and deploy each service instance as a separate VM**

-   Netflix packages each service as an EC2 AMI and deploys each service instance as a EC2 instance.

The benefits of this approach include:

-   Its straightforward to scale the service by increasing the number of instances. Amazon Autoscaling Groups can even do this automatically based on load.
-   The VM encapsulates the details of the technology used to build the service. All services are, for example, started and stopped in exactly the same way.
-   Each service instance is isolated
-   A VM imposes limits on the CPU and memory consumed by a service instance

**Package the service as a (Docker) container image and deploy each service instance as a container**

Docker is becoming an extremely popular way of packaging and deploying services. Each service is packaged as a Docker image and each service instance is a Docker container. There are several Docker clustering frameworks including:

-   [Kubernetes](http://kubernetes.io/)
-   [Marathon/Mesos](https://mesosphere.github.io/marathon/)
-   [Amazon EC2 Container Service](http://aws.amazon.com/ecs/)

The benefits of this approach include:

-   It is straightforward to scale up and down a service by changing the number of container instances.
-   The container encapsulates the details of the technology used to build the service. All services are, for example, started and stopped in exactly the same way.
-   Each service instance is isolated
-   A container imposes limits on the CPU and memory consumed by a service instance
-   Containers are extremely fast to build and start. For example, it’s 100x faster to package an application as a Docker container than it is to package it as an AMI. Docker containers also start much faster than a VM since only the application process starts rather than an entire OS.

## Microservice Chasis

[https://www.youtube.com/watch?v=57anw5XBl98](https://www.youtube.com/watch?v=57anw5XBl98)

When you start the development of an application you often spend a significant amount of time putting in place the mechanisms to handle cross-cutting concerns. Examples of cross-cutting concern include:

-   Externalized configuration - includes credentials, and network locations of external services such as databases and message brokers
-   Logging - configuring of a logging framework such as log4j or logback
-   Health checks - a url that a monitoring service can “ping” to determine the health of the application
-   Metrics - measurements that provide insight into what the application is doing and how it is performing
-   [Distributed tracing](https://microservices.io/patterns/observability/distributed-tracing.html)  - instrument services with code that assigns each external request an unique identifier that is passed between services.

As well as these generic cross-cutting concerns, there are also cross-cutting concerns that are specific to the technologies that an application uses. Applications that use infrastructure services such as databases or a message brokers require boilerplate configuration in order to do that. For example, applications that use a relational database must be configured with a connection pool. Web applications that process HTTP requests also need boilerplate configuration.

It is common to spend one or two days, sometimes even longer, setting up these mechanisms. If you going to spend months or years developing a monolithic application then the upfront investment in handling cross-cutting concerns is insignificant. The situation is very different, however, if you are developing an application that has the  [microservice architecture](https://microservices.io/patterns/microservices.html). There are tens or hundreds of services. You will frequently create new services, each of which will only take days or weeks to develop. You cannot afford to spend a few days configuring the mechanisms to handle cross-cutting concerns. What is even worse is that in a microservice architecture there are additional cross-cutting concerns that you have to deal with including service registration and discovery, and circuit breakers for reliably handling partial failure.

-   Creating a new microservice should be fast and easy
-   When creating a microservice you must handle cross-cutting concerns such as externalized configuration, logging, health checks, metrics, service registration and discovery, circuit breakers. There are also cross-cutting concerns that are specific to the technologies that the microservices uses.

Examples of microservice chassis frameworks:

-   Java
    -   [Spring Boot](http://projects.spring.io/spring-boot/)  and  [Spring Cloud](http://cloud.spring.io/)
    -   [Dropwizard](https://dropwizard.github.io/)
-   Go
    -   [Gizmo](http://open.blogs.nytimes.com/2015/12/17/introducing-gizmo/?_r=2)
    -   [Micro](https://github.com/micro)
    -   [Go kit](https://github.com/go-kit/kit)

## Externalized configuration

An application typically uses one or more infrastructure and 3rd party services. Examples of infrastructure services include: a  [Service registry](https://microservices.io/patterns/service-registry.html), a message broker and a database server. Examples of 3rd party services include: payment processing, email and messaging, etc.
How to enable a service to run in multiple environments without modification?

-   A service must be provided with configuration data that tells it how to connect to the external/3rd party services. For example, the database network location and credentials
-   A service must run in multiple environments - dev, test, qa, staging, production - without modification and/or recompilation
-   Different environments have different instances of the external/3rd party services, e.g. QA database vs. production database, test credit card processing account vs. production credit card processing account

### Solution

Externalize all application configuration including the database credentials and network location. On startup, a service reads the configuration from an external source, e.g. OS environment variables, etc.

## Inter-service communication

Use RPI (Remote Procedure Invocation) for inter-service communication. The client uses a request/reply-based protocol to make requests to a service.
There are numerous examples of RPI technologies

-   [REST](https://en.wikipedia.org/wiki/Representational_state_transfer)
-   [gRPC](http://www.grpc.io/)
-   [Apache Thrift](https://thrift.apache.org/)

This pattern has the following benefits:

-   Simple and familiar
-   Request/reply is easy
-   Simpler system since there in no intermediate broker

This pattern has the following drawbacks:

-   Usually only supports request/reply and not other interaction patterns such as notifications, request/async response, publish/subscribe, publish/async response
-   **Reduced availability since the client and the service must be available for the duration of the interaction**
-   **Client needs to discover locations of service instances**

**Use asynchronous messaging for inter-service communication. Services communicating by exchanging messages over messaging channels.**

There are several different styles of asynchronous communication:

-   Request/response - a service sends a request message to a recipient and expects to receive a reply message promptly
-   Notifications - a sender sends a message a recipient but does not expect a reply. Nor is one sent.
-   Request/asynchronous response - a service sends a request message to a recipient and expects to receive a reply message eventually
-   Publish/subscribe - a service publishes a message to zero or more recipients
-   Publish/asynchronous response - a service publishes a request to one or recipients, some of whom send back a reply

There are numerous examples of asynchronous messaging technologies

-   [Apache Kafka](http://kafka.apache.org/)
-   [RabbitMQ](https://www.rabbitmq.com/)

Pros
-   Loose runtime coupling since it decouples the message sender from the consumer
-   Improved availability since the message broker buffers messages until the consumer is able to process them
-   Supports a variety of communication patterns including request/reply, notifications, request/async response, publish/subscribe, publish/async response etc

## API Gateway

[https://microservices.io/patterns/apigateway.html](https://microservices.io/patterns/apigateway.html)

## Client side Service Discovery

Ref: [https://www.youtube.com/watch?v=GboiMJm6WlA](https://www.youtube.com/watch?v=GboiMJm6WlA)

Services typically need to call one another. In a monolithic application, services invoke one another through language-level method or procedure calls. In a traditional distributed system deployment, services run at fixed, well known locations (hosts and ports) and so can easily call one another using HTTP/REST or some RPC mechanism. However, a modern  [microservice-based](https://microservices.io/patterns/microservices.html)  application typically runs in a virtualized or containerized environments where the number of instances of a service and their locations changes dynamically.

![](https://microservices.io/i/servicediscovery/discovery-problem.jpg)

Consequently, you must implement a mechanism for that enables the clients of service to make requests to a dynamically changing set of ephemeral service instances.

How does the client of a service - the API gateway or another service - discover the location of a service instance?

-   Each instance of a service exposes a remote API such as HTTP/REST, or Thrift etc. at a particular location (host and port)
-   The number of services instances and their locations changes dynamically.
-   Virtual machines and containers are usually assigned dynamic IP addresses.
-   The number of services instances might vary dynamically. For example, an EC2 Autoscaling Group adjusts the number of instances based on load.

When making a request to a service, the client obtains the location of a service instance by querying a  [Service Registry](https://microservices.io/patterns/service-registry.html), which knows the locations of all service instances.

The following diagram shows the structure of this pattern.

![](https://microservices.io/i/servicediscovery/client-side-discovery.jpg)

This is typically handled by a  [Microservice chassis framework](https://microservices.io/patterns/microservice-chassis.html)

Client-side discovery has the following benefits:

-   Fewer moving parts and network hops compared to  [Server-side Discovery](https://microservices.io/patterns/server-side-discovery.html)

Client-side discovery also has the following drawbacks:

-   This pattern couples the client to the  [Service Registry](https://microservices.io/patterns/service-registry.html)
-   You need to implement client-side service discovery logic for each programming language/framework used by your application, e.g Java/Scala, JavaScript/NodeJS. For example,  [Netflix Prana](https://github.com/Netflix/Prana)  provides an HTTP proxy-based approach to service discovery for non-JVM clients.

## Server-side Service Discovery

When making a request to a service, the client makes a request via a router (a.k.a load balancer) that runs at a well known location. The router queries a  [service registry](https://microservices.io/patterns/service-registry.html), which might be built into the router, and forwards the request to an available service instance.

The following diagram shows the structure of this pattern.

![](https://microservices.io/i/servicediscovery/server-side-discovery.jpg)

An AWS Elastic Load Balancer (ELB) is an example of a server-side discovery router. A client makes HTTP(s) requests (or opens TCP connections) to the ELB, which load balances the traffic amongst a set of EC2 instances. An ELB can load balance either external traffic from the Internet or, when deployed in a VPC, load balance internal traffic. An ELB also functions as a  [Service Registry](https://microservices.io/patterns/service-registry.html). EC2 instances are registered with the ELB either explicitly via an API call or automatically as part of an auto-scaling group.

Some clustering solutions such as  [Kubernetes](https://github.com/GoogleCloudPlatform/kubernetes/blob/master/docs/services.md)  and  [Marathon](https://mesosphere.github.io/marathon/docs/service-discovery-load-balancing.html)  run a proxy on each host that functions as a server-side discovery router. In order to access a service, a client connects to the local proxy using the port assigned to that service. The proxy then forwards the request to a service instance running somewhere in the cluster.

Server-side service discovery has a number of benefits:

-   Compared to  [client-side discovery](https://microservices.io/patterns/client-side-discovery.html), the client code is simpler since it does not have to deal with discovery. Instead, a client simply makes a request to the router
-   Some cloud environments provide this functionality, e.g. AWS Elastic Load Balancer

It also has the following drawbacks:

-   Unless it’s part of the cloud environment, the router must is another system component that must be installed and configured. It will also need to be replicated for availability and capacity.
-   The router must support the necessary communication protocols (e.g HTTP, gRPC, Thrift, etc) unless it is TCP-based router
-   More network hops are required than when using  [Client Side Discovery](https://microservices.io/patterns/client-side-discovery.html)

## Service Registry

Implement a service registry, which is a database of services, their instances and their locations. Service instances are registered with the service registry on startup and deregistered on shutdown. Client of the service and/or routers query the service registry to find the available instances of a service. A service registry might invoke a service instance’s [health check API](https://microservices.io/patterns/observability/health-check-api.html) to verify that it is able to handle requests

examples of service registries (or technologies that are commonly used as service registries) include:

-   [Apache Zookeeper](http://zookeeper.apache.org/)
-   [Consul](https://consul.io/)
-   [Etcd](https://github.com/coreos/etcd)

Some systems such as Kubernetes, Marathon and AWS ELB have an implicit service registry.

The benefits of the Service Registry pattern include:

-   Client of the service and/or routers can discover the location of service instances.

There are also some drawbacks:

-   Unless the service registry is built in to the infrastructure, it is yet another infrastructure component that must be setup, configured and managed. Moreover, the service registry is a critical system component. Although clients should cache data provided by the service registry, if the service registry fails that data will eventually become out of date. Consequently, the service registry must be highly available.

You need to decide how service instances are registered with the service registry. There are two options:

-   [Self registration pattern](https://microservices.io/patterns/self-registration.html)  - service instances register themselves.
-   [3rd party registration pattern](https://microservices.io/patterns/3rd-party-registration.html)  - a 3rd party registers the service instances with the service registry.

The clients of the service registry need to know the location(s) of the service registry instances. Service registry instances must be deployed on fixed and well known IP addresses. Clients are configured with those IP addresses.

For example,  [Netflix Eureka](https://github.com/Netflix/eureka/wiki/Configuring-Eureka-in-AWS-Cloud)  service instances are typically deployed using elastic IP addresses. The available pool of Elastic IP addresses is configured using either a properties file or via DNS. When a Eureka instance starts up it consults the configuration to determine which available Elastic IP address to use. A Eureka client is also configured with the pool of Elastic IP addresses.

### Self registration

Service instances must be registered with the  [service registry](https://microservices.io/patterns/service-registry.html)  on startup so that they can be discovered and unregistered on shutdown.

How are service instances registered with and unregistered from the service registry?

-   Service instances must be registered with the service registry on startup and unregistered on shutdown
-   Service instances that crash must be unregistered from the service registry
-   Service instances that are running but incapable of handling requests must be unregistered from the service registry

#### Solution

A service instance is responsible for registering itself with the service registry. On startup the service instance registers itself (host and IP address) with the service registry and makes itself available for discovery. The client must typically periodically renew its registration so that the registry knows it is still alive. On shutdown, the service instance unregisters itself from the service registry.

The benefits of the Self Registration pattern include the following:

-   A service instance knows its own state so can implement a state model that’s more complex than UP/DOWN, e.g. STARTING, AVAILABLE, …

There are also some drawbacks:

-   Couples the service to the Service Registry
-   You must implement service registration logic in each programming language/framework that you use to write your services, e.g. NodeJS/JavaScript, Java/Scala, etc.
-   A service instance that is running yet unable to handle requests will often lack the self-awareness to unregister itself from the service registry

### 3rd party registration

A 3rd party registrar is responsible for registering and unregistering a service instance with the service registry. When the service instance starts up, the registrar registers the service instance with the service registry. When the service instance shuts downs, the registrar unregisters the service instance from the service registry.

Examples of the 3rd Party Registration pattern include:

-   [Netflix Prana](https://github.com/Netflix/Prana)  - a “side car” application that runs along side a non-JVM application and registers the application with Eureka.
-   [AWS Autoscaling Groups](http://aws.amazon.com/autoscaling/)  automatically (un)registers EC2 instances with Elastic Load Balancer
-   [Joyent’s Container buddy](https://github.com/joyent/containerbuddy)  runs in a Docker container as the parent process for the service and registers it with the registry
-   [Registrator](https://github.com/gliderlabs/registrator)  - registers and unregisters Docker containers with various service registries
-   Clustering frameworks such as  [Kubernetes](https://github.com/GoogleCloudPlatform/kubernetes/blob/master/docs/services.md)  and  [Marathon](https://mesosphere.github.io/marathon/docs/service-discovery-load-balancing.html)  (un)register service instances with the built-in/implicit registry

The benefits of the 3rd Party Registration pattern include:

-   The service code is less complex than when using the  [Self Registration pattern](https://microservices.io/patterns/self-registration.html)  since its not responsible for registering itself
-   The registrar can perform health checks on a service instance and register/unregister the instance based the health check

There are also some drawbacks:

-   The 3rd party registrar might only have superficial knowledge of the state of the service instance, e.g. RUNNING or NOT RUNNING and so might not know whether it can handle requests. However, as mentioned above some registrars such as Netflix Prana perform a health check in order to determine the availability of the service instance.
    
-   Unless the registrar is part of the infrastructure it’s another component that must be installed, configured and maintained. Also, since it’s a critical system component it needs to be highly available.

## Circuit Breaker

Ref: [https://www.youtube.com/watch?v=ADHcBxEXvFA](https://www.youtube.com/watch?v=ADHcBxEXvFA)

Services sometimes collaborate when handling requests. When one service synchronously invokes another there is always the possibility that the other service is unavailable or is exhibiting such high latency it is essentially unusable. Precious resources such as threads might be consumed in the caller while waiting for the other service to respond. This might lead to resource exhaustion, which would make the calling service unable to handle other requests. The failure of one service can potentially cascade to other services throughout the application.

How to prevent a network or service failure from cascading to other services?

A service client should invoke a remote service via a proxy that functions in a similar fashion to an electrical circuit breaker. When the number of consecutive failures crosses a threshold, the circuit breaker trips, and for the duration of a timeout period all attempts to invoke the remote service will fail immediately. After the timeout expires the circuit breaker allows a limited number of test requests to pass through. If those requests succeed the circuit breaker resumes normal operation. Otherwise, if there is a failure the timeout period begins again.

## Observability

**Log**: The application consists of multiple services and service instances that are running on multiple machines. Requests often span multiple service instances.

Each service instance generates writes information about what it is doing to a log file in a standardized format. The log file contains errors, warnings, information and debug messages.

Use a centralized logging service that aggregates logs from each service instance. The users can search and analyze the logs. They can configure alerts that are triggered when certain messages appear in the logs. (AWS Cloud Watch)

**Application Metrics** Instrument a service to gather statistics about individual operations. Aggregate metrics in centralized metrics service, which provides reporting and alerting. There are two models for aggregating metrics:

-   push - the service pushes metrics to the metrics service
-   pull - the metrics services pulls metrics from the service
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTQzNTUxNDIyMywxMzgxODkwOTcyLC00MT
I5MjQxOTgsLTEwNjEwNzE5NzMsLTUwNjIyNzU5LC0xMjE0MDQw
MjkwLDc5Mjg4OTIwLDg0MzUxMjk4MywtNzk5NjY1MTQzLC0xMT
E2NzM4NzE4LDk3NDMzNjMwMywxMzQxNjIyODYxLC0zNzk0Mjgw
MzIsMTQ2NTgyNDg0MywtMTIxMzY1OTY0Nyw4NTI2MzEzNDgsMT
M5MDAxOTAzNiwxMTY0ODkwMzA3LDUyODgwNjI1NSwtMTUxMDEw
MjE3M119
-->