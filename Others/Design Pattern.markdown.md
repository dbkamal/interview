## Creational patterns

In software engineering, creational design patterns are design patterns that deal with object creation mechanisms, trying to create objects in a manner suitable to the situation. The basic form of object creation could result in design problems or added complexity to the design. Creational design patterns solve this problem by somehow controlling this object creation.

### Factory Method

Ref: [https://refactoring.guru/design-patterns/factory-method](https://refactoring.guru/design-patterns/factory-method)

A Factory Pattern or Factory Method Pattern says that just define an interface or abstract class for creating an object but let the subclasses decide which class to instantiate. In other words, subclasses are responsible to create the instance of the class. The Factory Method Pattern is also known as Virtual Constructor.

A factory pattern is one of the core design principles to create an object, allowing clients to create objects of a library(explained below) in a way such that it doesn’t have tight coupling with the class hierarchy of the library.

**What is meant when we talk about library and clients?**

A library is something which is provided by some third party which exposes some public APIs and clients make calls to those public APIs to complete its task. A very simple example can be different kinds of Views provided by Android OS.

#### Discussion

Factory Method is to creating objects as Template Method is to implementing an algorithm. A superclass specifies all standard and generic behavior (using pure virtual "placeholders" for creation steps), and then delegates the creation details to subclasses that are supplied by the client.

Factory Method makes a design more customizable and only a little more complicated. Other design patterns require new classes, whereas Factory Method only requires a new operation.

People often use Factory Method as the standard way to create objects; but it isn't necessary if: the class that's instantiated never changes, or instantiation takes place in an operation that subclasses can easily override (such as an initialization operation).

Factory Method is similar to Abstract Factory but without the emphasis on families

- Advantage of Factory Design Pattern

Factory Method Pattern allows the sub-classes to choose the type of objects to create. It promotes the loose-coupling by eliminating the need to bind application-specific classes into the code. That means the code interacts solely with the resultant interface or abstract class, so that it will work with any classes that implement that interface or that extends that abstract class.

** Usage of Factory Design Pattern **

- When a class doesn't know what sub-classes will be required to create
- When a class wants that its sub-classes specify the objects to be created.
- When the parent classes choose the creation of objects to its sub-classes.


https://www.youtube.com/watch?v=pt1IbV1aSZ4

```java
/* library class & interface */
public interface Vehicle {
    void spec();
}

public class TwoWheelerImpl implements Vehicle {
    @Override
    public void spec() {
        System.out.println("Two wheeler");
    }
}

public class FourWheelerImpl implements Vehicle {
    @Override
    public void spec() {
        System.out.println("Four wheeler");
    }
}

/* create factory method to hide the object instantiation process and any subsequence change in library doesn't require client to recompile the code */

public class OptionalFactory {
    public Vehicle getInstance(String str) {
        if (str.equals("Two"))
            return new TwoWheelerImpl();
        else if (str.equals("Four"))
            return new FourWheelerImpl();
        else
            return null;
    }
}

/* client */

public class Client {
    public static void main(String[] args) {
        OptionalFactory optionalFactory = new OptionalFactory();
        Vehicle vehicle = optionalFactory.getInstance("Four");
        vehicle.spec();
    }
}
```

### Abstract Factory Pattern

Abstract Factory Pattern says that just define an interface or abstract class for creating families of related (or dependent) objects but without specifying their concrete sub-classes.That means Abstract Factory lets a class returns a factory of classes. So, this is the reason that Abstract Factory Pattern is one level higher than the Factory Pattern.

An Abstract Factory Pattern is also known as Kit.

### Strategy Pattern

Ref: [https://www.youtube.com/watch?v=-NCgRD9-C6o&list=PLF206E906175C7E07&index=3](https://www.youtube.com/watch?v=-NCgRD9-C6o&list=PLF206E906175C7E07&index=3)

**Strategy** is a behavioral design pattern that turns a set of behaviors into objects and makes them interchangeable inside original context object.

The original object, called context, holds a reference to a strategy object and delegates it executing the behavior. In order to change the way the context performs its work, other objects may replace the currently linked strategy object with another one.

In the following example, we have original class Animal (superclass) and it's corresponding subclasses. Now, we need to add one behavior but with minimal changes of the original context. Animal class references the interface called 'Fly'. 'Fly' has it's own implementation. So, subclass can dynamically change the instance variable by assigning different 'Fly' implementation. Even, if there is any change to the 'Fly' implementation there is no impact to the orginal context.

```java
package design.patterns;

public class StrategyPattern {
    public static void main(String[] args) {
        Animal animal;
        Dog dog = new Dog();
        Cat cat = new Cat();
        animal = cat;
        System.out.println(animal.getSound());
        animal.flying.fly();
    }
}

class Animal {
    private String name;
    private String sound;
    public Fly flying;

    public String getName() { return name; }

    public void setName(String name) {
        this.name = name;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }

    public void setFly(Fly flying) { this.flying = flying; }
}

class Dog extends Animal {

    Dog() {
        super();
        setSound("Bark");
        flying = new CantFly(); //can change dynamically
    }

    public void digHole() {
        System.out.println("Dog digs hole");
    }
}

class Cat extends Animal {

    Cat() {
        super();
        setSound("Meow");
        flying = new ItCanFly();
    }
}

interface Fly {
    void fly();
}

class ItCanFly implements Fly {
    @Override
    public void fly() {
        System.out.println("can fly");
    }
}

class CantFly implements Fly {
    @Override
    public void fly() {
        System.out.println("cant fly, can you help");
    }
}
```
**Payment Method in an e-Commerce Site**

Ref: [https://refactoring.guru/design-patterns/strategy/java/example#:~:targetText=Strategy%20in%20Java,delegates%20it%20executing%20the%20behavior.](https://refactoring.guru/design-patterns/strategy/java/example#:~:targetText=Strategy%20in%20Java,delegates%20it%20executing%20the%20behavior.)

### Observer Pattern

When you need many other objects to receive an update when another object changes. 

For example - stock market with thousands of stocks needs to send updates to objects representing individual stocks. The Subject (Publisher) sends many stocks to the Observers and the Observers(Subscribers) takes the ones they want and use them

**Loose coupling is a benefit**
- The Subject(Publisher) doesn't need to know anything about the Observer

```java
package design.patterns;

import java.util.ArrayList;

public class ObserverPattern {
    public static void main(String[] args) {
        Stock stock = new Stock();
        StockObserver observer = new StockObserver();
        stock.register(observer);
        stock.setAmazonPrice(140.00);
        stock.setGooglePrice(192.14);

        stock.unregister(observer);

        stock.setAmazonPrice(170.00);
    }
}

class Stock implements Subject {
    private ArrayList<Observer> observers;
    private double amazonPrice;
    private double googlePrice;

    Stock() {
        observers = new ArrayList<>();
    }

    @Override
    public void register(Observer o) {
        observers.add(o);
    }

    @Override
    public void unregister(Observer o) {
        int index = observers.indexOf(o);
        observers.remove(index);
    }

    @Override
    public void notifyObserver() {

        if (observers.isEmpty()) {
            System.out.println("No subscriber");
        }

        for (Observer o : observers) {
            o.update(amazonPrice, googlePrice);
        }
    }

    public void setAmazonPrice(double amazonPrice1) {
        this.amazonPrice = amazonPrice1;
        notifyObserver();
    }

    public void setGooglePrice(double googlePrice1) {
        this.googlePrice = googlePrice1;
        notifyObserver();
    }
}

class StockObserver implements Observer {

    private double amazonPrice;
    private double googlePrice;

    @Override
    public void update(double amazonPrice, double googlePrice) {
        this.amazonPrice = amazonPrice;
        this.googlePrice = googlePrice;
        this.printPrice();
    }

    public void printPrice() {
        System.out.println("Amazon " + amazonPrice + " Google " + googlePrice);
    }
}

interface Subject {
    void register(Observer o);
    void unregister(Observer o);
    void notifyObserver();
}

interface Observer {
    void update(double amazonPrice, double googlePrice);
}
```

Ref: [https://refactoring.guru/design-patterns/observer](https://refactoring.guru/design-patterns/observer)


<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE5OTg2MDIzMTcsMTY5NDU3NjgzMyw5Mz
A1ODk0NzEsMTQ5NjkyMTM2LDIwMzE1NjU0MTIsMTUzNjgxNDU0
MywtOTI1NTUzMjUsMTU2NDM5OTUwNiwtMjA4ODc0NjYxMl19
-->