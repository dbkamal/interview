## How to solve DP
- How to recognize a DP problem
- Identify problem variables like one variable for fibonacci
- prepare the recurrance relation
- identify the base case
- solve the problem recursively
- add memoization to reduce the costly calls

## Two Sum Problem
[https://leetcode.com/problems/two-sum/](https://leetcode.com/problems/two-sum/)

- below algo will work for sorted/unsorted, negative number as well
- store the complement of the array element if it is not available in the map. That is the element we didn't find earlier.

```java
class Solution {
    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<> ();
        int[] res = new int[2];
        
        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(nums[i])) {
                res[0] = map.get(nums[i]);
                res[1] = i;
                return res;
            }
            
            map.put(target - nums[i], i);
        }
        
        return res;
    }
}
```
Time & space complexity - O(n) ans O(n)
if we have sorted input then we can use two pointers technique to solve the problem rather than occupying any space.

## LRU Cache
[https://leetcode.com/problems/lru-cache/](https://leetcode.com/problems/lru-cache/)

The problem can be solved with a hashtable that keeps track of the keys and its values in the double linked list. One interesting property about double linked list is that the node can remove itself without other reference. In addition, it takes constant time to add and remove nodes from the head or tail.

One particularity about the double linked list that I implemented is that I create a pseudo head and tail to mark the boundary, so that we don't need to check the NULL node during the update. This makes the code more concise and clean, and also it is good for the performance.

[https://www.youtube.com/watch?v=S6IfqDXWa10](https://www.youtube.com/watch?v=S6IfqDXWa10)

Note: HashMap is an unsynchronized data structure, which means it's not thread-safe, but because it doesn't need to maintain thread safety, it has much less overhead, thus has better performance.

```java
class LRUCache {
    
    class Node {
        int key, val;
        Node prev, next;
        public Node(int key, int val) {
            this.key = key; this.val = val;
        }
    }
    
    Map<Integer, Node> map;
    Node head, tail;
    int size = 0, capacity = 0;

    public LRUCache(int capacity) {
        this.capacity = capacity;
        map = new HashMap<> ();

        /* dummy head and tail */
        head = new Node(-1, -1);
        tail = new Node(-1, -1);
        head.next = tail;
        tail.prev = head;
    }
    
    /* 
    if key already present remove the node 
    from the current position and insert into
    the head of the queue 
    */
    public int get(int key) {
        if (map.containsKey(key)) {
            Node node = map.get(key);
            deleteNode(node);
            moveToHead(node);
            return node.val;
        }
        return -1;
    }

    /*
    if the key already present update the value,
    and remove the node into head of the queue.
    otherwise, insert the node into the head of the
    queue and increment the size. if the size crosses
    the capacity then remove the node from the tail.
    */
    public void put(int key, int val) {
        if (map.containsKey(key)) {
            Node node = map.get(key);
            node.val = val;
            deleteNode(node);
            moveToHead(node);
        }
        else {
            Node node = new Node(key, val);
            map.put(key, node);
            size++;
            moveToHead(node);

            if (size > capacity) {
                map.remove(tail.prev.key);
                deleteNode(tail.prev);
                size--;
            }
        }
    }

    /*
    delete the node
    */
    public void deleteNode(Node node) {
        node.prev.next = node.next;
        node.next.prev = node.prev;
    }

    /*
    add to the queue
    */
    public void moveToHead(Node node) {
        node.next = head.next;
        node.prev = head;
        head.next.prev = node;
        head.next = node;
    }
}

/**
 * Your LRUCache object will be instantiated and called as such:
 * LRUCache obj = new LRUCache(capacity);
 * int param_1 = obj.get(key);
 * obj.put(key,value);
 */
```

** time & space** get() and put() operation runs in O(1) but the space is O(N) where N is the capacity

**Edge Case: can we use pointer rather than head and tail node?**

Further customization can be done using node reference rather than using the actual `head` and `tail` node which will save spaces.

```java
class LRUCache {
    
    class Node {
        int key, val;
        Node prev, next;

        public Node(int key, int val) {
            this.key = key; this.val = val;
        }

        /* remove the current node and move to the tail */
        private void update() {
            /* if the current node is the only node, no action require */
            if (tail == this)
                return;
            else {
                // remove from head
                if (head == this) {
                    head = this.next;
                }
                // remove from middle
                else {
                    this.prev.next = this.next;
                }

                this.next.prev = this.prev;
                // add to the tail
                this.append();
            }
        }

        /* add new node at the tail */
        private void append() {
            /* no node available in the queue*/
            if (tail == null) {
                head = this;
                tail = this;
            }
            else {
                this.next = null;
                this.prev = tail;
                tail.next = this;
                tail = this;
            }
        }

        /* remove the element from head */
        private void removeFromHead() {
            /* if 'this' is the only node, set both head and tail as null */
            if (tail == this) {
                head = null;
                tail = null;
            }
            else {
                head = this.next;
                head.prev = null;
            }
        }
    }
    
    Map<Integer, Node> map;
    Node head = null, tail = null;
    int size = 0, capacity = 0;

    public LRUCache(int capacity) {
        this.capacity = capacity;
        map = new HashMap<> ();
    }
    
    /* 
    if key already present remove the node 
    from the current position and insert into
    the head of the queue 
    */
    public int get(int key) {
        if (map.containsKey(key)) {
            Node node = map.get(key);
            node.update();
            return node.val;
        }
        return -1;
    }

    /*
    if the key already present update the value,
    and remove the node into head of the queue.
    otherwise, insert the node into the head of the
    queue and increment the size. if the size crosses
    the capacity then remove the node from the tail.
    */
    public void put(int key, int val) {
        if (map.containsKey(key)) {
            Node node = map.get(key);
            node.val = val;
            node.update();
        }
        else {
            Node node = new Node(key, val);
            map.put(key, node);
            size++;
            node.append();

            if (size > capacity) {
                map.remove(head.key);
                head.removeFromHead();
                size--;
            }
        }
    }
}

/**
 * Your LRUCache object will be instantiated and called as such:
 * LRUCache obj = new LRUCache(capacity);
 * int param_1 = obj.get(key);
 * obj.put(key,value);
 */
```

## Most Common Word
[https://leetcode.com/problems/most-common-word/](https://leetcode.com/problems/most-common-word/)

Explanation: [https://leetcode.com/articles/most-common-word/](https://leetcode.com/articles/most-common-word/)

```java
class Solution {
    public String mostCommonWord(String paragraph, String[] banned) {
        
        // store banned word list into set for constant lookup
        Set<String> set = new HashSet<>(Arrays.asList(banned));
        Map<String, Integer> map = new HashMap<>(); // store word vs frequency
        int maxOccurance = Integer.MIN_VALUE;
        String res = "";
        StringBuffer word = new StringBuffer();
        
        /* add extra delimiter add the end of the paragraph if not present */
        if (paragraph.charAt(paragraph.length() - 1) != '.')
            paragraph += '.';
        
        for (int i = 0; i < paragraph.length(); i++) {
            if (Character.isLetter(paragraph.charAt(i))) {
                word.append(Character.toLowerCase(paragraph.charAt(i)));
            }
            else if (word.length() > 0) {
                /* extract the word and reset the buffer */
                String searchWord = word.toString();
                word = new StringBuffer();

                /* if not banned word, increment the frequency */
                if (!set.contains(searchWord)) {
                    map.put(searchWord, map.getOrDefault(searchWord, 0) + 1);
                    if (maxOccurance < map.get(searchWord)) {
                        maxOccurance = map.get(searchWord);
                        res = searchWord;
                    }
                }
            }
        }

        return res;
    }
}
```

Time & Space: O(P + B) where P is the size of `paragraph` and B is the size of `banned`

Another way to solve the same problem however the overall runtime will increase due to the regex operation

```java
class Solution {
    public String mostCommonWord(String paragraph, String[] banned) {
        Set<String> set = new HashSet<> (Arrays.asList(banned));
        Map<String, Integer> map = new HashMap<> ();
        String res = "";
        int count = 0;

        /* A non-word character: [^\w] and */
        for (String s : paragraph.replaceAll("\\W+", " ").toLowerCase().split("\\s+")) {
            s = s.trim();
            if (s.length() == 0 || set.contains(s)) continue;
            map.put(s, map.getOrDefault(s, 0) + 1);
            if (count < map.get(s)) {
                count = map.get(s);
                res = s;
            }
        }

        return res;
    }
}
```

## Valid Parentheses
[https://leetcode.com/problems/valid-parentheses/](https://leetcode.com/problems/valid-parentheses/)

For any open braces push into stack and for any closing braces pop from stack if it's not empty. At the end check stack is empty or not.

```java
class Solution {
    public boolean isValid(String s) {
        Deque<Character> stack = new ArrayDeque<>();
        if (s == null || s.length() == 0) return true;

        /*
        for any open braces push into stack and for any closing braces
        pop from stack if it's not empty. At the end check stack is empty or not
        */
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (ch == '(' || ch == '{' || ch == '[')
                stack.addLast(ch);
            else if (ch == ')' || ch == '}' || ch == ']') {
                if (stack.isEmpty())
                    return false;
                char removeFromStack = stack.removeLast();
                if ((ch == ')' && removeFromStack != '(') || 
                    (ch == '}' && removeFromStack != '{') ||
                     (ch == ']' && removeFromStack != '['))
                    return false;
            }
        }

        return stack.isEmpty();
    }
}
```
Time & Space - O(N)

Below is another very clean code however Map takes significant spaces, so DO NOT use

```java
class Solution {
    
    static Map<Character, Character> map = new HashMap<> ();
    static {
        map.put(')', '(');
        map.put('}', '{');
        map.put(']', '[');
    }
    
    public boolean isValid(String s) {
        Deque<Character> stack = new ArrayDeque<>();
        if (s == null || s.length() == 0) return true;

        /*
        for any open braces push into stack and for any closing braces
        pop from stack if it's not empty. At the end check stack is empty or not
        */
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            
            if (map.containsKey(ch)) {
                System.out.println(stack.peek() + " " + map.get(ch));
                if (stack.isEmpty() || stack.removeLast() != map.get(ch))
                    return false;
            }
            else
                stack.addLast(ch);
        }

        return stack.isEmpty();
    }
}
```

**Best neat and clean code**
```java
class Solution {
    public boolean isValid(String s) {
        Deque<Character> stack = new ArrayDeque<>();
        if (s == null || s.length() == 0) return true;

        /*
        for any open braces push into stack and for any closing braces
        pop from stack if it's not empty. At the end check stack is empty or not
        */
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (ch == '(')
                stack.addLast(')');
            else if (ch == '{')
                stack.addLast('}');
            else if (ch == '[')
                stack.addLast(']');
            else {
                if (stack.isEmpty() || stack.removeLast() != ch)
                    return false;
            }
        }

        return stack.isEmpty();
    }
}
```

## Merge k Sorted Lists
[https://leetcode.com/problems/merge-k-sorted-lists/](https://leetcode.com/problems/merge-k-sorted-lists/)

Merge with Divide and Conquer approach [https://leetcode.com/articles/merge-k-sorted-list/](https://leetcode.com/articles/merge-k-sorted-list/)

```java
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public ListNode mergeKLists(ListNode[] lists) {
        if (lists == null || lists.length == 0)
            return null;
        
        int interval = 1;
        while (interval < lists.length) {
            for (int i = 0; i < lists.length - interval; i = i + interval * 2) {
                lists[i] = mergeTwoSortedLists(lists[i], lists[i + interval]);
            }
            interval *= 2;
        }
        return lists[0];
    }

    /* merge two sorted list using merge sort combine step */
    public ListNode mergeTwoSortedLists(ListNode l1, ListNode l2) {
        ListNode head = new ListNode(0);
        ListNode ptr = head;

        while (l1 != null && l2 != null) {
            ptr.next = new ListNode(l1.val >= l2.val ? l2.val : l1.val);
            ptr = ptr.next;

            if (l1.val >= l2.val)
                l2 = l2.next;
            else
                l1 = l1.next;
        }

        /* if l2 is exhausted copy rest of the l1 to the final list */
        while (l1 != null) {
            ptr.next = new ListNode(l1.val);
            ptr = ptr.next; l1 = l1.next;
        }

        /* if l1 is exhausted copy rest of the l2 to the final list */
        while (l2 != null) {
            ptr.next = new ListNode(l2.val);
            ptr = ptr.next; l2 = l2.next;
        }

        return head.next;
    }
}
```

Time ~ O(NlogK) where N - number of nodes and K - number of lists. Space ~ O(1)

## Word Ladder
[https://leetcode.com/problems/word-ladder/](https://leetcode.com/problems/word-ladder/)
Explanation: [https://leetcode.com/articles/word-ladder/](https://leetcode.com/articles/word-ladder/)

```java
class Solution {
    public int ladderLength(String beginWord, String endWord, List<String> wordList) {
        Set<String> visited = new HashSet<> ();
        Map<String, List<String>> map = new HashMap<> ();

        if (beginWord == null || endWord == null) return 0;

        /* transform the word list */
        for(String word : wordList) {
            for (String w : transformWord(word)) {
                List<String> li = map.getOrDefault(w, new LinkedList<>());
                li.add(word);
                map.put(w, li);
            }
        }

        Deque<String> Q = new ArrayDeque<> ();
        Q.addLast(beginWord);
        visited.add(beginWord);
        int count = 1;

        while (!Q.isEmpty()) {
            int size = Q.size();
            for (int i = 0; i < size; i++) {
                String str = Q.removeFirst();
                for (String s1 : transformWord(str)) {
                    for (String adjacentString : map.getOrDefault(s1, new LinkedList<> ())) {
                        if (adjacentString.equals(endWord))
                            return count + 1;
                        if (!visited.contains(adjacentString)) {
                            Q.addLast(adjacentString);
                            visited.add(adjacentString);
                        }
                    }
                }
            }
            count++;
        }
        return 0;
    }

    /* transform the word */
    public List<String> transformWord(String s) {
        List<String> res = new LinkedList<> ();
        for (int i = 0; i < s.length(); i++) {
            String str = s.substring(0, i) + "*" + s.substring(i + 1);
            res.add(str);
        }
        return res;
    }
}
```

Time & Space - O(M * N) where M - length of the words and N - total number of words in the word list.

Rewrite the above steps by using bi-directional BFS search from beginWord and endWord.

```java
import javafx.util.Pair;

class Solution {

    static Map<String, List<String>> map = new HashMap<> ();

    public int ladderLength(String beginWord, String endWord, List<String> wordList) {
        Map<String, Integer> visited_start = new HashMap<> ();
        Map<String, Integer> visited_end = new HashMap<> ();

        if (beginWord == null || endWord == null) return 0;
        
        if (!wordList.contains(endWord))
            return 0;

        /* transform the word list */
        for(String word : wordList) {
            for (String w : transformWord(word)) {
                List<String> li = map.getOrDefault(w, new LinkedList<>());
                li.add(word);
                map.put(w, li);
            }
        }

        // bi-directional BFS
        Deque<Pair<String, Integer>> Q_start = new ArrayDeque<> ();
        Deque<Pair<String, Integer>> Q_end = new ArrayDeque<> ();
        
        Q_start.addLast(new Pair(beginWord, 1)); Q_end.addLast(new Pair(endWord, 1));
        visited_start.put(beginWord, 1); visited_end.put(endWord, 1);

        /*
        start from the beginWord and endWord from bottom.
        traverse both bottom and up direction and as soon as one word found in another map stop the processing
        */
        while (!Q_start.isEmpty() && !Q_end.isEmpty()) {
            int count = visitWordNode(Q_start, visited_start, visited_end);
            if (count > -1)
                return count;

            count = visitWordNode(Q_end, visited_end, visited_start);
            if (count > -1)
                return count;
        }

        return 0;
    }

    public int visitWordNode(Deque<Pair<String, Integer>> Q, Map<String, Integer> started, Map<String, Integer> ended) {
        Pair<String, Integer> node = Q.removeFirst();
        String word = node.getKey();
        int level = node.getValue();

        for (String str : transformWord(word)) {
            for (String adjacentString : map.getOrDefault(str, new LinkedList<> ())) {
                // word found in another BFS
                if (ended.containsKey(adjacentString)) {
                    return level + ended.get(adjacentString);
                }

                if (!started.containsKey(adjacentString)) {
                    started.put(adjacentString, level + 1);
                    Q.addLast(new Pair(adjacentString, level + 1));
                }
            }
        }
        return -1;
    }

    /* transform the word */
    public List<String> transformWord(String s) {
        List<String> res = new LinkedList<> ();

        for (int i = 0; i < s.length(); i++) {
            String str = s.substring(0, i) + "*" + s.substring(i + 1);
            res.add(str);
        }

        return res;
    }
}
```

## Jump Game
[https://leetcode.com/problems/jump-game/](https://leetcode.com/problems/jump-game/)

- use simple backtracking

```java
class Solution {
    public boolean canJump(int[] nums) {
        return canJumpPos(0, nums);
    }

    public boolean canJumpPos(int pos, int[] nums) {
        if (pos == nums.length - 1) return true;

        int maxPosition = Math.min(pos + nums[pos], nums.length - 1);

        for (int i = maxPosition; i > pos; i--) {
            if (canJumpPos(i, nums))
                return true;
        }

        return false;
    }
}
```
Time O(2^n) and Space O(n) for recursion

**optimized code**

Idea is to work backwards from the last index. Keep track of the smallest index that can "jump" to the last index. Check whether the current index can jump to this smallest index.

[https://leetcode.com/articles/jump-game/](https://leetcode.com/articles/jump-game/)

```java
class Solution {
    public boolean canJump(int[] nums) {
        int n = nums.length;
        int last = n - 1;

        for (int i = n - 2; i >= 0; i--) {
            if (i + nums[i] >= last)
                last = i;
        }
        return last <= 0;
    }
}
```

## Minimum Window Substring
[https://leetcode.com/problems/minimum-window-substring/](https://leetcode.com/problems/minimum-window-substring/)

Explanation: [https://leetcode.com/articles/minimum-window-substring/](https://leetcode.com/articles/minimum-window-substring/)

- use two pointer technique. use hashmap to store the frequency of each character of T
- reduce the character frequency if any char match found in the S string
- if the frequency now equals to zero, then reduce the overall counter
- in case counter is zero (means all character of T found in S), increase the frequency of the char if found in S from start index 
- increase overall counter if the frequency is greater than zero
- take a global min to find the min window

```java
class Solution {
    public String minWindow(String s, String t) {
        if (s == null || s.length() == 0 || t == null || t.length() == 0 || s.length() < t.length())
            return "";

        Map<Character, Integer> map = new HashMap<> ();
        for (Character c : t.toCharArray()) map.put(c, map.getOrDefault(c, 0) + 1);

        int counter = map.size(), start = 0, end = 0, len = Integer.MAX_VALUE;
        String res = "";

        while (end < s.length()) {
            char ch = s.charAt(end);
            if (map.containsKey(ch)) {
                map.put(ch, map.get(ch) - 1);
                if (map.get(ch) == 0)
                    counter--;
            }
            end++;

            while (counter == 0) {
                char tch = s.charAt(start);
                if (map.containsKey(tch)) {
                    map.put(tch, map.get(tch) + 1);
                    if (map.get(tch) > 0)
                        counter++;
                }

                if (end - start < len) {
                    len = end - start;
                    res = s.substring(start, end);
                }
                start++;
            }
        }

        return res;
    }
}
```

Time ~ O(N + M) - N length of the S string, M length of the T string
Space ~ O(M)

constant space

```java
class Solution {
    public String minWindow(String s, String t) {
        int[] map = new int[256];
        for (char c : t.toCharArray()) map[c]++;

        int counter = t.length(), start = 0, end = 0, len = Integer.MAX_VALUE, head = 0;
        String res  = "";

        while (end < s.length()) {
            if (map[s.charAt(end++)]-- > 0) counter--;
            while (counter == 0) {
                if (end - start < len) {
                    len = end - start;
                    res = s.substring(start, end);
                }
                if (map[s.charAt(start++)]++ == 0) counter++;
            }
        }
        return res;
    }
}
```

## LFU Cache
[https://leetcode.com/problems/lfu-cache/](https://leetcode.com/problems/lfu-cache/)
[http://dhruvbird.com/lfu.pdf](http://dhruvbird.com/lfu.pdf)

```java
class LFUCache {

    private int capacity = 0, size = 0, minFrequency = 0;
    private Map<Integer, Node> map_node = new HashMap<> ();
    private Map<Integer, DLLNode> freq_list = new HashMap<> ();

    /*
    * @param capacity: total capacity of LFU Cache
    * @param size: current size of LFU cache
    * @param minFrequency: frequency of the last linked list (the minimum frequency of entire LFU cache)
    * @param map_node: a hash map that has key to Node mapping, which used for storing all nodes by their keys
    * @param freq_list: a hash map that has key to linked list mapping, which used for storing all
    * double linked list by their frequencies
    */
    public LFUCache(int capacity) {
        this.capacity = capacity;
    }
    
    /*
    if key does't exist, return -1
    otherwise increment the frequency of the node and remove the node from the current
    freq_list (doubly linked list) and place it the next frequency number
    */
    public int get(int key) {
        Node node = map_node.get(key);
        if (node == null) return -1;
        updateNode(node);
        return node.val;
    }
    
    /*
     * add new node into LFU cache, as well as double linked list
     * condition 1: if LFU cache has input key, update node value and node position in list
     * condition 2: if LFU cache does NOT have input key
     *  - sub condition 1: if LFU cache does NOT have enough space, remove the Least Recent Used node
     *  in minimum frequency list, then add new node
     *  - sub condition 2: if LFU cache has enough space, add new node directly
     */
    public void put(int key, int value) {
        if (capacity == 0) return;

        if (map_node.containsKey(key)) {
            Node node = map_node.get(key);
            node.val = value;
            map_node.put(key, node);
            updateNode(node);
        }
        else {
            size++;
            if (size > capacity) {
                // get the minimum frequency list
                DLLNode min_list = freq_list.get(minFrequency);
                Node deleteNode = min_list.removeTail();
                map_node.remove(deleteNode.key);
                size--;
            }
            minFrequency = 1;
            Node newNode = new Node(key, value);
            
            // get the list with frequency 1, and then add new node into the list, as well as into LFU cache
            DLLNode oldList = freq_list.getOrDefault(1, new DLLNode());
            oldList.addNode(newNode);
            freq_list.put(1, oldList);
            map_node.put(key, newNode);
        }
    }

    /*
    1. remove the node from the current frequency list
    2. increment the frequency of this node
    3. add the node on the latest frequency list based on node's frequency
    */
    public void updateNode (Node node) {
        DLLNode dllNode = freq_list.get(node.freq);
        dllNode.removeNode(node);

        // increment the minFrequency if linked list has no element for this frequency
        if (node.freq == minFrequency && dllNode.number_of_list == 0)
            minFrequency++;

        node.freq++;

        // add current node to another list has current frequency + 1,
        // if we do not have the list with this frequency, initialize it
        DLLNode newList = freq_list.getOrDefault(node.freq, new DLLNode());
        newList.addNode(node);
        freq_list.put(node.freq, newList);
    }

    /*
    * @param key: node key
    * @param val: node val
    * @param frequency: frequency count of current node
    * (all nodes connected in same double linked list has same frequency)
    * @param prev: previous pointer of current node
    * @param next: next pointer of current node
    * */
    static class Node {
        int key, val, freq;
        Node prev, next;
        public Node (int key, int val) {
            this.key = key; this.val = val; this.freq = 1;
        }
    }

    /*
    * @param number_of_list: current size of double linked list
    * @param head: head node of double linked list
    * @param tail: tail node of double linked list
    * */
    static class DLLNode {
        int number_of_list;
        Node head, tail;

        public DLLNode() {
            this.number_of_list = 0;
            this.head = new Node(0, 0);
            this.tail = new Node(0, 0);
            head.next = tail; tail.prev = head;
        }

        /*
        add the new node into the head of the doubly linked list and increase the list size
        */
        public void addNode(Node node) {
            node.next = head.next;
            node.prev = head;
            head.next.prev = node; head.next = node;
            this.number_of_list++;
        }

        /*
        remove input node and decrement the list size
        */
        public void removeNode(Node node) {
            node.next.prev = node.prev;
            node.prev.next = node.next;
            this.number_of_list--;
        }

        /*
        remove tail node from the list if it has more than zero element
        */
        public Node removeTail() {
            if (number_of_list > 0) {
                Node node = tail.prev;
                removeNode(node);
                return node;
            }
            return null;
        }
    }
}

/**
 * Your LFUCache object will be instantiated and called as such:
 * LFUCache obj = new LFUCache(capacity);
 * int param_1 = obj.get(key);
 * obj.put(key,value);
 */
```

Time ~ O(1) for get() and put() operation
Space ~ O(N) N is the capacity of the system

## Binary Tree Vertical Order Traversal
[https://leetcode.com/problems/binary-tree-vertical-order-traversal/](https://leetcode.com/problems/binary-tree-vertical-order-traversal/)

- use extra param @level for each node. for Root node level is zero and if it traverse to left child substract 1 and add 1 if it's traverse to the right child
- store @level as key in a TreeMap (which will store level as sorted order)
- use BFS to keep the ordering same

```java
class Solution {

    private Map<Integer, List<Integer>> map = new TreeMap<> ();

    public List<List<Integer>> verticalOrder(TreeNode root) {
        List<List<Integer>> res = new LinkedList<> ();
        if (root == null) return res;
        
        Deque<Pair<TreeNode, Integer>> Q = new ArrayDeque<> ();
        Q.addLast(new Pair<> (root, 0));

        while (!Q.isEmpty()) {
            int size = Q.size();
            for (int i = 0; i < size; i++) {
                Pair<TreeNode, Integer> pair = Q.removeFirst();
                TreeNode node = pair.getKey();
                int level = pair.getValue();

                List<Integer> li = map.getOrDefault(level, new LinkedList<> ());
                li.add(node.val);
                map.put(level, li);

                if (node.left != null) Q.addLast(new Pair<> (node.left, level - 1));
                if (node.right != null) Q.addLast(new Pair<> (node.right, level + 1));
            }
        }

        for (Integer i : map.keySet()) {
            res.add(new LinkedList<> (map.get(i)));
        }

        return res;
    }
}
```

Time & Space ~ O(N)

**Space utilized solution**
- calculate the level of each node starting 0 for root and left child `level - 1` and right child as `level + 1`
- store the min and max value of whole tree
- create list of list using (max - min) value
- BFS traversal starting root and level as `-min` 
- this way we can replace HashMap

```java
import javafx.util.Pair;

class Solution {

    private int min = 0, max = 0;

    public List<List<Integer>> verticalOrder(TreeNode root) {
        List<List<Integer>> res = new LinkedList<> ();
        if (root == null) return res;

        computeMinMax(root, 0);
        for (int i = min; i <= max; i++) res.add(new LinkedList<> ());
        
        Deque<Pair<TreeNode, Integer>> Q = new ArrayDeque<> ();
        Q.addLast(new Pair<> (root, -min)); // Important: always use -min to start off

        while (!Q.isEmpty()) {
            int size = Q.size();
            for (int i = 0; i < size; i++) {
                Pair<TreeNode, Integer> pair = Q.removeFirst();
                TreeNode node = pair.getKey();
                int level = pair.getValue();

                List<Integer> li = res.get(level);
                li.add(node.val);

                if (node.left != null) Q.addLast(new Pair<> (node.left, level - 1));
                if (node.right != null) Q.addLast(new Pair<> (node.right, level + 1));
            }
        }

        return res;
    }
	/* compute global min and max */
    public void computeMinMax(TreeNode node, int level) {
        if (node == null) return;

        min = Math.min(min, level);
        max = Math.max(max, level);
        computeMinMax(node.left, level - 1);
        computeMinMax(node.right, level + 1);
    }
}
```

Time & Space ~ O(N)

## The Skyline Problem
[https://leetcode.com/problems/the-skyline-problem/](https://leetcode.com/problems/the-skyline-problem/)

Explanation: [https://www.youtube.com/watch?v=GSBLe8cKu0s](https://www.youtube.com/watch?v=GSBLe8cKu0s)

- split the each building input into two halves i.e (x-cord, height) and (y-cord, height) where x-cord represents the building starting index and y-cord represent building ending index. save the split coordinates into a list
- to identify the building starting index, store the height as negative
- sort the list according to the height if there is same coordinates otherwise sorted based on X-cord
- for each list entry check if the height is negative. If yes, then it is the building starting point and add the height in a TreeMap (better than PriorityQueue as remove node would be constant). If it changes the previous max then add to the result
- for each building ending coordinates remove the entry from the TreeMap and if it changes the previous max then add the coordinates to the result

```java
class Solution {
    public List<List<Integer>> getSkyline(int[][] buildings) {
        List<List<Integer>> res = new LinkedList<> ();
        List<int[]> heights = new LinkedList<> ();

        for (int[] building : buildings) {
            heights.add(new int[] { building[0], -building[2] }); // building starting coords
            heights.add(new int[] { building[1], building[2] }); // building ending coords
        }

        Collections.sort(heights, (a, b) -> {
            if (a[0] == b[0])
                return a[1] - b[1];
            return a[0] - b[0];
        });

        TreeMap<Integer, Integer> map = new TreeMap<> ();
        map.put(0, 1); // default height 0 for occurance 1
        int prevMax = 0;

        for (int[] h : heights) {
            if (h[1] < 0) // add height for starting building
                map.put(-h[1], map.getOrDefault(-h[1], 0) + 1);
            else { // remove/decrement height for ending building
                if (map.get(h[1]) > 1)
                    map.put(h[1], map.get(h[1]) - 1);
                else
                    map.remove(h[1]);
            }

            int curMax = map.lastKey();

            /*
            compare current max height with previous max height, update result and previous max height if necessary
            */
            if (curMax != prevMax) {
                List<Integer> li = new LinkedList<> ();
                li.add(h[0]); li.add(curMax);
                res.add(new LinkedList<> (li));
                prevMax = curMax;
            }
        }

        return res;
    }
}
```

Time & Space ~ O(NlogN) and O(N)

## Pow(x, n)
[https://leetcode.com/problems/powx-n/](https://leetcode.com/problems/powx-n/)

- the naive solution is if n is positive iterate n times and multiple x with x. If n is negative, set x as `1/x` and n as `-n`

```java
class Solution {
    public double myPow(double x, int n) {
        if (n < 0) {
            x = 1 / x;
            n *= -1;
        }
        
        double res = 1;
        for (int i = 1; i <= n; i++)
            res *= x;
        return res;
    }
}
```
Time & Space ~ O(N), O(1)

Explanation [https://leetcode.com/articles/powx-n/](https://leetcode.com/articles/powx-n/)

**Fast Power approach**

```java
class Solution {
    public double myPow(double x, int n) {
        long N = n;
        if (N < 0) {
            x = 1 / x;
            N = - N;
        }

        return fastPower(x, N);
    }

    public double fastPower(double x, long n) {
        if (n == 0) return 1.0;

        double half = fastPower(x, n / 2);
        if (n % 2 == 0)
            return half * half;
        else
            return half * half * x;
    }
}
```
time & space - O(logN)

**space utilized version**
[https://en.wikipedia.org/wiki/Exponentiation_by_squaring](https://en.wikipedia.org/wiki/Exponentiation_by_squaring)
check the iterative approach.
```java
class Solution {
    public double myPow(double x, int n) {
        long N = n;
        if (N < 0) {
            x = 1 / x;
            N = -N;
        }
        double ans = 1;
        double current_product = x;
        for (long i = N; i > 0; i /= 2) {
            if ((i % 2) == 1) {
                ans = ans * current_product;
            }
            current_product = current_product * current_product;
        }
        return ans;
    }
};
```

## Max Area of Island
[https://leetcode.com/problems/max-area-of-island/](https://leetcode.com/problems/max-area-of-island/)

- use DFS and replace 1's with 0's
- for each adjacent DFS call increment the count
- use global max to find the max count

```java
class Solution {

    private int max = 0, count = 0;

    public int maxAreaOfIsland(int[][] grid) {
        if (grid == null || grid.length == 0) return 0;

        int R = grid.length, C = grid[0].length;

        for (int i = 0; i < R; i++) {
            for (int j = 0; j < C; j++) {
                if (grid[i][j] == 1) {
                    dfs(grid, i, j, R, C);
                    max = Math.max(max, count);
                    count = 0;
                }
            }
        }
        return max;
    }

    public void dfs(int[][] grid, int i, int j, int R, int C) {
        if (i < 0 || i >= R || j < 0 || j >= C || grid[i][j] == 0) return;

        count++;
        grid[i][j] = 0;
        
        dfs(grid, i - 1, j, R, C);
        dfs(grid, i + 1, j, R, C);
        dfs(grid, i, j - 1, R, C);
        dfs(grid, i, j + 1, R, C);
    }
}
```

Time & Space O(N * M)

## Number of Distinct Islands
[https://leetcode.com/problems/number-of-distinct-islands/](https://leetcode.com/problems/number-of-distinct-islands/)

Soln: https://leetcode.com/articles/number-of-distinct-islands/

- the idea is to think the starting position (i, j) as (0, 0). for example if we have (2,3) and (2,4) as a connected island then while processing store the (2,3) as (0,0) and (2,4) as (0,1)
- store the shapes in a hashset after converting the coordinates into integer `(r - r0) * 2 * grid[0].length + (c - c0)`
- return the size of Set shapes

```java
class Solution {

    private int[][] grid;
    private Set<Integer> shape;
    private boolean[][] visited;

    public int numDistinctIslands(int[][] grid) {
        if (grid == null || grid.length == 0) return 0;

        this.grid = grid;
        Set<Set<Integer>> unique_shapes = new HashSet<> ();
        visited = new boolean[grid.length][grid[0].length];

        for (int r = 0; r < grid.length; r++) {
            for (int c = 0; c < grid[0].length; c++) {
                shape = new HashSet<> ();
                explore(r, c, r, c);

                if (!shape.isEmpty())
                    unique_shapes.add(shape);
            }
        }

        return unique_shapes.size();
    }

    public void explore(int r, int c, int r0, int c0) {
        if (r >= 0 && r < grid.length && c >= 0 && c < grid[0].length && grid[r][c] == 1 && !visited[r][c]) {
            visited[r][c] = true;
            shape.add((r - r0) * 2 * grid[0].length + (c - c0)); // convert tuple into integers

            explore(r - 1, c, r0, c0);
            explore(r + 1, c, r0, c0);
            explore(r, c - 1, r0, c0);
            explore(r, c + 1, r0, c0);
        }
    }
}
```

time & space ~ O(N * M)

**optimized solution**

- for any dfs call save the path number into an List and add zero when the dfs call ends to differentiate between different shape

```java
class Solution {

    private int[][] grid;
    private List<Integer> shape;
    private boolean[][] visited;

    public int numDistinctIslands(int[][] grid) {
        if (grid == null || grid.length == 0) return 0;

        this.grid = grid;
        Set<List<Integer>> unique_shapes = new HashSet<> (); // store valid path references
        visited = new boolean[grid.length][grid[0].length];

        for (int r = 0; r < grid.length; r++) {
            for (int c = 0; c < grid[0].length; c++) {
                if (grid[r][c] == 1) {
                    shape = new ArrayList<> ();
                    explore(r, c, 0); // initial position as 0

                    if (!shape.isEmpty())
                        unique_shapes.add(shape);
                }
            }
        }

        return unique_shapes.size();
    }

    public void explore(int r, int c, int d) {
        if (r >= 0 && r < grid.length && c >= 0 && c < grid[0].length && grid[r][c] == 1 && !visited[r][c]) {
            visited[r][c] = true;
            shape.add(d);

            explore(r - 1, c, 1);
            explore(r + 1, c, 2);
            explore(r, c - 1, 3);
            explore(r, c + 1, 4);
            
            shape.add(0); // to mark the end of the DFS traversal
        }
    }
}
```

Time & Space ~ O(N * M)

**Reduce space payload**

```java
class Solution {
    private int[][] grid;
    public int numDistinctIslands(int[][] grid) {
        if (grid == null || grid.length == 0) return 0;
        this.grid = grid;
        Set<String> unique_shapes = new HashSet<> (); // store valid path references

        for (int r = 0; r < grid.length; r++) {
            for (int c = 0; c < grid[0].length; c++) {
                if (grid[r][c] == 1) {
                    StringBuffer sb = new StringBuffer();
                    explore(r, c, sb, "o"); // initial position as 0
                    grid[r][c] = 0; // visited the grid position
                    unique_shapes.add(sb.toString());
                }
            }
        }

        return unique_shapes.size();
    }

    public void explore(int r, int c, StringBuffer sb, String path) {
        if (r >= 0 && r < grid.length && c >= 0 && c < grid[0].length && grid[r][c] == 1) {
            grid[r][c] = 0;
            sb.append(path);

            explore(r - 1, c, sb, "u");
            explore(r + 1, c, sb, "d");
            explore(r, c - 1, sb, "l");
            explore(r, c + 1, sb, "r");
            
            sb.append("b"); // to mark the end of the DFS traversal
        }
    }
}
```

Time & Space ~ O(N * M)

## Number of Distinct Islands II
[https://leetcode.com/problems/number-of-distinct-islands-ii/](https://leetcode.com/problems/number-of-distinct-islands-ii/)

## Restore IP Addresses
[https://leetcode.com/problems/restore-ip-addresses/](https://leetcode.com/problems/restore-ip-addresses/)

- need four segment each with value range 1 to 255 no leading zero

```java
class Solution {
    public List<String> restoreIpAddresses(String s) {
        List<String> res = new LinkedList<> ();
        if (s == null || s.length() == 0) return res;
        
        StringBuffer sb = new StringBuffer();

        for (int a = 1; a <= 3; a++) {
            for (int b = 1; b <= 3; b++) {
                for (int c = 1; c <= 3; c++) {
                    int d = s.length() - a - b - c;

                    if (d > 0 && d <= 3 && a + b + c + d == s.length()) {
                        int seg_1 = Integer.parseInt(s.substring(0, a));
                        int seg_2 = Integer.parseInt(s.substring(a, a + b));
                        int seg_3 = Integer.parseInt(s.substring(a + b, a + b + c));
                        int seg_4 = Integer.parseInt(s.substring(a + b + c));

                        if (seg_1 <= 255 && seg_2 <= 255 && seg_3 <= 255 && seg_4 <= 255) {
                            sb.append(seg_1).append(".").append(seg_2).append(".").append(seg_3).
                                    append(".").append(seg_4);
                            if (sb.length() == s.length() + 3) // excludes the leading zero
                                res.add(sb.toString());
                        }
                        sb = new StringBuffer();
                    }
                }
            }
        }

        return res;
    }
}
```

Time & Space ~ 27 iteration overall and O(1) for space

## Reorder Data in Log Files
[https://leetcode.com/problems/reorder-data-in-log-files/](https://leetcode.com/problems/reorder-data-in-log-files/)

- letter log comes before digit log
- letter log ordered lexicographical
- in case if letter log ties -> identifier ordering
- digit log orginal order

```java
class Solution {
    public String[] reorderLogFiles(String[] logs) {
        Arrays.sort(logs, (a, b) -> {
            String[] w1 = a.split(" ", 2);
            String[] w2 = b.split(" ", 2);

            boolean isDigit1 = Character.isDigit(w1[1].charAt(0));
            boolean isDigit2 = Character.isDigit(w2[1].charAt(0));

            if (!isDigit1 && !isDigit2) {
                int cmp = w1[1].compareTo(w2[1]);
                if (cmp != 0) return cmp;

                return w1[0].compareTo(w2[0]);
            }

            return isDigit1 ? (isDigit2 ? 0 : 1) : -1;
        });

        return logs;
    }
}
```

Time & Space ~ O(NlogN), O(1)

**Optimized code**

- move all the digit logs to the write
- sort only the letter logs

```java
class Solution {
    public String[] reorderLogFiles(String[] logs) {
        // move all digit log in right side of the array
        int n = logs.length, i = n - 1, j = n - 1;

        while (i >= 0) {
            String str = logs[i];
            int indexAfterIdentifier = str.indexOf(' ');
            if (Character.isDigit(str.charAt(indexAfterIdentifier + 1))) {
                //swap i and j
                logs[i] = logs[j];
                logs[j] = str;
                i--; j--;
            }
            else { i--; }
        }

        Arrays.sort(logs, 0, j + 1, (a, b) -> {
            String[] w1 = a.split(" ", 2);
            String[] w2 = b.split(" ", 2);

            int cmp = w1[1].compareTo(w2[1]);
            if (cmp != 0) return cmp;
            return w1[0].compareTo(w2[0]);
        });

        return logs;
    }
}
```

Time & Space ~ O(KlogK + N - K), O(1) where K is the number of letter logs with N number of logs

**Better optimized code: NEED FURTHER ANALYSIS**

- create a empty array of the same size of the input array
- start iterating from end

```java
class Solution {
      public String[] reorderLogFiles(String[] logs) {

    int size = logs.length;
    String[] reorderedLogs = new String[size];
    int digitNextInsertIndex = size - 1;
    int alphaLastInsertIndex = -1;
    for (int i = size - 1; i >= 0; i--) {
      String log = logs[i];
      char firstNonIdentifierChar = log.charAt(log.indexOf(' ') + 1);
      if (firstNonIdentifierChar >= '0' && firstNonIdentifierChar <= '9') {
        reorderedLogs[digitNextInsertIndex--] = log;
      } else {
        for (int j = 0; j < size; j++) {
          if (reorderedLogs[j] == null) {
            reorderedLogs[j] = log;
            alphaLastInsertIndex++;
            break;
          } else if (isBefore(log, reorderedLogs[j])) {
            for (int k = alphaLastInsertIndex; k >= j; k--) {
              reorderedLogs[k+1] = reorderedLogs[k];
            }
            reorderedLogs[j] = log;
            alphaLastInsertIndex++;
            break;
          }
        }
      }
    }

    return reorderedLogs;
  }

  private boolean isBefore(String s1, String s2) {
    int s1fs = s1.indexOf(' ');
    int s2fs = s2.indexOf(' ');
    String s1_content = s1.substring(s1fs + 1);
    String s2_content = s2.substring(s2fs + 1);
    int comp = s1_content.compareTo(s2_content);
    if (comp != 0) {
      return comp < 0;
    }

    String s1Ident = s1.substring(0, s1fs);
    String s2Ident = s2.substring(0, s2fs);

    return s1Ident.compareTo(s2Ident) < 0;
  }
}
```

## Strongly Connected Component
[https://www.geeksforgeeks.org/strongly-connected-components/](https://www.geeksforgeeks.org/strongly-connected-components/)

A directed graph is strongly connected if there is a path between all pairs of vertices.  A strongly connected component (**SCC**) of a directed graph is a maximal strongly connected subgraph. For example, there are 3 SCCs in the following graph.

[![SCC](https://media.geeksforgeeks.org/wp-content/cdn-uploads/SCC.png)](https://media.geeksforgeeks.org/wp-content/cdn-uploads/SCC.png)

We can find all strongly connected components in O(V+E) time using  [Kosaraju’s algorithm](http://en.wikipedia.org/wiki/Kosaraju%27s_algorithm). Following is detailed Kosaraju’s algorithm.  
**1)**  Create an empty stack ‘S’ and do DFS traversal of a graph. In DFS traversal, after calling recursive DFS for adjacent vertices of a vertex, push the vertex to stack. In the above graph, if we start DFS from vertex 0, we get vertices in stack as 1, 2, 4, 3, 0.  
**2)**  Reverse directions of all arcs to obtain the transpose graph.  
**3)**  One by one pop a vertex from S while S is not empty. Let the popped vertex be ‘v’. Take v as source and do DFS (call  [DFSUtil(v)](https://www.geeksforgeeks.org/depth-first-traversal-for-a-graph/)). The DFS starting from v prints strongly connected component of v. In the above example, we process vertices in order 0, 3, 4, 2, 1 (One by one popped from stack).

```java
class SCC {
    static class Graph {
        int vertex;
        List<Integer>[] adjacencyList;
        boolean directed = false;

        public Graph(int vertex, boolean directed) {
            this.vertex = vertex;
            this.directed = directed;
            adjacencyList = new LinkedList[vertex];

            for (int i = 0; i < vertex; i++)
                adjacencyList[i] = new LinkedList<> ();
        }

        public void addVertex(int source, int dest) {
            if (directed) {
                adjacencyList[source].add(dest);
            }
            else {
                adjacencyList[source].add(dest);
                adjacencyList[dest].add(source);
            }
        }
    }

    /*
    Kosaraju's algorithm
    */
    public List<List<Integer>> getAllSCCs(Graph g) {
        Deque<Integer> stack = new ArrayDeque<> ();
        boolean[] visited = new visited[g.vertex];
        
        /* first DFS traversal for all nodes */
        for (int i = 0; i < g.vertex; i++) {
            if (!visited[i])
                dfsUtil(stack, g, visited, i);
        }
        
        /* reverse the directed edge */
        List<Integer>[] tempList = reverseEdge(g);

        /* pop nodes from the stack and do the dfs traversal */
        List<List<Integer>> res = new LinkedList<> ();
        visited = new visited[g.vertex];
        
        while (!stack.isEmpty()) {
            int vertex = stack.removeLast();
            List<Integer> list = new LinkedList<> ();

            if (!visited[vertex]) {
                findSCC(vertex, visited, tempList, list);
                res.add(new LinkedList<> (list));
            }
        }

        return res;
    }

    public void findSCC(int v, boolean[] visited, List<Integer>[] tempList, List<Integer> list) {
        visited[v] = true;
        for (int adjacentVertex : tempList[v]) {
            if (!visited[adjacentVertex])
                findSCC(adjacentVertex, visited, tempList, list);
        }
        list.add(v);
    }

    public void dfsUtil(Deque<Integer> stack, Graph g, boolean[] visited, int startVertex) {
        if (!visited[startVertex])
            visited[startVertex] = true;

        for (int adjacentVertex : g.adjacencyList[startVertex]) {
            if (!visited[adjacentVertex]) {
                dfsUtil(stack, g, visited, adjacentVertex);
            }
        }

        stack.addLast(startVertex);
    }

    public List<Integer>[] reverseEdge(Graph g) {
        List<Integer>[] tempList = new LinkedList[g.vertex];
        for (int i = 0; i < g.vertex; i++)
            tempList[i] = new LinkedList<> ();

        for (int i = 0; i < g.vertex; i++) {
            for (int dest : g.adjacencyList[i]) {
                tempList[dest].add(i);
            }
        }

        return tempList;
    }
}
```

## Articulation Point

[https://www.youtube.com/watch?v=jFZsDDB0-vo&t=253s](https://www.youtube.com/watch?v=jFZsDDB0-vo&t=253s)
[https://www.geeksforgeeks.org/articulation-points-or-cut-vertices-in-a-graph/](https://www.geeksforgeeks.org/articulation-points-or-cut-vertices-in-a-graph/)

Read the whole documents in the above link. one major coding example is [https://leetcode.com/problems/critical-connections-in-a-network/](https://leetcode.com/problems/critical-connections-in-a-network/)

## Critical Connections in a Network
[https://leetcode.com/problems/critical-connections-in-a-network/](https://leetcode.com/problems/critical-connections-in-a-network/)

- Use Articulation Point to find the critical connections in a network ([https://www.geeksforgeeks.org/articulation-points-or-cut-vertices-in-a-graph/](https://www.geeksforgeeks.org/articulation-points-or-cut-vertices-in-a-graph/)

```java
class Solution {

    private int time = 0;

    public List<List<Integer>> criticalConnections(int n, List<List<Integer>> connections) {
        List<List<Integer>> res = new LinkedList<> ();
        int[] disc = new int[n];
        int[] low = new int[n];
        List<Integer>[] adjList = new LinkedList[n];

        for (int i = 0; i < n; i++)
            adjList[i] = new LinkedList<> ();

        Arrays.fill(disc, -1);

        /* build the undirected graph */
        for (List<Integer> temp : connections) {
            int source = temp.get(0), dest = temp.get(1);
            adjList[source].add(dest);
            adjList[dest].add(source);
        }

        for (int i = 0; i < n; i++) {
            if (disc[i] == -1)
                dfs(i, disc, low, adjList, res, i);
        }

        return res;
    }

    public void dfs(int u, int[] disc, int[] low, List<Integer>[] adjList, List<List<Integer>> res, int preprocessor) {
        disc[u] = low[u] = ++time; // Initialize discovery time and low value

        for (int v : adjList[u]) {
            if (v == preprocessor)
                continue;
            else {
                if (disc[v] == -1) {
                    dfs(v, disc, low, adjList, res, u);
                    /* Check if the subtree rooted with v has a connection to one of the ancestors of u */
                    low[u] = Math.min(low[u], low[v]);
                    /* if u is not root and low value of one of its child is more than discovery value of u */
                    if (low[v] > disc[u])
                        res.add(Arrays.asList(u, v));
                }
                else
                    low[u] = Math.min(low[u], disc[v]); // Update low value of u for parent function calls
            }
        }
    }
}
```

Time & Space ~ O(V + E)

## Basic Calculator
[https://leetcode.com/problems/basic-calculator/](https://leetcode.com/problems/basic-calculator/)

- calculator evaluates from left to right but stack evaluates from right to left, so start index the character array from the near end
- if it finds a digit find out that if it a single digit character or multi digit like "123". process accordingly
- if encounders '(' open parenthesis, do the evaluations on the stack to calculate the sub-expression / main expresssion
- as soon as you process the sub-expression, add the result back to the stack. At the end if the stack is still not empty do the evaluations for the remaining values and return the result

```java
class Solution {
    public int calculate(String s) {
        int operand = 0, n = 0;
        Deque<Object> stack = new ArrayDeque<> ();
        
        for (int i = s.length() - 1; i >= 0; i--) {
            char ch = s.charAt(i);
            if (Character.isDigit(ch)) {
                // extract the multi digit character
                operand = (int) Math.pow(10, n) * (ch - '0') + operand;
                n += 1;
            }
            else if (ch != ' ') {
                // if we seen any digit previously
                if (n != 0) {
                    stack.addFirst(operand);
                    n = 0; operand = 0;
                }
                
                if (ch == '(') {
                    int res = expr(stack);
                    stack.removeFirst();
                    stack.addFirst(res);
                }
                else {
                    stack.addFirst(ch);
                }
            }
        }

        if (n != 0) stack.addFirst(operand);
        
        return expr(stack);
    }

    public int expr(Deque<Object> s) {
        int res = 0;

        if (!s.isEmpty()) res = (int) s.removeFirst();

        while (!s.isEmpty() && (char) s.peekFirst() != ')' ) {
            char sign = (char) s.removeFirst();

            if (sign == '+')
                res += (int) s.removeFirst();
            else if (sign == '-')
                res -= (int) s.removeFirst();
        }

        return res;
    }
}
```

Time & Space ~ O(N)

**optimized code**

check **approach 2** [https://leetcode.com/articles/basic-calculator/](https://leetcode.com/articles/basic-calculator/)

```java
class Solution {
    public int calculate(String s) {
        int operand = 0, res = 0, sign = 1;
        Deque<Integer> stack = new ArrayDeque<> ();
        
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);

            if (Character.isDigit(ch)) {
                operand =  10 * operand + (ch - '0');
            }
            else if (ch == '+') {
                res += operand * sign; sign = 1; operand = 0;
            }
            else if (ch == '-') {
                res += operand * sign; sign = -1; operand = 0;
            }
            else if (ch == '(') {
                stack.addFirst(res);
                stack.addFirst(sign);
                res = 0; sign = 1; operand = 0;
            }
            else if (ch == ')') {
                res += operand * sign;
                res *= stack.removeFirst();
                res += stack.removeFirst();
                operand = 0; sign = 1;
            }
        }

        return res + operand * sign;
    }
}
```

Time & Space ~ O(N)

## Basic Calculator II
[https://leetcode.com/problems/basic-calculator-ii/](https://leetcode.com/problems/basic-calculator-ii/)

- find all the digit and build the numeric operand for example "123"
- for plus and minus, use sign variable to evaluate the result by `res += operand * sign`  and set the sign appropriately
- but as per operator precedence, wheneven you encounter division or multiplication that should be the first preference.
- do the evaluation for division and multiplication and store the result on the operand for later processing

```java
class Solution {
    public int calculate(String s) {
        int operand = 0, sign = 1, res = 0, i = 0, temp = 0;

        while (i < s.length()) {
            char ch = s.charAt(i);

            // find all the digit and build the numeric operand
            if (Character.isDigit(ch)) {
                operand = 10 * operand + (ch - '0');
            }
            else if (ch == '+' || ch == '-') {
                res += operand * sign;
                operand = 0;
                sign = (ch == '+') ? 1 : -1;
            }
            else if (ch == '/' || ch == '*') {
                i += 1;
                
                // discard whitespaces
                while (i < s.length() && s.charAt(i) == ' ')
                    i++;
                
                // if '/' or '*' found, check the next numeric value
                while (i < s.length() && Character.isDigit(s.charAt(i))) {
                    temp = 10 * temp + (s.charAt(i) - '0');
                    i++;
                }
                operand = (ch == '/') ? operand / temp : operand * temp;
            }

            if (temp != 0) { temp = 0; }
            else { i++; }
        }

        return res + operand * sign;
    }
}
```

Time O(N) , Space O(1)

### Basic Calculator III
[https://leetcode.com/problems/basic-calculator-iii/](https://leetcode.com/problems/basic-calculator-iii/)

for detail approach check [https://leetcode.com/problems/basic-calculator-iii/discuss/113592/Development-of-a-generic-solution-for-the-series-of-the-calculator-problems](https://leetcode.com/problems/basic-calculator-iii/discuss/113592/Development-of-a-generic-solution-for-the-series-of-the-calculator-problems)

```java
class Solution {
    public int calculate(String s) {
        int operand_1 = 0, operand_2 = 1, sign_1 = 1, sign_2 = 1, i = 0;
        Deque<Integer> stack = new ArrayDeque<> ();

        while (i < s.length()) {
            char ch = s.charAt(i);
            if (Character.isDigit(ch)) {
                int num = 0;

                while (i < s.length() && Character.isDigit(s.charAt(i)))
                    num = 10 * num + (s.charAt(i++) - '0');

                i -= 1;
                operand_2 = (sign_2 == 1) ? operand_2 * num : operand_2 / num;
            }
            else if (ch == '(') {
                stack.addFirst(operand_1); stack.addFirst(operand_2);
                stack.addFirst(sign_1); stack.addFirst(sign_2);
                operand_1 = 0; operand_2 = 1; sign_1 = 1; sign_2 = 1;
            }
            else if (ch == ')') {
                int num = operand_1 + sign_1 * operand_2;
                sign_2 = stack.removeFirst(); sign_1 = stack.removeFirst();
                operand_2 = stack.removeFirst(); operand_1 = stack.removeFirst();

                operand_2 = (sign_2 == 1) ? operand_2 * num : operand_2 / num;
            }
            else if (ch == '*' || ch == '/')
                sign_2 = (ch == '*') ? 1 : -1;
            else if (ch == '+' || ch == '-') {
                /* handle negative integer */
                if (ch == '-' && (i == 0 || s.charAt(i - 1) == '(')) {
                    sign_1 = -1;
                    i++;
                    continue;
                }
                operand_1 = operand_1 + sign_1 * operand_2;
                sign_1 = (ch == '+') ? 1 : -1;
                operand_2 = 1; sign_2 = 1;
            }
            
            i++;
        }
        return operand_1 + sign_1 * operand_2;
    }
}
```

Time & Space ~ O(N)

## Longest Repeating Substring
[https://leetcode.com/problems/longest-repeating-substring/](https://leetcode.com/problems/longest-repeating-substring/)

[https://leetcode.com/articles/longest-repeating-substring/](https://leetcode.com/articles/longest-repeating-substring/)

**binary search + hashset of already seen strings**

- use binary search to find the length of the substring (L) within the whole string. The idea is start with left = 1 and right = `S.length()` and calculate the mid length L and check if you have duplicate substring of length L. if yes then set left = `L-1` to check the larger than L sized duplicate string. otherwise, reduce the L size by reducing right = `L-1`
- the next non-trivial task is to find the duplicate substring of size L. this code use hashset to store the substrings of size L and if you already seen this substring then return the starting index position of the substring

```java
class Solution {
    public int longestRepeatingSubstring(String S) {
        if (S == null || S.length() == 0) return 0;
        int n = S.length(), left = 1, right = n, L = 0;
        while (left <= right) {
            L = (left + right) / 2;
            if (searchSubstring(L, n, S) != -1) {
                left = L + 1;
            }
            else
                right = L - 1;
        }

        return left - 1;
    }

    public int searchSubstring(int len, int n, String S) {
        Set<String> set = new HashSet<> ();
        for (int i = 0; i < n - len + 1; i++) {
            String str = S.substring(i, i + len);
            if (set.contains(str))
                return i;
            set.add(str);
        }
        return -1;
    }
}
```
Time ~ O(NlogN), Space ~ O(N^2)

use rolling hash for optimization (check the article link)

```java
class Solution {
    public int longestRepeatingSubstring(String S) {
        if (S == null || S.length() == 0) return 0;
        int n = S.length(), left = 1, right = n, L = 0;

        int[] nums = new int[n];

        for (int i = 0; i < n; i++) {
            nums[i] = (int) S.charAt(i) - (int) 'a';
        }

        while (left <= right) {
            L = (left + right) / 2;
            if (searchSubstring(L, n, nums) != -1) {
                left = L + 1;
            }
            else
                right = L - 1;
        }

        return left - 1;
    }

    public int searchSubstring(int len, int n, int[] nums) {
        Set<Long> set = new HashSet<> ();
        int base = 26;
        long mod = (long) Math.pow(2, 24), h = 0;

        for (int i = 0; i < len; i++) {
            h = (h * base + nums[i]) % mod;
        }

        set.add(h);

        long aL = 1;
        for (int i = 0; i < len; i++) {
            aL = (base * aL) % mod;
        }

        for (int i = 1; i < n - len + 1; i++) {
            h = (h * base - nums[i - 1] * aL % mod + mod) % mod;
            h = (h + nums[i + len - 1]) % mod;

            if (set.contains(h))
                return 0;
            set.add(h);
        }
        return -1;
    }
}
```

Time ~ O(NlogN), Space ~ O(N)

## Longest Duplicate Substring
[https://leetcode.com/problems/longest-duplicate-substring/](https://leetcode.com/problems/longest-duplicate-substring/)

Same logic Binary Search + Rabin Karp (Rolling Hash) [https://leetcode.com/articles/longest-duplicate-substring/](https://leetcode.com/articles/longest-duplicate-substring/)

```java
class Solution {

    private int start = 0, end = 0, max = Integer.MIN_VALUE;

    public String longestDupSubstring(String S) {
        if (S == null || S.length() == 0) return "";
        int n = S.length(), left = 1, right = n, L = 0;

        int[] nums = new int[n];

        for (int i = 0; i < n; i++) {
            nums[i] = (int) S.charAt(i) - (int) 'a';
        }

        while (left <= right) {
            L = (left + right) / 2;
            if (searchSubstring(L, n, nums) != -1) {
                left = L + 1;
            }
            else
                right = L - 1;
        }

        return S.substring(start, end);
    }

    public int searchSubstring(int len, int n, int[] nums) {
        Set<Long> set = new HashSet<> ();
        int base = 26;
        long mod = (long) Math.pow(2, 32), h = 0;

        for (int i = 0; i < len; i++) {
            h = (h * base + nums[i]) % mod;
        }

        set.add(h);

        long aL = 1;
        for (int i = 0; i < len; i++) {
            aL = (base * aL) % mod;
        }

        for (int i = 1; i < n - len + 1; i++) {
            h = (h * base - nums[i - 1] * aL % mod + mod) % mod;
            h = (h + nums[i + len - 1]) % mod;

            if (set.contains(h)) {
                if (max < len) {
                    start = i; end = i + len;
                    max = len;
                }
                return 0;
            }
            set.add(h);
        }
        return -1;
    }
}
```

Time ~ O(NlogN), Space O(N)

**Suffix Array**
[https://leetcode.com/problems/longest-duplicate-substring/discuss/290852/Suffix-array-clear-solution](https://leetcode.com/problems/longest-duplicate-substring/discuss/290852/Suffix-array-clear-solution)

## Minimum Cost to Connect Sticks
[https://leetcode.com/problems/minimum-cost-to-connect-sticks/](https://leetcode.com/problems/minimum-cost-to-connect-sticks/)

- use min heap to store all the number and extract two minimum number of the heap
- add them and increment the global cost and add the value back to the heap until its size becomes one

```java
class Solution {
    public int connectSticks(int[] sticks) {
        PriorityQueue<Integer> pq = new PriorityQueue<> ();
        int cost = 0;
        if (sticks == null || sticks.lenght == 0) return cost;

        for (int i = 0; i < sticks.lenght; i++)
            pq.add(sticks[i]);

        while (pq.size() > 1) {
            int a = pq.poll(), b = pq.poll();
            cost += a + b;
            pq.add(a + b);
        }

        return cost;
    }
}
```
Time ~ O(NlogN), space ~ O(N)

## Prime Palindrome
[https://leetcode.com/problems/prime-palindrome/](https://leetcode.com/problems/prime-palindrome/)

- palimdrome number is for example 12321 and we need to find a palindrome number first and then check whether it is prime or not
- if the palimdrome of length `d` digits, then the palimdrome root is of length `(d + 1) / 2`. For example, if the palimdrome of length 5 digits i.e 12321 then the root is 123. Conversely, if you know the root then you can build odd length and even length palimdrome. Root -> 123, create odd length palimdrome 12321 and even length palimdrome 123321. But first check the odd length as it is the smaller than the even length palimdrome
- once you have the palimdrome number check whether it is larger than the given number and is prime or not

```java
class Solution {
    public int primePalindrome(int N) {
	    // for len = 5, the palimdrome can be build of length 11 digits
        for (int len = 1; len <= 5; len++) {
            //find the odd length palimdrome
            for(int root = (int) Math.pow(10, len - 1); root < (int) Math.pow(10, len); root++) {
                StringBuffer sb = new StringBuffer(Integer.toString(root));
                //add the reverse digit to the root to prepare the palimdrome
                //if the root is 123 then the palimdrome is 12321
                for (int k = len - 2; k >= 0; k--) {
                    sb.append(sb.charAt(k));
                }

                int x = Integer.parseInt(sb.toString());

                if (x >= N && isPrime(x))
                    return x;
            }

            //find the even length palimdrome
            for(int root = (int) Math.pow(10, len - 1); root < (int) Math.pow(10, len); root++) {
                StringBuffer sb = new StringBuffer(Integer.toString(root));
                //add the reverse digit to the root to prepare the palimdrome
                //if the root is 123 then the palimdrome is 123321
                for (int k = len - 1; k >= 0; k--) {
                    sb.append(sb.charAt(k));
                }

                int x = Integer.parseInt(sb.toString());

                if (x >= N && isPrime(x))
                    return x;
            }
        }

        return -1;
    }

    public boolean isPrime(int n) {
        if (n < 2) return false;

        int sqrt = (int) Math.sqrt(n);

        for (int d = 2; d <= sqrt; d++) {
            if (n % d == 0) return false;
        }
        return true;
    }
}
```

Time ~ O(N), Space ~ O(log N) taken by the `StringBuffer`

**Optimization**

[https://leetcode.com/problems/prime-palindrome/discuss/146798/All-Even-Digits-Palindrome-are-Divisible-by-11](https://leetcode.com/problems/prime-palindrome/discuss/146798/All-Even-Digits-Palindrome-are-Divisible-by-11)

All palindrome with even digits is multiple of  `11`.  
So among them, 11 is the only one prime  
`if (8 <= N <= 11) return 11`

For other cases,  **we consider only palindrome with odd dights.**

```java
class Solution {
    public int primePalindrome(int N) {
        
        if (N >= 8 && N <= 11) return 11;
        
        for (int len = 1; len <= 5; len++) {
            //find the odd length palimdrome
            for(int root = (int) Math.pow(10, len - 1); root < (int) Math.pow(10, len); root++) {
                StringBuffer sb = new StringBuffer(Integer.toString(root));
                //add the reverse digit to the root to prepare the palimdrome
                //if the root is 123 then the palimdrome is 12321
                for (int k = len - 2; k >= 0; k--) {
                    sb.append(sb.charAt(k));
                }

                int x = Integer.parseInt(sb.toString());

                if (x >= N && isPrime(x))
                    return x;
            }

        return -1;
    }

    public boolean isPrime(int n) {
        if (n < 2) return false;

        int sqrt = (int) Math.sqrt(n);

        for (int d = 2; d <= sqrt; d++) {
            if (n % d == 0) return false;
        }
        return true;
    }
}
```

Time ~ O(N), Space ~ O(log N) taken by the `StringBuffer`

More cleaner code

```java
class Solution {
    public int primePalindrome(int N) {
        
        if (N >= 8 && N <= 11) return 11;
        
        for (int x = 1; x < 100000; x++) {
            String s = Integer.toString(x);
            String rev = new StringBuffer(s).reverse().toString().substring(1);
            int y = Integer.parseInt(s + rev);
            if (y >= N && isPrime(y)) return y;
        }

        return -1;
    }

    public boolean isPrime(int n) {
        if (n < 2) return false;

        int sqrt = (int) Math.sqrt(n);

        for (int d = 2; d <= sqrt; d++) {
            if (n % d == 0) return false;
        }
        return true;
    }
}
```

## Longest Palindrome
[https://leetcode.com/problems/longest-palindrome/](https://leetcode.com/problems/longest-palindrome/)

[https://leetcode.com/articles/longest-palindrome/](https://leetcode.com/articles/longest-palindrome/)

- to build a palimdrome, if you have even length character like 'cccc', then it can be use left and right of the palindrome like 'ccacc'. If you have odd length character like 'ccc', then we can utilize the even length portion building the palindrome like 'cdc'.
- so always add the number of character divide by 2 and multiply by 2 to get the round integer value
- At the end, if there was any `num_char_len % 2 == 1`, then that letter could have been a unique center like 'ccdcc'

```java
class Solution {
    public int longestPalindrome(String s) {
        int[] map = new int[128];
        if (s == null || s.length() == 0) return 0;

        for (char ch : s.toCharArray())
            map[ch]++;

        int res = 0;
        for (int num : map) {
            res += num / 2 * 2;
            if (res % 2 == 0 && num % 2 == 1)
                res++;
        }

        return res;
    }
}
```

Time & Space ~ O(N), O(1)

## 132 Pattern
[https://leetcode.com/problems/132-pattern/](https://leetcode.com/problems/132-pattern/)

**naive method**

- iterate the array element and use three pointers to check the 132 pattern

```java
class Solution {
    public boolean find132pattern(int[] nums) {
        for (int i = 0; i < nums.length - 3 + 1; i++) {
            for (int j = i + 1; j < nums.length - 1; j++) {
                for (int k = j + 1; k < nums.length; k++) {
                    if (nums[k] < nums[j] && nums[k] > nums[i])
                        return true;
                }
            }
        }
        return false;
    }
}
```
Time O(N^3) , Space O(1)

[https://leetcode.com/articles/132-pattern/](https://leetcode.com/articles/132-pattern/)

- first find the A[i], A[j] pair where `A[i] < A[j]`, so we could use use a variable to hold the minimum number for A[j]
- if we find such pair, then search the K indexed number by iterating within the range of `j+1` to `A.length` and try to satisfy the condition `nums[k] > min_i && nums[j] > nums[k]`

```java
class Solution {
    public boolean find132pattern(int[] nums) {
        int min_i = Integer.MAX_VALUE;
        for (int j = 0; j < nums.length - 1; j++) {
            min_i = Math.min(min_i, nums[j]);

            for (int k = j + 1; k < nums.length; k++) {
                if (nums[k] > min_i && nums[j] > nums[k])
                    return true;
            }
        }
        return false;
    }
}
```
Time ~ O(N^2), Space O(1)

**better optimized code using stack**

- based on the above approach, if we know the min value of num[j] than we have `num[i] < num[j]`. So, preprocess the nums array and build a min array by finding the min between `min[i-1] and nums[i]`
- if the top of the stack is larger than min[j], then the condition doesn't meet so pop the element from the stack
- if the top of the stack is smaller than num[j], then the condition `nums[k] < nums[j]` meet, otherwise push the nums[j] elemeent on the stack

```java
class Solution {
    public boolean find132pattern(int[] nums) {
        Deque<Integer> stack = new ArrayDeque<> ();
        if (nums.length < 3) return false;

        int[] min = new int[nums.length];
        min[0] = nums[0];

        //build a min table
        for (int i = 1; i < nums.length; i++)
            min[i] = Math.min(min[i - 1], nums[i]);

        for (int j = nums.length - 1; j >= 0; j--) {
            if (nums[j] >= min[j]) {
                while (!stack.isEmpty() && stack.peekFirst() <= min[j]) // nums[i] >= nums[k]
                    stack.removeFirst();
                if (!stack.isEmpty() && stack.peekFirst() < nums[j]) // nums[k] < nums[j]
                    return true;
                stack.addFirst(nums[j]);
            }
        }

        return false;
    }
}
``` 
Time & Space ~ O(N)

**Follow up: return how many number of patterns there are if possible**

```java
// "static void main" must be defined in a public class.
public class Main {
    public static void main(String[] args) {
        int[] a = {3,1,4,2,7,5};
        int n = a.length;
        int[] min = new int[n]; min[0] = a[0];
        for (int j = 1; j < n; j++) {
            min[j] = Math.min(min[j - 1], a[j]);
        }

        Deque<Integer> stack = new ArrayDeque<>();
        List<int[]> list = new LinkedList<> ();

        for (int j = n - 1; j >= 0; j--) {
            if (a[j] >= min[j]) {
                while (!stack.isEmpty() && stack.peek() <= min[j]) stack.pop();
                if (!stack.isEmpty() && stack.peek() < a[j]) {
                    list.add(new int[] { min[j], stack.peek(), a[j] });
                }
                stack.push(a[j]);
            }
        }
        System.out.println(list.size());
        
        for (int[] pos : list) {
            System.out.println(Arrays.toString(pos));
        }
    }
}
```
Time & Space ~ O(n)

## Populating Next Right Pointers in Each Node
[https://leetcode.com/problems/populating-next-right-pointers-in-each-node/](https://leetcode.com/problems/populating-next-right-pointers-in-each-node/)

- use BFS traversal and in each level traversal link to another node via next pointer for example in the 3rd level of the tree if we have 4, 5, 6, 7 nodes they can connect using the next pointer like 4 -> 5 -> 6 -> 7

```java
class Solution {
    public Node connect(Node root) {
        if (root == null) return root;

        Deque<Node> Q = new ArrayDeque<> ();
        Q.addLast(root);

        while (!Q.isEmpty()) {
            int size = Q.size();
            Node connector = null;
            for (int i = 0; i < size; i++) {
                Node node = Q.removeFirst();

                if (node.left != null) Q.addLast(node.left);
                if (node.right != null) Q.addLast(node.right);

                if (connector != null) {
                    connector.next = node;
                }
                connector = node;
            }
        }

        return root;
    }
}
```

Time & Space O(N)

**optimize code**

- check if the current root has both left and right child then connect via next pointer `cur.left.next = cur.right`
- if the current root has right child and next pointer points to the next node, then connect `cur.right.next = cur.next.left;`
- to go the next node of the same level use `cur = cur.next` and to go down the level use `level_node = level_node.left`

```java
class Solution {
    public Node connect(Node root) {
        if (root == null) return root;

        Node level_node = root;

        while (level_node != null) {
            Node cur = level_node;
            while (cur != null) {
                if (cur.left != null && cur.right != null)
                    cur.left.next = cur.right;
                if (cur.right != null && cur.next != null)
                    cur.right.next = cur.next.left;

                cur = cur.next; // move to the next node of the level traversal
            }
            level_node = level_node.left; // go to the next level of the tree;
        }
        return root;
    }
}
```

Time ~ O(N), Space O(1)

## 4Sum
[https://leetcode.com/problems/4sum/](https://leetcode.com/problems/4sum/)

- use two sum technique for sorted array
- first sort the array and use two pointer one starting from `i = 0 to N - 3` and another from `j = i+1 to N - 2`
- find the compliment of the target `target - nums[i] - nums[j]` and search this value within the range of index `j+1 to N-1`
- as the array is sorted we can use two pointers technique to find if there is any matching sum
- to avoid any duplicate entry check the previous element and current element and execute the loop if there are not equals

```java
class Solution {
    public List<List<Integer>> fourSum(int[] nums, int target) {
        List<List<Integer>> res = new ArrayList<> ();
        int n = nums.length;
        if (n < 4) return res;

        Arrays.sort(nums);

        for (int i = 0; i < n - 3; i++) {
            if (i > 0 && nums[i] == nums[i - 1]) continue;

            for (int j = i + 1; j < n - 2; j++) {

                if (j > i + 1 && nums[j] == nums[j - 1]) continue;

                int searchVal = target - nums[i] - nums[j];
                int minVal = nums[j + 1] + nums[j + 2];
                int maxVal = nums[n - 1] + nums[n - 2];

                if (minVal > searchVal || searchVal > maxVal) continue; //outside of the range

                int l = j + 1, r = n - 1;
                while (l < r) {
                    int sum = nums[l] + nums[r];
                    
                    if (sum == searchVal) {
                        res.add(new ArrayList<> (Arrays.asList(nums[i], nums[j], nums[l], nums[r])));
                        /* avoid adding same number twice */
                        while (l < r && nums[l] == nums[l + 1])
                            l++;
                        l++;
                        while (l < r && nums[r] == nums[r - 1])
                            r--;
                        r--;
                    }
                    else if (sum < searchVal) {
                        /* avoid adding same number twice */
                        while (l < r && nums[l] == nums[l + 1])
                            l++;
                        l++;
                    }
                    else {
                        /* avoid adding same number twice */
                        while (l < r && nums[r] == nums[r - 1])
                            r--;
                        r--;
                    }
                }
            }
        }
        return res;
    }
}
```

Time ~ O(N^3), Space ~ O(1)

## Design Tic-Tac-Toe
[https://leetcode.com/problems/design-tic-tac-toe/](https://leetcode.com/problems/design-tic-tac-toe/)

- the idea is to maintain each row, column, diagonal or anti-diagonal to check whether all are filled with either player 1 or player 2
- for player one increment the value by +1 and for player two decrement the value by -1
- for one single move, based on the row and col number update the array element by adding or substracting 1. Similarly do this for the variable diagonal and anti-diagonal
- once the value of any of the above data stucture is equals to the board size, we have a winner. based on th positive or negative value which can identify the winner

```java
class TicTacToe {

    /** Initialize your data structure here. */
    private int[] rows; 
    private int[] cols; 
    private int diag = 0, anti_diag = 0;
    private int n;
    
    public TicTacToe(int n) {
        this.n = n;
        rows = new int[n];
        cols = new int[n];
    }
    
    /** Player {player} makes a move at ({row}, {col}).
        @param row The row of the board.
        @param col The column of the board.
        @param player The player, can be either 1 or 2.
        @return The current winning condition, can be either:
                0: No one wins.
                1: Player 1 wins.
                2: Player 2 wins. */
    public int move(int row, int col, int player) {
        int player_val = (player == 1) ? 1 : -1;
        rows[row] += player_val;
        cols[col] += player_val;

        if (rows[row] == n || cols[col] == n) return player;
        if (rows[row] == -n || cols[col] == -n) return player;

        if (row == col)
            diag += player_val;

        if(row + col == rows.length - 1)
            anti_diag += player_val;

        if (diag == n || anti_diag == n) return player;
        if (diag == -n || anti_diag == -n) return player;
        return 0;
    }
}

/**
 * Your TicTacToe object will be instantiated and called as such:
 * TicTacToe obj = new TicTacToe(n);
 * int param_1 = obj.move(row,col,player);
 */
```

Time ~ move() operation is O(1) and space ~ O(N)

## Kth Largest Element in an Array
[https://leetcode.com/problems/kth-largest-element-in-an-array/](https://leetcode.com/problems/kth-largest-element-in-an-array/)

- use quick select to find the Kth largest that is `N - K` smallest element

```java
class Solution {
    public int findKthLargest(int[] nums, int k) {
        int n = nums.length, start = 0, end = n - 1;

        while (start <= end) {
            if (start == end) return nums[start];

            int pivotIndex = partition(nums, start, end);
            if (pivotIndex == n - k) return nums[pivotIndex];
            else if (pivotIndex < n - k) start = pivotIndex + 1;
            else end = pivotIndex - 1;
        }
        return 0;
    }
    
    public int partition(int[] nums, int l, int r) {
        int pivot = nums[l];
        while (l < r) {
            while(l < r && nums[r] >= pivot) r--;
            nums[l] = nums[r];
            while (l < r && nums[l] < pivot) l++;
            nums[r] = nums[l];
        }
        nums[l] = pivot;
        return l;
    }
}
```
Time ~ O(N) avg. case but in worst case O(N^2) and space O(1)

## Kth Largest Element in a Stream
[https://leetcode.com/problems/kth-largest-element-in-a-stream/](https://leetcode.com/problems/kth-largest-element-in-a-stream/)

- use min heap i.e priority queue to store all the array element and remove from the head of the queue i.e. smaller value until the queue size becomes K
- when you add a new number, check the queue size
- if the queue size is smaller than K, add the number otherwise, if the top of the queue is smaller than the new number, remove the top of the queue and add new num
- return the top of the queue which always maintains the Kth largest element

```java
class KthLargest {

    private PriorityQueue<Integer> pq;
    private int k;

    public KthLargest(int k, int[] nums) {
        pq = new PriorityQueue<> (k);
        this.k = k;

        for (int n : nums) {
            pq.offer(n);
            if (!pq.isEmpty() && pq.size() > k)
                pq.poll();
        }
    }
    
    public int add(int val) {
        // if the queue size is smaller than K then add the num
        if (pq.size() < k)
            pq.offer(val);
        // check whether the num is larger than the head of the queue
        else if (pq.peek() < val) {
            pq.poll();
            pq.offer(val);
        }
        return pq.peek();
    }
}

/**
 * Your KthLargest object will be instantiated and called as such:
 * KthLargest obj = new KthLargest(k, nums);
 * int param_1 = obj.add(val);
 */
```

Time ~ building the class constructor will take O(NlogN) where logN is the maintain the min heap and add() mothod runs in O(logN). Space O(K)

## K-Similar Strings
[https://leetcode.com/problems/k-similar-strings/](https://leetcode.com/problems/k-similar-strings/)

- use BFS traversal method
- for each string, convert it into list of strings by swapping by two characters. if the newly build string is not visited before add into the Queue for next level processing
- once the desired string found, your count give me the shortest K-similar strings

```java
class Solution {
    public int kSimilarity(String A, String B) {
        Deque<String> Q = new ArrayDeque<> ();
        Set<String> visited = new HashSet<> ();
        
        Q.addLast(A); visited.add(A);
        int level = 0;
        
        while (!Q.isEmpty()) {
            int size = Q.size();
            for (int i = 0; i < size; i++) {
                String str = Q.removeFirst();
                if (str.equals(B)) return level;
                
                List<String> list = convertString(str);
                
                for (String temp : list) {
                    if (!visited.contains(temp)) {
                        Q.addLast(temp);
                        visited.add(temp);
                    }
                }
            }
            level++;
        }
        
        return 0;
    }
    
    public List<String> convertString(String s) {
        int n = s.length();
        List<String> li = new ArrayList<> ();
        
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                char[] ch = s.toCharArray();
                char temp = ch[i];
                ch[i] = ch[j];
                ch[j] = temp;
                li.add(new String(ch));
            }
        }
        return li;
    }
}
```

Time ~ O(N^3) where quadratic time takes by swapping and rebuilding the list of strings and exceed the time limit
Space ~ O(N)

**optimized code**

- rather than swap all the characters, only swap unmatched character
- for example, "abc" and "bca" first unmatched character found at index = 0. now use another index to point the next character of the first string with the unmatched character of the second string. if both characters are same, swap and build the new string.
- for example, at index = 1, the "abc" and at index = 0, "bca" matched by the char "b"

```java
class Solution {
    public int kSimilarity(String A, String B) {
        Deque<String> Q = new ArrayDeque<> ();
        Set<String> visited = new HashSet<> ();
        
        Q.addLast(A); visited.add(A);
        int level = 0;
        
        while (!Q.isEmpty()) {
            int size = Q.size();
            for (int i = 0; i < size; i++) {
                String str = Q.removeFirst();
                if (str.equals(B)) return level;
                
                List<String> list = convertString(str, B);
                
                for (String temp : list) {
                    if (!visited.contains(temp)) {
                        Q.addLast(temp);
                        visited.add(temp);
                    }
                }
            }
            level++;
        }
        
        return 0;
    }
    
    public List<String> convertString(String s, String target) {
        int n = s.length();
        List<String> li = new ArrayList<> ();
        int i = 0;

        // find the first unmatched character
        for (i = 0; i < n; i++) {
            if (s.charAt(i) != target.charAt(i))
                break;
        }
        
        char[] ch = s.toCharArray();

        for (int j = i + 1; j < n; j++) {
            if (s.charAt(j) == target.charAt(i)) {
                swap(ch, i, j);
                li.add(new String(ch));
                swap(ch, i, j);
            }
        }
        
        return li;
    }
    
    public void swap(char[] ch, int i, int j) {
        char temp = ch[i];
        ch[i] = ch[j]; ch[j] = temp;
    }
}
```

Time ~ O(N^2), space ~ O(N)

**DFS call**

```java
class Solution {
    public int kSimilarity(String A, String B) {
        char[] a = A.toCharArray();
        char[] b = B.toCharArray();
        return dfs(a, b, 0);
    }

    public int dfs(char[] a, char[] b, int start) {
        if (start == a.length) return 0;
        if (a[start] == b[start])
            return dfs(a, b, start + 1);

        int min = Integer.MAX_VALUE;

        for (int i = start + 1; i < a.length; i++) {
            if (a[i] == b[start] && a[i] != b[i]) {
                swap(a, start, i);
                min = Math.min(min, 1 + dfs(a, b, start + 1));
                swap(a, start, i);

                if (a[start] == b[i])
                    return min;
            }
        }
        return min;
    }
    
    public void swap(char[] ch, int i, int j) {
        char temp = ch[i];
        ch[i] = ch[j]; ch[j] = temp;
    }
}
```

## Palindromic Substrings
[https://leetcode.com/problems/palindromic-substrings/](https://leetcode.com/problems/palindromic-substrings/)

- expand around the center to find the palindrome string
- if the "ababa" is palindrome then "bab" is also palindrome

```java
class Solution {
    public int countSubstrings(String s) {
        if (s == null || s.length() == 0) return 0;
        int res = 0, n = s.length();
        
        for (int center = 0; center <= 2 * n - 1; center++) {
            int left = center / 2;
            int right = left + center % 2;

            while (left >= 0 && right < n && s.charAt(left) == s.charAt(right)) {
                res++; left--; right++;
            }
        }
        return res;
    }
}
```
Time ~ O(N^2), Space ~ O(1)

**Manacher's algo**
[https://www.youtube.com/watch?v=nbTSfrEfo6M&t=908s](https://www.youtube.com/watch?v=nbTSfrEfo6M&t=908s)

https://medium.com/hackernoon/manachers-algorithm-explained-longest-palindromic-substring-22cb27a5e96f#:~:text=Manacher's%20Algorithm%20Explained%E2%80%94%20Longest%20Palindromic%20Substring,-Mithra%20Talluri&text=Manacher's%20Algorithm%20helps%20us%20find,insights%20into%20how%20palindromes%20work.

```java
class Solution {
    public int countSubstrings(String S) {
        char[] A = new char[2 * S.length() + 3];
        A[0] = '@'; A[1] = '#'; A[A.length - 1] = '$';
        int t = 2;

        for (char c : S.toCharArray()) {
            A[t++] = c;
            A[t++] = '#';
        }

        int[] Z = new int[A.length];
        int center = 0, right = 0;

        for (int i = 1; i < Z.length - 1; i++) {
            if (i < right)
                Z[i] = Math.min(right - i, Z[2 * center - i]);

            while (A[i + 1 + Z[i]] == A[i - 1 - Z[i]])
                Z[i]++;

            if (i + Z[i] > right) {
                center = i;
                right = i + Z[i];
            }
        }

        int res = 0;
        for (int val : Z)
            res += (val + 1) / 2;
        return res;
    }
}
```
Time & Space ~ O(N)

**Followup: find the unique substring rather then count**
if the Z[i] is larger than zero, then the left bound would be `(i - Z[i]) / 2`  and right bound would be `(i + Z[i]) / 2 - 1`.
add this piece of code after manacher's algo run
```java
        Set<String> set = new HashSet<>();
        for (int i = 0; i < Z.length; i++) {
            if (Z[i] > 0) {
                int leftBound = (i - Z[i]) / 2;
                int rightBound = ((i + Z[i]) / 2) - 1;

                set.add(S.substring(leftBound, rightBound + 1));
            }
        }
```

## Pacific Atlantic Water Flow
https://leetcode.com/problems/pacific-atlantic-water-flow/

- Two Queue and add all the Pacific border to one queue; Atlantic border to another queue.
- Keep a visited matrix for each queue. In the end, add the cell visited by two queue to the result.
- BFS: Water flood from ocean to the cell. Since water can only flow from high/equal cell to low cell, add the neighboor cell with height larger or equal to current cell to the queue and mark as visited.

```java
class Solution {

    private int[][] dirs = {{0,1}, {0,-1}, {1,0}, {-1,0}};

    public List<List<Integer>> pacificAtlantic(int[][] matrix) {
        List<List<Integer>> res = new ArrayList<> ();
        
        if (matrix == null || matrix.length == 0) return res;

        int rows = matrix.length, cols = matrix[0].length;
        boolean[][] pacific = new boolean[rows][cols];
        boolean[][] atlantic = new boolean[rows][cols];

        Deque<int[]> Q_pacific = new ArrayDeque<> ();
        Deque<int[]> Q_atlantic = new ArrayDeque<> ();
        
        //add pacific and atlantic boundaries into queue
        //vertical boundaries
        for(int i = 0; i < rows; i++) {
            Q_pacific.addLast(new int[] {i, 0});
            Q_atlantic.addLast(new int[] {i, cols - 1});
            pacific[i][0] = true;
            atlantic[i][cols - 1] = true;
        }

        //horizontal boundaries
        for(int i = 0; i < cols; i++) {
            Q_pacific.addLast(new int[] {0, i});
            Q_atlantic.addLast(new int[] {rows - 1, i});
            pacific[0][i] = true;
            atlantic[rows - 1][i] = true;
        }

        //make bfs call and update the visited matrix and insert the queue if the new cell has higher or equal
        //water than the current one (coding in opposite direction than water flow)
        bfs(matrix, Q_pacific, pacific, rows, cols);
        bfs(matrix, Q_atlantic, atlantic, rows, cols);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (pacific[i][j] && atlantic[i][j]) {
                    List<Integer> temp = new ArrayList<> ();
                    temp.add(i); temp.add(j);
                    res.add(new ArrayList<> (temp));
                }
            }
        }

        return res;
    }

    public void bfs(int[][] matrix, Deque<int[]> Q, boolean[][] visited, int R, int C) {
        while (!Q.isEmpty()) {
            int[] cur = Q.removeFirst();
            for(int k = 0; k < dirs.length; k++) {
                int x = cur[0] + dirs[k][0], y = cur[1] + dirs[k][1];

                if (x < 0 || x == R || y < 0 || y == C || visited[x][y] || matrix[cur[0]][cur[1]] > matrix[x][y])
                    continue;

                visited[x][y] = true;
                Q.addLast(new int[] { x, y });
            }
        }
    }
}
```

Time, Space ~ O(n * m)

## Two Sum IV - Input is a BST
https://leetcode.com/problems/two-sum-iv-input-is-a-bst/

- do inorder traversal and store the data into list
- use two sum technique to find the target sum

```java
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public boolean findTarget(TreeNode root, int k) {
        List<Integer> list = new ArrayList<> ();
        inorder(root, list);

        int i = 0, j = list.size() - 1;

        while (i < j) {
            int sum = list.get(i) + list.get(j);
            if (sum == k) return true;
            else if (sum < k) i++;
            else j--;
        }
        return false;
    }

    public void inorder(TreeNode root, List<Integer> list) {
        if (root == null) return;

        inorder(root.left, list);
        list.add(root.val);
        inorder(root.right, list);
    }
}
```

Time & Space O(n)

## Best Time to Buy and Sell Stock
https://leetcode.com/problems/best-time-to-buy-and-sell-stock/

- choose the first stock price as the buy stock price `buy`
- iterate the stock prices and if the current stock price is smaller than the buy stock then reset the buy stock
- otherwise, calculate the profit and update the global max

```java
class Solution {
    public int maxProfit(int[] prices) {
        if (prices.length == 0) return 0;
        
        int buy = prices[0], max = 0;

        for (int i = 1; i < prices.length; i++) {
            if (prices[i] - buy < 0)
                buy = prices[i];
            else if (max < prices[i] - buy)
                max = prices[i] - buy;
        }
        return max;
    }
}
```

Time ~ O(n), Space O(1)

```java
class Solution {
    public int maxProfit(int[] prices) {
        
    }
}
```

## Three Sum
[https://leetcode.com/problems/3sum/](https://leetcode.com/problems/3sum/)

`Algo`
1. sort the array and take each array element (dont take same element) as the target by substracting from zero
2. use two pointers to calculate the sum
3. if sum is smaller than target move the left pointer to right and if sum is larger than target move the right pointer to left
4. If sum is equals to target move the left and right pointer until there is no same value

```java
class Solution {
    public List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> res = new LinkedList<> ();
        Arrays.sort(nums);
        for (int i = 0; i < nums.length; i++) {
            if (i > 0 && nums[i - 1] == nums[i])
                continue;
            
            int target = 0 - nums[i], j = i + 1, k = nums.length - 1;
            while (j < k) {
                int sum = nums[j] + nums[k];
                
                if (sum == target) {
                    List<Integer> li = new LinkedList<> (Arrays.asList(nums[i], nums[j], nums[k]));
                    res.add(li);
                    
                    while (j < k && nums[j] == nums[j + 1]) j++;
                    while (j < k && nums[k] == nums[k - 1]) k--;
                    
                    j++; k--;
                }
                else if (sum < target) j++;
                else k--;
            }
        }
        return res;
    }
}
```
time & space complexity - O(N^2)

### Median of two sorted array
[https://leetcode.com/problems/median-of-two-sorted-arrays/](https://leetcode.com/problems/median-of-two-sorted-arrays/)

Approach: [https://www.youtube.com/watch?v=LPFhl65R7ww](https://www.youtube.com/watch?v=LPFhl65R7ww)

- Take minimum size of two array. Possible number of partitions are from 0 to m in m size array.
- Try every cut in binary search way. When you cut first array at i then you cut second array at (m + n + 1)/2 - i
- Now try to find the i where a[i-1] <= b[j] and b[j-1] <= a[i]. So this i is partition around which lies the median.

```java
class Solution {
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int m = nums1.length, n = nums2.length;
        
        if (m > n) return findMedianSortedArrays(nums2, nums1);
        if (m == 0 && n == 0) return 0.0;
        
        int start = 0, end = m;
        
        while (start <= end) {
            int partX = (start + end) / 2, partY = (m + n + 1) / 2 - partX;
            int l1 = (partX == 0) ? Integer.MIN_VALUE : nums1[partX - 1];
            int r1 = (partX == m) ? Integer.MAX_VALUE : nums1[partX];
            int l2 = (partY == 0) ? Integer.MIN_VALUE : nums2[partY - 1];
            int r2 = (partY == n) ? Integer.MAX_VALUE : nums2[partY];
            
            if (l1 <= r2 && l2 <= r1) {
                if ((m + n) % 2 == 0)
                    return (double) (Math.max(l1, l2) + Math.min(r1, r2)) / 2.0;
                else
                    return (double) Math.max(l1, l2);
            }
            else if (r1 < l2)
                start = partX + 1;
            else
                end = partX - 1;
        }
        
        return 0.0;
    }
}
```

Time & Space complexity O(log(min(m, n))) & O(1).

## Linked List
### Add two numbers
[https://leetcode.com/problems/add-two-numbers/](https://leetcode.com/problems/add-two-numbers/)

- digits are stored in reverse order
- add two nodes and carry if the sum is more than 9

```java
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode head = new ListNode(0);
        int carry = 0;
        ListNode ptr = head;
        
        while (l1 != null || l2 != null) {
            int a = (l1 != null) ? l1.val : 0;
            int b = (l2 != null) ? l2.val : 0;
            int sum = a + b + carry;
            
            carry = sum / 10;
            
            ptr.next = new ListNode(sum % 10);
            ptr = ptr.next;
            l1 = (l1 != null) ? l1.next : null;
            l2 = (l2 != null) ? l2.next : null;
        }
        
        if (carry > 0)
            ptr.next = new ListNode(carry);
        
        return head.next;
    }
}
```
Time and space complexity - O(max(m,n))

What if the the digits in the linked list are stored in non-reversed order? Reverse the linked list and then use the above code to add the two linked list

Reverse linked list process:

```java
public static ListNode reverse(ListNode l1) {  
    ListNode head = null;  
  ListNode p1 = l1, p2 = null;  
  
 if (p1.next != null)  
        p2 = p1.next;  
  
 while (p2 != null) {  
        p1.next = head; head = p1; p1 = p2; p2 = p2.next;  
  }  
  
    p1.next = head;  
 return p1;  
}
```

### Merged two linked list
[https://leetcode.com/problems/merge-two-sorted-lists/](https://leetcode.com/problems/merge-two-sorted-lists/)
Algo: use same merged sort linked list logic

```java
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        ListNode head = new ListNode(0);
        ListNode ptr = head;
        
        while (l1 != null && l2 != null) {
            ptr.next = new ListNode(Math.min(l1.val, l2.val));
            if (l1.val <= l2.val) l1 = l1.next;
            else l2 = l2.next;
            ptr = ptr.next;
        }
        while (l1 != null) {
            ptr.next = new ListNode(l1.val); ptr = ptr.next; l1 = l1.next;
        }
        while (l2 != null) {
            ptr.next = new ListNode(l2.val); ptr = ptr.next; l2 = l2.next;
        }
        return head.next;
    }
}
```
time & space O(m+n), O(1)

## Dynamic Programming & String
### Longest Palindromic Subsequence
[https://www.geeksforgeeks.org/longest-palindromic-subsequence-dp-12/](https://www.geeksforgeeks.org/longest-palindromic-subsequence-dp-12/)
[https://leetcode.com/problems/longest-palindromic-subsequence/](https://leetcode.com/problems/longest-palindromic-subsequence/)

Algo: Let X[0..n-1] be the input sequence of length n and L(0, n-1) be the length of the longest palindromic subsequence of X[0..n-1].

If last and first characters of X are same, then L(0, n-1) = L(1, n-2) + 2.  
Else L(0, n-1) = MAX (L(1, n-1), L(0, n-2)).

`Naive recursive solution`

```java
public static int maxSubsequence(String str, int i, int j) {  
 if (i > j)  return 0;  
 if (i == j) return 1;   
 if (str.charAt(i) == str.charAt(j)) return 2 + maxSubsequence(str, i + 1, j - 1);  
 else return Math.max(maxSubsequence(str, i, j - 1), maxSubsequence(str, i + 1, j));  
}
```

`DP Approach`

```java
    public static int calcDP(String str) {
        int N = str.length();
        int[][] dp = new int[N][N];

        for (int i = 0; i < N; i++)
            dp[i][i] = 1;

        for(int len = 2; len <= N; len++) {
            for (int i = 0; i < N - len + 1; i++) {
                int j = i + len - 1;

                if (str.charAt(i) == str.charAt(j)) {
                    if (len == 2)
                        dp[i][j] = 2;
                    else
                        dp[i][j] = 2 + dp[i + 1][j - 1];
                }
                else
                    dp[i][j] = Math.max(dp[i][j - 1], dp[i + 1][j]);
            }
        }

        return dp[0][N - 1];
    }
```
Time & Space Complexity : O(N^2). As the above DP method will not use lower half of the matrix, below is most efficient space optimized coding:

```java
class Solution {
    public int longestPalindromeSubseq(String s) {
        char[] str = s.toCharArray();
        int[] dp = new int[str.length];
        int max = 0;

        for (int i = 0; i < dp.length; ++i) {
            dp[i] = 1;

            int maxSoFar = 0;
            for (int j = i-1; j >= 0; --j) {
                int prev = dp[j];

                if (str[i] == str[j]) {
                    dp[j] = maxSoFar + 2;
                }

                maxSoFar = Math.max(prev, maxSoFar);
            }
        }

        for (int i : dp) max = Math.max(max, i);
        return max;
    }
}
```
Time and Space complexity : O(N^2) and O(N)


### Trapping Rain Water
[https://leetcode.com/problems/trapping-rain-water/](https://leetcode.com/problems/trapping-rain-water/)

-   Find maximum height of bar from the left end upto an index i in the array left_max.
-   Find maximum height of bar from the right end upto an index i in the array right_max.
-   Iterate over the  \text{height}height  array and update ans:
    -   Add  min(left[i],right[i]) - height[i]

```java
class Solution {
    public int trap(int[] height) {
        int n = height.length;
        int[] left = new int[n];
        int[] right = new int[n];
        int left_max = Integer.MIN_VALUE, right_max = Integer.MIN_VALUE, count = 0;
        
        for (int i = 0; i < n; i++) {
            left_max = Math.max(height[i], left_max); left[i] = left_max;
        }
        
        for (int i = n - 1; i >= 0; i--) {
            right_max = Math.max(height[i], right_max); right[i] = right_max;
        }
        
        for (int i = 0; i < n; i++)
            count += (Math.min(left[i], right[i]) - height[i]) > 0 ? Math.min(left[i], right[i]) - height[i] : 0;
        
        return count;
    }
}
```

time & space O(N)

Optimized code using `two pointer method`: if left side is smaller than right side then the water will logged in the left path and vice versa.

```java
class Solution {
    public int trap(int[] height) {
        int l = 0, r = height.length - 1, left_max = 0, right_max = 0, sum = 0;
        
        for (int i = 0; i < height.length; i++) {
            if (height[l] < height[r]) {
                if (height[l] > left_max) left_max = height[l];
                else sum += left_max - height[l];
                l++;
            }
            else {
                if (height[r] > right_max) right_max = height[r];
                else sum += right_max - height[r];
                r--;
            }
        }
        
        return sum;
    }
}
```

## Graph
### [https://leetcode.com/problems/number-of-islands/](https://leetcode.com/problems/number-of-islands/)

DFS Traversal to connect all the lands and return the count of islands

```java
class Solution {
    
    private int[][] move = {{-1,0},{0,-1},{0,1},{1,0}};
    
    public int numIslands(char[][] grid) {
        int count = 0;
        if (grid.length == 0)
            return 0;
        
        int R = grid.length, C = grid[0].length;
        
        for (int i = 0; i < R; i++) {
            for (int j = 0; j < C; j++) {
                if (grid[i][j] == '1') {
                    dfs(grid, i, j, R, C); count++;
                }
            }
        }
        
        return count;
    }
    
    public void dfs(char[][] grid, int i, int j, int R, int C) {
        grid[i][j] = '0';
        
        for (int k = 0; k < move.length; k++) {
            int ix = i + move[k][0];
            int iy = j + move[k][1];
            
            if (ix >= 0 && ix < R && iy >= 0 && iy < C && grid[ix][iy] == '1')
                dfs(grid, ix, iy, R, C);
        }
    }
}
```

Time & Space Complexity O(m * n)

## Greedy Algorithm
### Maximum Subarray
[https://leetcode.com/problems/maximum-subarray/](https://leetcode.com/problems/maximum-subarray/)
Algo:
1. calculate the local optimum i.e. the total sum seen so far vs the current element value
2. find the local optimum vs global optimum

The problem to find maximum (or minimum) element (or sum) with a single array as the input is a good candidate to be solved by the greedy approach in linear time. Pick the  _locally_  optimal move at each step, and that will lead to the  _globally_  optimal solution.

The algorithm is general and straightforward: iterate over the array and update at each step the standard set for such problems:
-   current element
-   current  _local_  maximum sum (at this given point)
-   _global_  maximum sum seen so far.

```java
class Solution {
    public int maxSubArray(int[] nums) {
        int curr_sum = nums[0], max = nums[0];
        
        for (int i = 1; i < nums.length; i++) {
            curr_sum = Math.max(nums[i], curr_sum + nums[i]);
            max = Math.max(curr_sum, max);
        }
        
        return max;
    }
}
```
time & space - O(N), O(1)


## Most Common Amazon QA
### Subtree sum
[https://www.geeksforgeeks.org/subtree-given-sum-binary-tree/](https://www.geeksforgeeks.org/subtree-given-sum-binary-tree/)

use postorder traversal recursively to calculate the sum of the subtree.

```java
    public static TreeNode res = new TreeNode(-1);

    public static TreeNode sumTree(TreeNode root, int target) {
        if (root == null)
            return null;

        TreeNode l = sumTree(root.left, target);
        TreeNode r = sumTree(root.right, target);

        int sum = root.val + ((l != null) ? l.val : 0) + ((r != null) ? r.val : 0);
        if (sum == target) {
            res = root;
            return root;
        }

        return new TreeNode(sum);
    }
```

Time & Space - O(N)

### Path sum
[https://leetcode.com/problems/path-sum/](https://leetcode.com/problems/path-sum/)
```java
class Solution {
    public boolean hasPathSum(TreeNode root, int sum) {
        if (root == null)
            return false;
        return util(root, sum, 0);
    }
    
    public boolean util(TreeNode node, int sum, int curr_sum) {
        if (node == null)
            return false;
        
        if (node.left == null && node.right == null)
            return (sum == curr_sum + node.val);
        
        return util(node.left, sum, curr_sum + node.val) || util(node.right, sum, curr_sum + node.val);
    }
}
```
Time & space: O(N) and O(N) for unbalanced tree and O(logN) for balance tree

Use iterative solution using Stack and DFS.

```java
import javafx.util.Pair;
class Solution {
    public boolean hasPathSum(TreeNode root, int sum) {
        Deque<Pair<TreeNode, Integer>> stack = new ArrayDeque<> ();
        if (root == null)
            return false;
        
        stack.addLast(new Pair<>(root, sum - root.val));
        
        while (!stack.isEmpty()) {
            Pair<TreeNode, Integer> p = stack.removeLast();
            TreeNode node = p.getKey();
            int cur_sum = p.getValue();
            
            if ((node.left == null && node.right == null) && cur_sum == 0)
                return true;
            
            if (node.right != null)
                stack.addLast(new Pair<>(node.right, cur_sum - node.right.val));
            
            if (node.left != null)
                stack.addLast(new Pair<>(node.left, cur_sum - node.left.val));
        }
        
        return false;
    }
}
```
Time & space: O(N) and O(N) for unbalanced tree and O(logN) for balance tree

### Word Break Problem II
[https://leetcode.com/problems/word-break-ii/](https://leetcode.com/problems/word-break-ii/)

Algo:
1) use a index position `i` to move between the string position 1 to the `len` of the string and each time take the right substring from the current position `i`
2) if the right substring is part of the dictionary, recursively check the left substring and result the list of words that are part of the dictionary.
3) Use map to avoid any recalculation for prefix character

```java
class Solution {
    public List<String> wordBreak(String s, List<String> wordDict) {
        Map<String, List<String>> map = new HashMap<>();
        Set<String> set = new HashSet<> (wordDict);
        return util(s, map, set);
    }
    
    public List<String> util(String s, Map<String, List<String>> map, Set<String> set) {
        List<String> res = new LinkedList<String> ();
        if (s == null || s.length() == 0) return res;
        if (map.containsKey(s)) return map.get(s);
        if (set.contains(s)) res.add(s);
        
        for (int i = 1; i < s.length(); i++) {
            String str = s.substring(i);
            if (set.contains(str)) {
                List<String> temp = util(s.substring(0, i), map, set);
                
                for (int j = 0; j < temp.size(); j++) {
                    res.add(temp.get(j) + " " + str);
                }
            }
        }
        map.put(s, res);
        return res;
    }
}
```
Time and Space ~ **O(N^2) Not sure**

### Design in-memory file system
[https://leetcode.com/problems/design-in-memory-file-system/](https://leetcode.com/problems/design-in-memory-file-system/)
Algo: read the article [https://leetcode.com/articles/design-in-memory-file-system/](https://leetcode.com/articles/design-in-memory-file-system/)

```java
class FileSystem {
    
    class Dirs {
        Map<String, Dirs> dirs = new HashMap<> ();
        Map<String, String> files = new HashMap<> ();
    }
    
    private Dirs root = new Dirs();

    public FileSystem() {
        
    }
    
    public List<String> ls(String path) {
        List<String> res = new LinkedList<> ();
        Dirs temp = root;
        
        if (!path.equals("/")) {
            String[] d = path.split("/");
            
            for (int i = 1; i < d.length - 1; i++)
                temp = temp.dirs.get(d[i]);
            
            if (temp.files.containsKey(d[d.length - 1])) {
                res.add(d[d.length - 1]); return res;
            }
            else
                temp = temp.dirs.get(d[d.length - 1]);
        }
        
        res.addAll(new LinkedList<> (temp.dirs.keySet()));
        res.addAll(new LinkedList<> (temp.files.keySet()));
        Collections.sort(res);
        return res;
    }
    
    public void mkdir(String path) {
        String[] d = path.split("/");
        Dirs temp = root;
        
        for (int i = 1; i < d.length; i++) {
            if (!temp.dirs.containsKey(d[i])) {
                temp.dirs.put(d[i], new Dirs());
            }
            temp = temp.dirs.get(d[i]);
        }
    }
    
    public void addContentToFile(String filePath, String content) {
        String[] d = filePath.split("/");
        Dirs temp = root;
        
        for (int i = 1; i < d.length - 1; i++)
            temp = temp.dirs.get(d[i]);
        
        temp.files.put(d[d.length - 1], temp.files.getOrDefault(d[d.length - 1], "") + content);
        
    }
    
    public String readContentFromFile(String filePath) {
        String[] d = filePath.split("/");
        Dirs temp = root;
        
        for (int i = 1; i < d.length - 1; i++)
            temp = temp.dirs.get(d[i]);
        
        return temp.files.get(d[d.length - 1]);
    }
}

/**
 * Your FileSystem object will be instantiated and called as such:
 * FileSystem obj = new FileSystem();
 * List<String> param_1 = obj.ls(path);
 * obj.mkdir(path);
 * obj.addContentToFile(filePath,content);
 * String param_4 = obj.readContentFromFile(filePath);
 */
```

### Find duplicate file in a system
[https://leetcode.com/problems/find-duplicate-file-in-system/](https://leetcode.com/problems/find-duplicate-file-in-system/)

use map to store the content as key and directory + "/"+ file name as value

```java
class Solution {
    public List<List<String>> findDuplicate(String[] paths) {
        Map<String, List<String>> map = new HashMap<> ();
        
        for (String path : paths) {
            String[] d = path.split(" ");
            for (int i = 1; i < d.length; i++) {
                int idx = d[i].indexOf("(");
                String fileName = d[i].substring(0, idx);
                String content = d[i].substring(idx);
                content = content.replace(")", "");
                
                List<String> li = map.getOrDefault(content, new LinkedList<> ());
                li.add(d[0] + "/"+ fileName);
                map.put(content, li);
            }
        }
        
        List<List<String>> res = new LinkedList<> ();
        for (String key : map.keySet()) {
            if (map.get(key).size() > 1)
                res.add(map.get(key));
        }
        
        return res;
    }
}
```
time & space - if we have N string array and each string has M avg character them O(N * M)


### Design hangman play

Design hangman with a given helper function that returns the array of indices for given character, return the string if guessed properly  

`public String guessWord(int length, int numWrongGuesses)`
`pubic int[] helper(char c)`

Follow-up: how can we improve it? My ideas-(maintain a hashmap of <character,numOfWords> start by guessing letters with max count, but this approach occupies more space. We can also use Trie in some manner to make this easy)

Ref: [https://shobhankblog.wordpress.com/2016/04/24/design-interview-questions/](https://shobhankblog.wordpress.com/2016/04/24/design-interview-questions/)

### Merge sorted array
[https://leetcode.com/problems/merge-sorted-array/](https://leetcode.com/problems/merge-sorted-array/)

1) as both arrays are sorted use two pointers technique and start from the end of the array
2) compare both array element and choose the max amoung them and move the pointer to the left

```java
class Solution {
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int i = m - 1, j = n - 1, k = m + n - 1;
        
        while (i >= 0 || j >= 0) {
            int a = (i >= 0) ? nums1[i] : Integer.MIN_VALUE;
            int b = (j >= 0) ? nums2[j] : Integer.MIN_VALUE;
            
            nums1[k] = Math.max(a, b);
            k--;
            
            if (a > b) i--;
            else j--;
        }
    }
}
```

time & space - O(m + n), O(1)

### Write a Trie

- Trie implementations [https://leetcode.com/explore/learn/card/trie/150/introduction-to-trie/1046/](https://leetcode.com/explore/learn/card/trie/150/introduction-to-trie/1046/)
- [https://leetcode.com/explore/learn/card/trie/147/basic-operations/1048/](https://leetcode.com/explore/learn/card/trie/147/basic-operations/1048/)

### Dice roll with target sum
[https://leetcode.com/problems/number-of-dice-rolls-with-target-sum/](https://leetcode.com/problems/number-of-dice-rolls-with-target-sum/)
algo: [https://leetcode.com/problems/number-of-dice-rolls-with-target-sum/discuss/355940/C%2B%2B-Coin-Change-2](https://leetcode.com/problems/number-of-dice-rolls-with-target-sum/discuss/355940/C%2B%2B-Coin-Change-2)

```java
class Solution {
    
    public int numRollsToTarget(int d, int f, int target) {
        if(d * f < target || target < d) return 0;
        if(d * f == target || target == d) return 1;
        
        final int mod = 1000000007;
        long[][] num = new long[d + 1][target + 1];
        
        for (int face = 1; face <= f; face++) {
            if(target >= face)
                num[1][face] = 1;
        }
        
        for(int i = 2; i <= d; i++) {
            for(int j = 1; j <= target; j++) {
                for(int k = 1; k <= f; k++) {
                    if(j - k >= 0) {
                        num[i][j] += num[i - 1][j - k];
                        num[i][j] %= mod;
                    }
                }
            }
        }
        return (int)(num[d][target]);
    }
}
```
time & space - O(d * f * target), O(d * target)

### Binary tree zigzag level order traversal
[https://leetcode.com/problems/binary-tree-zigzag-level-order-traversal/](https://leetcode.com/problems/binary-tree-zigzag-level-order-traversal/)

1) use queue for BFS traversal and use boolean flag to change the order while saving the output

```java
class Solution {
    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        Deque<TreeNode> Q = new ArrayDeque<> ();
        List<List<Integer>> res = new LinkedList<> ();
        boolean fl = false;
        
        if (root == null) return res;
        Q.addLast(root);
        
        while (!Q.isEmpty()) {
            int size = Q.size();
            List<Integer> li = new LinkedList<> ();
            for (int i = 0; i < size; i++) {
                TreeNode node = Q.removeFirst();
                if (!fl) li.add(node.val);
                else li.add(0, node.val);
                
                if (node.left != null) Q.addLast(node.left);
                if (node.right != null) Q.addLast(node.right);
            }
            fl = !fl;
            res.add(li);
        }
        return res;
    }
}
```
time & space - O(N) N- number of tree node

### Longest increasing path in a matrix
[https://leetcode.com/problems/longest-increasing-path-in-a-matrix/](https://leetcode.com/problems/longest-increasing-path-in-a-matrix/)

DFS + Memo

```java
class Solution {
    
    private Map<String, Integer> map = new HashMap<> ();
    private int[][] move = {{-1,0}, {0,-1}, {0,1}, {1,0}};
    
    public int longestIncreasingPath(int[][] matrix) {
        if (matrix.length == 0)
            return 0;
        
        int res = 0, R = matrix.length, C = matrix[0].length;
        for (int i = 0; i < R; i++) {
            for (int j = 0; j < C; j++) {
                res = Math.max(res, util(i, j, R, C, matrix));
            }
        }
        
        return res;
    }
    
    public int util(int i, int j, int R, int C, int[][] matrix) {
        String str = i + "," + j;
        if (map.containsKey(str)) return map.get(str);
        
        int count = 0;
        
        for (int k = 0; k < move.length; k++) {
            int ix = i + move[k][0], iy = j + move[k][1];
            if (ix >= 0 && ix < R && iy >= 0 && iy < C && matrix[ix][iy] > matrix[i][j]) {
                count = Math.max(count, util(ix, iy, R, C, matrix));
            }
        }
        
        map.put(str, count + 1);
        return count + 1;
    }
}
```
time & space - O(M * N) where M = number of rows and N = number of columns

### binary tree longest consecutive sequence
[https://leetcode.com/problems/binary-tree-longest-consecutive-sequence/](https://leetcode.com/problems/binary-tree-longest-consecutive-sequence/)

A top down approach is similar to an in-order traversal. We use a variable `length` to store the current consecutive path length and pass it down the tree. As we traverse, we compare the current node with its parent node to determine if it is consecutive. If not, we reset the length.

```java
class Solution {
    private int maxLen = 0;
    
    public int longestConsecutive(TreeNode root) {
        dfs(root, null, 0);
        return maxLen;
    }
    
    public void dfs(TreeNode ch, TreeNode parent, int len) {
        if (ch == null) return;
        
        len = (parent != null && ch.val == parent.val + 1) ? len + 1 : 1;
        maxLen = Math.max(maxLen, len);
        dfs(ch.left, ch, len);
        dfs(ch.right, ch, len);
    }
}
```
time & space - O(N)

## Course Schedule
[https://leetcode.com/problems/course-schedule/](https://leetcode.com/problems/course-schedule/)

use topological sort mainly Kahn's algo which also detect cycles if any ([https://en.wikipedia.org/wiki/Topological_sorting](https://en.wikipedia.org/wiki/Topological_sorting))

```java
class Solution {
    public boolean canFinish(int numCourses, int[][] prerequisites) {
        List<Integer>[] graph = new LinkedList[numCourses];
        int[] indeg = new int[numCourses];
        Deque<Integer> Q = new ArrayDeque<> ();
        
        for (int i = 0; i < numCourses; i++) graph[i] = new LinkedList<> ();
        
        for (int i = 0; i < prerequisites.length; i++) {
            int[] pair = prerequisites[i]; 
            int parent = pair[1], child = pair[0];
            graph[parent].add(child);
            indeg[child]++;
        }
        
        int count = 0;
        for (int i = 0; i < numCourses; i++) {
            if (indeg[i] == 0) Q.addLast(i);
        }
        
        while (!Q.isEmpty()) {
            int node = Q.removeFirst();
            count++;
            for (int i : graph[node]) {
                if (--indeg[i] == 0) Q.addLast(i);
            }
        }
        
        return count == numCourses;
    }
}
```

## Course Schedule II
[https://leetcode.com/problems/course-schedule-ii/](https://leetcode.com/problems/course-schedule-ii/)

- use map to store prereq course number and list of dependent courses
- maintain another map for in-degree i.e number of incoming edges for each vertex
- use topological sort to order the output ([https://en.wikipedia.org/wiki/Topological_sorting](https://en.wikipedia.org/wiki/Topological_sorting) Kahn's algorithm) starting with zero in-degree

```java
class Solution {
  public int[] findOrder(int numCourses, int[][] prerequisites) {

    boolean isPossible = true;
    Map<Integer, List<Integer>> adjList = new HashMap<Integer, List<Integer>>();
    int[] indegree = new int[numCourses];
    int[] topologicalOrder = new int[numCourses];

    // Create the adjacency list representation of the graph
    for (int i = 0; i < prerequisites.length; i++) {
      int dest = prerequisites[i][0];
      int src = prerequisites[i][1];
      List<Integer> lst = adjList.getOrDefault(src, new ArrayList<Integer>());
      lst.add(dest);
      adjList.put(src, lst);

      // Record in-degree of each vertex
      indegree[dest] += 1;
    }

    // Add all vertices with 0 in-degree to the queue
    Queue<Integer> q = new LinkedList<Integer>();
    for (int i = 0; i < numCourses; i++) {
      if (indegree[i] == 0) {
        q.add(i);
      }
    }

    int i = 0;
    // Process until the Q becomes empty
    while (!q.isEmpty()) {
      int node = q.remove();
      topologicalOrder[i++] = node;

      // Reduce the in-degree of each neighbor by 1
      if (adjList.containsKey(node)) {
        for (Integer neighbor : adjList.get(node)) {
          indegree[neighbor]--;

          // If in-degree of a neighbor becomes 0, add it to the Q
          if (indegree[neighbor] == 0) {
            q.add(neighbor);
          }
        }
      }
    }

    // Check to see if topological sort is possible or not.
    if (i == numCourses) {
      return topologicalOrder;
    }

    return new int[0];
  }
}
```

Time and Space ~ O(N)

**build an API for the external user**

```java
/*
build an API for external user
*/

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.LinkedList;
import java.util.Queue;
import java.io.Serializable;

public class Solution implements Cloneable, Serializable {

    private static final long serialVersionUID = 362498820763181265L;

    public static int[] findOrder(int numCourses, int[][] prerequisites) {

        boolean isPossible = true;
        Map<Integer, List<Integer>> adjList = new HashMap<Integer, List<Integer>>();
        int[] indegree = new int[numCourses];
        int[] topologicalOrder = new int[numCourses];

        // Create the adjacency list representation of the graph
        for (int i = 0; i < prerequisites.length; i++) {
            int dest = prerequisites[i][0];
            int src = prerequisites[i][1];
            List<Integer> lst = adjList.getOrDefault(src, new ArrayList<Integer>());
            lst.add(dest);
            adjList.put(src, lst);

            // Record in-degree of each vertex
            indegree[dest] += 1;
        }

        // Add all vertices with 0 in-degree to the queue
        Queue<Integer> q = new LinkedList<Integer>();
        for (int i = 0; i < numCourses; i++) {
            if (indegree[i] == 0) {
                q.add(i);
            }
        }

        int i = 0;
        // Process until the Q becomes empty
        while (!q.isEmpty()) {
            int node = q.remove();
            topologicalOrder[i++] = node;

            // Reduce the in-degree of each neighbor by 1
            if (adjList.containsKey(node)) {
                for (Integer neighbor : adjList.get(node)) {
                    indegree[neighbor]--;

                    // If in-degree of a neighbor becomes 0, add it to the Q
                    if (indegree[neighbor] == 0) {
                        q.add(neighbor);
                    }
                }
            }
        }

        // Check to see if topological sort is possible or not.
        if (i == numCourses) {
          return topologicalOrder;
        }

        return new int[0];
    }
}
```

### First unique character in a string
[https://leetcode.com/problems/first-unique-character-in-a-string/](https://leetcode.com/problems/first-unique-character-in-a-string/)

two solution one is using the map and another is using int array holding the character value
```java
class Solution {
    public int firstUniqChar(String s) {
        Map<Character, Integer> map = new HashMap<> ();
        
        for (int i = 0; i < s.length(); i++)
            map.put(s.charAt(i), map.getOrDefault(s.charAt(i), 0) + 1);
        
        for (int i = 0; i < s.length(); i++) {
            if (map.get(s.charAt(i)) == 1) return i;
        }
        
        return -1;
    }
}
```
time & space - O(N)

```java
class Solution {
    public int firstUniqChar(String s) {
        int[] ch = new int[26];
        for (int i = 0; i < s.length(); i++) ch[s.charAt(i) - 'a']++;
        for (int i = 0; i < s.length(); i++) {
            if (ch[s.charAt(i) - 'a'] == 1) return i;
        }
        
        return -1;
    }
}
```
time & space - O(N) and O(1)

### Letter combinations of a phone number
[https://leetcode.com/problems/letter-combinations-of-a-phone-number/](https://leetcode.com/problems/letter-combinations-of-a-phone-number/)

use BFS + Queue

```java
class Solution {
    public List<String> letterCombinations(String digits) {
        String[] mapping = {"", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
        LinkedList<String> res = new LinkedList<> ();
        
        if (digits.length() == 0) return res;
        res.add("");
        
        for (int i = 0; i < digits.length(); i++) {
            int val = Character.getNumericValue(digits.charAt(i));
            while (res.peek().length() == i) {
                String str = res.remove();
                for (char ch : mapping[val].toCharArray())
                    res.add(str + ch);
            }
        }
        
        return res;
    }
}
```
time & space - O(3^M  * 4 ^ N) where M maps with 3 character string in the phone pad and N maps with 4 character string in the phone pad

### Insert Delete GetRandom O(1)
[https://leetcode.com/problems/insert-delete-getrandom-o1/](https://leetcode.com/problems/insert-delete-getrandom-o1/)

Use Map and List: Map stores the keys and the list position as values.

```java
class RandomizedSet {
    
    private Map<Integer, Integer> map = new HashMap<>();
    private List<Integer> li = new ArrayList<>();

    /** Initialize your data structure here. */
    public RandomizedSet() {
    }
    
    /** Inserts a value to the set. Returns true if the set did not already contain the specified element. */
    public boolean insert(int val) {
        if (map.containsKey(val)) return false;
        
        map.put(val, li.size()); // add the value and it's index position of the list
        li.add(val);
        return true;
    }
    
    /** Removes a value from the set. Returns true if the set contained the specified element. */
    public boolean remove(int val) {
        if (!map.containsKey(val)) return false;
        
        int currentPosition = map.get(val);
        //copy the last element in the list and paste over the currentPosition
        if (currentPosition < li.size() - 1) {
            int lastElement = li.get(li.size() - 1);
            li.set(currentPosition, lastElement);
            map.put(lastElement, currentPosition);
        }
        map.remove(val);
        li.remove(li.size() - 1);
        return true;
    }
    
    /** Get a random element from the set. */
    public int getRandom() {
        Random rand = new Random();
        return li.get(rand.nextInt(li.size()));
    }
}

/**
 * Your RandomizedSet object will be instantiated and called as such:
 * RandomizedSet obj = new RandomizedSet();
 * boolean param_1 = obj.insert(val);
 * boolean param_2 = obj.remove(val);
 * int param_3 = obj.getRandom();
 */
```

Time & Space - O(1) & O(N)

### Insert, Delete, GetRandom But Duplicate Allowed
[https://leetcode.com/problems/insert-delete-getrandom-o1-duplicates-allowed/](https://leetcode.com/problems/insert-delete-getrandom-o1-duplicates-allowed/)

Idea is to use list to store all the numbers and use map with set to store the currently added value and it's position

```java
class RandomizedCollection {

    /** Initialize your data structure here. */
    List<Integer> li = new ArrayList<> ();
    Map<Integer, Set<Integer>> map = new HashMap<> ();
    Random rand;
    
    public RandomizedCollection() {
        rand = new Random();
    }
    
    /** Inserts a value to the collection. Returns true if the collection did not already contain the specified element. */
    public boolean insert(int val) {
        boolean isContain = map.containsKey(val);
        
        if (!isContain) map.put(val, new HashSet<> ());
        map.get(val).add(li.size());
        li.add(val);
        return !isContain;
    }
    
    /** Removes a value from the collection. Returns true if the collection contained the specified element. */
    public boolean remove(int val) {
        if (!map.containsKey(val)) return false;
        
        int currentPosition = map.get(val).iterator().next();
        map.get(val).remove(currentPosition);
        
        if (currentPosition < li.size() - 1) {
            int lastVal = li.get(li.size() - 1);
            li.set(currentPosition, lastVal);
            map.get(lastVal).remove(li.size() - 1);
            map.get(lastVal).add(currentPosition);
        }
        li.remove(li.size() - 1);
        
        if (map.get(val).isEmpty())
            map.remove(val);
        
        return true;
    }
    
    /** Get a random element from the collection. */
    public int getRandom() {
        return li.get(rand.nextInt(li.size()));
    }
}

/**
 * Your RandomizedCollection object will be instantiated and called as such:
 * RandomizedCollection obj = new RandomizedCollection();
 * boolean param_1 = obj.insert(val);
 * boolean param_2 = obj.remove(val);
 * int param_3 = obj.getRandom();
 */
```

### Design Tic Tac Toe
[https://leetcode.com/problems/design-tic-tac-toe/](https://leetcode.com/problems/design-tic-tac-toe/)

Idea is to use two array acting as row and col. When you see player 1, add 1 to the row and col array and add -1 if it's player 2. Count the same for diagonal and anti-diagonal element.

```java
class TicTacToe {

    /** Initialize your data structure here. */
    private int[] R; 
    private int[] C;
    private int diag;
    private int anti_diag;
    
    public TicTacToe(int n) {
        R = new int[n]; C = new int[n];
    }
    
    /** Player {player} makes a move at ({row}, {col}).
        @param row The row of the board.
        @param col The column of the board.
        @param player The player, can be either 1 or 2.
        @return The current winning condition, can be either:
                0: No one wins.
                1: Player 1 wins.
                2: Player 2 wins. */
    public int move(int row, int col, int player) {
        int add = (player == 1) ? 1 : -1;
        int size = R.length;
        R[row] += add;
        C[col] += add;
        
        if (row == col)
            diag += add;
        
        if (row + col == size - 1)
            anti_diag += add;
        
        if (Math.abs(R[row]) == size || Math.abs(C[col]) == size || Math.abs(diag) == size || Math.abs(anti_diag) == size)
            return player;
        
        return 0;
    }
}

/**
 * Your TicTacToe object will be instantiated and called as such:
 * TicTacToe obj = new TicTacToe(n);
 * int param_1 = obj.move(row,col,player);
 */
```

time & space - in worst case situation, there would O(N^2) move but the space O(N)

### Given a list of points, determine if they form a symmetric set (symmetric about some vertical line, such that for a point there is another point that is equidistant from the line)
[https://leetcode.com/discuss/interview-experience/365500/Amazon-or-SDE1-or-Santiago-or-Aug-2019-Offer](https://leetcode.com/discuss/interview-experience/365500/Amazon-or-SDE1-or-Santiago-or-Aug-2019-Offer)

### Check if two trees are mirror
[https://www.geeksforgeeks.org/check-if-two-trees-are-mirror/](https://www.geeksforgeeks.org/check-if-two-trees-are-mirror/)

For two trees ‘a’ and ‘b’ to be mirror images, the following three conditions must be true:

1.  Their root node’s key must be same
2.  Left subtree of root of ‘a’ and right subtree root of ‘b’ are mirror.
3.  Right subtree of ‘a’ and left subtree of ‘b’ are mirror.

```java
public class MirrorTree {
    static class TreeNode {
        int val;
        TreeNode left, right;
        TreeNode(int val) { this.val = val; }
    }
    public static void main(String[] args) {
        TreeNode n1 = new TreeNode(1);
        n1.left = new TreeNode(3);
        n1.right = new TreeNode(2);
        n1.right.left = new TreeNode(5);
        n1.right.right = new TreeNode(4);

        TreeNode n2 = new TreeNode(1);
        n2.right = new TreeNode(3);
        n2.left = new TreeNode(2);
        n2.left.left = new TreeNode(4);
        n2.left.right = new TreeNode(5);

        System.out.println(isMirror(n1, n2));
    }
    public static boolean isMirror(TreeNode n1, TreeNode n2) {
        if (n1 == null && n2 == null) return true;
        if (n1 == null || n2 == null) return false;
        if (n1.val != n2.val) return false;

        return isMirror(n1.left, n2.right) && isMirror(n1.right, n2.left);
    }
}
```
Time & space - O(M + N)

Iterative approache: use two queues and store child node differently. One queue stores right then left and another queue stores left then right node.

```java
    public static boolean isMirrorIterative(TreeNode n1, TreeNode n2) {
        Deque<TreeNode> Q1 = new ArrayDeque<>();
        Deque<TreeNode> Q2 = new ArrayDeque<>();
        if (n1 == null && n2 == null) return true;
        if (n1 == null || n2 == null) return false;
        Q1.addLast(n1); Q2.addLast(n2);
        while (!Q1.isEmpty() || !Q2.isEmpty()) {
            TreeNode t1 = (Q1.isEmpty() == true) ? new TreeNode(-1) : Q1.removeFirst();
            TreeNode t2 = (Q2.isEmpty() == true) ? new TreeNode(-1) : Q2.removeFirst();
            if (t1.val != t2.val)
                return false;
            if (t1.val != -1) {
                if (t1.right != null) Q1.addLast(t1.right);
                if (t1.left != null) Q1.addLast(t1.left);
            }
            if (t2.val != -1) {
                if (t2.left != null) Q2.addLast(t2.left);
                if (t2.right != null) Q2.addLast(t2.right);
            }
        }
        return true;
    }
```
Time & space - O(M + N)

###
[https://leetcode.com/problems/symmetric-tree/](https://leetcode.com/problems/symmetric-tree/)

Two trees are a mirror reflection of each other if:

1.  Their two roots have the same value.
2.  The right subtree of each tree is a mirror reflection of the left subtree of the other tree.

```java
class Solution {
    public boolean isSymmetric(TreeNode root) {
        if (root == null) return true;
        return util(root.left, root.right);
    }
    public boolean util(TreeNode n1, TreeNode n2) {
        if (n1 == null && n2 == null) return true;
        if (n1 == null || n2 == null) return false;
        
        return n1.val == n2.val && util(n1.left, n2.right) && util(n1.right, n2.left);
    }
}
```
time & space - O(N)

Iterative approach

```java
public boolean isSymmetric(TreeNode root) {
    Queue<TreeNode> q = new LinkedList<>();
    q.add(root);
    q.add(root);
    while (!q.isEmpty()) {
        TreeNode t1 = q.poll();
        TreeNode t2 = q.poll();
        if (t1 == null && t2 == null) continue;
        if (t1 == null || t2 == null) return false;
        if (t1.val != t2.val) return false;
        q.add(t1.left);
        q.add(t2.right);
        q.add(t1.right);
        q.add(t2.left);
    }
    return true;
}
```

### Print a Organization chart of a company from bottom (starting from a given employee ) to top (root node)

Follow DFS traversal and keep the parent into the Stack. Each node can have it's value and it could have N children (N-ary Tree).

```java
class TreeNode {
	int val;
	Map<Integer, TreeNode> children;
}
```

### Split Array Largest Sum
[https://leetcode.com/problems/split-array-largest-sum/](https://leetcode.com/problems/split-array-largest-sum/)

## Paint House
https://leetcode.com/problems/paint-house/

- the idea is to use the min cost of coloring the house and two adjacent house can't have same color
- update the 2nd house red color cost + include the min cost of painting the previous house i.e 1st house with blue or green color
- update the 2nd house blue color cost + include the min cost of painting the previous house i.e 1st house with red or green color
- update the 2nd house green color cost + include the min cost of painting the previous house i.e 1st house with red or blue color
- do this until nth house and take the min cost of the three color

```java
class Solution {
    public int minCost(int[][] costs) {
        if (costs.length == 0) return 0;

        int n = costs.length;

        for (int i = 1; i < n; i++) {
            costs[i][0] += Math.min(costs[i - 1][1], costs[i - 1][2]);
            costs[i][1] += Math.min(costs[i - 1][0], costs[i - 1][2]);
            costs[i][2] += Math.min(costs[i - 1][0], costs[i - 1][1]);
        }

        return Math.min(Math.min(costs[n - 1][0], costs[n - 1][1]), costs[n - 1][2]);
    }
}
```

Time ~ O(n), space O(1)

## Min Cost Climbing Stairs
https://leetcode.com/problems/min-cost-climbing-stairs/

- DP java bottom-up solution:
- dp[i] represents the minimum cost that we can reach i-th stair.
- We can either come from (i - 2)-th stair or (i - 1)-th stair.
- Notice: return the minimum of the last and second-last stair's cost.

```java
class Solution {
    public int minCostClimbingStairs(int[] cost) {
        if (cost.length == 0) return 0;
        
        int n = cost.length;
        int[] dp = new int[n];
        dp[0] = cost[0]; dp[1] = cost[1];
        
        for (int i = 2; i < n; i++) {
            dp[i] = Math.min(dp[i - 1] + cost[i], dp[i - 2] + cost[i]);
        }
        
        return Math.min(dp[n - 1], dp[n - 2]);
    }
}
```
Time & Space ~ O(n)

## Paint Fence
https://leetcode.com/problems/paint-fence/

Approach: https://www.youtube.com/watch?v=deh7UpSRaEY
https://leetcode.com/problems/paint-fence/discuss/71156/O(n)-time-java-solution-O(1)-space

- when we have two fench (n = 2) and K number of color, we have two approach to color them 1) make same color and 2) make diff color
- for same color: same = (1st fench will have K options to color) * (but 2nd fench will have 1 option as we are keeping the same color) = K * 1
- for diff color: diff = (1st fench will have K options) * (2nd fench will have K - 1 options to make two different color on the 2 fenches) = K * (K - 1) posibilities
- when we have n = 3, i.e 3 fenches then the posibilities of different coloring would depend on whatever options we had on the previoues 2 fenches, it could be possible that 1st and 2nd fenches have same color or different color: 
- diff for 3rd fench = (same + diff) * (K - 1)
- same for 3rd fench = if the previous two fenches have same color, we can't use it here as per the policy but previous two fenches could have different color, so that we can choose the same color for 2nd and 3rd fench
- same for 3rd fench = (diff) * 1
- at the end total possibilites are = same + diff of nth post

```java
class Solution {
    public int numWays(int n, int k) {
        if (n == 0) return 0;
        if (n == 1) return k;

        int same = k * 1, diff = k * (k - 1);
        if (n == 2) return same + diff;

        for (int i = 3; i <= n; i++) {
            int same_current = diff * 1;
            int diff_current = (same + diff) * (k - 1);
            same = same_current; diff = diff_current;
        }
        return same + diff;
    }
}
```
Time ~ O(n), space O(1)

## Vowel Spellchecker
https://leetcode.com/problems/vowel-spellchecker/

- store the word list into a Set for contant lookup
- convert the word in word list into lowercase and store them as key (lowercase word) and value as the original word
- convert the vowel with asterics and store this converted word as key and value as the original word

```java
class Solution {

    private Set<String> original_list = new HashSet<>();
    private Map<String, String> lowercase_list = new HashMap<> ();
    private Map<String, String> vowelconverted_list = new HashMap<> ();

    public String[] spellchecker(String[] wordlist, String[] queries) {
        for (String w : wordlist) {
            //store the word list into a Set for contant lookup
            original_list.add(w);
            //convert the word in word list into lowercase and 
            //store them as key (lowercase word) and value as the original word
            String lowercase = w.toLowerCase();
            lowercase_list.putIfAbsent(lowercase, w);
            //convert the vowel with asterics and store this converted word as key and value as the original word
            String vowel = convertVowel(lowercase);
            vowelconverted_list.putIfAbsent(vowel, w);
        }

        String[] res = new String[queries.length];

        for(int i = 0; i < res.length; i++) {
            res[i] = solve(queries[i]);
        }
        return res;
    }

    public String solve(String str) {
        if (original_list.contains(str))
            return str;
            
        String lower = str.toLowerCase();
        if (lowercase_list.containsKey(lower))
            return lowercase_list.get(lower);
            
        String vowels = convertVowel(lower);
        if (vowelconverted_list.containsKey(vowels))
            return vowelconverted_list.get(vowels);
            
        return "";
    }

    public String convertVowel(String str) {
        char[] ch = str.toCharArray();
        for (int i = 0; i < ch.length; i++) {
            if (ch[i] == 'a' || ch[i] == 'e' || ch[i] == 'i' || ch[i] == 'o' || ch[i] == 'u')
                ch[i] = '*';
        }
        return new String(ch);
    }
}
```

Time & Space ~ O(N) where N is the total content of the wordlist and queries

## Search in a Sorted Array of Unknown Size
https://leetcode.com/problems/search-in-a-sorted-array-of-unknown-size/

- as the size is unknown, start with start index 0 and end index 2. if the target is not found within [0,2] range, double the range in every half until not found

```java
class Solution {
    public int search(ArrayReader reader, int target) {
        int l = 0, r = 2;

        while (l <= r) {
            int l1 = l, r1 = r;
            while (l1 <= r1) {
                int mid = (l1 + r1) / 2;
                if (reader.get(mid) == target)
                    return mid;
                else if (reader.get(mid) < target)
                    l1 = mid + 1;
                else
                    r1 = mid - 1;
            }
            l = l + 1; r = 2 * r;
        }
        return -1;
    }
}
```

time ~ (klogn) where k times we have double the range to do the binary search

**optimize code**

```java
class Solution {
    public int search(ArrayReader reader, int target) {
        int l = 0, r = 1;

        //find the boundaries
        while (reader.get(r) < target) {
            l = r; r = r * 2;
        }

        //use binary search to find the value
        while (l <= r) {
            int mid = (l + r) / 2;
            if (reader.get(mid) == target)
                return mid;
            else if (reader.get(mid) < target)
                l = mid + 1;
            else
                r = mid - 1;
        }
        return -1;
    }
}
```

time ~ (logn), space O(1)

## Find the nearest smaller numbers on left side in an array
https://www.geeksforgeeks.org/find-the-nearest-smaller-numbers-on-left-side-in-an-array/

- use a stack to store all the nearest smaller numbers seen while traversing from left to right
- if the stack is empty push the array index (use index, as there could be dups value)
- if the top of the stack is larger than the current array element, then top of the stack can't the nearest smaller number, so pop it
- if the top of the stack is smaller than the current array element, return the result and push the current array element

```java
class Solution {
    public static void main (String[] args) {
        Deque<Integer> stack = new ArrayDeque<> ();
        int[] a = {1, 6, 4, 10, 2, 5};
        int[] res = new int[a.length];
        Arrays.fill(res, -1);

        for (int i = 0; i < a.length; i++) {            
            while (!stack.isEmpty() && a[stack.peekFirst()] > a[i])
                stack.removeFirst();
            
            if (stack.isEmpty())
                stack.addFirst(i);

            if (!stack.isEmpty() && a[stack.peekFirst()] < a[i]) {
                res[i] = a[stack.peekFirst()];
                stack.addFirst(i);
            }
        }
    }
}
```

time & space ~ O(n)

## Number of Islands
https://leetcode.com/problems/number-of-islands/

- if the cell value is '1', start DFS call and go horizontally and vertically until no other place to go
- change the grid value '1' to '0' to mark the grid is visited
- count the initial dfs call
- use global variable to store the grid data structure to reduce the the memory footprint during 
recursive call

```java
class Solution {

    char[][] grid;
    
    public int numIslands(char[][] grid) {
        if (grid == null || grid.length == 0)
            return 0;
        
        this.grid = grid;
        int count = 0, numRows = grid.length, numCols = grid[0].length;

        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numCols; j++) {
                if (grid[i][j] == '1') {
                    dfs(i, j, numRows, numCols);
                    count++;
                }
            }
        }

        return count;
    }

    public void dfs(int i, int j, int numRows, int numCols) {
        /* base condition to stop the recursion */
        if (i < 0 || i >= numRows || j < 0 || j >= numCols || grid[i][j] != '1')
            return;
        
        /* mark the grid visited */
        grid[i][j] = '0';

        /* check adjacent cells horizontally and vertically */
        dfs(i - 1, j, numRows, numCols);
        dfs(i + 1, j, numRows, numCols);
        dfs(i, j - 1, numRows, numCols);
        dfs(i, j + 1, numRows, numCols);
    }
}
```

Time & Space - O(M * N)

## K-Closest Point to Origin
https://leetcode.com/problems/k-closest-points-to-origin/

- idea is sort the point according to increasing distance from origin
- take the first K number of points

The advantages of this solution are short, intuitive and easy to implement. The disadvatages of this solution are not very efficient and have to know all of the points previously, and it is unable to deal with real-time(online) case, it is an off-line solution.

Now generally sorting takes O(NlogN) in runtime, but `QuickSelect` algorithm can be a better choice.
The avg runtime of quickselect is linear O(N).

- quickselect uses the same principle as quick sort algorithm i.e. find the pivot and move the
element lower than pivot on left side and larger element on the right side.
- repeat this until the pivot position is not equals to K and **elements returned can be in any order**

```java
class Solution {
    public int[][] kClosest(int[][] points, int K) {
        /* edge cases */
        if (points == null || points.length == 0 || K == points.length)
            return points;

        int left = 0, right = points.length - 1;

        /* 
        if the partition index equals to K then all the element to its left are
        smaller than the pivot and which is we want. otherwise if the pivot is smaller
        then K then we need to find more points on the right side.
        */
        while (left <= right) {
            int mid = partition(points, left, right);
            if (mid == K)
                break;
            else if (mid < K)
                left = mid + 1;
            else
                right = mid - 1;
        }

        return Arrays.copyOfRange(points, 0, K);
    }

    /*
    choose the first element as pivot and
    compare with all the element in the right side which suppose to be
    larger than the pivot. if this is not the case, move the smaller element
    to the left. Same idea for large element found in left side
    */
    public int partition(int[][] points, int left, int right) {
        int[] pivot = points[left];

        while (left < right) {
            while (left < right && compare(points[right], pivot) >= 0)
                right--;
            points[left] = points[right]; // small element move to left side

            while (left < right && compare(pivot, points[left]) > 0)
                left++;
            points[right] = points[left]; // large element move to right side
        }

        points[left] = pivot; // position the pivot
        return left;
    }

    /* return the difference */
    public int compare(int[] a, int[] b) {
        return a[0]*a[0] + a[1]*a[1] - b[0]*b[0] - b[1]*b[1];
    }
}
```

Time and Space - O(N) avg case run time and for space O(N) in worst case.

**Note**: Because in the quicksort, we need to take care of two sides of the pivot. But in quickselect, we only focus on the side the target object should be. So, in optimal case, the running time of quicksort is (n + 2 * (n/2) + 4 * (n/4)...), it has NlogN iterations. Instead, the running time of quickselect would be (n + n/2 + n/4 +...) it has logn iterations as well. Therefore, the running time of quicksort is O(nlogn), instead, the running time of quickselect of quickselect is O(n)

## Reorder data in log files
https://leetcode.com/problems/reorder-data-in-log-files/

- sort the data according to the requirement

```java
class Solution {
    public String[] reorderLogFiles(String[] logs) {
        Arrays.sort(logs, (a, b) -> {
            /* split the string based on space delimiter */
            String[] str_a = a.split(" ", 2);
            String[] str_b = b.split(" ", 2);

            /* check if digit after identifier */
            boolean isDigit1 = Character.isDigit(str_a[1].charAt(0));
            boolean isDigit2 = Character.isDigit(str_b[1].charAt(0));

            /* if both character after identifier is not digit */
            if (!isDigit1 && !isDigit2) {
                int cmp = str_a[1].compareTo(str_b[1]);
                if (cmp != 0)
                    return cmp;
                /* in case of tie use identifier for lexicograph sorting */
                return str_a[0].compareTo(str_b[0]);
            }

            /* handle if any or both of the character is digit after identifier */
            return isDigit1 ? (isDigit2 ? 0 : 1) : -1;
        });

        return logs;
    }
}
```

Time - O(NlogN) and space O(1).

## Longest Palindromic Substring
https://leetcode.com/problems/longest-palindromic-substring/

Brute force method is to take the substring and check whether it is palindrome or not and then find out
the max length of palindrome. Runtime is O(N^3)

```java
public class Main {
    public static void main(String[] args) {
        String str = "babad";
        int max = Integer.MIN_VALUE;
        String res = "";
        
        for (int i = 0; i < str.length(); i++) {
            for (int j = i; j < str.length(); j++) {
                if (isPalindrome(str.substring(i, j + 1))) {
                    if (max < j - i + 1) {
                        max = j - i + 1;
                        res = str.substring(i, j + 1);
                        System.out.println(str.substring(i, j + 1) + " -> " + (j - i + 1));   
                    }
                }
            }
        }
        
        System.out.println("output -> " + res);
    }
    
    public static boolean isPalindrome(String str) {
        int i = 0, j = str.length() - 1;
        while (i <= j) {
            if (str.charAt(i) == str.charAt(j)) {
                i++;
                j--;
            }
            else
                return false;
        }
        return true;
    }
}
```

Use dynamic programming to reduce the runtime but it will increase the space upto O(N^2).
Another option is expand around center:

In fact, we could solve it in O(n^2) time using only constant space.

We observe that a palindrome mirrors around its center. Therefore, a palindrome can be expanded from its center, and there are only `2n - 1` such centers.

You might be asking why there are `2n - 1` but not n centers? The reason is the center of a palindrome can be in between two letters. Such palindromes have even number of letters (such as "abba") and its center are between the two 'b's.

```java
class Solution {
    public String longestPalindrome(String s) {
        if (s == null || s.length() == 0)
            return s;

        int start = 0, end = 0;
        for (int i = 0; i < s.length(); i++) {
            int odd_len = expand(s, i, i); // find the expand length if it's odd position
            int even_len = expand(s, i, i + 1); // find the expand length if its even position
            int len = Math.max(odd_len, even_len);

            /* start and end position is halfway of the len distributed around i */
            if (len > end - start) {
                start = i - (len - 1) / 2;
                end = i + (len / 2);
            }
        }

        return s.substring(start, end + 1);
    }

    /* 
    expand from middle and move i to its left and j to its right
    until i and j are exhausted or doesnt have any matching char
     */
    public int expand(String s, int i, int j) {
        while (i >= 0 && j < s.length() && s.charAt(i) == s.charAt(j)) {
            i--; j++;
        }
        return j - i - 1;
    }
}
```

Time ~ O(N^2) and Space ~ O(1)

**optimized code: manacher's algorithm**

soln: https://web.archive.org/web/20150313044313/http://articles.leetcode.com/2011/11/longest-palindromic-substring-part-ii.html



## Copy List with Random Pointer
https://leetcode.com/problems/copy-list-with-random-pointer/

Cloning essentially means creating a new node.

Read this article about all the steps https://leetcode.com/articles/copy-list-with-random-pointer/

`Approach 1: Recursion`

```java
class Solution {
    
    Map<Node, Node> map = new HashMap<> (); // store node and it's cloned node
    
    public Node copyRandomList(Node head) {
        if (head == null) return head;

        /* get the cloned node if already created */
        if (map.containsKey(head))
            return map.get(head);
        else {
            Node node = new Node(head.val);
            map.put(head, node);
            /* recursively call the next and random pointer */
            node.next = copyRandomList(head.next);
            node.random = copyRandomList(head.random);
            return node;
        }
    }
}
```
Time and Space ~ O(N)

`Approach 2: Iterative with O(N) space`

```java
class Solution {

    Map<Node, Node> map = new HashMap<> (); // store node and it's cloned node

    public Node copyRandomList(Node head) {
        if (head == null) return null;

        Node oldNode = head;
        Node newNode = new Node(head.val);
        map.put(oldNode, newNode);

        while (oldNode != null) {
            newNode.random = getClonedNode(oldNode.random);
            newNode.next = getClonedNode(oldNode.next);

            oldNode = oldNode.next;
            newNode = newNode.next;
        }
        return map.get(head);
    }

    public Node getClonedNode(Node node) {
        if (node == null) return null;
        
        if (!map.containsKey(node)) {
            Node temp = new Node(node.val);
            map.put(node, temp);
        }
        return map.get(node);
    }
}
```

Time & Space - O(N)

`Approach 3: Iterative constant space`

```java
class Solution {
    public Node copyRandomList(Node head) {
        if (head == null) return head;

        Node node = head;
        /* interweaving based on next pointer */
        while (node != null) {
            Node newNode = new Node(node.val);
            /*  insert the cloned node with original node A -> B now A -> A` -> B */
            newNode.next = node.next;
            node.next = newNode;
            node = newNode.next;
        }

        /* weave the node based on the random pointer */
        node = head;
        while (node != null) {
            node.next.random = (node.random != null) ? node.random.next : null;
            node = node.next.next;
        }

        /* unweaving the cloned noded from the original node */
        Node headCloned = head.next, head_old = head.next;
        while (head != null) {
            head.next = head.next.next;
            headCloned.next = (headCloned.next != null) ? headCloned.next.next : null;
            head = head.next;
            headCloned = headCloned.next;
        }
        return head_old;
    }
}
```

Time ~ O(N) and Space ~ O(1)

## Trapping Rain Water
https://leetcode.com/problems/trapping-rain-water/

The idea is from the current bar location, find the maximum bar height in left and right side.
Choose the minimum height amoung them and substract from the current bar height. Do this for
all the bar height.

```java
class Solution {
    public int trap(int[] height) {
        
        if (height == null || height.length == 0) return 0;
        
        int len = height.length;
        int[] left = new int[len]; int[] right = new int[len];
        left[0] = height[0]; right[len - 1] = height[len - 1];
        int sum = 0;

        /* take the max between current bar height and previously seen max height */
        for (int i = 1; i < len; i++) left[i] = Math.max(left[i - 1], height[i]);
        for (int i = len - 2; i >= 0; i--) right[i] = Math.max(right[i + 1], height[i]);

        /* substract from current bar height from the min of the left and right side max height */
        for (int i = 0; i < len; i++) {
            sum += (Math.min(left[i], right[i]) - height[i] > 0) ? Math.min(left[i], right[i]) - height[i] : 0;
        }
        return sum;
    }
}
```

Time & Space ~ O(N) for runtime its three pass to complete the task. We can improve the three pass to 
single pass and use constant space.

```java
class Solution {
    public int trap(int[] height) {
        if (height == null || height.length == 0) return 0;
        
        int left_max = 0, right_max = 0, sum = 0, left = 0, right = height.length - 1;
        
        while (left < right) {
            if (height[left] <= height[right]) {
                if (height[left] > left_max)
                    left_max = height[left];
                else
                    sum += (left_max - height[left]);
                left++;
            }
            else {
                if (height[right] > right_max)
                    right_max = height[right];
                else
                    sum += right_max - height[right];
                right--;
            }
        }

        return sum;
    }
}
```

Time and Space ~ O(N) and O(1)

## Find All Anagrams in a String
https://leetcode.com/problems/find-all-anagrams-in-a-string/

**Brute Force**

- for every character take the substring of size P string
- sort the substring and compare with sorted P string
- if there is a match then anagram found at the starting character position
- continue to follow above steps for the remaining character
- below code can handle duplicate character in search string (P)

S - "cbaebabacd" and P - "abc"
at index 0 - "cba" substring from S and after sorting substring of S and P, we have "abc" and anagram found

```java
class Solution {
    public List<Integer> findAnagrams(String s, String p) {
        List<Integer> res = new LinkedList<> ();

        /*
        edge cases: if source string size is smaller than search string
        and if any string is null or size is zero
        */
        if (s == null || s.length() == 0 || p == null || p.length() == 0 || s.length() < p.length())
            return res;

        int n = s.length(), m = p.length();
        char[] searchArray = p.toCharArray();
        Arrays.sort(searchArray); // sort the search string

        for (int i = 0; i < n - m + 1; i++) {
            String str = s.substring(i, i + m);
            char[] ch = str.toCharArray();
            Arrays.sort(ch);

            //compare the substring with P
            int j = 0;
            for (j = 0; j < m; j++) {
                if (ch[j] != searchArray[j])
                    break;
            }

            /* anagram found after char by char matching */
            if (j == m)
                res.add(i);
        }
        return res;
    }
}
```

Time - O(N * MlogM) : N - lenght of the source string and M is the length of the search string. To sort the
search string, it takes MlogM time complexity
Space - O(1)

**Study this article**
https://leetcode.com/problems/find-all-anagrams-in-a-string/discuss/92007/Sliding-Window-algorithm-template-to-solve-all-the-Leetcode-substring-search-problem

```java
class Solution {
    public List<Integer> findAnagrams(String s, String p) {
        List<Integer> res = new LinkedList<> ();
        if (s == null || s.length() == 0 || p == null || p.length() == 0 || s.length() < p.length())
            return res;

        Map<Character, Integer> map = new HashMap<> (); // char freq of P string
        for (int i = 0; i < p.length(); i++)
            map.put(p.charAt(i), map.getOrDefault(p.charAt(i), 0) + 1);

        /* setup the window search */
        int counter = map.size(), start = 0, end = 0;

        while (end < s.length()) {
            char ch = s.charAt(end);

            /*
            if the char found within the P string, reduce the freq of the map
            reduce the counter if the freq of this char is zero
            */
            if (map.containsKey(ch)) {
                map.put(ch, map.get(ch) - 1);
                if (map.get(ch) == 0)
                    counter--;
            }
            end++;

            /*
            if counter is zero, increment the start pointer
            save the output into the linkedlist (res) if the end and start pointer different equals
            to the length of the P string
            */
            while (counter == 0) {
                char chSearch = s.charAt(start);
                if (map.containsKey(chSearch)) {
                    map.put(chSearch, map.get(chSearch) + 1);
                    if (map.get(chSearch) > 0)
                        counter++;
                }

                if (end - start == p.length())
                    res.add(start);

                start++;
            }
        }

        return res;
    }
}
```

Time - O(N) , every character in S strings runs exactly one time
Space - O(M), in worst case, the hashmap will occupify P length of memory

Rewriting the above code using int[] array rather than hashmap

```java
class Solution {
    public List<Integer> findAnagrams(String s, String p) {
        List<Integer> res = new LinkedList<> ();
        if (s == null || s.length() == 0 || p == null || p.length() == 0 || s.length() < p.length())
            return res;

        int[] map = new int[256];
        for (Character c : p.toCharArray())
            map[c]++;

        int counter = p.length(), start = 0, end = 0;

        while (end < s.length()) {
            if (map[s.charAt(end++)]-- > 0) counter--;

            while (counter == 0) {
                if (map[s.charAt(start)]++ == 0) counter++;
                if (end - start == p.length()) res.add(start);
                start++;
            }
        }

        return res;
    }
}
```

Time - O(N) , every character in S strings runs exactly one time
Space - O(1)

## Search a 2D Matrix II
https://leetcode.com/problems/search-a-2d-matrix-ii/

**Brute Force**
- check the start and end value of each row and if the search numner within the range
- do the binary search
- if not found go to the next row

```java
class Solution {
    public boolean searchMatrix(int[][] matrix, int target) {
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) return false;
        int r = matrix.length, c = matrix[0].length;

        /* do a binary search if the target within the range of that row */
        for (int i = 0; i < r; i++) {
            if (target >= matrix[i][0] && target <= matrix[i][c - 1]) {
                if (find(matrix[i], target, 0, c - 1))
                    return true;
            }
        }

        return false;
    }

    /* binary search */
    public boolean find(int[] a, int target, int start, int end) {
        while (start <= end) {
            int mid = (start + end) / 2;
            if (a[mid] == target)
                return true;
            else if (target > a[mid])
                start = mid + 1;
            else
                end = mid - 1;
        }
        return false;
    }
}
```

Time - R * log C where R number of rows and C number of columns. [logC due to the binary search]
Space - O(1)

Check the article: https://leetcode.com/problems/search-a-2d-matrix-ii/solution/
approach 4

```java
class Solution {
    public boolean searchMatrix(int[][] matrix, int target) {
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) return false;
        int r = 0, c = matrix[0].length - 1;

        while (r < matrix.length && c >= 0) {
            if (target == matrix[r][c])
                return true;
            else if (target > matrix[r][c])
                r++;
            else
                c--;
        }
        return false;
    }
}
```

## Snakes and Ladders
https://leetcode.com/problems/snakes-and-ladders/

explanation for BFS: https://leetcode.com/articles/snakes-and-ladders/

```java
class Solution {
    public int snakesAndLadders(int[][] board) {
        if (board == null || board.length == 0) return -1;
        
        int N = board.length;
        // store already visited grid and min distance covered from starting grid [5][0]
        Map<Integer, Integer> visited = new HashMap<> ();
        Deque<Integer> Q = new ArrayDeque<> ();
        visited.put(1, 0);
        Q.addLast(1);

        while (!Q.isEmpty()) {
            int pos = Q.removeFirst();
            if (pos == N * N) return visited.get(pos);

            for (int i = pos + 1; i <= Math.min(pos + 6, N * N); i++) {
                int[] rc = getIndexPos(i, N);
                int posFinal = (board[rc[0]][rc[1]] == -1) ? i : board[rc[0]][rc[1]];

                if (!visited.containsKey(posFinal)) {
                    visited.put(posFinal, visited.get(pos) + 1);
                    Q.addLast(posFinal);
                }
            }
        }

        return -1;
    }

    /* convert grid number into row and col index position */
    public int[] getIndexPos(int s, int N) {
        int quosient = (s - 1) / N;
        int remaining = (s - 1) % N;
        int row = N - 1 - quosient;
        int col = (row % 2 != N % 2) ? remaining : N - 1 - remaining;
        return new int[] {row, col};
    }
}
```

Time & Space ~ O(N * N)

Reduce the payload of the HashMap by using a boolean array to store the grid cell is visited or not

```java
class Solution {
    public int snakesAndLadders(int[][] board) {
        if (board == null || board.length == 0) return -1;

        int N = board.length, steps = 1;
        boolean[] visited = new boolean[N * N];
        Deque<Integer> Q = new ArrayDeque<> ();
        visited[0] = true;
        Q.addLast(0);

        while (!Q.isEmpty()) {
            int size = Q.size();
            for (int i = 0; i < size; i++) {
                int pos = Q.removeFirst();
                for (int j = pos + 1; j <= Math.min(pos + 6, N * N - 1); j++) {
                    if (visited[j]) continue;
                    visited[j] = true;
                    if (j == N * N - 1) return steps;

                    // convert the grid number into row and col index position
                    int r = N - 1 - j / N;
                    int c = ((j / N) % 2 == 0) ? (j % N) : (N - 1 - j % N);

                    if (board[r][c] == -1)
                        Q.addLast(j);
                    else {
                        if (board[r][c] == N * N) return steps;
                        if (!visited[board[r][c] - 1])
                            Q.addLast(board[r][c] - 1);
                    }
                }
            }
            steps++;
        }
        return -1;
    }
}
```

Time & Space ~ O(N * N) in worst case

## Max Points on a Line
https://leetcode.com/problems/max-points-on-a-line/

The idea is very simple : draw the lines passing through the point i and each of the next points
https://leetcode.com/articles/max-points-on-a-line/

- calculate slope delta y / delta x of two points. Now the tricky part when we have horizontal and vertical
lines
- better to calculate GCD of delta x and delta y
- store the delta X as key in hashtable and value would be another map with delta Y as key and number
of occurances as value
- use global pointer to find the max count

```java
class Solution {
    public int maxPoints(int[][] points) {
        Map<Integer, Map<Integer, Integer>> map = new HashMap<> ();
        int result = 0;
        
        if (points == null || points.length == 0) return 0;
        if (points.length == 1) return 1;
        
        for (int i = 0; i < points.length - 1; i++) {
            // store the maximum count and overlap for this point[i] and reset for every new points[i]
            int count = 0, overlap = 0;
            map.clear();
            
            for (int j = i + 1; j < points.length; j++) {
                int x = points[i][0] - points[j][0];
                int y = points[i][1] - points[j][1];

                // same point
                if (x == 0 && y == 0) {
                    overlap++;
                    continue;
                }

                int gcd = generateGCD(x, y);

                if (gcd != 0) {
                    x /= gcd; y /= gcd;
                }
                
                if (map.containsKey(x)) {
                    Map<Integer, Integer> temp_map = map.get(x);
                    temp_map.put(y, temp_map.getOrDefault(y, 0) + 1);
                    map.put(x, temp_map);
                }
                else {
                    Map<Integer, Integer> temp_map = new HashMap<> ();
                    temp_map.put(y, 1);
                    map.put(x, temp_map);
                }

                count = Math.max(count, map.get(x).get(y));
            }
            result = Math.max(result, count + overlap + 1); // +1 for point[i] itself
        }
        return result;
    }

    public int generateGCD(int a, int b) {
        if (b == 0) return a;
        else return generateGCD(b, a % b);
    }
}
```

Time & Space ~ O(N^2) and O(N)

**Space optimize solution**

- consider two adjacent points and if they are not the same points count as 2
- find the delta X and delta Y from two adjacenet point
- search through all the points (other than these two adjacent point) and check the slope is same or not
- delta X1 and Y1 for A -> B points and delta X2 and Y2 for A -> C points
- to create a single line using A -> B -> C if delta X1 * delta Y2 == delta Y1 * delta X2
- if same increment the local max count and update global max count

```java
class Solution {
    
    private int ans = 0;
    
    public int maxPoints(int[][] points) {        
        if (points == null || points.length == 0) return 0;
        if (points.length <= 2) return points.length;

        for (int i = 0; i < points.length - 1; i++) {
            int x = points[i + 1][0] - points[i][0];
            int y = points[i + 1][1] - points[i][1];

            if (x != 0 || y != 0)
                solve(points, x, y, i);
        }

        return ans == 0 ? points.length : ans;
    }

    // make sure to use long for delta X and Y and multiplication can overflow for int
    public void solve(int[][] points, long delta_x, long delta_y, int base) {
        int count = 2;
        for (int i = 0; i < points.length; i++) {
            if (i != base && i != base + 1) {
                // delta X1 * delta Y2 == delta Y1 * delta X2 for three points to be on a line
                if ((points[i][0] - points[base][0]) * delta_y == (points[i][1] - points[base][1]) * delta_x)
                    count++;
            }
        }
        ans = Math.max(ans, count);
    }
}
```

Time & Space ~ O(N^2) and O(1)

## Reverse Nodes in k-Group
https://leetcode.com/problems/reverse-nodes-in-k-group/

- count the number of nodes in the list and calculate the number of groups `round = len / K`
- iterate `round` until it exhausted
- in each round we need to reverse all the nodes. if K = 3 we need to reverse 2 linking A -> B -> C
- reversal process: create a dummy node and connect with the head of the node i.e dummy -> A -> B -> C
- within K rotation, point A's next to the B's next and B's next points to dummy's next

```java
class Solution {
    public ListNode reverseKGroup(ListNode head, int k) {
        if (head == null) return head;
        int len = 0, round = 0;
        ListNode dummy = new ListNode(-1);
        ListNode ptr = head, pre = dummy;
        dummy.next = head;

        while (ptr != null) { ptr = ptr.next; len++; }

        round = len / k;
        if (round == 0) return head;

        for (int i = 0; i < round; i++) {
            ListNode start = pre.next;
            ListNode end = start.next;

            for (int j = 0; j < k - 1; j++) {
                start.next = end.next;
                end.next = pre.next;
                pre.next = end;
                end = start.next;
            }
            pre = start;
        }
        return dummy.next;
    }
}
```

Time & Space ~ O(N) and O(1)

## Minimum Cost to Merge Stones
https://leetcode.com/problems/minimum-cost-to-merge-stones/

- use backtracting to check all possible options and find the minimum cost

```java
class Solution {

    private int min = Integer.MAX_VALUE;

    public int mergeStones(int[] stones, int K) {
        List<Integer> li = new LinkedList<> ();
        for (int i : stones) li.add(i);
        util(li, K, 0);
        return min;
    }

    public void util(List<Integer> list, int K, int sum) {
        if (list.size() == 1) min = Math.min(min, sum);
        if (list.size() != 1 && list.size() < K) {
            min = -1; return;
        };

        for (int i = 0; i < list.size() - K + 1; i++) {
            List<Integer> temp = new LinkedList<> ();

            int index = 0;
            while (index < i) { // copy left non-merge item
                temp.add(list.get(index));
                index++;
            }

            //merge cells
            int merge = i, localSum = 0;
            while(merge < i + K) {
                localSum += list.get(merge);
                merge++;
            }
            temp.add(localSum);

            //add rest of the cells
            index = i + K;
            while (index < list.size()) {
                temp.add(list.get(index));
                index++;
            }

            util(temp, K, sum + localSum);
        }
    }
}
```

Time & Space ~ exponenial

Dynamic Programming

`stones[i] ~ stones[j]` will merge as much as possible. Finally `(j - i) % (K - 1) + 1` piles will be left. It's less than K piles and no more merger happens.

`dp[i][j]` means the minimum cost needed to merge `stones[i] ~ stones[j]`.

```java
class Solution {
    public int mergeStones(int[] stones, int K) {
        int n = stones.length;
        if ((n - 1) % (K - 1) > 0) return -1;
        
        int[] prefixSum = new int[n + 1];
        int[][] dp = new int[n][n];
        
        for (int i = 1; i < prefixSum.length; i++)
            prefixSum[i] = prefixSum[i - 1] + stones[i - 1];
        
        for (int m = K; m <= n; m++) {
            for (int i = 0; i <= n - m; i++) {
                int j = i + m - 1;
                dp[i][j] = Integer.MAX_VALUE;
                
                for (int mid = i; mid < j; mid += K - 1) {
                    dp[i][j] = Math.min(dp[i][j], dp[i][mid] + dp[mid + 1][j]);
                }
                
                if ((j - i) % (K - 1) == 0)
                    dp[i][j] += prefixSum[j + 1] - prefixSum[i];
            }
        }
        
        return dp[0][n - 1];
    }
}
```

Time O(N^3/K) Space O(N^2)

## Last Stone Weight
https://leetcode.com/problems/last-stone-weight/

- use priority queue and add all the numbers in reverse increasing order
- remove two number and add them back their abs difference if it's not zero

```java
class Solution {
    public int lastStoneWeight(int[] stones) {
        PriorityQueue<Integer> pq = new PriorityQueue<> ((a, b) -> { 
            return b - a;
        });
        
        if (stones == null || stones.length == 0) return 0;
        
        // insert the weights into pq
        for (int i : stones) pq.offer(i);
        
        while (pq.size() > 1) {
            int x = pq.poll(), y = pq.poll();
            
            if (x == y)
                continue;
            else
                pq.offer(Math.abs(x - y));
        }
        
        if (pq.size() == 0)
            return 0;
        else
            return pq.poll();
    }
}
```

Time & Space ~ O(NlogN) and O(N)

- If the number of stones with the same size is even or zero, these stones can be totally destroyed pair by pair or there is no such size stone existing, we can just ignore this situation.      
- When the number of stones with the same size is odd, there should leave one stone which is to smash with the smaller size one.

```java
class Solution {
    public int lastStoneWeight(int[] stones) {
        int[] bucket = new int[1001];
        
        if (stones == null || stones.length == 0) return 0;
        // store stones into bucket
        for (int i = 0; i < stones.length; i++)
            bucket[stones[i]]++;

        int end = bucket.length - 1;

        /* 
        if the bucket index frequency is even number then the value will be destroyed.
        otherwise, start finding the next valid frequency and add the frequency by finding the diff
        */
        while (end > 0) {
            if (bucket[end] % 2 != 0) {
                int start = end - 1;
                while (start > 0 && bucket[start] == 0)
                    start--;

                if (bucket[start] == 0) break;
                bucket[start]--;
                bucket[end - start]++;
            }
            end--;
        }

        return end;
    }
}
```

Time & Space ~ O(N) and O(1)

## Rotting Oranges
https://leetcode.com/problems/rotting-oranges/

```java
class Solution {
    public int orangesRotting(int[][] grid) {
        if (grid == null || grid.length == 0) return 0;
        
        int freshOrangeCount = 0, time = 0, rows = grid.length, cols = grid[0].length;
        int[][] dirs = {{1,0}, {0,1}, {-1,0}, {0,-1}};
        
        // count the fresh orange
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (grid[i][j] == 1)
                    freshOrangeCount++;
            }
        }

        if (freshOrangeCount == 0) return time;

        /*
        in each iteration, check the rotten orange and reduce the fresh orange count if it's 
        adjacent to the rotten one. Mark the newer rotten one as -2 and set them as 2 in later time
        which avoids any requirement of additional storage space
        */
        while (freshOrangeCount >= 0) {
            int prev = freshOrangeCount;
            
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < cols; j++) {
                    // update adjacent fresh oranges
                    if (grid[i][j] == 2) {

                        for (int k = 0; k < dirs.length; k++) {
                            int x = i + dirs[k][0], y = j + dirs[k][1];

                            if (x >= 0 && x < rows && y >= 0 && y < cols && grid[x][y] == 1) {
                                grid[x][y] = -2;
                                freshOrangeCount--;
                            }
                        }
                    }
                }
            }

            if (prev == freshOrangeCount)
                return -1;
            if (freshOrangeCount == 0)
                return time + 1;

            // set the original value
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < cols; j++) {
                    if (grid[i][j] == -2)
                        grid[i][j] = 2;
                }
            }
            
            time++;
        }

        return time;
    }
}
```

Time & Space ~ O(K * M * N) where K number of fresh orange, M number of rows and N number of cols. O(1) for space

- Use BFS

```java
class Solution {
    public int orangesRotting(int[][] grid) {
        if (grid == null || grid.length == 0) return 0;
        
        int freshOrangeCount = 0, time = 0, rows = grid.length, cols = grid[0].length;
        int[][] dirs = {{1,0}, {0,1}, {-1,0}, {0,-1}};
        Deque<int[]> Q = new ArrayDeque<> ();
        
        // count the fresh orange
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (grid[i][j] == 1)
                    freshOrangeCount++;
                if (grid[i][j] == 2)
                    Q.addLast(new int[] {i, j});
            }
        }

        while (!Q.isEmpty()) {
            int size = Q.size();
            for (int k = 0; k < size; k++) {
                int[] pos = Q.removeFirst();

                for (int p = 0; p < dirs.length; p++) {
                    int x = pos[0] + dirs[p][0], y = pos[1] + dirs[p][1];

                    if (x >= 0 && x < rows && y >= 0 && y < cols && grid[x][y] == 1) {
                        grid[x][y] = 2;
                        freshOrangeCount--;
                        Q.addLast(new int[] {x, y});
                    }
                }
            }
            time++;
        }

        if (freshOrangeCount == 0)
            return time;
        return -1;
    }
}
```

Time ~ O(M * N), Space ~ O(M * N)

## Design Snake Game
https://leetcode.com/problems/design-snake-game/

- use double ended queue to add new position to the head and remove the tail in O(1) time
- use set to track the current snake body positions and new head position is clashing with its own body or not
- in every move, check whether it's crossing the boundary
- remove the tail position from the set as the snake is moving
- if the snake landed the food, then add the tail back the snake as it is growing

```java
class SnakeGame {
    
    private Deque<Integer> body;
    private Set<Integer> set; // to track any clash in the body
    private int[][] food;
    private int foodCount = 0, width = 0, height = 0;

    /** Initialize your data structure here.
        @param width - screen width
        @param height - screen height 
        @param food - A list of food positions
        E.g food = [[1,1], [1,0]] means the first food is positioned at [1,1], the second is at [1,0]. */
    public SnakeGame(int width, int height, int[][] food) {
        body = new ArrayDeque<> ();
        set = new HashSet<> ();
        this.food = food; this.width = width; this.height = height;
        body.addLast(0); // initial snake position [0, 0]
        set.add(0);
    }
    
    /** Moves the snake.
        @param direction - 'U' = Up, 'L' = Left, 'R' = Right, 'D' = Down 
        @return The game's score after the move. Return -1 if game over. 
        Game over when snake crosses the screen boundary or bites its body. */
    public int move(String direction) {
        // get the current snake head position
        int x = body.peekFirst() / width, y = body.peekFirst() % width;

        if (direction.equals("U")) { x -= 1; }
        else if (direction.equals("L")) { y -= 1; }
        else if (direction.equals("R")) { y += 1; }
        else if (direction.equals("D")) { x += 1; }

        int newHead = x * width + y;

        // check if this valid move
        if (x < 0 || x == height || y < 0 || y == width) return -1;

        // remove the tail position from the set
        set.remove(body.peekLast());
        body.addFirst(newHead);

        // check if the new head position clashes with its own body
        if (set.contains(newHead)) return -1;
        set.add(newHead);

        // if the new head position is a valid food position
        if (foodCount < food.length && x == food[foodCount][0] && y == food[foodCount][1]) {
            set.add(body.peekLast()); // add the tail back
            foodCount += 1;
            return foodCount;
        }

        // if no food position found yet, remove the tail after valid movement
        body.pollLast();
        return foodCount;
    }
}

/**
 * Your SnakeGame object will be instantiated and called as such:
 * SnakeGame obj = new SnakeGame(width, height, food);
 * int param_1 = obj.move(direction);
 */
```

Time for `Move()` is O(1), but space O(N * M)

## Unique Paths
https://leetcode.com/problems/unique-paths/

- use set of sets to store new path
- do a DFS traversal on the grid using backtracking

```java
class Solution {

    private Set<Set<Integer>> res = new HashSet<> ();
    private int[][] dirs = {{0, 1}, {1, 0}};

    public int uniquePaths(int m, int n) {
        int start = 0; // initial position [0, 0]
        int end = (n - 1) * m + (m - 1); // for m = 3, n = 2, position 5 is the end pos
        Set<Integer> set = new HashSet<> ();
        set.add(start);

        dfs(start, end, set, m, n);
        return res.size();
    }

    public void dfs (int currentPos, int end, Set<Integer> set, int m, int n) {
        if (currentPos == end) {
            res.add(new HashSet<> (set));
        }
        else {
            int x = currentPos / m, y = currentPos % m;
            for (int k = 0; k < dirs.length; k++) {
                int x1 = x + dirs[k][0], y1 = y + dirs[k][1];
                if (x1 >= 0 && x1 < n && y1 >= 0 && y1 < m) {
                    int newPos = x1 * m + y1;
                    set.add(newPos);
                    dfs(newPos, end, set, m, n);
                    set.remove(newPos);
                }
            }
        }
    }
}
```

Time & Space ~ O(n * m)

Check the approach: https://www.youtube.com/watch?v=GO5QHC_BmvM

Since the robot can only move right and down, when it arrives at a point, it either arrives from left or above. If we use `dp[i][j]` for the number of unique paths to arrive at the point (i, j), then the state equation is `dp[i][j] = dp[i][j - 1] + dp[i - 1][j]`. Moreover, we have the base cases `dp[0][j] = dp[i][0] = 1` for all valid i and j.

```java
class Solution {
    public int uniquePaths(int m, int n) {
        int[][] dp = new int[n][m];

        for (int i = 0; i < m; i++) dp[0][i] = 1;
        for (int i = 0; i < n; i++) dp[i][0] = 1;

        for(int i = 1; i < n; i++) {
            for (int j = 1; j < m; j++) {
                dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
            }
        }

        return dp[n - 1][m - 1];
    }
}
```

Time & Space ~ O(n * m)

## Unique Paths II
https://leetcode.com/problems/unique-paths-ii/

- create a dp matrix of same size of the input matrix
- fill the initial [0, 0] position if it has no obstacle
- update the 0th row and 0th col based on the current [i, j] in the grid whether it has any obstacle or 
the previous dp value is zero. If the previous dp value is zero that means that is no way you can reach to this path and
mark it as zero
- now check all the matrix cell and update the grid

```java
class Solution {
    public int uniquePathsWithObstacles(int[][] obstacleGrid) {
        if (obstacleGrid == null || obstacleGrid.length == 0)
            return 0;
        
        int row = obstacleGrid.length, col = obstacleGrid[0].length;
        int[][] dp = new int[row][col];
        
        dp[0][0] = (obstacleGrid[0][0] == 0) ? 1 : 0; // starting position has any obstacle or not
        if (dp[0][0] == 0) return 0;
        
        for (int i = 1; i < col; i++) {
            dp[0][i] = (obstacleGrid[0][i] == 0 && dp[0][i - 1] > 0) ? 1 : 0;
        }
        
        for (int i = 1; i < row; i++) {
            dp[i][0] = (obstacleGrid[i][0] == 0 && dp[i - 1][0] > 0) ? 1 : 0;
        }
        
        for (int i = 1; i < row; i ++) {
            for (int j = 1; j < col; j++) {
                dp[i][j] = (obstacleGrid[i][j] == 0) ? dp[i - 1][j] + dp[i][j - 1] : 0;
            }
        }
        
        return dp[row - 1][col - 1];
    }
}
```

Time & Space ~ O(n * m)

**space utilized version**

- utilize the same input matrix to update the grid

```java
class Solution {
    public int uniquePathsWithObstacles(int[][] obstacleGrid) {
        if (obstacleGrid == null || obstacleGrid.length == 0)
            return 0;
        
        int row = obstacleGrid.length, col = obstacleGrid[0].length;
        
        if (obstacleGrid[0][0] == 1) return 0;
        obstacleGrid[0][0] = (obstacleGrid[0][0] == 0) ? 1 : 0; // starting position has any obstacle or not
        
        for (int i = 1; i < col; i++) {
            obstacleGrid[0][i] = (obstacleGrid[0][i] == 0 && obstacleGrid[0][i - 1] > 0) ? 1 : 0;
        }
        
        for (int i = 1; i < row; i++) {
            obstacleGrid[i][0] = (obstacleGrid[i][0] == 0 && obstacleGrid[i - 1][0] > 0) ? 1 : 0;
        }
        
        for (int i = 1; i < row; i ++) {
            for (int j = 1; j < col; j++) {
                obstacleGrid[i][j] = (obstacleGrid[i][j] == 0) ? obstacleGrid[i - 1][j] + obstacleGrid[i][j - 1] : 0;
            }
        }
        
        return obstacleGrid[row - 1][col - 1];
    }
}
```

## Unique Paths III
https://leetcode.com/problems/unique-paths-iii/

use dfs + backtracting

First find out where the start and the end is.
Also We need to know the number of empty cells.

We we try to explore a cell,
it will change 0 to -2 and do a dfs in 4 direction.

If we hit the target and pass all empty cells, increment the result.

Time complexity is as good as dp, but it take less space and easier to implement.

```java
class Solution {

    private int row = 0, col = 0, start_x = 0, start_y = 0, end_x = 0, end_y = 0, res = 0;
    private int[][] grid;
    private int[][] dirs = {{-1,0}, {1,0}, {0,-1}, {0,1}};

    public int uniquePathsIII(int[][] grid) {
        int number_empty_space = 0;
        row = grid.length; col = grid[0].length;
        this.grid = grid;

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (grid[i][j] != -1)
                    number_empty_space++;

                if (grid[i][j] == 1) { start_x = i; start_y = j; }
                if (grid[i][j] == 2) { end_x = i; end_y = j; }
            }
        }

        if (number_empty_space == 0) return res;

        dfs(start_x, start_y, number_empty_space);
        return res;
    }

    public void dfs(int i, int j, int number_empty_space) {
        number_empty_space--;
        
        if (number_empty_space < 0) return;

        if (i == end_x && j == end_y) {
            if (number_empty_space == 0)
                res++;
            else
                return;
        }

        grid[i][j] = 3; //visited

        for (int k = 0; k < dirs.length; k++) {
            int n_i = i + dirs[k][0], n_j = j + dirs[k][1];

            if (n_i >= 0 && n_i < row && n_j >= 0 && n_j < col) {
                if (grid[n_i][n_j] % 2 == 0) // only 0 and 2 value are valid to move
                    dfs(n_i, n_j, number_empty_space);
            }
        }

        grid[i][j] = 0;
    }
}
```

Time ~ O(4 ^ (n * m)), Space ~ O(n * m)

## Next Greater Element I
https://leetcode.com/problems/next-greater-element-i/

- store second array element in map with index as value as num2 has unique value
- for each element in first array, lookup the index in the map and start traversing to the right until 
you find the first greater number.

```java
class Solution {
    public int[] nextGreaterElement(int[] nums1, int[] nums2) {
        Map<Integer, Integer> map = new HashMap<> ();
        for (int i = 0; i < nums2.length; i++)
            map.put(nums2[i], i);

        int[] res = new int[nums1.length];
        Arrays.fill(res, Integer.MIN_VALUE);

        for (int i = 0; i < nums1.length; i++) {
            int searchVal = nums1[i], searchPos = map.get(searchVal);

            for (int j = searchPos + 1; j < nums2.length; j++) {
                if (nums2[j] > searchVal) {
                    res[i] = nums2[j];
                    break;
                }
            }
            
            if (res[i] == Integer.MIN_VALUE)
                res[i] = -1;
        }
        return res;
    }
}
```

Time O(n * m), Space O(n) n - length of first array and m - length of 2nd array

**optimized code**
https://www.youtube.com/watch?v=uFso48YRRao

- use stack and map
- if stack is empty or the array element is smaller than the top of the stack, push the array element
- if the array element is larger than the top of the stack, pop the stack until it is empty or you found larger
value the the array element
- build the map by adding the popped value as key and the array element as the value
- if the array element doesnt found in map then there is no greather number available

```java
class Solution {
    public int[] nextGreaterElement(int[] nums1, int[] nums2) {
        Map<Integer, Integer> map = new HashMap<> ();
        int[] res = new int[nums1.length];
        Deque<Integer> stack = new ArrayDeque<> ();

        for (int i = 0; i < nums2.length; i++) {
            if (stack.isEmpty() || stack.peekFirst() > nums2[i])
                stack.addFirst(nums2[i]);
            else if (!stack.isEmpty() && stack.peekFirst() < nums2[i]) {
                while (!stack.isEmpty() && stack.peekFirst() < nums2[i]) {
                    int smaller = stack.removeFirst();
                    map.put(smaller, nums2[i]);
                }
                 stack.addFirst(nums2[i]);
            }
        }

        for (int i = 0; i < nums1.length; i++) {
            if (map.containsKey(nums1[i]))
                res[i] = map.get(nums1[i]);
            else
                res[i] = -1;
        }
        return res;
    }
}
```

Time ~ (n + m), Space ~ O(n + m)

## Next Greater Element II
https://leetcode.com/problems/next-greater-element-ii/

- use stack
- as the array element is in cyclic so iterate the array in 2 pass both from running right to left
- store array index in stack as the array can contain dups value
- if the stack is non empty and the top of the stack is smaller/equal than current element, then no next greather element found and pop the element from the stack until the condition becomes false that is the top of the stack is larger than the current array element
- if the top of the stack is larger than the current array element, then add the value to the result as the next greather value and push the index to the stack

```java
class Solution {
    public int[] nextGreaterElements(int[] nums) {
        Deque<Integer> stack = new ArrayDeque<> ();
        int n = nums.length;
        int[] res = new int[n];
        Arrays.fill(res, -1);

        for (int rotate = 1; rotate <= 2; rotate++) {
            for (int i = n - 1; i >= 0; i--) {
                while (!stack.isEmpty() && nums[stack.peekFirst()] <= nums[i])
                    stack.removeFirst();
                if (stack.isEmpty())
                    stack.addFirst(i);
                else if (nums[stack.peekFirst()] > nums[i]) {
                    res[i] = nums[stack.peekFirst()];
                    stack.addFirst(i);
                }
            }
        }

        return res;
    }
}
```

Time & Space ~ O(N)

## Longest Absolute File Path
https://leetcode.com/problems/longest-absolute-file-path/

- split the string based on the "\n" and find the number of tabs available within each string
- number of tabs gives the level count and how deep the file/sub-folder is in
- for the same level if the stack size is more, remove the top of the stack until you have the previous level value
- find the current length of the full path and if the file is encounter ("." is helpful to identify a file existence) then update the global max

```java
class Solution {
    public int lengthLongestPath(String input) {
        Deque<Integer> stack = new ArrayDeque<> ();
        stack.addFirst(0);

        String[] dirs = input.split("\n");
        int maxLen = 0;

        for (String s : dirs) {
            int numberOfTabs = s.lastIndexOf("\t") + 1;
            int level = numberOfTabs + 1;

            while (level < stack.size()) stack.poll();

            int curLen = stack.peek() + s.length() - numberOfTabs + 1;
            stack.push(curLen);

            if (s.contains("."))
                maxLen = Math.max(maxLen, curLen - 1);
        }

        return maxLen;
    }
}
```

Time & Space ~ O(N)

## Shortest Path in Binary Matrix
https://leetcode.com/problems/shortest-path-in-binary-matrix/

- use BFS

```java
class Solution {
    public int shortestPathBinaryMatrix(int[][] grid) {        
        if (grid == null || grid.length == 0) return -1;
        
        int level = 0, R = grid.length, C = grid[0].length;
        int[][] dirs = {{-1,-1}, {-1,0}, {-1,1}, {0,-1}, {0,1}, {1,-1}, {1,0}, {1,1}};
        Deque<int[]> Q = new ArrayDeque<> ();
        boolean[][] visited = new boolean[R][C];
        
        if (grid[0][0] != 0 || grid[R - 1][C - 1] != 0) return -1;

        Q.addLast(new int[] { 0, 0 });
        visited[0][0] = true;

        while (!Q.isEmpty()) {
            int size = Q.size();
            for (int i = 0; i < size; i++) {
                int[] pos = Q.removeFirst();

                if (pos[0] == R - 1 && pos[1] == C - 1) return level + 1;

                for (int k = 0; k < dirs.length; k++) {
                    int x = pos[0] + dirs[k][0], y = pos[1] + dirs[k][1];

                    if (x >= 0 && x < R && y >= 0 && y < C && grid[x][y] == 0 && !visited[x][y]) {
                        Q.addLast(new int[] { x, y });
                        visited[x][y] = true;
                    }
                }
            }
            level++;
        }
        return -1;
    }
}
```

- Time & Space ~ O(R * C)

## Analyze User Website Visit Pattern
https://leetcode.com/problems/analyze-user-website-visit-pattern/

- create a map of all users and their corresponding website visit based on timestamp
- use the map iterate for all users and count the number of users visited the websites (3 websites)
- 

```java
import javafx.util.Pair;

class Solution {
    public List<String> mostVisitedPattern(String[] username, int[] timestamp, String[] website) {
        Map<String, List<Pair<Integer, String>>> userBrowserMap = new HashMap<> ();

        for (int i = 0; i < username.length; i++) {
            userBrowserMap.putIfAbsent(username[i], new ArrayList<> ());
            userBrowserMap.get(username[i]).add(new Pair(timestamp[i], website[i]));
        }

        Map<String, Integer> countSite = new HashMap<> ();
        String res = "";

        for (String user : userBrowserMap.keySet()) {
            Set<String> set = new HashSet<> ();
            List<Pair<Integer, String>> list = userBrowserMap.get(user);
            Collections.sort(list, (a, b) -> ( a.getKey() - b.getKey() ));

            int n = list.size();

            for (int i = 0; i < n; i++) {
                for (int j = i + 1; j < n; j++) {
                    for (int k = j + 1; k < n; k++) {
                        String str = list.get(i).getValue() + " " + list.get(j).getValue() + " " + list.get(k).getValue();
                        if (!set.contains(str)) {
                            set.add(str);
                            countSite.put(str, countSite.getOrDefault(str, 0) + 1);
                        }

                        // choose lexicographically smallest 3 seq if there is match
                        if (res == "" || countSite.get(res) < countSite.get(str) ||
                            (countSite.get(res) == countSite.get(str) && res.compareTo(str) > 0))
                            res = str;
                    }
                }
            }
        }

        String[] li = res.split(" ");
        return new LinkedList<> (Arrays.asList(li));
    }
}
```

Time ~ O(n^3), space ~ O(n)

## Find the Town Judge
https://leetcode.com/problems/find-the-town-judge/

- use a hashmap to store all the incoming connections. for all in-degee node, increment the in-degree count and mark any
source node as -1

```java
class Solution {
    public int findJudge(int N, int[][] trust) {
        Map<Integer, Integer> map = new HashMap<> ();
        
        if (trust.length == 0) return N;

        for (int[] val : trust) {
            int source = val[0], judge = val[1];
            map.put(source, -1);

            if (map.containsKey(judge) && map.get(judge) >= 0)
                map.put(judge, map.get(judge) + 1);

            if (!map.containsKey(judge))
                map.put(judge, 1);
        }

        for (int judge : map.keySet()) {
            if (map.get(judge) > 0 && map.get(judge) == N - 1)
                return judge;
        }
        return -1;
    }
}
```

Time & Space ~ O(N)

## Insert Interval
https://leetcode.com/problems/insert-interval/

- create a new array of and insert the new interval after copying the inteval from the original array
- sort the new interval array based on the starting time
- rearrange the intervals by merging the interval: use the first interval as pivot and iterate the array from the next
pointer. if the new interval is not lie within the pivot, insert the pivot into the list and set the new interval as the pivot
- if the new interval is within the pivot then update the existing pivot

```java
class Solution {
    public int[][] insert(int[][] intervals, int[] newInterval) {
        
        if (intervals.length == 0) return new int[][] {newInterval};
        
        int[][] new_intervals = new int[intervals.length + 1][2];
        //copy the existing intervals
        for (int i = 0; i < intervals.length; i++)
            new_intervals[i] = intervals[i];
        
        new_intervals[intervals.length] = newInterval; // insert the new intervals
        
        Arrays.sort(new_intervals, (a, b) -> (a[0] - b[0])); //sort based on the starting time
        
        return util(new_intervals);
    }
    
    public int[][] util(int[][] intervals) {
        //rearrange the intervals by merging the interval if require
        int[] init = intervals[0];
        List<int[]> li = new ArrayList<> ();
        li.add(init);
        
        for (int i = 0; i < intervals.length; i++) {
            if (init[1] < intervals[i][0]) {
                init = intervals[i];
                li.add(init);
            }
            else if (init[1] >= intervals[i][0]) {
                li.get(li.size() - 1)[1] = Math.max(init[1], intervals[i][1]);
            }
        }

        int[][] res = new int[li.size()][2];
        int t = 0;
        for (int[] v : li)
            res[t++] = v;

        return res;
    }
}
```

Time ~ O(nlogn), space ~ O(n)

**optimized code**

- iterate the intervals and add to the list if the start time of new intervals is larger than the current interval
- now add the new interval and merge if require
- iterate the rest of the intervals and merge if require. use a variable to hold the current intervals

```java
class Solution {
    public int[][] insert(int[][] intervals, int[] newInterval) {
        List<int[]> list = new ArrayList<> ();
        int i = 0;
        int[] temp = new int[2];

        // add to the list if the start time of new intervals is larger than the current interval
        while (i < intervals.length && newInterval[0] > intervals[i][0])
            list.add(intervals[i++]);

        // now add the new interval and merge if require
        // check overlapping condition
        if (list.size() == 0 || list.get(list.size() - 1)[1] < newInterval[0])
            list.add(newInterval);
        else {
            temp = list.remove(list.size() - 1);
            temp[1] = Math.max(newInterval[1], temp[1]);
            list.add(temp);
        }

        //iterate the rest of the intervals and merge if require
        while (i < intervals.length) {
            if (list.get(list.size() - 1)[1] < intervals[i][0])
                list.add(intervals[i]);
            else {
                temp = list.remove(list.size() - 1);
                temp[1] = Math.max(intervals[i][1], temp[1]);
                list.add(temp);
            }
            i++;
        }
        return new int[list.size()][2] { list.toArray() };
    }
}
```
Time & Space O(n)

## Counting Bits
https://leetcode.com/problems/counting-bits/

- for even number: the number of 1 bits depend on the `f(num / 2)`
- for odd number: the number of 1 bits depend on the `1 + f(num / 2)`

```java
class Solution {
    public int[] countBits(int num) {
        
        if (num == 0) return new int[] { 0 };
        
        int[] dp = new int[num + 1];
        dp[0] = 0; dp[1] = 1;
        
        for (int i = 2; i <= num; i++) {
            if (i % 2 == 0)
                dp[i] = dp[i / 2];
            else
                dp[i] = 1 + dp[i / 2];
        }
        return dp;
    }
}
```
Time & Space ~ O(n)

## Triangle
https://leetcode.com/problems/triangle/

a good explanation: https://www.youtube.com/watch?v=6zcFB1nIoq8

- consider the triangle a graph and traverse each path till bottom to find the cost
- store the min cost when you reach at bottom and do this for each path (lots of overlapping)

```java
class Solution {
    
    private int min = Integer.MAX_VALUE, size = 0;
    
    public int minimumTotal(List<List<Integer>> triangle) {
        this.size = triangle.size();
        util(triangle, 0, triangle.get(0).get(0), 0);
        return min;
    }
    
    public void util(List<List<Integer>> triangle, int level, int sum, int pos) {
        if (level == size - 1) {
            min = Math.min(min, sum);
        }
        else {
            util(triangle, level + 1, sum + triangle.get(level + 1).get(pos), pos);
            util(triangle, level + 1, sum + triangle.get(level + 1).get(pos + 1), pos + 1);
        }
    }
}
```
Time ~ as the program is trying to check all the possibilities, it has exponential runtime

**dp**

```java
class Solution {
    public int minimumTotal(List<List<Integer>> triangle) {
        int n = triangle.size();
        int[][] dp = new int[n][n];

        //fill the last level of the dp array using the leaf node value
        for (int i = 0; i < triangle.get(n - 1).size(); i++)
            dp[n - 1][i] = triangle.get(n - 1).get(i);

        //start from the one step upper level than the leaf node
        //for every node find the min value of the two leaf node and add with it's current node
        //at the end return the dp[0][0]
        for (int i = n - 2; i >= 0; i--) {
            for (int j = 0; j < triangle.get(i).size(); j++) {
                dp[i][j] = triangle.get(i).get(j) + Math.min(dp[i + 1][j], dp[i + 1][j + 1]);
            }
        }
        return dp[0][0];
    }
}
```

time & space ~ O(n)

## Minimum Path Sum
https://leetcode.com/problems/minimum-path-sum/

```java
class Solution {
    public int minPathSum(int[][] grid) {
        int R = grid.length, C = grid[0].length;
        int[][] dp = new int[R][C];
        dp[0][0] = grid[0][0];
        
        //fill up the first row and first col with cumulative sum
        for(int i = 1; i < C; i++)
            dp[0][i] = dp[0][i - 1] + grid[0][i];
        
        for(int i = 1; i < R; i++)
            dp[i][0] = dp[i - 1][0] + grid[i][0];
        
        //for every cell take the min from [i - 1][j] and [i][j - 1]
        for (int i = 1; i < R; i++) {
            for(int j = 1; j < C; j++) {
                dp[i][j] = grid[i][j] + Math.min(dp[i - 1][j], dp[i][j - 1]);
            }
        }
        return dp[R-1][C-1];
    }
}
```

Time & Space ~ O(m * n)

**1D array**

- as we are moving only right and down ward, use a 1D array and keep updating based on the column index

```java
class Solution {
    public int minPathSum(int[][] grid) {
        int R = grid.length, C = grid[0].length;
        int[] dp = new int[C];
        
        for (int i = 0; i < R; i++) {
            for (int j = 0; j < C; j++) {
                if (i == 0 && j != 0)
                    dp[j] = grid[i][j] + dp[j - 1];
                else if (j == 0 && i != 0)
                    dp[j] = grid[i][j] + dp[j];
                else if (i != 0 && j != 0)
                    dp[j] = grid[i][j] + Math.min(dp[j], dp[j - 1]);
                else
                    dp[j] = grid[i][j];
            }
        }
        return dp[C - 1];
    }
}
```

time ~ o(n * m), space O(m)

**space optimization on updating the source grid**

- update the data on the grid itself

```java
class Solution {
    public int minPathSum(int[][] grid) {
        int R = grid.length, C = grid[0].length;
        
        //fill up the first row and first col with cumulative sum
        for(int i = 1; i < C; i++)
            grid[0][i] = grid[0][i - 1] + grid[0][i];
        
        for(int i = 1; i < R; i++)
            grid[i][0] = grid[i - 1][0] + grid[i][0];
        
        //for every cell take the min from [i - 1][j] and [i][j - 1]
        for (int i = 1; i < R; i++) {
            for(int j = 1; j < C; j++) {
                grid[i][j] = grid[i][j] + Math.min(grid[i - 1][j], grid[i][j - 1]);
            }
        }
        return grid[R-1][C-1];
    }
}
```

time ~ O(n * m), space O(1)

## Convert Sorted List to Binary Search Tree
https://leetcode.com/problems/convert-sorted-list-to-binary-search-tree/

soln: https://leetcode.com/articles/convert-sorted-list-to-binary-search-tree/

```java
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public TreeNode sortedListToBST(ListNode head) {
        if (head == null) return null;

        ListNode slow_ptr = head, fast_ptr = head, prev_ptr = null;

        //find the middle of the linked list using one slow pointer which moves one step
        //and fast pointer which moves 2 steps at a time
        //once we find the middle node based on slow pointer, divided the list into two halves
        while (fast_ptr != null && fast_ptr.next != null) {
            prev_ptr = slow_ptr;
            slow_ptr = slow_ptr.next;
            fast_ptr = fast_ptr.next.next;
        }

        //edge case: handle single node
        if (prev_ptr != null)
            prev_ptr.next = null;

        TreeNode root = new TreeNode(slow_ptr.val);

        //edge case
        if (head == slow_ptr)
            return root;

        //split the list into two halves
        root.left = sortedListToBST(head);
        root.right = sortedListToBST(slow_ptr.next);
        return root;
    }
}
```

time ~ O(NlogN), space ~ O(logN) for skewed tree it would be O(N) but due to height balance BST, we can keep it log scale

**optimized code**

- Convert the given linked list into an array. Let's call the beginning and the end of the array as left and right
- Find the middle element as (left + right) / 2. Let's call this element as mid. This is a O(1) time operation and is the only major improvement over the previous algorithm.
- The middle element forms the root of the BST.
- Recursively form binary search trees on the two halves of the array represented by (left, mid - 1) and (mid + 1, right) respectively.


```java
class Solution {
    
    private List<Integer> li = new ArrayList<> ();
    
    public TreeNode sortedListToBST(ListNode head) {
        if (head == null) return null;

        ListNode ptr = head;

        while (ptr != null) {
            li.add(ptr.val);
            ptr = ptr.next;
        }
        
        return util(0, li.size() - 1);
    }

    public TreeNode util(int l, int r) {
        if (l < r) {
            int mid = (l + r) / 2;
            TreeNode root = new TreeNode(li.get(mid));
            root.left = util(l, mid - 1);
            root.right = util(mid + 1, r);
            return root;
        }
        
        if (l == r)
            return new TreeNode(li.get(l));
        
        return null;
    }
}
```

Time & Space ~ O(n)

## Longest Valid Parentheses
https://leetcode.com/problems/longest-valid-parentheses/

Solutions: https://leetcode.com/articles/longest-valid-parentheses/

- use stack and push -1 to count the valid parenthesis if it started at index 0 for example "()..."
- if '(' push the index. if ')' pop the index from the stack. Then calculate the length of the valid parenthesis by substracting current index from the top of the stack (if it is not empty). If it is empty, push the current index

```java
class Solution {
    public int longestValidParentheses(String s) {
        if (s == null || s.length() == 0) return 0;
        
        Deque<Integer> stack = new ArrayDeque<> ();
        //push -1 to count the valid parenthesis if it started at index 0 for example "()..."
        stack.push(-1);
        int maxlen = 0;

        /*
        if '(' push the index. if ')' pop the index from the stack. Then calculate the length of the valid parenthesis by substracting current index from the top of the stack (if it is not empty). If it is empty, push the current index
        */
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(')
                stack.push(i);
            else if (s.charAt(i) == ')') {
                int len = 0;
                if (!stack.isEmpty()) {
                    stack.pop();
                }
                len = (stack.isEmpty() == true) ? 0 : i - stack.peek();
                maxlen = Math.max(maxlen, len);

                if (stack.isEmpty())
                    stack.push(i);
            }
        }
        return maxlen;
    }
}
```

Time & space ~ O(n)

**space optimized code**

- In this approach, we make use of two counters left and right. First, we start traversing the string from the left towards the right and for every '(' encountered, we increment the left counter and for every ')' encountered, we increment the right counter. Whenever left becomes equal to right, we calculate the length of the current valid string and keep track of maximum length substring found so far. If rightright becomes greater than left we reset left and right to 0.

Next, we start traversing the string from right to left and similar procedure is applied.

```java
class Solution {
    public int longestValidParentheses(String s) {
        if (s == null || s.length() == 0) return 0;

        //1st pass        
        int left = 0, right = 0, maxlen = 0;

        for (int i = 0; i < s.length(); i++) {
            int len = 0;
            if (s.charAt(i) == '(') left++;
            else if (s.charAt(i) == ')') right++;

            if (left == right) {
                len = left + right; 
                maxlen = Math.max(maxlen, len);
            }
            else if (left < right) {
                left = 0; right = 0;
            }
        }

        //2nd pass        
        left = 0; right = 0;

        for (int i = s.length() - 1; i >= 0; i--) {
            int len = 0;
            if (s.charAt(i) == '(') left++;
            else if (s.charAt(i) == ')') right++;

            if (left == right) {
                len = left + right;
                maxlen = Math.max(maxlen, len);
            }
            else if (left > right) {
                left = 0; right = 0;
            }
        }
        return maxlen;
    }
}
```

Time ~ O(n), space O(1)

## Bus Routes
https://leetcode.com/problems/bus-routes/

- build the graph if there is any common bus stop. graph should be undirected and use BFS to find the min number of bus required
- in visited and target set store the route index (not the start or stop bus points)

```java
class Solution {
    public int numBusesToDestination(int[][] routes, int S, int T) {
        if (S == T) return 0;
        int N = routes.length;

        List<Integer>[] adjList = new ArrayList[N];
        for (int i = 0; i < N; i++) {
            adjList[i] = new ArrayList<> ();
        }

        //build the graph if there is any common bus stop. graph should be undirected
        for (int i = 0; i < N; i++) {
            for (int j = i + 1; j < N; j++) {
                if(intersect(routes[i], routes[j])) {
                    adjList[i].add(j);
                    adjList[j].add(i);
                }
            }
        }

        Set<Integer> visited = new HashSet<> ();
        Deque<Integer> Q = new ArrayDeque<> ();
        Set<Integer> target = new HashSet<> ();

        //store the start and stop bus route index
        for(int i = 0; i < N; i++) {
            for (int j = 0; j < routes[i].length; j++) {
                if (routes[i][j] == S) {
                    visited.add(i);
                    Q.addLast(i);
                }
                
                if (routes[i][j] == T) {
                    target.add(i);
                    
                    //edge case: if the route contains both start and stop bus routes
                    if (visited.contains(i))
                        return 1;
                }
            }
        }
    

        int level = 0;
        while (!Q.isEmpty()) {
            int size = Q.size();
            for (int k = 0; k < size; k++) {
                int node = Q.removeFirst();
                if (target.contains(node)) return level + 1;

                for (int adj : adjList[node]) {
                    if (!visited.contains(adj)) {
                        visited.add(adj);
                        Q.addLast(adj);
                    }
                }
            }
            level++;
        }
        return -1;
    }

    public boolean intersect(int[] a, int[] b) {
        Set<Integer> set = new HashSet<> ();
        
        for (int i = 0; i < a.length; i++)
            set.add(a[i]);
        
        for (int i = 0; i < b.length; i++) {
            if (set.contains(b[i])) {
                return true;
            }
        }
        return false;
    }
}
```

Time ~ O(n * m * n), space O(n * m)

**optimized code**

```java
class Solution {
    public int numBusesToDestination(int[][] routes, int S, int T) {
        if (S == T) return 0;
        int N = routes.length;

        List<Integer>[] adjList = new ArrayList[N];
        for (int i = 0; i < N; i++) {
            Arrays.sort(routes[i]);
            adjList[i] = new ArrayList<> ();
        }

        //build the graph
        for (int i = 0; i < N; i++) {
            for (int j = i + 1; j < N; j++) {
                if(intersect(routes[i], routes[j])) {
                    adjList[i].add(j);
                    adjList[j].add(i);
                }
            }
        }

        Set<Integer> visited = new HashSet<> ();
        Deque<Integer> Q = new ArrayDeque<> ();
        Set<Integer> target = new HashSet<> ();

        for(int i = 0; i < N; i++) {
            if (Arrays.binarySearch(routes[i], S) >= 0) {
                visited.add(i);
                Q.addLast(i);                
            }
            
            if (Arrays.binarySearch(routes[i], T) >= 0) {
                target.add(i);
                if (visited.contains(i)) return 1;
            }
        }
    

        int level = 0;
        while (!Q.isEmpty()) {
            int size = Q.size();
            for (int k = 0; k < size; k++) {
                int node = Q.removeFirst();
                if (target.contains(node)) return level + 1;

                for (int adj : adjList[node]) {
                    if (!visited.contains(adj)) {
                        visited.add(adj);
                        Q.addLast(adj);
                    }
                }
            }
            level++;
        }
        return -1;
    }

    public boolean intersect(int[] A, int[] B) {
        int i = 0, j = 0;
        
        while (i < A.length && j < B.length) {
            if (A[i] == B[j]) return true;
            if (A[i] < B[j]) i++; 
            else j++;
        }
        
        return false;
    }
}
```
time & space O(n^2)

## Coin Change
https://leetcode.com/problems/coin-change/

Soln: https://www.youtube.com/watch?v=jgiZlGzXMBw
https://leetcode.com/problems/coin-change/solution/

```java
class Solution {
    public int coinChange(int[] coins, int amount) {
        int[] dp = new int[amount + 1];
        Arrays.fill(dp, amount + 1);
        dp[0] = 0;
        
        for (int i = 1; i <= amount; i++) {
            for (int j = 0; j < coins.length; j++) {
                if (i >= coins[j]) {
                    dp[i] = Math.min(dp[i], dp[i - coins[j]] + 1);
                }
            }
        }
        
        return (dp[amount] > amount) ? -1 : dp[amount];
    }
}
```

Time & Space ~ O(n)

## Minimum Cost For Tickets
https://leetcode.com/problems/minimum-cost-for-tickets/

soln: https://leetcode.com/articles/minimum-cost-for-tickets/
best soln: https://leetcode.com/problems/minimum-cost-for-tickets/discuss/226659/Two-DP-solutions-with-pictures

```java
class Solution {

    private int[] dp = new int[366];
    private Set<Integer> set = new HashSet<> ();
    private int[] costs;

    public int mincostTickets(int[] days, int[] costs) {
        this.costs = costs;
        Arrays.fill(dp, -1);
        
        for(int day : days)
            set.add(day);

        return util(1);
    }

    public int util(int i) {
        if (i > 366) return 0;
        if (dp[i] != -1) return dp[i];

        int ans = 0;
        if (set.contains(i)) {
            ans = Math.min(util(i + 1) + costs[0], util(i + 7) + costs[1]);
            ans = Math.min(ans, util(i + 30) + costs[2]);
        }
        else {
            ans = util(i + 1);
        }

        dp[i] = ans;
        return ans;
    }
}
```

Time & Space ~ O(365)

**iterative way**

- We track the minimum cost for all calendar days in dp. For non-travel days, the cost stays the same as for the previous day. For travel days, it's a minimum of yesterday's cost plus single-day ticket, or cost for 8 days ago plus 7-day pass, or cost 31 days ago plus 30-day pass.
- on trip day i then dp[i] = dp[i - 1] and dp[i] = 0 if i <= 0
- on trip day i then dp[i] = min(dp[i - 1] + single_day_ticket_cost, dp[i - 7] + 7_days_ticket_cost, dp[i - 30] + 30_days_ticket_cost)

```java
class Solution {
    public int mincostTickets(int[] days, int[] costs) {
        boolean[] travel_days = new boolean[366];
        int[] dp = new int[366];

        for (int day : days)
            travel_days[day] = true;

        int minCost = 0; dp[0] = 0;

        for (int i = 1; i < 366; i++) {
            // no travel day
            if (!travel_days[i]) {
                dp[i] = dp[i - 1];
            }
            else {
                int cost = Math.min(dp[i - 1] + costs[0], dp[Math.max(0, i - 7)] + costs[1]);
                cost = Math.min(cost, dp[Math.max(0, i - 30)] + costs[2]);
                dp[i] = cost;
            }
        }
        return dp[365];
    }
}
```
Time & Space ~ O(365)

**optimized code**

- We track the minimum cost for each travel day. We process only travel days and store {day, cost} for 7-and 30-day passes in the last7 and last30 variables. After a pass 'expires', we remove it from the queue. This way, our queues only contains travel days for the last 7 and 30 days, and the cheapest pass prices are in the front of the queues.

```java
class Solution {
    public int mincostTickets(int[] days, int[] costs) {
        if (days.length == 1) return costs[0];
        int n = days.length;

        int[] dp = new int[days[n - 1] + 1];

        for (int i = 0; i < days.length; i++) {
            int c1 = 0, c7 = 0, c30 = 0, minCost = 0;

            if (days[i] - 1 <= 0) c1 = costs[0];
            else c1 = dp[days[i] - 1] + costs[0];

            if (days[i] - 7 <= 0) c7 = costs[1];
            else c7 = dp[days[i] - 7] + costs[1];

            if (days[i] - 30 <= 0) c30 = costs[2];
            else c30 = dp[days[i] - 30] + costs[2];

            minCost = Math.min(c1, c7);
            minCost = Math.min(minCost, c30);

            dp[days[i]] = minCost;

            // fillup the no travel days with the previous travel day cost
            for (int j = days[i] + 1; i + 1 < days.length && j < days[i + 1]; j++) {
                dp[j] = dp[j - 1];
            }
        }

        return dp[dp.length - 1];
    }
}
```

Time ~ O(N), Space ~ O(number of days)

## Maximum Difference Between Node and Ancestor
https://leetcode.com/problems/maximum-difference-between-node-and-ancestor/

- use dfs traversal and always maintain max and min value within the subtrees

```java
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public int maxAncestorDiff(TreeNode root) {
        return dfs(root, root.val, root.val);
    }

    public int dfs(TreeNode root, int max, int min) {
        if (root == null) return max - min;

        max = Math.max(max, root.val);
        min = Math.min(min, root.val);

        return Math.max(dfs(root.left, max, min), dfs(root.right, max, min));
    }
}
```

Time & Space ~ O(n)

## Connecting Cities With Minimum Cost
https://leetcode.com/problems/connecting-cities-with-minimum-cost/

- find the minimum spanning tree and use kruskal's algorithm
- to avoid any cycle use disjoint set union-find operations

```java
class Solution {

    private int n;
    private int[] parent;

    public int minimumCost(int N, int[][] connections) {
        //build the parent table for union-find
        this.n = N;
        parent = new int[N + 1];
        int res = 0;

        for (int i = 0; i <= N; i++)
            parent[i] = i;

        //sort the connections based on weight
        Arrays.sort(connections, (a, b) -> (a[2] - b[2]));

        //take the min weight edge and if the connected vertex belongs to different set, add the weight to the final count and insert the vertex on the parent
        for (int[] c : connections) {
            int x = c[0], y = c[1];

            if (find(x) != find(y)) {
                res += c[2];
                union(x, y);
            }
        }
        return (n == 1) ? res : -1;
    }

    public int find(int x) {
        if (parent[x] == x)
            return parent[x];

        parent[x] = find(parent[x]);
        return parent[x];
    }

    public void union(int x, int y) {
        int px = find(x);
        int py = find(y);

        if (px != py) {
            parent[px] = py;
            n--;
        }
    }
}
```

Time ~ O(nlogn), space ~ O(1)

## High Five
https://leetcode.com/problems/high-five/

- sort the array based on student id and then scores in decresing order
- take the top 5 scores and save into the data structure until you find another student id

```java
class Solution {
    public int[][] highFive(int[][] items) {
        Arrays.sort(items, (a, b) -> {
            int cmp = a[0] - b[0];
            if (cmp != 0) return cmp;
            return b[1] - a[1];
        });

        int i = 0, st_id = items[0][0], avg = 0, count = 5, j = 0;
        List<int[]> res = new ArrayList<> ();

        while (i < items.length) {
            while (i < items.length && st_id == items[i][0]) {
                if (count > 0) {
                    avg += items[i][1];
                    count--;
                }
                i++;
            }

            res.add(new int[] { st_id, avg / 5 });
            
            //avoid the array out of index
            if (i < items.length) {
                st_id = items[i][0];
                avg = 0;
                count = 5;   
            }
        }

        int[][] val = new int[res.size()][2];

        for(int[] v : res)
            val[j++] = v;
        return val;
    }
}
```

time ~ O(nlogn), space ~ O(n)

**optimized code**

- use hashmap and store student id as key and scores in priority queue (only top 5 score) as value

```java
class Solution {
    public int[][] highFive(int[][] items) {
        TreeMap<Integer, PriorityQueue<Integer>> map = new TreeMap<> ();

        for (int[] val : items) {
            PriorityQueue<Integer> pq = map.getOrDefault(val[0], new PriorityQueue<Integer> (5));
            pq.offer(val[1]);

            if (pq.size() > 5)
                pq.poll();
            map.put(val[0], pq);
        }

        int[][] res = new int[map.size()][2];
        int i = 0;

        for (int st_id : map.keySet()) {
            PriorityQueue<Integer> pq = map.get(st_id);
            int avg = 0;

            while (!pq.isEmpty()) {
                avg += pq.poll();
            }
            res[i][0] = st_id; res[i++][1] = avg / 5;
        }
        return res;
    }
}
```

time ~ O(nlogn), space ~ O(n)

## Path In Zigzag Labelled Binary Tree
https://leetcode.com/problems/path-in-zigzag-labelled-binary-tree/

- If the tree is numbered left-to-right (not zigzag), the parent's label can be always determined as label / 2. For zigzag, we need to "invert" the parent label
- find the actual label and invert the label in each time

```java
class Solution {
    public List<Integer> pathInZigZagTree(int label) {
        List<Integer> res = new ArrayList<>();
        int max = 1;
        while (label > max) { max = max * 2 + 1; } //find the first larger value than label
        int reverseLabel = (max / 2 + 1) + max - label; //reverseLabel = firstValue of this level + differ

        //find the reverseLabel, E.g. label = 14, then the reverseLabel is 9
        //we can pick one from label / 2
        //(1),2,(4),9
        //1,(3),7,(14)

        while (label != 0 && reverseLabel != 0) {
            res.add(0, label); //pick 14, 9/2,14/2/2,9/2/2/2 which is 14,4,3,1
            label /= 2; reverseLabel /= 2;
            //swap label and reverseLabel
            int temp = reverseLabel;
            reverseLabel = label;
            label = temp;
        }
        return res;
    }
}
```

Time ~ O(logn), space ~ O(1)

## Gas Station
https://leetcode.com/problems/gas-station/

- iterate each gas station and find the starting gas station if the tank will have >= 0 value to move
- so the starting index would be where `gas[i] - cost[i] >= 0`
- from the starting index move to the next status and calculate the gas consumption minus the cost to move to the next station

```java
class Solution {
    public int canCompleteCircuit(int[] gas, int[] cost) {
        int N = gas.length;
        for(int i = 0; i < gas.length; i++) {
            if (gas[i] - cost[i] >= 0) {
                int start = i, count = N, tank = gas[i] - cost[i];

                while (count > 0) {
                    start++;
                    tank += gas[start % N] - cost[start % N];

                    // if tank is empty then start over from the next station
                    if (tank < 0)
                        break;

                    count--;
                }

                if (count == 0)
                    return i;
            }
        }
        return -1;
    }
}
```

time ~ O(n^2), space ~ O(1)

**optimized code**

https://leetcode.com/articles/gas-station/

- add the total_tank and current_tank in each iteration by `gas[i] - cost[i]`
- if current_tank value is less than zero, then it can't move to the next station, so choose the next station as the starting index and reset the current_tank = 0
- at the end if the total_tank is less than zero then no car can complete the cycle

```java
class Solution {
    public int canCompleteCircuit(int[] gas, int[] cost) {
        int total_tank = 0, current_tank = 0, start_index = 0;

        for (int i = 0; i < gas.length; i++) {
            total_tank += gas[i] - cost[i];
            current_tank += gas[i] - cost[i];

            if (current_tank < 0) {
                start_index = i + 1;
                current_tank = 0;
            }
        }
        return (total_tank >= 0) ? start_index : -1;
    }
}
```

time ~ O(n), space ~ O(1)

## Maximum Product of Three Numbers
https://leetcode.com/problems/maximum-product-of-three-numbers/

- sort the array and choose the last three element and calculate the product. if the array has negative number, then two negative number can contribute to find the max product

```java
class Solution {
    public int maximumProduct(int[] nums) {
        Arrays.sort(nums);
        int n = nums.length;
        return Math.max(nums[n - 1] * nums[n - 2] * nums[n - 3], nums[0] * nums[1] * nums[n - 1]);
    }
}
```
Time ~ O(nlogn), space ~ O(1)

**optimized code**

https://leetcode.com/articles/maximmum-product-of-three-numbers/

- find 2 smallest and 3 largest number

```java
class Solution {
    public int maximumProduct(int[] nums) {
        int min1 = Integer.MAX_VALUE, min2 = Integer.MAX_VALUE, max1 = Integer.MIN_VALUE, 
            max2 = Integer.MIN_VALUE, max3 = Integer.MIN_VALUE;

        for (int n : nums) {
            if (n <= min1) {
                min2 = min1;
                min1 = n;
            }
            else if (n <= min2) {
                min2 = n; //n lies between min1 and min2
            }

            if (n >= max1) {
                max3 = max2;
                max2 = max1;
                max1 = n;
            }
            else if (n >= max2) {
                max3 = max2;
                max2 = n;
            }
            else if (n >= max3) {
                max3 = n;
            }
        }
        return Math.max(min1 * min2 * max1, max1, max2, max3);
    }
}
```

Time ~ O(n)

## Number of Connected Components in an Undirected Graph
https://leetcode.com/problems/number-of-connected-components-in-an-undirected-graph/

```java
class Solution {

    private List<Integer>[] adjList;

    public int countComponents(int n, int[][] edges) {
        adjList = new ArrayList[n];

        for (int i = 0; i < n; i++) {
            adjList[i] = new ArrayList<> ();
        }

        for (int[] edge : edges) {
            adjList[edge[0]].add(edge[1]); //source -> dest
            adjList[edge[1]].add(edge[0]); //dest -> source
        }

        int count = 0;
        boolean[] visited = new boolean[n];
        for (int v = 0; v < n; v++) {
            if (!visited[v]) {
                dfs(v, visited);
                count++;
            }
        }
        return count;
    }

    public void dfs(int v, boolean[] visited) {
        visited[v] = true;

        for (int adj : adjList[v]) {
            if (!visited[adj])
                dfs(adj, visited);
        }
    }
}
```

Time ~ O(n), space ~ O(n)

**optimized code**

- use union-find with path compression algo

```java
class Solution {
    public int countComponents(int n, int[][] edges) {
        int[] parent = new int[n];
        for (int i = 0; i < n; i++)
            parent[i] = i;
        
        for (int[] edge : edges) {
            int px = find(parent, edge[0]);
            int py = find(parent, edge[1]);
            
            if (px != py) {
                parent[px] = py; //union operation
                n--;
            }
        }
        return n;
    }
    
    public int find(int[] P, int x) {
        while (P[x] != x) {
            P[x] = P[P[x]]; //path compression
            x = P[x];
        }
        return P[x];
    }
}
```

Time & Space ~ O(nlogn), space ~ O(n)

Union-find runtime analysis: https://www.slideshare.net/WeiLi73/time-complexity-of-union-find-55858534

## Number of Atoms
https://leetcode.com/problems/number-of-atoms/

- use stack and store treemap in the stack. initialy add one treemap and wheneve any '(' encounter push a new treemap
- if ')' encounter top of the stack and update the peek of the stack after multiply with the formula number

```java
class Solution {
    public String countOfAtoms(String formula) {
        Deque<Map<String, Integer>> stack = new ArrayDeque<> ();
        stack.push(new TreeMap<> ());

        int i = 0, n = formula.length();
        while (i < n) {
            if (formula.charAt(i) == '(') {
                stack.push(new TreeMap<> ());
                i++;
            }
            else if (formula.charAt(i) == ')') {
                Map<String, Integer> map = stack.pop();
                int iStart = ++i;
                //find the multiplier
                while (i < n && Character.isDigit(formula.charAt(i))) i++;
                int digit = i > iStart ? Integer.parseInt(formula.substring(iStart, i)) : 1;
                //save the result on the recent top of the stack
                for (String key : map.keySet()) {
                    int d = map.get(key);
                    stack.peek().put(key, stack.peek().getOrDefault(key, 0) + d * digit);
                }
            }
            else {
                int iStart = i++;
                //find the lowercase
                while (i < n && Character.isLowerCase(formula.charAt(i))) i++;
                String name = formula.substring(iStart, i);
                iStart = i;
                //find the multiplier
                while (i < n && Character.isDigit(formula.charAt(i))) i++;
                int digit = i > iStart ? Integer.parseInt(formula.substring(iStart, i)) : 1;
                //save the result on the stack
                stack.peek().put(name, stack.peek().getOrDefault(name, 0) + digit); //K: 4, H: 2
            }
        }

        StringBuilder res = new StringBuilder();
        for (String name : stack.peek().keySet()) {
            res.append(name);
            int digit = stack.peek().get(name);

            if (digit > 1)
                res.append("" + digit);
        }
        return new String(res);
    }
}
```

Time ~ O(n^2), space ~ O(n)

## Median of Two Sorted Arrays
https://leetcode.com/problems/median-of-two-sorted-arrays/

Soln: https://www.youtube.com/watch?v=LPFhl65R7ww&t=577s

- brute force approach is to merge the two sorted array and find the median and time ~ O(n + m)
- better option: partition the array the two array such way that the number of element in left and right side should be equal

```java
class Solution {
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int m = nums1.length, n = nums2.length;

        if (m > n) return findMedianSortedArrays(nums2, nums1);

        int start = 0, end = m;
        while (start <= end) {
            int partX = (start + end) / 2, partY = (m + n + 1) / 2 - partX;
            int l1 = (partX == 0) ? Integer.MIN_VALUE : nums1[partX - 1];
            int r1 = (partX == m) ? Integer.MAX_VALUE : nums1[partX];
            int l2 = (partY == 0) ? Integer.MIN_VALUE : nums2[partY - 1];
            int r2 = (partY == n) ? Integer.MAX_VALUE : nums2[partY];

            if (l1 <= r2 && l2 <= r1) {
                if ((m + n) % 2 == 0)
                    return (double) (Math.max(l1, l2) + Math.min(r1, r2)) / 2.0;
                else
                    return (double) Math.max(l1, l2);
            }
            else if (l1 > r2) end = partX - 1;
            else start = partX + 1;
        }
        return 0.0;
    }
}
```

Time ~ O(log(min(m, n))), space ~ O(1)

## Serialize and Deserialize Binary Tree
https://leetcode.com/problems/serialize-and-deserialize-binary-tree/

Soln: https://leetcode.com/articles/serialize-and-deserialize-binary-tree/

- use preorder traversal and use store "X" if the node is null

```java
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Codec {

    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        return preorder(root, "");
    }

    public String preorder(TreeNode node, String str) {
        if (node == null) {
            str += "X" + ",";
            return str;
        }

        str += node.val + ",";
        str = preorder(node.left, str);
        str = preorder(node.right, str);
        return str;
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        String[] s = data.split(",");
        List<String> li = new ArrayList<> ();
        
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTY3ODAxMzg1MywtMTQ2MDc5MTM3NCwtMT
E0ODY1NjU1NiwtMTk4NjU0MjQ0NSwxMjY5MTY5NDMwLC0xMTQ3
Mjg0MTkzLC04MTY0MDM2MDEsLTczNTQ5MTgwMywtMTQwNjkzMj
kxMywxMTMzMzk3NDU0LC0xMjg2MDY5NDg4LDE2NzU4NDQwNTEs
LTE0MjY0ODU4NjYsMTAyNzAyMzA4OCw1NDQwNzUyNCwtODkxNz
kyNzAxLDU3MjgxNTU0OCwtODYzODgwNTMwLC01MjEzNjIxNDks
MTcxNjMxODUwN119
-->