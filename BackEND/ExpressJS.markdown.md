# ExpressJS

Express is a minimal and flexible Node.js web application framework that provides a robust set of features for web and mobile applications.

For detail tutorials: [https://expressjs.com](https://expressjs.com/)

## Basics

### Routing
Routing refers to determining how an application responds to a client request to a particular endpoint, which is a URI (or path) and a specific HTTP request method (GET, POST, and so on).

Route definition takes the following structure:

```javascript
app.METHOD(PATH, HANDLER)
```

Where:

-   `app`  is an instance of  `express`.
-   `METHOD`  is an  [HTTP request method](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#Request_methods), in lowercase.
-   `PATH`  is a path on the server.
-   `HANDLER`  is the function executed when the route is matched.

### Static file

To serve static files such as images, CSS files, and JavaScript files, use the `express.static` built-in middleware function in Express.

For example, use the following code to serve images, CSS files, and JavaScript files in a directory named  `public`:

```javascript
app.use(express.static('public'))
```

Now, you can load the files that are in the  `public`  directory:

```plain-text
http://localhost:3000/images/kitten.jpg
http://localhost:3000/css/style.css
http://localhost:3000/js/app.js
http://localhost:3000/images/bg.png
http://localhost:3000/hello.html
```

Express looks up the files relative to the static directory, so the name of the static directory is not part of the URL.

To create a virtual path prefix (where the path does not actually exist in the file system) for files that are served by the  `express.static`  function,  [specify a mount path](https://expressjs.com/en/4x/api.html#app.use)  for the static directory, as shown below:

```javascript
app.use('/static', express.static('public'))
```

Now, you can load the files that are in the  `public`  directory from the  `/static`  path prefix.

```plain-text
http://localhost:3000/static/images/kitten.jpg
```

### express.Router

Use the  `express.Router`  class to create modular, mountable route handlers. A  `Router`  instance is a complete middleware and routing system; for this reason, it is often referred to as a “mini-app”.

The following example creates a router as a module, loads a middleware function in it, defines some routes, and mounts the router module on a path in the main app.

Create a router file named  `birds.js`  in the app directory, with the following content:

```javascript
var express = require('express')
var router = express.Router()

// middleware that is specific to this router
router.use(function timeLog (req, res, next) {
  console.log('Time: ', Date.now())
  next()
})
// define the home page route
router.get('/', function (req, res) {
  res.send('Birds home page')
})
// define the about route
router.get('/about', function (req, res) {
  res.send('About birds')
})

module.exports = router
```

Then, load the router module in the app:

```javascript
var birds = require('./birds')
// ...
app.use('/birds', birds)
```

The app will now be able to handle requests to  `/birds`  and  `/birds/about`, as well as call the  `timeLog`  middleware function that is specific to the route.

### Middleware

_Middleware_  functions are functions that have access to the  [request object](https://expressjs.com/en/4x/api.html#req)  (`req`), the  [response object](https://expressjs.com/en/4x/api.html#res)  (`res`), and the  `next`  function in the application’s request-response cycle. The  `next`  function is a function in the Express router which, when invoked, executes the middleware succeeding the current middleware.

Middleware functions can perform the following tasks:

-   Execute any code.
-   Make changes to the request and the response objects.
-   End the request-response cycle.
-   Call the next middleware in the stack.

If the current middleware function does not end the request-response cycle, it must call  `next()`  to pass control to the next middleware function. Otherwise, the request will be left hanging.

```js
// Route Handler / middleware
const  cb0  =  function (req, res, next) {
	console.log('cb0')
	next()
}

app.get('/handler', cb0, (req, res) => {
	res.send('handler runs successfully')
})
```

### Template Engine

A _template engine_ enables you to use static template files in your application. At runtime, the template engine replaces variables in a template file with actual values, and transforms the template into an HTML file sent to the client. This approach makes it easier to design an HTML page.

To render template files, set the following  [application setting properties](https://expressjs.com/en/4x/api.html#app.set), set in  `app.js`  in the default app created by the generator:

-   `views`, the directory where the template files are located. Eg:  `app.set('views', './views')`. This defaults to the  `views`directory in the application root directory.
-   `view engine`, the template engine to use. For example, to use the Pug template engine:  `app.set('view engine', 'pug')`.

Note: The view engine cache does not cache the contents of the template’s output, only the underlying template itself. The view is still re-rendered with every request even when the cache is on.

### Database Integration with Express

[https://expressjs.com/en/guide/database-integration.html](https://expressjs.com/en/guide/database-integration.html)

### Process Manager

When you run Express apps for production, it is helpful to use a  _process manager_  to:

-   Restart the app automatically if it crashes.
-   Gain insights into runtime performance and resource consumption.
-   Modify settings dynamically to improve performance.
-   Control clustering.

-   **[PM2](https://github.com/Unitech/pm2)**: A production process manager for Node.js applications that has a built-in load balancer. PM2 enables you to keep applications alive forever, reloads them without downtime, helps you to manage application logging, monitoring, and clustering.

### Security best practices

[https://expressjs.com/en/advanced/best-practice-security.html](https://expressjs.com/en/advanced/best-practice-security.html)

### Production best practices

[https://expressjs.com/en/advanced/best-practice-performance.html](https://expressjs.com/en/advanced/best-practice-performance.html)
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTk0MzcyNTgyMCwtMTUyMTA1NjMyMywtMT
I0NzAxOTUwMywtMTU0OTg1NTU4NSwxMTU0NDU1MzM4LDE0NTM1
NzQxODgsLTE0ODkyNjY2MzQsLTE5NzYxMzU5ODIsLTE2MDI5OT
k1NzUsMjE0NDYzNDA0NCw4MjQ1MDk4NzNdfQ==
-->