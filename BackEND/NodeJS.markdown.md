# NodeJS

- Node.js is a JavaScript runtime built on Chrome's V8 JavaScript engine. (Ref: https://www.freecodecamp.org/news/what-exactly-is-node-js-ae36e97449f5/)
- Event Loop and call stack (Ref: https://www.youtube.com/watch?time_continue=5&v=8aGhZQkoFbQ) and [https://blog.logrocket.com/a-complete-guide-to-the-node-js-event-loop/](https://blog.logrocket.com/a-complete-guide-to-the-node-js-event-loop/)
- NodeJS doesn't have objects like document, windows rather it has global, process which is helpful doing the server side scripting. To receive the command line argument use ```process.argv``` which has all the arguments.
- Non-blocking IO (Ref: https://stackoverflow.com/questions/10570246/what-is-non-blocking-or-asynchronous-i-o-in-node-js)
- "require" method (ref: https://stackoverflow.com/questions/9901082/what-is-this-javascript-require)
- "module.exports" (ref: https://www.sitepoint.com/understanding-module-exports-exports-node-js/)
- use ```JSON.stringify(arg)``` to convert javascript object to JSON string and use ```JSON.parse(arg)``` to convert JSON string back to javascript object

## FileSystem

Write file synchronously. If the file is not exists, ```fs.writeFileSync``` function creates a new file otherwise overwrites with the existing file.

```fs.appendFileSync``` Synchronously append data to a file, creating the file if it does not yet exist. data can be a string or a Buffer.

```js
const fs = require('fs');

fs.writeFileSync('message.txt', 'Hello NodeJS');
fs.appendFileSync('message.txt', 'Append data');
```

## Node Package Manager (npm)

1. Run ```npm init``` to initialize the package manager and save the details into ```package.json``` file. Another use case of package.json (and package-lock.json) is when you share your code with others. Basically, we do not share the "node_modules" as you can grab them by running ```npm install``` but make sure you have correct package.json and package-lock.json file available in the directory.

2. Run ```npm install <package_name>@[version_num] --save``` to install the specific version of the package. After running the above command, package.json file will add this package as dependent application

Some popular Node packages are:

- validator: validates and sanitizes strings only (https://www.npmjs.com/package/validator)
- chalk: customize the colors of console output, which is helpful in debugging (https://www.npmjs.com/package/chalk)
- nodemon: automatically restart the node application when file changes in the directory are detected (https://www.npmjs.com/package/nodemon)
- yargs: helps you build interactive command line tools, by parsing arguments and generating an elegant user interface (https://www.npmjs.com/package/yargs)
- body-parser: It Parses incoming request bodies in a middleware before your handlers, available under the `req.body` property.
- loadash: Lodash makes JavaScript easier by taking the hassle out of working with arrays, numbers, objects, strings, etc. Lodash’s modular methods are great for: Iterating arrays, objects, & strings, Manipulating & testing values, Creating composite functions
- babel: Babel converted ES6 code to ES5 code
- async: Async is a utility module which provides straight-forward, powerful functions for working with asynchronous JavaScript. Although originally designed for use with Node.js, it can also be used directly in the browser.
- pug: Formerly known as Jade (although still using old website), Pug is a node templating engine. What is a templating engine? Well, it’s the kind of an engine that helps you to eliminate the need for writing complex HTML and JS code that could otherwise be shortened down to a more slim, more compact version. That’s what a templating engine is. Pug has been crafted specifically for Node, so you will be getting a lot of benefits and recognizable patterns, all to help you be more productive and efficient while working.
- morgan: With millions of downloads each month, Morgan is one of the top priority libraries for Node devs. Morgan is a HTTP request logger, storing HTTP requests and giving you concise insight into how your app is being used, and where there could be potential errors or other issues that you haven’t yet explored. Easily the most reliable HTTP logger known to the Node.js community.

Ref: 
- https://www.sitepoint.com/beginners-guide-node-package-manager/
- https://docs.npmjs.com/cli-documentation/cli

## Template Engine

- Handlebars, EJS
- partials

## Q&A

1. [https://blog.risingstack.com/node-js-interview-questions-and-answers-2017/](https://blog.risingstack.com/node-js-interview-questions-and-answers-2017/)
2. [https://blog.risingstack.com/javascript-clean-coding-best-practices-node-js-at-scale/](https://blog.risingstack.com/javascript-clean-coding-best-practices-node-js-at-scale/)
3. [https://medium.com/@vigowebs/frequently-asked-node-js-interview-questions-and-answers-b74fa1f20678](https://medium.com/@vigowebs/frequently-asked-node-js-interview-questions-and-answers-b74fa1f20678)
4. [https://www.edureka.co/blog/interview-questions/top-node-js-interview-questions-2016/](https://www.edureka.co/blog/interview-questions/top-node-js-interview-questions-2016/)
5. [https://dev.to/aershov24/7-hardest-nodejs-interview-questions--answers-3lje](https://dev.to/aershov24/7-hardest-nodejs-interview-questions--answers-3lje)


## NodeJS Best Practices

1. [https://www.codementor.io/mattgoldspink/nodejs-best-practices-du1086jja](https://www.codementor.io/mattgoldspink/nodejs-best-practices-du1086jja)
2. [https://google.github.io/styleguide/jsguide.html](https://google.github.io/styleguide/jsguide.html)
3. [https://devcenter.heroku.com/categories/nodejs-support] [](https://devcenter.heroku.com/categories/nodejs-support)

**Heroku is must read for NodeJS**

### Start every new project with npm init

npm’s  `init`  command will scaffold out a valid package.json for your project, inferring common properties from the working directory.

```term
$ mkdir my-awesome-app
$ cd my-awesome-app
$ npm init --yes
```

I’m lazy, so I run it with the  `--yes`  flag and then open package.json to make changes. The first thing you should do is specify an ‘engines’ key with your current version of node (`node -v`):

```json
"engines": {
  "node": "10.3.0"
}
```

### use .npmrc

By default, npm doesn’t save installed dependencies to package.json (and you should always track your dependencies!).

If you use the  `--save`  flag to auto-update package.json, npm installs the packages with a leading carat (^), putting your modules at risk of drifting to different versions. This is fine for module development, but not good for apps, where you want to keep consistent dependencies between all your environments.

One solution is installing packages like this:

```term
$ npm install foobar --save --save-exact
```

Even better, you can set these options in  `~/.npmrc`  to update your defaults:

```term
$ npm config set save=true
$ npm config set save-exact=true
$ cat ~/.npmrc
```

Now,  `npm install foobar`  will automatically add  `foobar`  to package.json and your dependencies won’t drift between installs.

You can lock down your dependencies further with  [npm-shrinkwrap](https://docs.npmjs.com/cli/shrinkwrap). However, note that the shrinkwrap workflow can be counterintuitive, and shrinkwrap has several known issues in older versions of npm.

### Cluster your app

Since the node runtime is limited to a single CPU core and about 1.5 GB of memory, deploying a non-clustered node app on a large server is a huge waste of resources.

To take advantage of multiple cores and memory beyond 1.5 GB, bake  [Cluster support](https://nodejs.org/api/cluster.html)  into your app. Even if you’re only running a single process on small hardware today, Cluster gives you easy flexibility for the future.

Testing is the best way to determine the ideal number of clustered processes for your app, but it’s good to start with the  [reasonable defaults](https://devcenter.heroku.com/articles/node-concurrency)  offered by your platform, with a simple fallback, eg:

```js
const CONCURRENCY = process.env.WEB_CONCURRENCY || 1;
```

Choose a  [Cluster abstraction](https://www.npmjs.com/search?q=cluster)  to avoid reinventing the wheel of process management. If you’d like separate master and worker files, you can try  [forky](https://www.npmjs.com/package/forky). If you prefer a single entrypoint file and function, take a look at  [throng](https://www.npmjs.com/package/throng).

### Be environmentally aware

Don’t litter your project with environment-specific config files! Instead, take advantage of  _environment variables_.

To provide a local development environment, create a .gitignore’d  `.env`  file, which will be loaded by  `heroku local`:

```
DATABASE_URL='postgres://localhost/foobar'
HTTP_TIMEOUT=10000
```

Now start your app with  `heroku local`, and it will automatically pull in these environment variables into your app under  `process.env.DATABASE_URL`  and  `process.env.HTTP_TIMEOUT`. And, when you deploy your project, it will  [automatically adapt](http://12factor.net/config)  to the variables on its new host.

This is simpler and more flexible than 'config/abby-dev.js’, 'config/brian-dev.js’, 'config/qa1.js’, 'config/qa2.js’, 'config/prod.js’, etc.

### Avoid Garbase Collector

Node (V8) uses a lazy and greedy garbage collector. With its default limit of about 1.5 GB, it sometimes waits until it absolutely has to before reclaiming unused memory. If your memory usage is increasing, it might not be a leak - but rather  [node’s usual lazy behavior](https://github.com/nodejs/node/issues/3370#issuecomment-148108323).

To gain more control over your app’s garbage collector, you can provide flags to V8 in your  `Procfile`:

```
web: node --optimize_for_size --max_old_space_size=920 --gc_interval=100 server.js
```

This is especially important if your app is running in an environment with less than 1.5 GB of available memory. For example, if you’d like to tailor node to a 512 MB container, try:

```
web: node --optimize_for_size --max_old_space_size=460 --gc_interval=100 server.js
```

### Hook Things up

npm’s  [lifecycle scripts](https://docs.npmjs.com/misc/scripts)  make great hooks for automation. Heroku provides  [custom hooks](https://devcenter.heroku.com/articles/nodejs-support#heroku-specific-build-steps)  that allow you to run custom commands before or after we install your dependencies. If you need to run something before building your app, you can use the  `heroku-prebuild`  script. Need to build assets with grunt, gulp, browserify, or webpack? Do it in a  `build`  script.

In package.json:

```js
"scripts": {
  "build": "bower install && grunt build",
  "start": "nf start"
}
```

You can also use environment variables to control these scripts:

```js
"build": "if [ $BUILD_ASSETS ]; then npm run build-assets; fi",
"build-assets": "bower install && grunt build"
```

If your scripts start getting out of control, move them to files:

```js
"build": "scripts/build.sh"
```

Scripts in package.json automatically have  `./node_modules/.bin`  added to their  `PATH`, so you can execute binaries like  `bower`  or  `webpack`  directly.

## Cluster NodeJS

1. [https://www.youtube.com/watch?v=6xIbVPyh9wo](https://www.youtube.com/watch?v=6xIbVPyh9wo)
2. [https://www.youtube.com/watch?v=w1IzRF6AkuI](https://www.youtube.com/watch?v=w1IzRF6AkuI)
3. [https://nodejs.org/api/cluster.html](https://nodejs.org/api/cluster.html)

A single instance of Node.js runs in a single thread. To take advantage of multi-core systems, the user will sometimes want to launch a cluster of Node.js processes to handle the load.

The cluster module allows easy creation of child processes that all share server ports.

```js
const cluster = require('cluster');
const http = require('http');
const numCPUs = require('os').cpus().length;

if (cluster.isMaster) {
  console.log(`Master ${process.pid} is running`);

  // Fork workers.
  for (let i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  cluster.on('exit', (worker, code, signal) => {
    console.log(`worker ${worker.process.pid} died`);
  });
} else {
  // Workers can share any TCP connection
  // In this case it is an HTTP server
  http.createServer((req, res) => {
    res.writeHead(200);
    res.end('hello world\n');
  }).listen(8000);

  console.log(`Worker ${process.pid} started`);
}
```

Running Node.js will now share port 8000 between the workers:

```txt
$ node server.js
Master 3596 is running
Worker 4324 started
Worker 4520 started
Worker 6056 started
Worker 5644 started

```

On Windows, it is not yet possible to set up a named pipe server in a worker.

### How it works

The worker processes are spawned using the  [`child_process.fork()`](https://nodejs.org/api/child_process.html#child_process_child_process_fork_modulepath_args_options)  method, so that they can communicate with the parent via IPC and pass server handles back and forth.

The cluster module supports two methods of distributing incoming connections.

The first one (and the default one on all platforms except Windows), is the round-robin approach, where the master process listens on a port, accepts new connections and distributes them across the workers in a round-robin fashion, with some built-in smarts to avoid overloading a worker process.

The second approach is where the master process creates the listen socket and sends it to interested workers. The workers then accept incoming connections directly.

The second approach should, in theory, give the best performance. In practice however, distribution tends to be very unbalanced due to operating system scheduler vagaries. Loads have been observed where over 70% of all connections ended up in just two processes, out of a total of eight.

Because  `server.listen()`  hands off most of the work to the master process, there are three cases where the behavior between a normal Node.js process and a cluster worker differs:

1.  `server.listen({fd: 7})`  Because the message is passed to the master, file descriptor 7  **in the parent**  will be listened on, and the handle passed to the worker, rather than listening to the worker's idea of what the number 7 file descriptor references.
2.  `server.listen(handle)`  Listening on handles explicitly will cause the worker to use the supplied handle, rather than talk to the master process.
3.  `server.listen(0)`  Normally, this will cause servers to listen on a random port. However, in a cluster, each worker will receive the same "random" port each time they do  `listen(0)`. In essence, the port is random the first time, but predictable thereafter. To listen on a unique port, generate a port number based on the cluster worker ID.

Node.js does not provide routing logic. It is, therefore important to design an application such that it does not rely too heavily on in-memory data objects for things like sessions and login.

Because workers are all separate processes, they can be killed or re-spawned depending on a program's needs, without affecting other workers. As long as there are some workers still alive, the server will continue to accept connections. If no workers are alive, existing connections will be dropped and new connections will be refused. Node.js does not automatically manage the number of workers, however. It is the application's responsibility to manage the worker pool based on its own needs.

Although a primary use case for the  `cluster`  module is networking, it can also be used for other use cases requiring worker processes.

## NodeJS Performance Optimization

READ BELOW LINKS
1. [https://medium.com/tech-tajawal/clustering-in-nodejs-utilizing-multiple-processor-cores-75d78aeb0f4f](https://medium.com/tech-tajawal/clustering-in-nodejs-utilizing-multiple-processor-cores-75d78aeb0f4f)
2. [https://dev.to/acanimal/understanding-the-nodejs-cluster-module-14-2bi](https://dev.to/acanimal/understanding-the-nodejs-cluster-module-14-2bi)
3. [https://www.tutorialdocs.com/article/nodejs-performance.html](https://www.tutorialdocs.com/article/nodejs-performance.html)

## Static code analysis

[https://www.perforce.com/blog/qac/what-static-code-analysis](https://www.perforce.com/blog/qac/what-static-code-analysis)
[https://www.youtube.com/watch?v=d_BCGvXbpKs](https://www.youtube.com/watch?v=d_BCGvXbpKs)

## Good practices for high-performance and scalable Node.js applications

Ref: 
1. [https://medium.com/iquii/good-practices-for-high-performance-and-scalable-node-js-applications-part-1-3-bb06b6204197](https://medium.com/iquii/good-practices-for-high-performance-and-scalable-node-js-applications-part-1-3-bb06b6204197)

The main focus will be about efficiency and performance, in order to obtain the best result with less resources.

One way to improve the throughput of a web application is to scale it, instantiate it multiple times balancing the incoming connection between the multiple instances, so this first article will be about  **how to horizontally scale a Node.js application**, on multiple cores or on multiple machines.

When you scale up, you have to be careful about different aspects of your application, from the state to the authentication, so the second article will cover some **things you must consider**  when scaling up a Node.js application.

Over the mandatories ones, there are some  **good practices you can address**  that will be covered in the third article, like splitting api and worker processes, the adoption of priority queues, the management of periodic jobs like cron processes, that are not intended to run N times when you scale up to N processes/machines

### Chapter 1 — Horizontally scaling a Node.js application

Horizontal scaling is about duplicating your application instance to manage a larger number of incoming connections. This action can be performed on a single multi-core machine or across different machines.

Vertical scaling is about increasing the single machine performances, and it do not involve particular work on the code side.

#### Multiple processes on same machine

One common way to increase the throughput of your application is to spawn one process for each core of your machine. By this way the already efficient “concurrency” management of requests in Node.js (see “event driven, non-blocking I/O”) can be multiplied and parallelized.

It is probably not clever to spawn a number of processes bigger than the number of cores, because at the lower level the OS will likely balance the CPU time between those processes.

There are different strategies for scaling on a single machine, but the common concept is to have multiple processes running on the same port, with some sort of internal load balancing used to distribute the incoming connections across all the processes/cores.

![](https://miro.medium.com/max/904/1*p6YEK7y6JsVYBaZkhu4UbQ.png)

The strategies described below are the standard Node.js  **cluster mode**  and the automatic, higher-level  **PM2 cluster**  functionality.

#### Native cluster mode

The native Node.js cluster module is the basic way to scale a Node app on a single machine ([https://Node.js.org/api/cluster.html](https://nodejs.org/api/cluster.html?source=post_page---------------------------)). One instance of your process (called “master”) is the one responsible to spawn the other child processes (called “workers”), one for each core, that are the ones that runs your application. The incoming connections are distributed following a round-robin strategy across all the workers, that exposes the service on the same port.

The main drawback of this approach is the necessity to manage inside the code the difference between master and worker processes manually, typically with a classic if-else block, without the ability to easily modify the number of processes on-the-fly. [see Cluster NodeJS above]

#### PM2 Cluster mode

If you are using PM2 as your process manager (I suggest you to), there is a magic cluster feature that let you scale your process across all the cores without worrying about the cluster module. The PM2 daemon will cover the role of the “master” process, and it will spawn N processes of your application as workers, with round-robin balancing.

By this way you simply write your application as you would do for single-core usage (with some cautions that we’ll cover in next article), and PM2 will care about the multi-core part.


![](https://miro.medium.com/max/1400/0*zWc1jyWm1FNEeNgZ.)

Once your application is started in cluster mode, you can adjust the number of instances on-the-fly using “pm2 scale”, and perform “0-second-downtime” reloads, where the processes are restarted in series in order to have always at least one process online.

As a process manager, PM2 will also take care of restarting your processes if they crash like many other useful things you should consider when running node in production.

If you need to scale even further, you’ll probably need to deploy more machines.

### Multiple machines with network load balancing

The main concept in scaling across multiple machines is similar to scaling on multiple cores, there are multiple machines, each one running one or more processes, and a balancer to redirect traffic to each machine.

Once the request is sent to a particular node, the internal balancer described in the previous paragraph sends the traffic to a particular process.

![](https://miro.medium.com/max/1220/1*ryiL00dESNJTL_jRnUyAyA.png)

A network balancer can be deployed in different ways. If you use AWS to provision your infrastructure, a good choice is to use a managed load balancer like ELB (Elastic Load Balancer), because it supports useful features like auto-scaling, and it is easy to set up.

But if you want to do it old-school, you can deploy a machine and setup a balancer with NGINX by yourself. The configuration of a reverse proxy that points to an upstream is quite simple for this job. Below an example for the configuration:

```js
http {
	upstream myapp1 {
		server srv1.example.com;
		server srv2.example.com;
		server srv3.example.com;
	}

	server {
		listen 80;
			location / {
			proxy_pass http://myapp1;
			}
	}
}
```

By this way the load balancer will be the only entrypoint of your application exposed to the outer world. If you worry about it being the single point of failure of your infrastructure, you can deploy multiple load balancers that points to the same servers.

In order to distribute the traffic between the balancers (each one with its own ip address), you can add multiple DNS “A” records to your main domain, so the DNS resolver will distribute the traffic between your balancers, resolving to a different IP address each time.

By this way you can achieve redundancy also on the load balancers.

![](https://miro.medium.com/max/1192/1*iSVmpaGmwYzXWydLJnzM3A.png)

What we have seen here is how to scale a Node.js app at different levels in order to obtain the highest possible performance from your infrastructure, from single node, to multi node and multi balancer, but be careful: if you want to use your application in a multi-process environment, it must be prepared and ready for that, or you will incur in several problems and undesired behaviours.

This is how to horizontally scale a Node.js application, without worrying about the code. This time we’ll talk about some aspects you must consider in order to prevent undesired behaviours when you scale up your process.

### Decouple application instances from DB

The first hint isn’t about code, but about your  **infrastructure**.

If you want your application to be able to scale across different hosts, you must deploy your database on independent machines, so you can freely duplicate your application machine as you wish.

![](https://miro.medium.com/max/1254/1*uSNVUpjeSG8H8AUK8-Yv7A.png)

Deploying application and database on the same machine can be cheap and used for development purpose, but it is absolutely not recommended for production environments, where application and database must be able to scale independently. The same applies to in-memory databases like Redis.

### Be stateless

If you spawn multiple instances of your application,  **each process will have its own memory space**. This means that even if you are running on a single machine, when you store some value in a global variable, or more commonly a session in memory, you won’t find it there if the balancer redirects you to another process during the next request.

This applies both to session data and internal values like any sort of app-wide settings.

For settings or configurations that can change during run-time, the solution is to store them on the external database (on storage or in-memory) in order to make it accessible by all processes.

### Stateless authentication with JWT

Authentication is one of the first topics to consider when developing a stateless application.  If you store sessions in memory, they will be scoped to that single process.

In order to make things work, you should configure your network load balancer to redirect the same user always to the same machine, and your local one to redirect the same user always to the same process (sticky sessions).

A trivial solution to this problem is to set the sessions’ storage strategy to any form of persistence, for example storing them on DB instead of RAM. But if your application checks session data on each request, there will be disk I/O on every API call, that is definitely not good from the performance point of view.

A better and fast solution (if your authentication framework supports it) is to store sessions on an in-memory DB like Redis. A Redis instance is usually external to application instances like the DB one, but working in memory makes it much faster. Anyway storing sessions in RAM makes you need more memory when your concurrent sessions number grows.

If you want to embrace a more efficient approach to stateless authentication, you can take a look a  **JSON Web Tokens**.

The idea behind JWT is quite simple: When a user logs in, the server generates a token that is essentially a base64 encode of a JSON object containing the payload, plus a signature obtained hashing that payload with a secret key owned by the server. The payload can contain data used to authenticate and authorize the user, for example the userID and its associated ACL roles. The token is sent back to the client and used by it to authenticate every API request.

When the server process an incoming request, it takes the payload of the token and recreates the signature using its secret key. If the two signatures match, the payload can be considered valid and not altered, and the user can be identified.

It’s important to remember that  **JWT doesn’t provide any form of encryption**. The payload is only encoded in base64 and it is sent in clear text, so if you need to hide the content you must use SSL.

![](https://miro.medium.com/max/1400/1*7T41R0dSLEzssIXPHpvimQ.png)

During the authentication process the server doesn’t need to access session data stored somewhere, so each request can be processed by a different process or machine in a very efficient way. No data is saved in RAM, and you don’t need to perform storage I/O, so this approach is really useful as you scale up.

### Storage on S3

When you use multiple machines, you can’t save user-generated assets directly on the filesystem, because those files would be accessible only to the processes local to that server. The solution is to  **store all the content on an external service**, possibly on a dedicated one like Amazon S3, and save in your DB only the absolute URL that points to that resource.

![](https://miro.medium.com/max/1148/1*kmIPoA7Ab60n4kO36LWtNQ.png)

Every process/machine will then have access to that resource in the same way.

Using the official AWS sdk for Node.js is quite easy and lets you integrate the service inside your application without particular effort. S3 is quite cheap and it is optimized for this purpose, making it a good choice also in the case your app isn’t multi-process.

### Properly configure WebSockets

If your application uses WebSockets for real-time interaction between clients or between client and server, you will need to  **link your backend instance**s in order to correctly propagate broadcast messages, or messages between clients connected to different nodes.

The Socket.io library provides a special adapter for this purpose, called socket.io-redis, that lets you link your server instances using Redis pub-sub functionality.

In order to use a multi-node socket.io environment you will also need to force the protocol to “websockets”, because long-polling needs sticky-sessions to work.

## Additional good practices for efficiency and performance

### Web and Worker processes

As you probably know,  **Node.js is in practice single-threaded**, so a single instance of the process can only perform one action at a time. During the life-cycle of a web application,  **many different tasks**  are performed: managing API calls, reading/writing to DB, communicating with external network services, execution of some sort of inevitable CPU intensive work etc.

Although you use asynchronous programming, delegating all these actions to the same process that responds to your API calls can be a quite inefficient approach.

A common pattern is based on the  **separation of responsibilities**  between two different kind of processes that compose your application, usually a  **web**  process and a  **worker**  one.

![](https://miro.medium.com/max/1400/1*4u5WMX_JB8-E2byEBcUyYw.png)

The web process is designed to mainly manage the  **incoming network calls**, and to dispatch them as fast as possible. Any time a non-blocking task needs to be performed, like for example sending an email/notification, write a log, execute a triggered action which result is not necessary to answer the API call, the web process delegates the action to the worker one.

The  **communication between web and worker**  processes can be implemented in different ways. A common and efficient solution is a priority queue, like the one implemented in Kue, described in the next paragraph.

One big win of this approach is the  **possibility to scale web and worker processes independently**, on the same or on different machines.

For instance if your application is an high-traffic one, with little side-work generated, you can deploy more web processes than worker ones, while if there are few network requests that generates a lot of jobs for the worker, you can redistribute your resources accordingly.

### Kue

In order to make web and worker processes talk to each other, a  **queue**  is a flexible approach that lets you not to worry about inter-process communication.

[Kue](http://automattic.github.io/kue/?source=post_page---------------------------)  is a common queue library for Node.js, it is based on Redis and lets you put in communication processes that are spawned on the same or on different machines in the exact same way.

Any kind of process can create a job and put it in the queue, then worker processes are configured to pick these jobs and execute them. A lot of options can be provided for each job, like priority, TTL, delay etc.

The more worker processes you spawn, the more parallel throughput you have to execute these jobs.

### Cron

It is common for an application to have some tasks that need to be  **performed periodically**. Usually this kind of operations are managed through  **cron jobs**  at the OS level, where a single script is invoked from the outside of your application.

This approach introduces the need of extra work when deploying your application on a new machine, that makes the process uncomfortable if you want to automate the deploy.

A more comfortable way to achieve the same result is to use the  [**cron module**](https://www.npmjs.com/package/cron?source=post_page---------------------------) **available on NPM**. It lets you to define cron jobs inside the Node.js code, making it independent from the OS configuration.

According to the web/worker pattern described above, the worker process can create the cron, which invokes a function putting a new job on the queue periodically.

Using the queue makes it more clean and can take advantage of all the features offered by kue like priority, retries, etc.

The problems come out when you have more than one worker process, because the cron function would wake app on every process at the same time, putting into the queue duplicates of the same job that would be executed many times.

In order to solve this problem it is necessary to  **identify a single worker process that will perform the cron operations**.

### Leader election and cron-cluster

This kind of problem is known as “**leader election**”, and for this specific scenario there is an NPM package that does the trick for us, called  [cron-cluster](https://www.npmjs.com/package/cron-cluster?source=post_page---------------------------).

It expose the same API that powers the cron module, but during setup it requires a  **redis connection**  used to communicate with the other processes and perform the leader election algorithm.

![](https://miro.medium.com/max/1400/1*kDpGv4d1Mj_AGg9TFVFhhQ.png)

Using redis as a single source of truth,  **all the process will agree about who will execute the cron**, and only one copy of the jobs will be put in the queue. After that, all the worker processes will be eligible to execute the jobs as usual.

### Caching API calls

**Server-side caching**  is a common way to  **improve performance and reactivity**  of your API calls, but it is a very wide topic with with a lot of possible implementations.

In a distributed environment like the one we described in this serie, using redis to store the cached values is probably the best approach in order to have all the nodes behaving equally.

The most difficult aspect to consider with caching is its invalidation. The quick-and-dirty solution considers only time, so the values in cache are flushed after a fixed TTL, with the downside of having to wait the next flush in order to see updates in the responses.

If you have more time to spend, it would be a better idea to implement the invalidation at the application level, manually flushing the records on the redis cache when the values change on DB.

## Study Below Courses

1. [https://learning.oreilly.com/videos/mastering-node-js-12-x/9781789539899](https://learning.oreilly.com/videos/mastering-node-js-12-x/9781789539899)
2. [https://learning.oreilly.com/videos/node-js-design-patterns/9781789538397](https://learning.oreilly.com/videos/node-js-design-patterns/9781789538397)
3. [https://learning.oreilly.com/videos/learn-nodejs-by/9781787122215](https://learning.oreilly.com/videos/learn-nodejs-by/9781787122215)
4. [https://learning.oreilly.com/videos/building-microservices-with/9781788620451](https://learning.oreilly.com/videos/building-microservices-with/9781788620451)
5. [https://caolan.github.io/async/v3/](https://caolan.github.io/async/v3/)
6. [https://eslint.org/](https://eslint.org/)
7. [https://nodejs.org/en/docs/guides/](https://nodejs.org/en/docs/guides/)
8. [https://colorlib.com/wp/npm-packages-node-js/](https://colorlib.com/wp/npm-packages-node-js/)
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTI2NDE0NjIyMiwtOTUyNDk2ODc4LDIwNz
c4MzgzNjIsLTEwODYxMDk4MTgsMzk2MTg4MCw4NDMyMjYyMTUs
LTE1NDkwODM3NDUsODEyMzM2ODg3LDEzOTQ2MjE5LDk5MjA0Mj
Y2OSwyOTYzNjkwNDUsLTEyOTM2NDQ1NTksNjUyMTk3MjY5LC0x
NTQ2NzY2NDQzLC03MjUxODU0NjIsODkxNTM2NDQwLC0yMDg4MT
UxNjc1LDI3MDYzODQyMywtMzEyMjQyMDk4LC01NzQwMjU2Nl19

-->